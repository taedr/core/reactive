import { Bus, map, ReactiveBoolean, ReactiveString, tie } from '@taedr/reactive';

const message = `WASTED`;

it(`Should retranslate same value when any source emitts`, async () => {
   const Iemited: Array<string | number | boolean> = [];

   const bus_1 = new ReactiveString(``);
   const bus_2 = new Bus<number>();
   const bus_3 = new ReactiveBoolean(false);

   const latest = tie([bus_1, bus_2, bus_3]);

   latest.watch(value => { Iemited.push(value); });

   const Eemitted: [string, number, boolean] = [`A`, 4, true];

   bus_1.send(Eemitted[0]);
   bus_2.send(Eemitted[1]);
   bus_3.send(Eemitted[2]);

   expect(Iemited).toEqual(Eemitted);
});


it(`Should retranslate same value when any source emitts`, async () => {
   const Iemited: Array<string | number | boolean> = [];

   const bus_1 = new ReactiveString(``);
   const bus_2 = new Bus<number>();
   const bus_3 = new ReactiveBoolean(false);

   const latest = tie([bus_1, bus_2, bus_3]);

   latest.watch({
      value: (value) => {
         Iemited.push(value);
      }
   });

   const Eemitted: [string, number, boolean] = [`A`, 4, true];

   bus_1.send(Eemitted[0]);
   bus_2.send(Eemitted[1]);
   bus_3.send(Eemitted[2]);

   expect(Iemited).toEqual(Eemitted);
});

it(`Should share latest value between pipelines`, async () => {
   const Iemited_1: string[] = [];
   const Iemited_2: string[] = [];
   const Iemited_3: string[] = [];

   const bus_1 = new ReactiveString(``);
   const bus_2 = new Bus<number>();
   const bus_3 = new ReactiveBoolean(false);

   const connector = tie([bus_1]);

   const pipe_1 = bus_2.pipe(
      map(value => value.toString()),
      connector
   );

   const pipe_2 = bus_3.pipe(
      map(value => value.toString()),
      connector
   );

   connector.watch(value => { Iemited_1.push(value); });
   pipe_1.watch(value => { Iemited_2.push(value); });
   pipe_2.watch(value => { Iemited_3.push(value); });

   bus_1.send(`A`);
   bus_2.send(4);
   bus_3.send(true);

   const Eemitted = [`false`, `A`, `4`, `true`];

   expect(Iemited_1).toEqual(Eemitted);
   expect(Iemited_2).toEqual(Eemitted);
   expect(Iemited_3).toEqual(Eemitted);
});
