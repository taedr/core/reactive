import { Bus, debounce, EDetachMessage, iterate, map, Reactive, shutdownOn, suppress } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';


it(``, () => {

})
/* When auto shutdown triggered
* pipes before shutdownOn will shutdowned instantly
* pipes after shutdown will awaited to finish async tasks */
/* it(`Stages`, async () => {
   const bus = new Bus<number[]>();
   const iterator = iterate<number>();
   const suppresser_1 = suppress<number>(20);
   const shutdowner_1 = shutdownOn<number>(4);
   const suppresser_2 = suppress<number>(30);
   const shutdowner_2 = shutdownOn<number>(value => value === 3);
   const pipe = bus.pipe(
      iterator,
      suppresser_1,
      shutdowner_1,
      suppresser_2,
      shutdowner_2
   );

   const newValues: number[] = [];

   pipe.watch(value => { newValues.push(value); });

   bus.send([1, 2, 3, 4, 5]);

   console.log(newValues); // []

   await FN_DELAY(105);

   expect(iterator.shutdown).toEqual(EDetachMessage.Auto);
   expect(suppresser_1.shutdown).toEqual(EDetachMessage.Auto);
   expect(shutdowner_1.shutdown).toEqual(EDetachMessage.Auto);
   expect(suppresser_2.shutdown).toEqual(``);
   expect(suppresser_2.buffer).toEqual([3, 4])

   await FN_DELAY(60);

   console.log(newValues); // [1, 2, 3]
   console.log(pipe.shutdown === EDetachMessage.Auto); // true
   expect(newValues).toEqual([1, 2, 3]);
   expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
}); */
