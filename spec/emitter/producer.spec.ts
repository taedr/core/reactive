import { Producer } from '@taedr/reactive';
import { FN_DELAY, FN_PACK } from '@taedr/utils';

it(`Basic`, async () => {
   const values = FN_PACK(10);
   const message = `WASTED`;
   const producer = new Producer<number>((next, complete) => {
      for (let i = 0; i < values.length; i++) {
         const value = values[i];
         next(value);
      }

      complete(message);
   });
   const Ivalues_1: number[] = [];
   let Imessage_1: string | undefined;

   producer.watch({
      value: value => { Ivalues_1.push(value); },
      detach: mess => Imessage_1 = mess,
   });

   expect(producer.values).toEqual(values);
   expect(Ivalues_1).toEqual(values);

   await FN_DELAY(1);

   expect(Imessage_1).toEqual(message);

   const Ivalues_2: number[] = [];
   let Imessage_2: string | undefined;

   producer.watch({
      value: value => { Ivalues_2.push(value); },
      detach: mess => Imessage_2 = mess,
   });

   expect(Imessage_2).toEqual(message);
   expect(Ivalues_2).toEqual(values);
});
