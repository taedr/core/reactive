import { Ticker } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';


it(`Basic`, async () => {
   const delayMs = 10;
   const ticker = new Ticker(delayMs);

   const emitted: number[] = [];

   const watcherId = ticker.watch(value => { emitted.push(value) });

   expect(ticker.delayMs).toEqual(delayMs);
   expect(ticker.activatedAt).not.toBeNull();

   await FN_DELAY(delayMs);

   const ticks_1 = 1;
   expect(emitted).toEqual([ticks_1]);
   expect(ticker.ticks).toEqual(ticks_1);

   await FN_DELAY(delayMs);

   const ticks_2 = 2;
   expect(ticker.ticks).toEqual(ticks_2);
   expect(emitted).toEqual([ticks_1, ticks_2]);

   ticker.watchers.detach(watcherId);

   await FN_DELAY(delayMs);

   expect(ticker.activatedAt).toBeUndefined();
   expect(emitted).toEqual([ticks_1, ticks_2]);
}, 1000);
