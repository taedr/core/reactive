import { EDetachMessage, Timer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';


it(`Should emit value once after given delay`, async () => {
   const delayMs = 20;
   const value = 1;
   const timer = new Timer(delayMs, value);
   let Ivalue_1: number | undefined
   let shutdown_1 = ``;
   let Ivalue_2: number | undefined;
   let shutdown_2 = ``;

   expect(timer.value).toEqual(value);
   expect(timer.delayMs).toEqual(delayMs);

   timer.watch({
      value: value => Ivalue_1 = value,
      detach: message => shutdown_1 = message,
   });

   timer.watch({
      value: value => Ivalue_2 = value,
      detach: message => shutdown_2 = message,
   });
   expect(timer.watchers.size).toEqual(2);

   await FN_DELAY(delayMs);

   expect(timer.watchers.size).toEqual(0);
   expect(shutdown_1).toEqual(EDetachMessage.Emittion);
   expect(Ivalue_1).toEqual(value);
   expect(shutdown_2).toEqual(EDetachMessage.Emittion);
   expect(Ivalue_2).toEqual(value);
});

it(`Detach`, async () => {
   const delayMs = 20;
   const timer = new Timer(delayMs, 1);
   let emitted = false;

   const watcherId = timer.watch(() => { emitted = true });

   timer.watchers.detach(watcherId);

   await FN_DELAY(delayMs);

   expect(timer.watchers.size).toEqual(0);
   expect(emitted).toBeFalsy();
});
