import { Bus, map, Pipeline } from "@taedr/reactive";
import { FN_MIRROR } from '@taedr/utils';

it(`Bus`, () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      map(value => value)
   );
   let Ivalue: number | undefined;

   expect(bus.watchers.size).toEqual(0);
   expect(pipe.watchers.size).toEqual(0);
   expect(pipe).toBeInstanceOf(Pipeline);
   expect(pipe.origin).toBe(bus);

   const watcherId = pipe.watch(value => Ivalue = value);

   expect(bus.watchers.size).toEqual(1);
   expect(pipe.watchers.size).toEqual(1);
   expect(Ivalue).toEqual(undefined);

   bus.send(1);

   expect(pipe['_value']).toBeUndefined();
   expect(Ivalue).toEqual(1);

   pipe.watchers.detach(watcherId);

   expect(bus.watchers.size).toEqual(0);
   expect(pipe.watchers.size).toEqual(0);

   pipe.watch(FN_MIRROR);

   expect(bus.watchers.size).toEqual(1);
   expect(pipe.watchers.size).toEqual(1);

   pipe.shutdown = `WASTED`;

   expect(bus.watchers.size).toEqual(0);
   expect(pipe.watchers.size).toEqual(0);
});

it(`SHOULD work when no pipes provided`, () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe();
   let Ivalue: number | undefined;

   pipe.watch(value => Ivalue = value);

   bus.send(1);
   expect(Ivalue).toBe(1);
});
