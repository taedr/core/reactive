import { Bus, map, ReactiveNumber, skipInitial } from '@taedr/reactive';

it(`Can create Reactive from any source with specified initial value`, () => {
   const bus = new Bus<number>();
   const pipe = bus.suffix(new ReactiveNumber(1));
   let Ivalue: number | undefined;

   expect(pipe).toBeInstanceOf(ReactiveNumber);
   expect(pipe.value).toEqual(1);

   pipe.watch(value => Ivalue = value);

   expect(Ivalue).toEqual(1);

   bus.send(1);

   expect(pipe.value).toEqual(1);
   expect(pipe.value).toEqual(Ivalue);
});

it(`'undefined' as initial value can be useful if you are sure that during creation the source will provide value instantly.
Or when creating pipe tree, since value 'undefined' lower nodes will not be triggered with unwanted initial value`, () => {
   const bus = new Bus<number>();

   const root = bus.pipe(
      map(value => value)
   ).suffix();

   const lvl_1_1 = root.pipe(
      map(value => value * -1)
   ).suffix();

   const lvl_1_2 = root.pipe(
      map(value => value * 2)
   );

   const lvl_2_1 = lvl_1_2.pipe(
      map(value => value * 10)
   ).suffix();

   expect(root.value).toEqual(undefined);
   expect(lvl_1_1.value).toEqual(undefined);
   expect(lvl_2_1.value).toEqual(undefined);

   bus.send(3);

   expect(root.value).toEqual(3);
   expect(lvl_1_1.value).toEqual(-3);
   expect(lvl_2_1.value).toEqual(60);
});
