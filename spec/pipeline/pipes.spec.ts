import { IBusChange, collect, EDetachMessage, map, Sequence, suppress, passUnique } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

it(`Line`, () => {
   const Ivalues: (readonly string[])[] = [];

   new Sequence([1, 2, 1, 3, 1, 2, 2, 3, 4, 5, 6]).pipe(
      map(value => value),
      passUnique(),
      map(value => `${value}`),
      collect(3)
   ).watch(value => {
      Ivalues.push(value)
   })

   const Evalues = [['1', '2', '3'], ['4', '5', '6']];
   expect(Ivalues).toEqual(Evalues);
});

it(`Pipes`, async () => {
   const delayMs = 20;
   const size = 2;

   const mapper = map<number, string, IBusChange>(value => `${value}`);
   const suppresser = suppress<string>(delayMs);
   const collector = collect<string>(size);

   const Isuppresser: string[] = [];
   const Imapper: string[] = [];
   const Icollector: (readonly string[])[] = [];
   const Ipipe: (readonly string[])[] = [];

   mapper.watch(value => {
      Imapper.push(value);
   });
   suppresser.watch(value => {
      Isuppresser.push(value);
   });
   collector.watch(value => {
      Icollector.push(value);
   });

   const pipe = new Sequence([1, 2]).pipe(
      mapper,
      suppresser,
      collector,
   );

   pipe.watch(value => { Ipipe.push(value); });

   const Esuppresser = [`1`, `2`];
   const Emapper = [`1`, `2`];
   const Ecollector = [[`1`, `2`]];
   const Epipe = [[`1`, `2`]];

   await FN_DELAY(delayMs * 10);

   expect(Isuppresser).toEqual(Esuppresser);
   expect(Imapper).toEqual(Emapper);
   expect(Icollector).toEqual(Ecollector);
   expect(Ipipe).toEqual(Epipe);
   expect(pipe.shutdown).toEqual(EDetachMessage.Emittion);
});
