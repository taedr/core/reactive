import { IPossibleChange } from '@taedr/reactive';
import { initial, User, ReactiveUser, Note } from './_model';

let calls: number[] = [];
let object: ReactiveUser;
let Ecalls: number[];
let Evalue: User | undefined;
const getWatcher = (level: number) => (value: unknown, change: IPossibleChange) => {
   if (change.initial) return;
   calls.push(level);
   expect(object.value).toEqual(Evalue);
}

beforeEach(() => {
   Evalue = undefined;
   object = new ReactiveUser(initial);
   object.watch(getWatcher(1));
   calls = [];
   Ecalls = [];
});

afterEach(() => {
   expect(calls).toEqual(Ecalls);
});

it(`Replace`, () => {
   const name = object.fields.name;
   const address = object.fields.address;
   const notes = object.fields.notes;
   const update = {
      address: {
         country: `USA`
      },
      name: `Ivan`,
   };

   Ecalls = [3, 2, 2, 1];
   Evalue = { ...initial };
   Evalue.name = update.name;
   Evalue.address = {
      country: update.address.country,
      city: initial.address.city,
   };

   name.watch(getWatcher(2));
   address.watch(getWatcher(2));
   notes.watch(getWatcher(2));

   address.fields.city.watch(getWatcher(3));
   address.fields.country.watch(getWatcher(3));

   object.send(update);
});

describe(`Fields`, () => {
   it(`primitive`, () => {
      const nameField = object.fields.name;
      const name = `Petya`;

      Evalue = { ...initial, name };
      Ecalls = [2, 1];

      nameField.watch(getWatcher(2));
      nameField.send(name);
   });

   it(`object`, () => {
      const addressField = object.fields.address;
      const cityField = addressField.fields.city;
      const city = `Harkiv`;

      Evalue = { ...initial, address: { ...initial.address, city } };
      Ecalls = [3, 2, 1];

      addressField.watch(getWatcher(2));
      cityField.watch(getWatcher(3));
      cityField.send(city);
   });

   it(`array`, () => {
      const notes = object.fields.notes;
      const [note] = notes.wrappers;
      const newNote = new Note(`CHEKED`);

      Evalue = { ...initial, notes: [newNote, ...notes.value.slice(1)] };
      Ecalls = [3, 2, 1];

      notes.watch(getWatcher(2));
      note.watch(getWatcher(3));
      note.fields.text.send(newNote.text);
   });

   it(`record`, () => {
      const marks = object.fields.marks;
      const mark = marks.wrappers.one;
      const newMark = 4;

      Evalue = { ...initial, marks: { one: newMark, two: marks.value.two } };
      Ecalls = [3, 2, 1];

      marks.watch(getWatcher(2));
      mark.watch(getWatcher(3));
      mark.send(newMark);
   });
});
