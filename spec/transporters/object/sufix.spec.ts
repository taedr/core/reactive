import { Bus, ERRORS } from "@taedr/reactive";
import { TDeepPartial } from "@taedr/utils";
import { initial, ReactiveUser, User } from "./_model";

it(`Suffix`, () => {
   const bus = new Bus<TDeepPartial<User>>();
   const sufix = bus.suffix(new ReactiveUser(initial));

   const newName = `Testa`;

   bus.send({
      name: newName
   });

   expect(sufix.value.name).toEqual(newName);
});
