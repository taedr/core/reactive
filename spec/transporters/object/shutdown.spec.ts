import { ERRORS } from "@taedr/reactive";
import { initial, ReactiveUser, User } from "./_model";

it(`Shutdown`, () => {
   const message = `WASTED`;
   const object = new ReactiveUser(initial);
   let Ivalue: User | undefined;
   let Imessage: string | undefined;

   object.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });

   expect(Ivalue).toEqual(initial);

   object.shutdown = message;

   expect(() => object.send({ name: `Gat` })).toThrowError(ERRORS.shutdown.change);

   for (const [_, field] of Object.entries(object.fields)) {
      expect(field.shutdown).toEqual(message);
   }

   expect(object.shutdown).toEqual(message);
   expect(Imessage).toEqual(message);
   expect(Ivalue).toEqual(initial);
   expect(object.watchers.size).toEqual(0);

   let Ivalue_1: User | undefined;
   let Imessage_1 = ``;

   object.watch({
      value: value => Ivalue_1 = value,
      detach: _mess => Imessage_1 = _mess,
   });

   expect(Imessage_1).toEqual(message);
   expect(Ivalue_1).toEqual(initial);
});


it(`Shutdown field`, () => {
   const message = `WASTED`;
   const object = new ReactiveUser(initial);
   const age = object.fields.age;

   let Ivalue: User | undefined;
   let Imessage: string | undefined;

   object.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });

   age.shutdown = message;

   const Euser_1 = { ...initial, name: `Orest` };

   object.send({
      name: Euser_1.name
   });

   expect(Ivalue).toEqual(Euser_1);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Euser_1);
});
