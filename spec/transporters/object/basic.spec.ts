import { ReactiveNumber, ReactiveObject, ReactiveString } from '@taedr/reactive';
import { Address, initial, ReactiveUser, User } from './_model';

let object: ReactiveUser;

beforeEach(() => {
   object = new ReactiveUser(initial);
});

it(`Should not allow to modify fields`, async () => {
   expect(() => (<any>object.value)['age'] = 12).toThrowError();
   expect(() => (<any>object.value)['address']['city'] = 12).toThrowError();

   object.send({ name: `Petya` });

   expect(() => (<any>object.value)['name'] = ``).toThrowError();
});

it(`Sub object fields should be the same ref to entry value`, async () => {
   expect(object.value.address === object.fields.address.value).toBeTruthy();
});

it(`Should use only known fields from update object`, async () => {
   const update: any = { name: `Petya`, grade: 24 };

   object.send(update);

   expect(object.value).toEqual({ ...initial, name: update.name });
});

it(`If builder provided updated value should be the class instance`, async () => {
   const address = object.fields.address;

   expect(object.value).toBeInstanceOf(User);
   expect(address.value).toBeInstanceOf(Address);

   object.send({ name: `Petya` });

   expect(object.value).toBeInstanceOf(User);
   expect(address.value).toBeInstanceOf(Address);

   object.fields.name.send(`Evdokia`);

   expect(object.value).toBeInstanceOf(User);
   expect(address.value).toBeInstanceOf(Address);
});

it(`Literal`, () => {
   const object = new ReactiveObject({
      name: new ReactiveString(``),
      age: new ReactiveNumber<number | null>(null)
   });

   const newValue = {
      age: 22,
      name: `Orest`
   }

   object.send(newValue);

   expect(object.value).toEqual(newValue);
   expect(object.fields.age.value).toEqual(newValue.age);
   expect(object.fields.name.value).toEqual(newValue.name);
})
