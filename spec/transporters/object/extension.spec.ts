import { accumulate, AReactiveObject, AReactivesArray, AReactiveWrapperArray, IIndexAdd, IIndexDeleted, IIndexUpdated, IMove, IReactiveArrayModification, IReadonlyBus, IReadonlyReactive, IReadonlyRecord, map, passIf, Reactive, ReactiveArray, ReactiveArrayChange, ReactiveRecord, ReactiveStringArray, Sequence, TNewWrapper, TObjectSymbols } from '@taedr/reactive';
import { TMapper, TNew, TStringKeys } from '@taedr/utils';

interface ITucCtx<T> {
   validators?: TMapper<T, string>[];
}

class TucErrors extends ReactiveStringArray {
   protected _custom?: readonly string[];

   public get custom() { return this._custom ?? []; }
   public set custom(errors: readonly string[]) {
      this._custom = errors;
   }

   protected createValuesChange(
      values: string[],
      added: readonly IIndexAdd<string>[],
      deleted: readonly IIndexDeleted<string>[],
      updated: readonly IIndexUpdated<string>[],
      moved: readonly IMove[]
   ): ReactiveArrayChange<string> {
      return new ReactiveArrayChange([])
   }

}


abstract class ATuc<T> {
   protected _validators?: TMapper<T, string>[];
   protected _errors = new TucErrors();
   protected _customErrors?: readonly string[];
   protected _forceHighlight?: true;
   protected _touched = false;

   public get errors(): IReadonlyReactive<readonly string[]> { return this._errors; }
   public get customErrors() { return this._customErrors; }
   public set customErrors(errors: readonly string[] | undefined) {
      errors ? this._customErrors = errors : delete this._customErrors;
   }
   public get highlight() { return this._forceHighlight ?? this._touched; }
   public set highlight(highlight: boolean) {
      highlight ? this._forceHighlight = highlight : delete this._forceHighlight;
   }

   constructor(
      readonly bus: Reactive<T>,
      ctx?: ITucCtx<T>,
   ) {
      if (ctx?.validators) this._validators = ctx.validators;
      this.watchBus();
   }

   protected watchBus() {
      this.bus.watch({
         id: `TUC`,
         value: this.onBusValue.bind(this),
      })
   }

   protected onBusValue() {
      if (this._validators) this._errors.send(Object.freeze(this.getErrors()));
   }

   protected getErrors(): string[] {
      const validators = this._validators as TMapper<T, string>[];
      new Sequence(validators).pipe(
         map(value => value(this.bus.value)),
         passIf(value => !!value),
         accumulate(),
      ).suffix(new ReactiveStringArray()).value;

      return validators
         .map(v => v(this.bus.value))
         .filter(Boolean);
   }
}


interface ITucPrimitiveCtx<T> extends ITucCtx<T> {
   formatter?: TMapper<T, string>;
}

abstract class ATucPrimitive<T> extends ATuc<T> {
   protected _formatted: string;
   protected _formatter?: TMapper<T, string>;

   readonly bus: Reactive<T>;

   public get formatted() { return this._formatted; }


   constructor(
      value: T,
      ctx?: ITucPrimitiveCtx<T>,
   ) {
      super(new Reactive(value), ctx);
      if (ctx?.validators) this._formatter = ctx.formatter;
   }

   protected onBusValue() {
      super.onBusValue();
      this._formatted = this.getFormatted();
   }

   protected getFormatted() {
      return this._formatter ? this._formatter(this.bus.value) : `${this.bus.value}`;
   }
}


class TucString extends ATucPrimitive<string> { }
class TucNumber extends ATucPrimitive<number> { }
class TucBoolean extends ATucPrimitive<boolean> { }
class TucBigint extends ATucPrimitive<bigint> { }

abstract class ATucArray<T> extends ATuc<readonly T[]>{
   readonly bus: ReactiveArray<T>;


}

class TucsArrayBus<T, W extends TTuc<T>> extends AReactiveWrapperArray<T, W> {
   protected getReactive(wrapper: W) {
      return wrapper.bus;
   }
}


class TucsArray<T, W extends TTuc<T>> extends ATucArray<T> {
   readonly bus: TucsArrayBus<T, W>;

   constructor(
      builder: TNewWrapper<T, W>,
      value: readonly T[],
      source?: IReadonlyBus<IReactiveArrayModification<T>>
   ) {
      super(new TucsArrayBus(builder, value, source));
   }
}


abstract class ATucPrimitiveArray<T> extends ATucArray<T> {
   constructor(
      value: readonly T[],
      source?: IReadonlyBus<IReactiveArrayModification<T>>
   ) {
      super(new ReactiveArray(value, source));
   }
}

class TucNumberArray extends ATucPrimitiveArray<number> { }
class TucStringArray extends ATucPrimitiveArray<string> { }
class TucBooleanArray extends ATucPrimitiveArray<boolean> { }
class TucBigintArray extends ATucPrimitiveArray<bigint> { }

class TucArray<T> extends ATucArray<T> {
   constructor(
      value: readonly T[],
      source?: IReadonlyBus<IReactiveArrayModification<T>>
   ) {
      super(new ReactiveArray(value, source));
   }
}

class TucMap<T> extends ATuc<IIterableMap<T>> {
   readonly bus: ReactiveRecord<T>;

   constructor(
      value: IReadonlyRecord<T>
   ) {
      super(new ReactiveRecord(value));
   }
}

class TucObjectBus<T extends object, F extends TTucObjectFields<T>> extends AReactiveObject<T, TTuc<any>, F> {
   protected getReactive(wrapper: TTuc<T>) {
      return wrapper.bus;
   }
}

class TucObject<T extends object, F extends TTucObjectFields<T> = TTucObjectFields<T>> extends ATuc<Readonly<T>>{
   readonly bus: TucObjectBus<T, F>;

   public get highlight() { return this._forceHighlight ?? this._touched; }
   public set highlight(highlight: boolean) {
      highlight ? this._forceHighlight = highlight : delete this._forceHighlight;
   }


   constructor(
      builder: TNew<T>,
      fields: F,
      symbols?: TObjectSymbols<T>
   ) {
      super(new TucObjectBus(builder, fields, symbols));
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------

   protected getErrors(): string[] {
      const objectErrors = super.getErrors();
      const fieldsErrors = Object.entries(this.bus.fields).reduce((acc: string[], [key, field]) => ([
         ...acc, ...field.errors.value.map(v => `${key} : ${v}`),
      ]), []);
      return [...objectErrors, ...fieldsErrors];
   }

   protected setHighlight() {

   }
}

export type TTucObjectFields<T extends object> = {
   [K in keyof Pick<T, TStringKeys<T>>]: TTuc<T[K]>;
}

export type TTuc<T> =
   T extends string ? TucString :
   T extends number ? TucNumber :
   T extends boolean ? TucBoolean :
   T extends bigint ? TucBigint :
   T extends readonly unknown[] ? TucArray<T[number]> :
   T extends object ? T extends Record<string, T[TStringKeys<T>]> ?
   TucMap<T[string]> : TucObject<T> : ATuc<T>;



class Address {
   constructor(
      public country: string,
      public city: string,
   ) { }
}

class TucAddress extends TucObject<Address, ReturnType<typeof TucAddress.getFields>> {
   constructor(address: Address) {
      super(Address, TucAddress.getFields(address));
   }

   protected static getFields(address: Address) {
      return {
         country: new TucString(address.country),
         city: new TucString(address.city),
      }
   }
}

class Ref {
   constructor(
      public url: string
   ) { }
}

class TucRef extends TucObject<Ref, ReturnType<typeof TucRef.getFields>> {
   constructor(ref: Ref) {
      super(Ref, TucRef.getFields(ref));
   }

   protected static getFields(ref: Ref) {
      return {
         url: new TucString(ref.url),
      }
   }
}

class TucRefArray extends TucsArray<Ref, TucRef> {
   constructor(
      refs: readonly Ref[]
   ) {
      super(TucRef, refs);
   }
}

class User {
   constructor(
      public name: string,
      public age: number,
      public verified: boolean,
      public notes: string[],
      public refs: Ref[],
      public marks: Record<string, number>,
      public address: Address,
   ) { }
}

class TucUser extends TucObject<User, ReturnType<typeof TucUser.getFields>> {
   constructor(user: User) {
      super(User, TucUser.getFields(user));
   }

   protected static getFields(user: User) {
      return {
         name: new TucString(user.name, {
            formatter: name => `Duzze harnii Pan - ${name}`,
            validators: [
               name => name === `Andri` ? `KRASAVA` : `NIT`,
            ]
         }),
         age: new TucNumber(user.age),
         verified: new TucBoolean(user.verified),
         notes: new TucStringArray(user.notes),
         refs: new TucsArray(TucRef, user.refs),
         marks: new TucMap(user.marks),
         address: new TucAddress(user.address),
      };
   }
}


it(`Test`, () => {
   const user = new User(`Andrii`, 22, true, [`A`, `B`], [{ url: `Tudi` }], { one: 1, two: 2 }, new Address(`Ukraine`, `Chornomorsk`));
   const tuc = new TucUser(user);

   expect(tuc.bus).toBeInstanceOf(TucObjectBus);
   expect(tuc.bus.value).toEqual(user);
   expect(tuc.bus.value).toBeInstanceOf(User);
   expect(tuc.bus.fields.age).toBeInstanceOf(TucNumber)
   expect(tuc.bus.fields.age.bus.value).toEqual(user.age)
   expect(tuc.bus.fields.name).toBeInstanceOf(TucString)
   expect(tuc.bus.fields.name.bus.value).toEqual(user.name)
   expect(tuc.bus.fields.verified).toBeInstanceOf(TucBoolean)
   expect(tuc.bus.fields.verified.bus.value).toEqual(user.verified)
   expect(tuc.bus.fields.marks).toBeInstanceOf(TucMap)
   expect(tuc.bus.fields.marks.bus.value).toEqual(user.marks)
   expect(tuc.bus.fields.notes).toBeInstanceOf(TucStringArray)
   expect(tuc.bus.fields.notes.bus.value).toEqual(user.notes)
   expect(tuc.bus.fields.refs).toBeInstanceOf(TucsArray)
   expect(tuc.bus.fields.refs.bus.value).toEqual(user.refs)
   expect(tuc.bus.fields.address).toBeInstanceOf(TucAddress)
   expect(tuc.bus.fields.address.bus.value).toEqual(user.address)

   tuc.bus.fields.age.bus.send(22);

   expect(tuc.bus.value.age).toEqual(22);

   tuc.bus.fields.notes.bus.add([`C`, `D`]);

   expect(tuc.bus.value.notes).toEqual(["A", "B", "C", "D"]);

   tuc.bus.fields.marks.bus.add({ three: 3, four: 4 });

   expect(tuc.bus.value.marks).toEqual({ "four": 4, "one": 1, "three": 3, "two": 2 });

   tuc.bus.fields.refs.bus.wrappers[0].bus.fields.url.bus.send(`Sudi`);

   expect(tuc.bus.value.refs).toEqual([{ url: `Sudi` }]);

   tuc.bus.fields.name //?


   tuc
});

it(``, () => {



})
