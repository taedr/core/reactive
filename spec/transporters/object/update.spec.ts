import { TDeepPartial } from '@taedr/utils';
import { initial, ReactiveUser, User } from './_model';

const update_1: TDeepPartial<User> = {
   age: 111,
   name: `Masha`,
   address: {
      city: `Kyiv`
   },
};

const update_2: TDeepPartial<User> = {
   isAlive: true,
   notes: [`two`, `three`]
};

it(`Should allow partial fields update`, () => {
   const object = new ReactiveUser(initial);

   let curr: User = { ...initial };
   let fieldCalls = 0; // Equal to chaged fields count
   let rootCalls = 0; // Equal to value assign count
   let Rupdate: TDeepPartial<User> = {};

   object.watch(value => {
      rootCalls++;
      expect(value).toEqual(curr);
   });

   for (const key in initial) {
      object.fields[key].watch(value => {
         curr[key] = value;
         Rupdate[key] = value;
         fieldCalls++;
      });
   }

   expect(object.value).toEqual(curr);
   expect(fieldCalls).toEqual(6);
   expect(rootCalls).toEqual(1);

   object.send(update_1);

   expect(Rupdate).toEqual(curr);
   expect(object.value).toEqual(curr);
   expect(fieldCalls).toEqual(9);
   expect(rootCalls).toEqual(2);

   object.send(update_2);

   expect(Rupdate).toEqual(curr);
   expect(object.value).toEqual(curr);
   expect(fieldCalls).toEqual(11);
   expect(rootCalls).toEqual(3);
});

it(`Should update root reactive when updating fields`, () => {
   const object = new ReactiveUser(initial);

   let update: TDeepPartial<User> = initial;
   let curr: User = { ...initial };
   let Rupdate: TDeepPartial<User> = {};
   let fieldCalls = 0; // Equal to chaged fields count
   let rootCalls = 0; // initial call +1 on every field change

   object.watch(value => {
      rootCalls++;
      expect(value).toEqual(curr);
   });

   for (const key in initial) {
      object.fields[key as any].watch(value => {
         curr[key] = value;
         fieldCalls++;
         Rupdate[key] = value;
      });
   }

   expect(object.value).toEqual(curr);
   expect(fieldCalls).toEqual(6);
   expect(rootCalls).toEqual(1);

   for (const key in update_1) {
      object.fields[key as any].send(update_1[key]);
   }

   expect(Rupdate).toEqual(curr);
   expect(object.value).toEqual(curr);
   expect(fieldCalls).toEqual(9);
   expect(rootCalls).toEqual(4);

   for (const key in update_2) {
      object.fields[key as any].send(update_2[key]);
   }

   expect(Rupdate).toEqual(curr);
   expect(object.value).toEqual(curr);
   expect(rootCalls).toEqual(6);
   expect(fieldCalls).toEqual(11);
});
