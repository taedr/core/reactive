import { ReactiveNumber, ReactiveObject, ReactiveString } from '@taedr/reactive';

it(`SHOULD reset to null only the root value`, () => {
   const object = new ReactiveObject({
      name: new ReactiveString(`A`),
      age: new ReactiveNumber(11),
   }, null);
   const { name, age } = object.fields;

   object.send(null);

   expect(object.value).toBeNull();
   expect(name.value).toEqual(`A`);
   expect(age.value).toEqual(11);

   object.send({
      age: 22
   });

   expect(object.value).toEqual({ name: `A`, age: 22 });

   object.send(null);

   name.send(`B`);

   expect(object.value).toEqual({ name: `B`, age: 22 });
});
