import { ReactiveString, AReactiveObject } from '@taedr/reactive';


it(`should transfer symbols value`, async () => {
   const APP = Symbol(`APP`);

   class User {
      public [APP]: number;

      constructor(
         public name: string,
         app: number,
      ) {
         this[APP] = app;
      }
   }

   class ReactiveUser extends AReactiveObject<User, never, ReturnType<typeof ReactiveUser.getFields>> {
      constructor(
         user: User
      ) {
         super({
            fields: ReactiveUser.getFields(user),
            symbols: user
         })
      }

      protected createInstance(): User {
         return new (User as any)();
      }

      protected static getFields(user: User) {
         return {
            name: new ReactiveString(user.name)
         }
      }
   }

   const initial = new User(`Vasya`, 12);
   const object = new ReactiveUser(initial);


   object.send({
      name: `Kolyan`
   });
   expect(object.value[APP]).toEqual(initial[APP]);

   const appUpdate = 45;
   object.send({
      [APP]: appUpdate
   });

   expect(object.value[APP]).toEqual(appUpdate);
});
