import { AReactiveObject, AReactivesObjectArray, IReactive, ReactiveBoolean, ReactiveNumber, ReactiveNumberRecord, ReactiveObjectArray, ReactivesNumberRecord, ReactivesStringArray, ReactiveString, ReactiveStringArray, TReactiveObjectFields } from "@taedr/reactive";
import { TMapper, TProducer } from "@taedr/utils";

export class Address {
   constructor(
      public country: string,
      public city: string,
   ) { }
}

export class ReactiveAddress extends AReactiveObject<Address, never, ReturnType<typeof ReactiveAddress.createFields>> {
   constructor(
      address: Address,
   ) {
      super({
         fields: ReactiveAddress.createFields(address),
      });
   }

   protected createInstance(): Address {
      return new (Address as any)();
   }

   protected static createFields(address: Address) {
      return {
         city: new ReactiveString(address.city),
         country: new ReactiveString(address.country),
      };
   }
}

export class Note {
   constructor(
      readonly text: string
   ) { }
}

export class ReactiveNote extends AReactiveObject<Note> {
   constructor(
      note: Note
   ) {
      super({
         fields: {
            text: new ReactiveString(note.text)
         }
      });
   }

   protected createInstance(): Note {
      return new (Note as any)();
   }
}

export class ReactivesNoteArray extends AReactivesObjectArray<Note, ReactiveNote> {
   protected createWrapper(value: Note): ReactiveNote {
      return new ReactiveNote(value);
   }
}

export class User {
   constructor(
      public name: string,
      public age: number,
      public isAlive: boolean,
      public notes: Note[],
      public marks: Record<string, number>,
      public address: Address,
   ) {
   }
}


export class ReactiveUser extends AReactiveObject<User, never, ReturnType<typeof ReactiveUser.getFields>> {
   constructor(
      user: User,
   ) {
      super({
         fields: ReactiveUser.getFields(user)
      });
   }

   protected createInstance(): User {
      return new (User as any)();
   }

   protected static getFields(user: User) {
      return {
         name: new ReactiveString(user.name),
         age: new ReactiveNumber(user.age),
         isAlive: new ReactiveBoolean(user.isAlive),
         address: new ReactiveAddress(user.address),
         marks: new ReactivesNumberRecord(user.marks),
         notes: new ReactivesNoteArray(user.notes)
      }
   }
}

export const initial = new User(`Vasya`, 21, false, [{ text: 'A' }, { text: 'B' }], { one: 1, two: 2 }, new Address(`Ukraine`, `Odessa`));
