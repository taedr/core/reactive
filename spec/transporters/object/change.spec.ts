import { IReactiveArrayChange, IReactiveArrayModification, IReactiveObjectChange, IReactivesArrayChange } from '@taedr/reactive';
import { TDeepPartial } from '@taedr/utils';
import { Address, initial, Note, ReactiveUser, User } from './_model';

it(`Initial`, async () => {
   const object = new ReactiveUser(initial);
   const Echange: IReactiveObjectChange<User> = {
      initial: true,
      changed: true,
      delta: initial,
   };
   const Evalue = initial;

   let Ichange: IReactiveObjectChange<User> | undefined;
   let Ivalue: User | undefined;

   object.watch((value, change) => {
      Ivalue = value;
      Ichange = change
   });

   expect(Ichange).toEqual(Echange);
   expect(Ivalue).toEqual(Evalue);
   expect(Object.isFrozen(Ichange)).toBeTruthy();
});


it(`Update Change`, async () => {
   const object = new ReactiveUser(initial);

   const newCity = `Chornomorsk`;
   const newAge = 18;
   const newNotes = [{ text: `One` }, { text: `Two` }];

   const addressField = object.fields.address;
   const notesField = object.fields.notes;

   let IuserChange: IReactiveObjectChange<User> | undefined;
   let Iuser: User | undefined;
   let IaddressChange: IReactiveObjectChange<Address> | undefined;
   let InotesChange: IReactiveArrayModification<Note> | undefined;

   object.watch((value, change) => {
      Iuser = value;
      IuserChange = change;
   });

   addressField.watch((_, change) => IaddressChange = change);
   notesField.watch((_, change) => InotesChange = change);

   const update: any = {
      age: newAge,
      h: 22,
      notes: newNotes,
      address: {
         city: newCity,
         grt: 12,
      },
   };



   const EaddressDelta: TDeepPartial<Address> = {
      city: newCity
   };

   const EuserDelta: TDeepPartial<User> = {
      age: newAge,
      notes: newNotes,
      address: EaddressDelta
   };


   const Eaddress: Address = { ...initial.address, ...EaddressDelta };
   const EaddressChange: IReactiveObjectChange<Address> = {
      changed: true, delta: EaddressDelta
   };
   const Euser: User = { ...initial, ...EuserDelta as any, address: Eaddress };
   const deletedNotesWrappers = notesField.wrappers;

   object.send(update);

   const EnotesChange: IReactivesArrayChange<Note> = {
      changed: true,
      added: [{ index: 0, values: newNotes, wrappers: notesField.wrappers }],
      deleted: [{ index: 0, count: initial.notes.length, values: initial.notes, wrappers: deletedNotesWrappers }],
      updated: [],
      moved: []
   };

   const EuserChange: IReactiveObjectChange<User> = {
      changed: true, delta: EuserDelta
   };

   expect(Iuser).toEqual(Euser);
   expect(IuserChange).toEqual(EuserChange);
   expect(IaddressChange).toEqual(EaddressChange);
   expect(InotesChange).toEqual(EnotesChange);
   expect(Object.isFrozen(IuserChange)).toBeTruthy();
});


it(`Update from fields`, async () => {
   const object = new ReactiveUser(initial);

   let Iuser: User | undefined;
   let IuserChange: IReactiveObjectChange<User> | undefined;
   let InotesChange: IReactiveArrayChange<Note> | undefined;

   const notesField = object.fields.notes;

   object.watch((value, change) => {
      Iuser = value;
      IuserChange = change;
   });
   notesField.watch((_, change) => InotesChange = change);

   const index = 1;
   const noteField = notesField.wrappers[index];
   const newNote = new Note(`SSS`);
   const Enotes = [notesField.value[0], newNote];

   noteField.fields.text.send(newNote.text);

   const EnotesChange: IReactivesArrayChange<Note> = {
      changed: true,
      added: [],
      deleted: [],
      updated: [{ index: 1, value: newNote, wrapper: notesField.wrappers[1] }],
      moved: []
   };

   const Euser = { ...object.value, notes: Enotes };
   const EuserChange: IReactiveObjectChange<User> = {
      changed: true,
      delta: { notes: Enotes },
   };

   expect(Iuser).toEqual(Euser);
   expect(IuserChange).toEqual(EuserChange);
   expect(InotesChange).toEqual(EnotesChange);
   expect(Object.isFrozen(IuserChange)).toBeTruthy();
});
