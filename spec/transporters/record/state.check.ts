import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactiveRecord, IReadonlyReactivesRecord, IReadonlyRecord } from "@taedr/reactive";
import { ReactiveUser, User } from "./_model";

export function checkReactivesUsersState(
   record: IReadonlyReactivesRecord<User>,
   mirror: IReadonlyReactivesRecord<User>,
   IAchange: IReactivesRecordChange<User>,
   IMchange: IReactivesRecordChange<User>,
   Ichange: IReactiveRecordChange<User>,
   Echange: IReactiveRecordChange<User>,
   Evalue: IReadonlyRecord<User>,
) {
   for (const key in record.value) {
      const wrapper = record.wrappers[key];
      const value = record.value[key];
      expect(value).toBeInstanceOf(User);
      expect(wrapper).toBeInstanceOf(ReactiveUser);
      expect(wrapper).not.toBe(mirror.wrappers[key]);
   }

   checkReactivesRecordState(record, mirror, IAchange, IMchange, Ichange, Echange, Evalue);
}

export function checkReactivesRecordState(
   record: IReadonlyReactivesRecord<unknown>,
   mirror: IReadonlyReactivesRecord<unknown>,
   IAchange: IReactivesRecordChange<unknown>,
   IMchange: IReactivesRecordChange<unknown>,
   Ichange: IReactiveRecordChange<unknown>,
   Echange: IReactiveRecordChange<unknown>,
   Evalue: IReadonlyRecord<unknown>,
) {
   for (const key in record.value) {
      const wrapper = record.wrappers[key];
      const value = record.value[key];
      expect(wrapper.value).toBe(value);
      expect(mirror.wrappers[key]).not.toBe(wrapper);
      expect(wrapper.watchers.has(record['_id'])).toBeTruthy();
   }

   for (const { wrapper } of IAchange.deleted) {
      expect(wrapper.watchers.size).toEqual(0);
   }

   expect(Object.isFrozen(record.wrappers)).toBeTruthy();

   checkReactiveRecordState(record, mirror, IAchange, IMchange, Ichange, Echange, Evalue);
}

export function checkReactiveRecordState(
   record: IReadonlyReactiveRecord<unknown>,
   mirror: IReadonlyReactiveRecord<unknown>,
   IAchange: IReactiveRecordChange<unknown>,
   IMchange: IReactiveRecordChange<unknown>,
   Ichange: IReactiveRecordChange<unknown>,
   Echange: IReactiveRecordChange<unknown>,
   Evalue: IReadonlyRecord<unknown>,
) {
   expect(record.value).toEqual(Evalue);
   expect(mirror.value).toEqual(record.value);
   expect(Ichange).toBe(IAchange);
   expect(Object.isFrozen(IAchange)).toBeTruthy();
   expect(Object.isFrozen(IMchange)).toBeTruthy();

   for (const record of [
      IAchange.added,
      IAchange.deleted,
      IAchange.updated,
   ]) {
      expect(Object.isFrozen(record)).toBeTruthy();

      for (const value of record) {
         expect(Object.isFrozen(value)).toBeTruthy();
      }
   }

   for (const key of [
      `added`, `updated`, `deleted`
   ]) {
      expect(IAchange[key].length).toEqual(Echange[key].length);
      expect(Echange[key].length).toEqual(IMchange[key].length);

      for (let i = 0; i < IAchange[key].length; i++) {
         const Eadded = Echange[key][i];
         const IAadded = IAchange[key][i];
         const IMadded = IMchange[key][i];

         expect(Eadded.key).toEqual(IAadded.key);
         expect(Eadded.value).toEqual(IAadded.value);
         expect(IAadded.key).toEqual(IMadded.key);
         expect(IAadded.value).toEqual(IMadded.value);

         expect(Object.isFrozen(IAadded)).toBeTruthy();
         expect(Object.isFrozen(IMadded)).toBeTruthy();
      }
   }
}
