import { AReactiveObject, AReactivesObjectRecord, IReadonlyRecord, ReactiveNumber, ReactiveObjectRecord, ReactiveString } from "@taedr/reactive";

export class User {
   constructor(
      readonly name: string,
      readonly age: number,
   ) { }
}

export class ReactiveUser extends AReactiveObject<User, never, ReturnType<typeof ReactiveUser.getFields>> {
   constructor(
      user: User
   ) {
      super({ fields: ReactiveUser.getFields(user) });
   }

   protected createInstance(): User {
      return new (User as any)();
   }

   protected static getFields(user: User) {
      return {
         age: new ReactiveNumber(user.age),
         name: new ReactiveString(user.name),
      };
   }
}


export class ReactiveUserRecord extends ReactiveObjectRecord<User> {
}

export class ReactivesUserRecord extends AReactivesObjectRecord<User, never, ReactiveUser> {
   protected createWrapper(value: User): ReactiveUser {
      return new ReactiveUser(value);
   }
}

export const initial = {
   1: new User(`Vasya`, 1),
   2: new User(`Petya`, 2),
   3: new User(`Katya`, 3),
}

export const modification = {
   4: new User(`Ibragim`, 4),
   5: new User(`Kolyan`, 5),
};

export const getInitialWrappers = () => Object.entries(initial).reduce((acc, [key, value]) => ({
   ...acc,
   [key]: new ReactiveUser(value)
}), {}) as Record<string, ReactiveUser>;

export const getModificationWrappers = () => Object.entries(modification).reduce((acc, [key, value]) => ({
   ...acc,
   [key]: new ReactiveUser(value)
}), {}) as Record<string, ReactiveUser>;
