import { IReactiveRecordChange, IReadonlyReactiveRecord } from "@taedr/reactive";
import { checkReactiveRecordState } from "../state.check";
import { initial, modification, ReactiveUserRecord, User } from "../_model";

let record: ReactiveUserRecord;
let mirror: IReadonlyReactiveRecord<User>;
let IRchange: IReactiveRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: typeof IRchange;
let Evalues: ReactiveUserRecord['value'];

beforeEach(() => {
   record = new ReactiveUserRecord(initial);
   mirror = record.suffix(new ReactiveUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {
      1: modification[4],
      2: initial[2],
      3: modification[5],
   };
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [
         { key: `1`, value: modification[4] },
         { key: `3`, value: modification[5] },
      ]
   };
});

afterEach(() => {
   checkReactiveRecordState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

it(`Compare`, async () => {
   Ichange = record.update((_, key) => {
      if (key === `1`) return modification['4'];
      if (key === `3`) return modification['5'];
   });
});

it(`Record`, async () => {
   Ichange = record.update({
      1: modification[4],
      3: modification[5]
   });
});

it(`Entries`, async () => {
   Ichange = record.update(Echange.updated);
});

describe(`SHOULD NOT be changed`, () => {
   beforeEach(() => {
      Evalues = initial;
      Echange.changed = false;
      Echange.updated = [];
   });

   it(`Compare`, async () => {
      Ichange = record.update(() => { });
   });

   it(`Record`, async () => {
      Ichange = record.update({});
   });

   it(`Entries`, async () => {
      Ichange = record.update([]);
   });
});
