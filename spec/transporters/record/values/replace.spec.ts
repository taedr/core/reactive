import { IReactiveRecordChange, IReadonlyReactiveRecord } from '@taedr/reactive';
import { checkReactiveRecordState } from '../state.check';
import { initial, modification, ReactiveUserRecord, User } from '../_model';

let record: ReactiveUserRecord;
let mirror: IReadonlyReactiveRecord<User>;
let IRchange: IReactiveRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: typeof IRchange;
let Evalues: ReactiveUserRecord['value'];


beforeEach(() => {
   record = new ReactiveUserRecord(initial);
   mirror = record.suffix(new ReactiveUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {};
   Echange = {
      changed: true,
      added: [],
      deleted: Object.entries(initial).map(([key, value]) => ({ key, value })),
      updated: []
   };
});

afterEach(() => {
   checkReactiveRecordState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

it(`values`, async () => {
   Evalues = modification;
   Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

   Ichange = record.send(modification);
});

it(`null`, () => {
   Ichange = record.send(null);
});


it(`SHOULD NOT be changed`, () => {
   Echange.changed = false;
   Echange.deleted = [];

   record.send({});
   Ichange = record.send({});
});
