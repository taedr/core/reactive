import { IReactiveRecordChange, IReadonlyReactiveRecord } from '@taedr/reactive';
import { checkReactiveRecordState } from '../state.check';
import { initial, modification, ReactiveUserRecord, User } from '../_model';


let record: ReactiveUserRecord;
let mirror: IReadonlyReactiveRecord<User>;
let IRchange: IReactiveRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: typeof IRchange;
let Evalues: ReactiveUserRecord['value'];

const deleteKeys = [`2`, `4`];

beforeEach(() => {
   record = new ReactiveUserRecord({ ...initial, ...modification });
   mirror = record.suffix(new ReactiveUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {
      1: initial[1],
      3: initial[3],
      5: modification[5],
   };
   Echange = {
      changed: true,
      added: [],
      deleted: [
         { key: `2`, value: initial[2] },
         { key: `4`, value: modification[4] },
      ],
      updated: []
   };
});

afterEach(() => {
   checkReactiveRecordState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

it(`Delete Compare`, async () => {
   Ichange = record.delete((_, key) => deleteKeys.includes(key));
});

it(`Delete Keys`, async () => {
   Ichange = record.delete(...deleteKeys.map(key => ({ key })));
});

it(`Delete Values`, async () => {
   Ichange = record.delete(deleteKeys.map(key => record.value[key]));
});

describe(`SHOULD NOT be changed`, () => {
   beforeEach(() => {
      Evalues = { ...initial, ...modification };
      Echange.changed = false;
      Echange.deleted = [];
   });

   it(`Delete Compare`, async () => {
      Ichange = record.delete(() => { });
   });

   it(`Delete Keys`, async () => {
      Ichange = record.delete();
   });

   it(`Delete Values`, async () => {
      Ichange = record.delete([]);
   });
});
