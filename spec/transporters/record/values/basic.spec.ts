import { IReactiveRecordChange, IReadonlyReactiveRecord } from '@taedr/reactive';
import { checkReactiveRecordState } from '../state.check';
import { initial, modification, ReactiveUserRecord, User } from '../_model';


let record: ReactiveUserRecord;
let mirror: IReadonlyReactiveRecord<User>;
let IRchange: IReactiveRecordChange<User>;
let IMchange: IReactiveRecordChange<User>;

beforeEach(() => {
   record = new ReactiveUserRecord(initial);
   mirror = record.suffix(new ReactiveUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
});

it(`Initial`, async () => {
   const Echange: IReactiveRecordChange<User> = {
      initial: true,
      changed: true,
      added: Object.entries(initial).map(([key, value]) => ({ key, value })),
      deleted: [],
      updated: []
   };

   checkReactiveRecordState(record, mirror, IRchange, IMchange, IRchange, Echange, initial);
});

it(`has`, () => {
   expect(record.has(record.value[1])).toBeTruthy();
   expect(record.has(value => value === record.value[1])).toBeTruthy();
   expect(record.has(modification[4])).toBeFalsy();
   expect(record.has(value => value === modification[4])).toBeFalsy();
});
