import { ERRORS, IReactiveRecordChange } from '@taedr/reactive';
import { initial, ReactiveUserRecord, User } from '../_model';

const message = `WASTED`;

let Ichange: IReactiveRecordChange<User>;
let Imessage: string;
let record: ReactiveUserRecord;
let mirror: ReactiveUserRecord;

beforeEach(() => {
   Ichange = undefined as any;
   Imessage = undefined as any;
   record = new ReactiveUserRecord(initial);
   mirror = record.suffix(new ReactiveUserRecord({}));
   record.watch({
      value: (_, change) => Ichange = change,
      detach: message => Imessage = message,
   });
});

it(`Shutdown`, () => {
   record.shutdown = message;

   expect(() => record.send({})).toThrowError(ERRORS.shutdown.change);
   expect(() => record.add({})).toThrowError(ERRORS.shutdown.change);
   expect(() => record.delete([])).toThrowError(ERRORS.shutdown.change);
   expect(() => record.update([])).toThrowError(ERRORS.shutdown.change);

   expect(record.shutdown).toEqual(message);
   expect(mirror.shutdown).toEqual(message);
   expect(record.watchers.size).toEqual(0);
   expect(Imessage).toEqual(message);
   expect(record.value).toEqual(initial);

   let Ichange_1: IReactiveRecordChange<User> | undefined;
   let Imessage_1: string | undefined;

   record.watch({
      value: (_, change) => Ichange_1 = change,
      detach: message => Imessage_1 = message,
   });

   expect(Imessage_1).toEqual(message);
   expect(Ichange_1).toEqual(Ichange);
});
