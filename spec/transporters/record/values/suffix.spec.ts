import { ReactiveNumberRecord } from "@taedr/reactive";

let array: ReactiveNumberRecord;
let mirror: ReactiveNumberRecord;

beforeEach(() => {
   array = new ReactiveNumberRecord();
   mirror = array.suffix(new ReactiveNumberRecord());
});

it(`Source shutdown`, () => {
   array.shutdown = `WASTED`;
   expect(mirror.shutdown).toEqual(`WASTED`);
});

it(`Suffix shutdown`, () => {
   mirror.shutdown = `WASTED`;

   expect(mirror.shutdown).toEqual(`WASTED`);
   expect(array.watchers.size).toEqual(0);
});
