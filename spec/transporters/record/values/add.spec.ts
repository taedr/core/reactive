import { IReactiveRecordChange, IReadonlyReactiveRecord } from '@taedr/reactive';
import { checkReactiveRecordState } from '../state.check';
import { initial, modification, ReactiveUserRecord, User } from '../_model';


let record: ReactiveUserRecord;
let mirror: IReadonlyReactiveRecord<User>;
let IRchange: IReactiveRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: typeof IRchange;
let Evalues: ReactiveUserRecord['value'];

beforeEach(() => {
   record = new ReactiveUserRecord(initial);
   mirror = record.suffix(new ReactiveUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {};
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: []
   };
});

afterEach(() => {
   checkReactiveRecordState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

it(`SHOULD add keys from proveded record`, async () => {
   Evalues = { ...initial, ...modification };
   Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

   Ichange = record.add(modification);
});

it(`IF during add already existing key detected value under it SHOULD be marked as updated`, async () => {
   const modification: Record<string, User> = {
      2: new User(`Prokofia`, 24),
      4: new User(`Lavrentii`, 56),
   };

   Echange.added = [{ key: `4`, value: modification[4] }];
   Echange.updated = [{ key: `2`, value: modification[2] }];
   Evalues = {
      '1': initial[1],
      '2': modification[2],
      '3': initial[3],
      '4': modification[4],
   };

   Ichange = record.add(modification);
});

it(`SHOULD NOT be changed`, async () => {
   Echange.changed = false;
   Evalues = initial;

   Ichange = record.add({});
});
