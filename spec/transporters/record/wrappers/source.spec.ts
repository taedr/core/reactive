import { ReactivesNumberRecord } from "@taedr/reactive";

let array: ReactivesNumberRecord;
let mirror: ReactivesNumberRecord;

beforeEach(() => {
   array = new ReactivesNumberRecord();
   mirror = array.suffix(new ReactivesNumberRecord());
});

it(`Source shutdown`, () => {
   array.shutdown = `WASTED`;
   expect(mirror.shutdown).toEqual(`WASTED`);
});

it(`Suffix shutdown`, () => {
   mirror.shutdown = `WASTED`;

   expect(mirror.shutdown).toEqual(`WASTED`);
   expect(array.watchers.size).toEqual(0);
});
