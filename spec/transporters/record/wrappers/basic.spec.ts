import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactivesRecord } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { initial, getInitialWrappers, getModificationWrappers, ReactivesUserRecord, User, ReactiveUser } from '../_model';


let record: ReactivesUserRecord;
let mirror: IReadonlyReactivesRecord<User>;
let IRchange: IReactivesRecordChange<User>;
let IMchange: IReactivesRecordChange<User>;
let initialWrappers: Record<string, ReactiveUser>;
let modificationWrappers: Record<string, ReactiveUser>;

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   record = new ReactivesUserRecord(initialWrappers);
   mirror = record.suffix(new ReactivesUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
});

it(`Initial`, () => {
   const Echange: IReactiveRecordChange<User> = {
      initial: true,
      changed: true,
      added: Object.entries(initial).map(([key, value]) => ({ key, value })),
      deleted: [],
      updated: []
   };

   expect(record.wrappers).toEqual(initialWrappers);
   checkReactivesUsersState(record, mirror, IRchange, IMchange, IRchange, Echange, initial);
});

it(`has`, () => {
   expect(record.hasWrapper(record.wrappers[1])).toBeTruthy();
   expect(record.hasWrapper(value => value === record.wrappers[1])).toBeTruthy();
   expect(record.hasWrapper(modificationWrappers[4])).toBeFalsy();
   expect(record.hasWrapper(value => value === modificationWrappers[4])).toBeFalsy();
});
