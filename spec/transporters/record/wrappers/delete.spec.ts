import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactivesRecord } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { initial, modification, ReactivesUserRecord, User } from '../_model';


let record: ReactivesUserRecord;
let mirror: IReadonlyReactivesRecord<User>;
let IRchange: IReactivesRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: IReactiveRecordChange<User>;
let Evalues: ReactivesUserRecord['value'];

const deleteKeys = [`2`, `4`];

beforeEach(() => {
   record = new ReactivesUserRecord({ ...initial, ...modification });
   mirror = record.suffix(new ReactivesUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {
      1: initial[1],
      3: initial[3],
      5: modification[5],
   };
   Echange = {
      changed: true,
      added: [],
      deleted: [
         { key: `2`, value: initial[2] },
         { key: `4`, value: modification[4] },
      ],
      updated: []
   };
});

afterEach(() => {
   checkReactivesUsersState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

describe(`Values`, () => {
   it(`Delete Compare`, () => {
      Ichange = record.delete((_, key) => deleteKeys.includes(key));
   });

   it(`Delete Keys`, () => {
      Ichange = record.delete(...deleteKeys.map(key => ({ key })));
   });

   it(`Delete Values`, () => {
      Ichange = record.delete(deleteKeys.map(key => record.value[key]));
   });
});


describe(`Wrappers`, () => {
   it(`Delete Compare`, () => {
      Ichange = record.deleteWrappers((_, key) => deleteKeys.includes(key));
   });

   it(`Delete Values`, () => {
      Ichange = record.deleteWrappers(deleteKeys.map(key => record.wrappers[key]));
   });
});
