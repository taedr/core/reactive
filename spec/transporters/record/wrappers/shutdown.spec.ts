import { ERRORS, IReadonlyRecord } from '@taedr/reactive';
import { initial, modification, ReactivesUserRecord, User } from '../_model';

const message = `WASTED`;

let Ivalue: IReadonlyRecord<User>;
let record: ReactivesUserRecord;
let mirror: ReactivesUserRecord;
let Imessage: string;

beforeEach(() => {
   if (record) record.send({});
   record = new ReactivesUserRecord(initial);
   mirror = record.suffix(new ReactivesUserRecord({}));
   record.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message,
   });
});

it(`Shutdown`, () => {
   record.shutdown = message;

   expect(() => record.send({})).toThrowError(ERRORS.shutdown.change);
   expect(() => record.add({})).toThrowError(ERRORS.shutdown.change);
   expect(() => record.delete([])).toThrowError(ERRORS.shutdown.change);
   expect(() => record.update({})).toThrowError(ERRORS.shutdown.change);
   expect(() => record.wrappers[1].send(modification[0])).not.toThrowError(ERRORS.shutdown.change);

   expect(record.shutdown).toEqual(message);
   expect(mirror.shutdown).toEqual(message);
   expect(record.watchers.size).toEqual(0);
   expect(Imessage).toEqual(message);
   expect(Ivalue).toEqual(initial);

   let Ivalue_1: IReadonlyRecord<User> | undefined;
   let Imessage_1: string | undefined;

   record.watch({
      value: value => Ivalue_1 = value,
      detach: message => Imessage_1 = message,
   });

   expect(Imessage_1).toEqual(message);
   expect(Ivalue_1).toEqual(initial);
});
