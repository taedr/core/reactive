import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactivesRecord } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { getInitialWrappers, getModificationWrappers, initial, modification, ReactivesUserRecord, ReactiveUser, User } from '../_model';


let record: ReactivesUserRecord;
let mirror: IReadonlyReactivesRecord<User>;
let IRchange: IReactivesRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: IReactiveRecordChange<User>;
let Evalues: ReactivesUserRecord['value'];
let initialWrappers: Record<string, ReactiveUser>;
let modificationWrappers: Record<string, ReactiveUser>;

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   record = new ReactivesUserRecord(initialWrappers);
   mirror = record.suffix(new ReactivesUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {};
   Echange = {
      changed: true,
      added: [],
      deleted: Object.entries(initial).map(([key, value]) => ({ key, value })),
      updated: []
   };
});

afterEach(() => {
   checkReactivesUsersState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

describe(`Values`, () => {
   it(`values`, () => {
      Evalues = modification;
      Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

      Ichange = record.send(modification);
   });

   it(`null`, () => {
      Ichange = record.send(null);
   });

   it(`SHOULD NOT be changed`, () => {
      Echange.changed = false;
      Echange.deleted = [];

      record.send({});
      Ichange = record.send({});
   });
});


describe(`Wrappers`, () => {
   it(`values`, () => {
      Evalues = modification;
      Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

      Ichange = record.sendWrappers(modificationWrappers);

      for (const key in record.wrappers) {
         expect(record.wrappers[key]).toBe(modificationWrappers[key]);
      }
   });

   it(`null`, () => {
      Ichange = record.sendWrappers(null);
   });

   it(`SHOULD NOT be changed`, () => {
      Echange.changed = false;
      Echange.deleted = [];

      record.sendWrappers({});
      Ichange = record.sendWrappers({});
   });
});
