import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactivesRecord } from '@taedr/reactive';
import { getRuntimeId } from '@taedr/utils';
import { checkReactivesUsersState } from '../state.check';
import { getInitialWrappers, getModificationWrappers, initial, modification, ReactivesUserRecord, ReactiveUser, User } from '../_model';


let record: ReactivesUserRecord;
let mirror: IReadonlyReactivesRecord<User>;
let IRchange: IReactivesRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: IReactiveRecordChange<User>;
let Evalues: ReactivesUserRecord['value'];
let initialWrappers: Record<string, ReactiveUser>;
let modificationWrappers: Record<string, ReactiveUser>;

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   if (record) record.send({});
   record = new ReactivesUserRecord(initialWrappers);
   mirror = record.suffix(new ReactivesUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {
      1: modification[4],
      2: initial[2],
      3: modification[5],
   };
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [
         { key: `1`, value: modification[4] },
         { key: `3`, value: modification[5] },
      ]
   };
});

afterEach(() => {
   checkReactivesUsersState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

describe(`Values`, () => {
   let calls: number[] = [];
   const id = getRuntimeId();
   const keys = [`1`, `3`];

   beforeEach(() => {
      record.watch(() => calls.push(1));

      for (const key of keys) {
         record.wrappers[key].watch({
            id,
            value: () => calls.push(2)
         });
      }

      calls = [];
   });

   afterEach(() => {
      keys.forEach(key => record.wrappers[key].watchers.detach(id, ``))
      expect(record.wrappers).toEqual(initialWrappers);
      expect(calls).toEqual([2, 2, 1]);
   });


   it(`Compare`, () => {
      Ichange = record.update((_, key) => {
         if (key === `1`) return modification['4'];
         if (key === `3`) return modification['5'];
      });

   });

   it(`Record`, () => {
      calls
      Ichange = record.update({
         1: modification[4],
         3: modification[5]
      });
   });

   it(`Entries`, () => {
      calls
      Ichange = record.update(Echange.updated);
   });
});

describe(`Wrappers`, () => {
   afterEach(() => {
      expect(record.wrappers[1]).toBe(modificationWrappers[4]);
      expect(record.wrappers[3]).toBe(modificationWrappers[5]);
   });

   it(`Compare`, () => {
      Ichange = record.updateWrappers((_, key) => {
         if (key === `1`) return modificationWrappers['4'];
         if (key === `3`) return modificationWrappers['5'];
      });
   });

   it(`Record`, () => {
      Ichange = record.updateWrappers({
         1: modificationWrappers[4],
         3: modificationWrappers[5]
      });
   });

   it(`Entries`, () => {
      Ichange = record.updateWrappers([
         { key: `1`, value: modificationWrappers[4] },
         { key: `3`, value: modificationWrappers[5] },
      ]);
   });
});
