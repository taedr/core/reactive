import { IReactiveChange, ReactiveObject } from '@taedr/reactive';
import { getInitialWrappers, ReactivesUserRecord } from '../_model';


it(`Flow`, () => {
   const user = getInitialWrappers()[1];
   const first = new ReactivesUserRecord({ 1: user });
   const second = new ReactivesUserRecord({ 1: user });
   const third = new ReactiveObject({
      users: new ReactivesUserRecord({ 1: user })
   });
   const Euser = { ...user.value };
   const Earray = { 1: Euser };


   const watcher = (_: any, { initial }: IReactiveChange) => {
      if (initial) return;
      expect(first.value).toEqual(Earray);
      expect(second.value).toEqual(Earray);
      expect(third.value.users).toEqual(Earray);
      expect(user.value).toEqual(Euser);
   };

   user.watch(watcher);
   first.watch(watcher);
   second.watch(watcher);
   third.watch(watcher);

   Euser.name = `Mariam`;
   user.fields.name.send(Euser.name);

   Euser.name = `Polya`;
   first.update([{ key: `1`, value: Euser }]);

   Euser.name = `Motrya`;
   second.update([{ key: `1`, value: Euser }]);

   Euser.name = `Emma`;
   third.fields.users.update([{ key: `1`, value: Euser }]);
});
