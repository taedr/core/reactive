import { IReactiveRecordChange, IReactivesRecordChange, IReadonlyReactivesRecord } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { ReactiveUser, initial, modification, getModificationWrappers, ReactivesUserRecord, User, getInitialWrappers } from '../_model';


let record: ReactivesUserRecord;
let mirror: IReadonlyReactivesRecord<User>;
let IRchange: IReactivesRecordChange<User>;
let IMchange: typeof IRchange;
let Ichange: typeof IRchange;
let Echange: IReactiveRecordChange<User>;
let Evalues: ReactivesUserRecord['value'];
let initialWrappers: Record<string, ReactiveUser>;
let modificationWrappers: Record<string, ReactiveUser>;

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   record = new ReactivesUserRecord(initialWrappers);
   mirror = record.suffix(new ReactivesUserRecord());
   record.watch((_, change) => IRchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalues = {};
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: []
   };
});

afterEach(() => {
   checkReactivesUsersState(record, mirror, IRchange, IMchange, Ichange, Echange, Evalues);
});

describe(`Values`, () => {
   it(`SHOULD add keys from proveded record`, () => {
      Evalues = { ...initial, ...modification };
      Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

      Ichange = record.add(modification);
   });

   it(`IF during add already existing key detected value under it SHOULD be marked as updated`, () => {
      const modification: Record<string, User> = {
         2: new User(`Prokofia`, 24),
         4: new User(`Lavrentii`, 56),
      };

      Echange.added = [{ key: `4`, value: modification[4] }];
      Echange.updated = [{ key: `2`, value: modification[2] }];
      Evalues = {
         '1': initial[1],
         '2': modification[2],
         '3': initial[3],
         '4': modification[4],
      };

      Ichange = record.add(modification);
   });


   it(`SHOULD NOT be changed`, () => {
      Echange.changed = false;
      Evalues = initial;

      Ichange = record.add({});
   });
});


describe(`Wrappers`, () => {
   it(`SHOULD add keys from proveded record`, () => {
      Evalues = { ...initial, ...modification };
      Echange.added = Object.entries(modification).map(([key, value]) => ({ key, value }));

      Ichange = record.addWrappers(modificationWrappers);

      for (const key in modification) {
         expect(record.wrappers[key]).toBe(modificationWrappers[key]);
      }
   });

   it(`IF during add already existing key detected value under it SHOULD be marked as updated`, () => {
      const modification: Record<string, ReactiveUser> = {
         2: new ReactiveUser(new User(`Prokofia`, 24)),
         4: new ReactiveUser(new User(`Lavrentii`, 56)),
      };

      Echange.added = [{ key: `4`, value: modification[4].value }];
      Echange.updated = [{ key: `2`, value: modification[2].value }];
      Evalues = {
         '1': initial[1],
         '2': modification[2].value,
         '3': initial[3],
         '4': modification[4].value,
      };

      Ichange = record.addWrappers(modification);

      for (const key in modification) {
         expect(record.wrappers[key]).toBe(modification[key]);
      }
   });


   it(`SHOULD NOT be changed`, () => {
      Echange.changed = false;
      Evalues = initial;

      Ichange = record.addWrappers({});
   });
});
