import { ERRORS, ReactiveNumber } from '@taedr/reactive';

const message = `WASTED`;

it(`SHould not add watcher if bus detached`, () => {
   const value = 1;
   const reactive = new ReactiveNumber(value);
   let Ivalue: number | undefined;
   let Imessage: string | undefined;

   const watcherId = reactive.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });
   const watchers = reactive.watchers;

   expect(watchers.size).toEqual(1);
   expect(watchers.has(watcherId)).toBeTruthy();

   reactive.watchers.detach(watcherId);
   reactive.send(4);

   expect(Ivalue).toEqual(value);
   expect(Imessage).toEqual(watcherId);
   expect(watchers.size).toEqual(0);
});

it(`SHould not add watcher if bus shutdowned`, () => {
   const value = 1;
   const reactive = new ReactiveNumber(value);
   let Ivalue: number | undefined;
   let Imessage: string | undefined;

   reactive.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });

   reactive.shutdown = message;

   expect(Ivalue).toEqual(value);
   expect(Imessage).toEqual(message);
   expect(reactive.watchers.size).toEqual(0);
   expect(() => reactive.send(4)).toThrowError(ERRORS.shutdown.change);

   let Ivalue_1: number | undefined;
   let Imessage_1: string | undefined;

   reactive.watch({
      value: value => Ivalue_1 = value,
      detach: message => Imessage_1 = message
   });

   expect(Ivalue_1).toEqual(value);
   expect(Imessage_1).toEqual(message);
   expect(reactive.watchers.size).toEqual(0);
});
