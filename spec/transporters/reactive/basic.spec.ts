import { Reactive, AReadonlyBus, Bus, ReactiveBigint, ReactiveBoolean, ReactiveNumber, ReactiveString, IReactiveChange } from '@taedr/reactive';

it(`Should notify watchers with current value and on every set, undefined should be converted to null`, () => {
   const initial = 0;
   const reactive = new ReactiveNumber(initial);
   let Ichange: IReactiveChange | undefined;
   let Ivalue: number | undefined;

   reactive.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   });

   const Evalue = initial;
   const Echange: IReactiveChange = {
      initial: true, changed: true
   };

   expect(reactive.value).toEqual(initial);
   expect(Ichange).toEqual(Echange);
   expect(Ivalue).toEqual(Evalue);
   expect(Object.isFrozen(Ichange)).toBeTruthy();

   for (const value of [1, null, 2, undefined, 3]) {
      reactive.send(value as any);

      expect(reactive.value).toEqual(value);
      expect(Object.isFrozen(Ichange)).toBeTruthy();
      expect(Ivalue).toEqual(value);
      expect(Ichange).toEqual({
         changed: true
      });
   }

   reactive.send(3);

   expect(Ichange).toEqual({
      changed: false
   });
   expect(Ivalue).toEqual(3);
});


it(`Should set null as initial value and notify watcher on watch`, () => {
   const reactive = new ReactiveNumber<number | null>(null);

   let Ivalue: number | null | undefined;

   reactive.watch((value) => {
      Ivalue = value;
   });

   expect(reactive.value).toBeNull();
   expect(Ivalue).toBeNull();

   const first = 1;
   reactive.send(first);

   expect(reactive.value).toEqual(first);
   expect(Ivalue).toEqual(first);

   reactive.send(null);

   expect(reactive.value).toBeNull();
   expect(Ivalue).toBeNull();
});


it(`Instance`, () => {
   const number = new ReactiveNumber(0);
   const string = new ReactiveString(``);
   const boolean = new ReactiveBoolean(true);
   const bigint = new ReactiveBigint(0n);

   expect(number).toBeInstanceOf(AReadonlyBus);
   expect(number).toBeInstanceOf(Bus);
   expect(number).toBeInstanceOf(Reactive);
   expect(string).toBeInstanceOf(Reactive);
   expect(boolean).toBeInstanceOf(Reactive);
   expect(bigint).toBeInstanceOf(Reactive);
});
