import { IIndexUpdate, IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray } from '@taedr/reactive';
import { checkReactivesArrayState } from '../state.check';
import { getInitialWrappers, getModificationWrappers, initial, modification, ReactivesUserArray, ReactiveUser, User } from '../_model';

let array: ReactivesUserArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;
let Ichange: typeof IAchange;
let Echange: IReactiveArrayChange<User>;
let initialWrappers: ReactiveUser[];
let modificationWrappers: ReactiveUser[];


let Evalue: readonly User[];
const valuesUpdate = [
   { index: 0, value: modification[1] },
   { index: 2, value: modification[0] },
];

let wrappersUpdate: IIndexUpdate<ReactiveUser>[];

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   wrappersUpdate = [
      { index: 0, value: modificationWrappers[1] },
      { index: 2, value: modificationWrappers[0] },
   ];

   array = new ReactivesUserArray(initialWrappers);
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Evalue = [modification[1], initial[1], modification[0]];
   Echange = {

      changed: true,
      added: [],
      deleted: [],
      updated: valuesUpdate,
      moved: []
   };
});

afterEach(() => {
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});


describe(`Values`, () => {
   let calls: number[] = [];

   beforeEach(() => {
      array.watch(() => calls.push(1));

      for (const index of [0, 2]) {
         array.wrappers[index].watch(() => calls.push(2));
      }

      calls = [];
   });

   afterEach(() => {
      expect(array.wrappers).toEqual(initialWrappers);
      expect(calls).toEqual([2, 2, 1]);
   });

   it(`Update Indexes`, async () => {
      Ichange = array.update(...valuesUpdate);
   });

   it(`Update Compare`, async () => {
      Ichange = array.update((_, i) => {
         if (i === 0) return valuesUpdate[0].value;
         if (i === 2) return valuesUpdate[1].value;
      });

   });

});

describe(`Wrappers`, () => {
   afterEach(() => {
      expect(array.wrappers).toEqual([
         modificationWrappers[1],
         initialWrappers[1],
         modificationWrappers[0]
      ]);
   })

   it(`Update Indexes`, async () => {
      Ichange = array.updateWrappers(...wrappersUpdate);

      expect(array.wrappers)
   });

   it(`Update Compare`, async () => {
      Ichange = array.updateWrappers((_, i) => {
         if (i === 0) return wrappersUpdate[0].value;
         if (i === 2) return wrappersUpdate[1].value;
      });
   });
});

it(`Update Enrty`, async () => {
   const [first, _, third] = array.wrappers;
   let calls: number[] = [];

   array.watch(() => calls.push(1));

   for (const index of [0, 2]) {
      array.wrappers[index].watch(() => calls.push(2));
   }

   expect(calls).toEqual([1, 2, 2]);
   calls = [];
   first.fields.age.send(12);
   third.fields.age.send(22);

   expect(calls).toEqual([2, 1, 2, 1]);

   array.send([]);
   array.send([]);
   Echange.updated = [];
   Evalue = [];
});
