import { AReactiveObject, AReactivesArray, ReactiveString } from '@taedr/reactive';

class User {
   constructor(
      public name: string
   ) { }
}

class ReactiveUser extends AReactiveObject<User, null> {
   constructor(
      user: User | null
   ) {
      super({
         null: user === null,
         fields: {
            name: new ReactiveString(user?.name ?? ``)
         }
      });
   }

   protected createInstance(): User & object {
      return new (User as any)();
   }
}

class ReactiveUserArray extends AReactivesArray<User | null, ReactiveUser> {
   protected createWrapper(value: User): ReactiveUser {
      return new ReactiveUser(value);
   }
}


it(`Wrappers`, () => {
   const initial = [
      new ReactiveUser(new User(`A`)),
      new ReactiveUser(null),
      new ReactiveUser(new User(`B`)),
   ];
   const users = new ReactiveUserArray(initial);
   const Evalue = initial.map(({ value }) => value);

   expect(users.value).toEqual(Evalue);

   initial[0].send(null);

   expect(users.value).toEqual([null, null, { name: `B` }]);
});
