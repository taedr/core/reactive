import { IIndexDeleted, IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, ReactiveNumber, ReactivesNumberArray } from '@taedr/reactive';
import { checkReactivesArrayState } from '../state.check';

let array: ReactivesNumberArray;
let mirror: IReadonlyReactivesArray<number>;
let IAchange: IReactivesArrayChange<number>;
let IMchange: IReactivesArrayChange<number>;
let Ichange: typeof IAchange;

const initial = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
let Evalue: readonly number[];
let toDeleteValues: number[];
let toDeleteWrappers: ReactiveNumber[];
const Edeleted: IIndexDeleted<number>[] = [
   { index: 9, count: 1, values: [9] },
   { index: 6, count: 2, values: [6, 7] },
   { index: 3, count: 2, values: [3, 4] },
   { index: 1, count: 1, values: [1] },
];
let Echange: IReactiveArrayChange<number>

beforeEach(() => {
   array = new ReactivesNumberArray(initial);
   mirror = array.suffix(new ReactivesNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Evalue = [0, 2, 5, 8];
   Echange = {

      changed: true,
      added: [],
      deleted: Edeleted,
      updated: [],
      moved: []
   };

   toDeleteValues = array.value.filter(v => !Evalue.includes(v));
   toDeleteWrappers = array.wrappers.filter(({ value }) => !Evalue.includes(value));
});

afterEach(() => {
   expect(Ichange).toBe(IAchange);
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`Values`, () => {
   it(`SHOULD delete entry if compare function returns 'true'`, async () => {
      Ichange = array.delete(value => toDeleteValues.includes(value));
   });

   it(`Delete Indexes`, async () => {
      Ichange = array.delete(...Edeleted);
   });

   it(`Delete Values`, async () => {
      Ichange = array.delete(toDeleteValues);
   });
});

describe(`Wrappers`, () => {
   it(`SHOULD delete entry if compare function returns 'true'`, async () => {
      Ichange = array.deleteWrappers(value => toDeleteWrappers.includes(value));
   });

   it(`Delete Values`, async () => {
      Ichange = array.deleteWrappers(toDeleteWrappers);
   });
});
