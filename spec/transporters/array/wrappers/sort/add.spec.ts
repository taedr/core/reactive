import { IIndexAdd, IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactiveArray, IReadonlyReactivesArray, IValue, ReactiveNumber, ReactiveNumberArray, ReactivesNumberArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../../state.check';


let array: ReactivesNumberArray;
let mirror: IReadonlyReactivesArray<number>;
let IAchange: IReactivesArrayChange<number>;
let IMchange: IReactivesArrayChange<number>;
let Echange: IReactiveArrayChange<number>;

const initial = [1, 4, 5, 6, 9];
const sorter = (_, __, a: ReactiveNumber, b: ReactiveNumber) => b.value - a.value;
let Evalue: readonly number[];

beforeEach(() => {
   array = new ReactivesNumberArray(initial, { sorter });
   mirror = array.suffix(new ReactivesNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalue = [];
   Echange = {

      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});


it(`SHOULD add values according to sort and determine add indexes after sort`, async () => {
   Evalue = [9, 8, 7, 6, 5, 4, 3, 2, 1];
   Echange.added = [
      { index: 1, values: [8, 7] },
      { index: 6, values: [3, 2] }
   ];

   array.add([8, 3, 7, 2]);
   IAchange
});

it(`SHOULD ignore indexes and reassemble packs depending on insert by sort order`, async () => {
   const toAdd: IIndexAdd<number>[] = [
      { index: 0, values: [2, 12] },
      { index: 1, values: [3, 10, 7] },
      { index: -1, values: [11, 8] },
   ];

   Evalue = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
   Echange.added = [
      { index: 0, values: [12, 11, 10] },
      { index: 4, values: [8, 7] },
      { index: 9, values: [3, 2] }
   ];

   array.add(...toAdd);
});
