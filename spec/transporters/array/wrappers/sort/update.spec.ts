import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, TReactiveWrapperArraySorter } from '@taedr/reactive';
import { checkReactiveArrayState } from '../../state.check';
import { ReactivesUserArray, ReactiveUser, User } from '../../_model';

let array: ReactivesUserArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;
let Echange: IReactiveArrayChange<User>;

const initial = [
   new User(`0`, `A`, 0),
   new User(`1`, `B`, 3),
   new User(`2`, `C`, 5),
   new User(`3`, `D`, 7),
   new User(`4`, `E`, 9),
];

const sorter: TReactiveWrapperArraySorter<User, ReactiveUser> = (_, __, a, b) => a.value.age - b.value.age;

const modification = [
   new User(`4`, `E`, 8),
   new User(`1`, `B`, 10),
   new User(`0`, `D`, 9),
];

let Evalue = [
   initial[2],
   initial[3],
   modification[0],
   modification[2],
   modification[1],
];

beforeEach(() => {
   array = new ReactivesUserArray(initial, { sorter });
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Echange = {

      changed: true,
      added: [],
      deleted: [],
      updated: [
         { index: 0, value: modification[2] },
         { index: 1, value: modification[1] },
         { index: 4, value: modification[0] }
      ],
      moved: [
         { from: 0, to: 3 },
         { from: 0, to: 4 },
         { from: 3, to: 2 }
      ]
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`IF update changes value weight it should be moved according to sort`, () => {
   it(`function`, () => {
      array.update((value) => {
         return modification.find(v => v.id === value.id);
      });
   });

   it(`entries`, () => {
      array.update(...Echange.updated);
   });

   it(`wrapper`, () => {
      const [first, second, third] = modification.map(m => {
         return array.getWrapper(v => m.id === v.value.id) as ReactiveUser
      });

      first.send(modification[0]);

      expect(IAchange.updated).toEqual([
         { index: 4, value: modification[0], wrapper: array.wrappers[4] }
      ]);
      expect(IAchange.moved).toEqual([]);

      second.send(modification[1]);

      expect(IAchange.updated).toEqual([
         { index: 1, value: modification[1], wrapper: array.wrappers[4] }
      ]);
      expect(IAchange.moved).toEqual([{ from: 1, to: 4 }]);

      third.send(modification[2]);

      Echange.updated = [{ index: 0, value: modification[2] }];
      Echange.moved = [{from: 0, to: 3}];
   });

   it(`SHOULD NOT change`, () => {
      array.wrappers[0].send(array.value[0]);

      Echange.moved = [];
      Echange.updated = [];
      Echange.changed = false;
      Evalue = initial;
   });
});
