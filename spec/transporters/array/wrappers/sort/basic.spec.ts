import { ERRORS, IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, IValue, ReactiveNumber, ReactivesNumberArray } from '@taedr/reactive';
import { checkReactivesArrayState } from '../../state.check';


let array: ReactivesNumberArray;
let mirror: IReadonlyReactivesArray<number>;
let IAchange: IReactivesArrayChange<number>;
let IMchange: IReactivesArrayChange<number>;
let Echange: IReactiveArrayChange<number>;

const initial = [1, 2, 3, 4, 5];
const sorter = (_, __, a: ReactiveNumber, b: ReactiveNumber) => b.value - a.value;
const Einitial = [5, 4, 3, 2, 1];
let Evalue: readonly number[];

beforeEach(() => {
   array = new ReactivesNumberArray(initial, { sorter });
   mirror = array.suffix(new ReactivesNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalue = Einitial;
   Echange = {

      changed: true,
      added: [{ index: 0, values: Einitial }],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`SHOULD sort existing values`, () => {
   const values = [5, 9, 0, 3, 2, 6, 1, 4, 8, 7];

   Echange.added = [];
   Evalue = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
   Echange.moved = [
      { from: 1, to: 0, swap: true },
      { from: 8, to: 1, swap: true },
      { from: 9, to: 2, swap: true },
      { from: 5, to: 3, swap: true },
      { from: 8, to: 4, swap: true },
      { from: 7, to: 5, swap: true },
      { from: 7, to: 6, swap: true },
      { from: 8, to: 7, swap: true }
   ];

   array.sorter = undefined;
   array.send(values);
   array.sorter = sorter;
});

it(`SHOULD sort initial values`, () => {
   Echange.initial = true;
   Echange.changed = false;
});

it(`Sort Replace`, async () => {
   array.send([]);
   array.send(initial);
});


it(`SHOULD not allow to move while sort specified`, () => {
   Echange.changed = false;
   Echange.added = [];

   array.sorter = sorter;

   expect(() => array.move(2)).toThrowError(ERRORS.moveSort);

   array.sorter = undefined;

   expect(() => array.move()).not.toThrowError(ERRORS.moveSort);
});
