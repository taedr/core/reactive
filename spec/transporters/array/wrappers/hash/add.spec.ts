import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray } from "@taedr/reactive";
import { checkReactivesArrayState } from "../../state.check";
import { initial, ReactivesUserArray, ReactivesUserIdArray, User } from "../../_model";

let array: ReactivesUserIdArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;

const addValues = [
   new User(`1`, `Vladlen`, 27),
   new User(`3`, `Motrya`, 19)
];
let Evalue = [initial[0], addValues[0], initial[2], addValues[1]];
const Echange: IReactiveArrayChange<User> = {

   changed: true,
   added: [{ index: 3, values: [addValues[1]] }],
   deleted: [],
   updated: [{ index: 1, value: addValues[0] }],
   moved: []
};

beforeEach(() => {
   array = new ReactivesUserIdArray(initial);
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`If during add some pack contains value which hash already present should
NOT add it, but update existing value instead`, () => {
   it(`Pack`, () => {
      array.add(addValues);
   });

   it(`Indexes`, () => {
      array.add({ values: addValues });
   });
});
