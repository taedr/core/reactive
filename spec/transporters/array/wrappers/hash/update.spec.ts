import { ERRORS, IReactiveArrayChange, IReadonlyReactivesArray } from "@taedr/reactive";
import { checkReactiveArrayState } from "../../state.check";
import { getInitialWrappers, initial, ReactivesUserArray, ReactivesUserIdArray, User } from "../../_model";

let array: ReactivesUserIdArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;

const vladlen = new User(`1`, `Vladlen`, 27);
const Evalue = [initial[0], vladlen, initial[2]];
const Echange: typeof IAchange = {

   changed: true,
   added: [],
   deleted: [],
   updated: [{ index: 1, value: vladlen }],
   moved: []
};

beforeEach(() => {
   array = new ReactivesUserIdArray(getInitialWrappers());
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`If trying to update with value which hash already present in array
should redirect this update to index of those existing hash`, () => {
   it(`Index`, () => {
      array.update({ index: 0, value: vladlen });
   });

   it(`Compare`, () => {
      array.update((_, index) => {
         if (index === 0) return vladlen;
      });
   });

   it(`Values`, () => {
      array.update([vladlen]);
   });

});
