import { ERRORS } from "@taedr/reactive";
import { initial, modification, ReactivesUserIdArray, User } from "../../_model";

let array: ReactivesUserIdArray;

beforeEach(() => {
   array = new ReactivesUserIdArray(initial);
});

it(`has`, () => {
   expect(array.has(initial[0].id)).toBeTruthy();
   expect(array.has(modification[0].id)).toBeFalsy();
});

it(`get`, () => {
   expect(array.get(initial[0].id)).toEqual(0);
   expect(array.get(modification[0].id)).toEqual(undefined);
});


it(`send`, () => {
   const newValues = [
      new User(`0`, `A`, 1),
      new User(`1`, `B`, 2),
      new User(`0`, `C`, 3),
      new User(`3`, `D`, 4),
      new User(`1`, `E`, 5),
   ];

   const Evalues = [
      newValues[0],
      newValues[1],
      newValues[3],
   ];

   array.send(newValues);

   expect(array.value).toEqual(Evalues);
});

it(`SHOULD throw an error if value with same hash already present in the array`, () => {
   expect(() => array.wrappers[0].fields.id.send(`1`)).toThrowError(ERRORS.sameHash(0));
});
