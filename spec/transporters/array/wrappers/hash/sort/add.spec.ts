import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, TReactiveWrapperArraySorter } from "@taedr/reactive";
import { checkReactivesArrayState } from "../../../state.check";
import { ReactivesUserArray, ReactivesUserIdArray, ReactiveUser, User } from "../../../_model";

let array: ReactivesUserIdArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;

const sorter: TReactiveWrapperArraySorter<User, ReactiveUser> =
   (_, __, a, b) => a.value.age - b.value.age;
const initial = [
   new User(`0`, `A`, 0),
   new User(`1`, `B`, 1),
   new User(`2`, `C`, 2),
   new User(`7`, `E`, 7),
];
const modification = [
   new User(`4`, `D`, 4),
   new User(`8`, `I`, 8),
   new User(`1`, `B`, 6),
   new User(`0`, `A`, 3),
   new User(`5`, `F`, 5),
];

const Evalue = [
   initial[2],
   modification[3],
   modification[0],
   modification[4],
   modification[2],
   initial[3],
   modification[1],
];

const Echange: IReactiveArrayChange<User> = {

   changed: true,
   deleted: [],
   added: [
      { index: 3, values: [modification[0], modification[4]] },
      { index: 6, values: [modification[1]] },
   ],
   updated: [
      { index: 1, value: modification[2] },
      { index: 0, value: modification[3] }
   ],
   moved: [
      { from: 1, to: 4 },
      { from: 0, to: 1 }
   ]
};

beforeEach(() => {
   array = new ReactivesUserIdArray(initial, { sorter });
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`Pack`, () => {
   array.add(modification);
});


it(`Indexes`, () => {
   array.add({ values: modification });
});
