import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, TReactiveWrapperArraySorter } from "@taedr/reactive";
import { checkReactivesArrayState } from "../../../state.check";
import { initial, ReactivesUserArray, ReactivesUserIdArray, ReactiveUser, User } from "../../../_model";

let array: ReactivesUserIdArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;

const sorter: TReactiveWrapperArraySorter<User, ReactiveUser> =
   (_, __, a, b) => b.value.age - a.value.age;
const vladlen = new User(`2`, `Vladlen`, 7);
const Evalue = [initial[1], initial[0], vladlen];
const Echange: IReactiveArrayChange<User> = {

   changed: true,
   added: [],
   deleted: [],
   updated: [
      { index: 1, value: vladlen }
   ],
   moved: [
      { from: 1, to: 2 },
   ]
};

beforeEach(() => {
   array = new ReactivesUserIdArray(initial, { sorter });
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(``, () => {
   it(`Index`, () => {
      array.update({ index: 0, value: vladlen });
   });

   it(`Compare`, () => {
      array.update((_, index) => {
         if (index === 0) return vladlen;
      });
   });

   it(`Values`, () => {
      array.update([vladlen]);
   });
});
