import { IReactiveArrayChange, IReadonlyReactiveArray, ReactiveNumberArray, ReactiveObjectArray } from "@taedr/reactive";
import { FN_TO_STRING } from "@taedr/utils";
import { checkReactiveArrayState } from "../../state.check";

let array: ReactiveNumberArray;
let mirror: IReadonlyReactiveArray<number>;
let IAchange: IReactiveArrayChange<number>;
let IMchange: IReactiveArrayChange<number>;
let Echange: IReactiveArrayChange<number>;

const initial = [1, 1, 2, 2, 3, 3, 4];
const EinitialValue = [1, 2, 3, 4];
let Evalue: readonly number[];

beforeEach(() => {
   array = new ReactiveNumberArray(initial, { getHash: FN_TO_STRING });
   mirror = array.suffix(new ReactiveNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   Evalue = [];
   Echange = {

      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`Initial`, () => {
   Echange.initial = true;
   Echange.changed = false;
   Evalue = EinitialValue;
   Echange.added = [
      { index: 0, values: EinitialValue }
   ];
});

it(`Array`, () => {
   const toAdd = [5, 6];

   Evalue = [...EinitialValue, ...toAdd];
   Echange.added = [{ index: array.size, values: toAdd }];

   array.add([5, 2, 1, 6]);
});

it(`Entries`, () => {
   Evalue = [1, 2, 7, 3, 4, 8];
   Echange.added = [
      { index: 2, values: [7] },
      { index: EinitialValue.length + 1, values: [8] }
   ];

   array.add({
      index: 2,
      values: [7, 1]
   }, {
      values: [3, 8]
   });
});
