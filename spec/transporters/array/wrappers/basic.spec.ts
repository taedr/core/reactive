import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray, ReactiveNumber, ReactivesNumberArray } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { initial, getInitialWrappers, ReactivesUserArray, ReactiveUser, User } from '../_model';

let array: ReactivesUserArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;
let initialWrappers: ReactiveUser[];

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   array = new ReactivesUserArray(initialWrappers);
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
});

it(`Initial`, async () => {
   const Echange: IReactiveArrayChange<User> = {
      initial: true,
      changed: false,
      added: [{ index: 0, values: initial }],
      deleted: [],
      updated: [],
      moved: []
   };

   checkReactivesUsersState(array, mirror, IAchange, IMchange, Echange, initial);
});

it(`Should create entries for initial values`, async () => {
   expect(array.value).toEqual(initial);

   for (let i = 0; i < array.size; i++) {
      const wrapper = array.wrappers[i];
      const value = array.value[i];
      expect(wrapper.value).toBe(value);
   }
});

it(`Get`, async () => {
   for (let i = 0; i < initialWrappers.length; i++) {
      const wrapper = array.getWrapper(i);
      expect(wrapper).toEqual(initialWrappers[i]);
   }

   for (let i = initialWrappers.length - 1; i >= 0; i--) {
      const value = array.getWrapper(-i);
      expect(value).toEqual(initialWrappers.slice(-i)[0]);
   }

   expect(array.get(11)).toBeUndefined();

   expect(array.getWrapper(v => v.value.id === `0`)).toEqual(initialWrappers[0]);
   expect(array.getWrapper(v => v.value.id === `9`)).toEqual(undefined);
});

it(`Has`, async () => {
   const _user = new ReactiveUser(new User(``, ``, 22));

   for (let i = 0; i < initialWrappers.length; i++) {
      const has = array.hasWrapper((value) => value === initialWrappers[i]);
      expect(has).toBeTruthy();
   }

   expect(array.hasWrapper(array.wrappers[0])).toBeTruthy();
   expect(array.hasWrapper((value) => value === _user)).toBeFalsy();
});


it(`Sort replace`, async () => {
   const array = new ReactivesNumberArray([1, 2, 3]);
   const mirror = array.suffix(new ReactivesNumberArray());
   const Evalues = [3, 2, 1];
   const Evalues_2 = [7, 6, 5];

   array.sorter = (a, b) => b - a;

   expect(array.value).toEqual(Evalues);
   expect(mirror.value).toEqual(Evalues);

   array.sendWrappers([5, 6, 7].map(v => new ReactiveNumber(v)));
   expect(array.value).toEqual(Evalues_2);
   expect(mirror.value).toEqual(Evalues_2);
});
