import { IReactiveChange, ReactiveObject } from '@taedr/reactive';
import { getInitialWrappers, ReactivesUserArray } from '../_model';


it(`Flow`, () => {
   const [user] = getInitialWrappers();
   const first = new ReactivesUserArray([user]);
   const second = new ReactivesUserArray([user]);
   const third = new ReactiveObject({
      users: new ReactivesUserArray([user])
   });
   const Euser = { ...user.value };
   const Earray = [Euser];

   const watcher = (_: any, { initial }: IReactiveChange) => {
      if (initial) return;
      expect(first.value).toEqual(Earray);
      expect(second.value).toEqual(Earray);
      expect(third.value.users).toEqual(Earray);
      expect(user.value).toEqual(Euser);
   };

   user.watch(watcher);
   first.watch(watcher);
   second.watch(watcher);

   Euser.name = `Mariam`;
   user.fields.name.send(Euser.name);

   Euser.name = `Polya`;
   first.update({ index: 0, value: Euser });

   Euser.name = `Motrya`;
   second.update({ index: 0, value: Euser });

   Euser.name = `Emma`;
   third.fields.users.update({ index: 0, value: Euser });
});
