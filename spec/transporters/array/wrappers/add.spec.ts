import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactivesArray } from '@taedr/reactive';
import { checkReactivesUsersState } from '../state.check';
import { getInitialWrappers, getModificationWrappers, initial, modification, ReactivesUserArray, ReactiveUser, User } from '../_model';

let array: ReactivesUserArray;
let mirror: IReadonlyReactivesArray<User>;
let IAchange: IReactivesArrayChange<User>;
let IMchange: IReactivesArrayChange<User>;
let Echange: IReactiveArrayChange<User>;
let Ichange: typeof IAchange;
let initialWrappers: ReactiveUser[];
let modificationWrappers: ReactiveUser[];
let Evalue = initial.concat(modification);

beforeEach(() => {
   initialWrappers = getInitialWrappers();
   modificationWrappers = getModificationWrappers();
   array = new ReactivesUserArray(initialWrappers);
   mirror = array.suffix(new ReactivesUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Echange = {

      changed: true,
      added: [{ index: array.size, values: modification }],
      deleted: [],
      updated: [],
      moved: [],
   };
});

afterEach(() => {
   expect(Ichange).toEqual(IAchange);
   checkReactivesUsersState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`Values`, () => {
   it(`IF adding array of values without index specification SHOULD add to the end `, () => {
      Ichange = array.add(modification);
   });

   it(`IF packs follow each other without gaps they SHOULD be merged into one`, () => {
      Ichange = array.add({
         values: [modification[0]]
      }, {
         values: [modification[1]]
      });
   });
});

describe(`Wrappers`, () => {
   it(`IF adding array of values without index specification SHOULD add to the end `, () => {
      Ichange = array.addWrappers(modificationWrappers);
   });

   it(`IF packs follow each other without gaps they SHOULD be merged into one`, () => {
      Ichange = array.addWrappers({
         values: [modificationWrappers[0]]
      }, {
         values: [modificationWrappers[1]]
      });
   });
});
