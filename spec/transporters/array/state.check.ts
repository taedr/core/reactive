import { IReactiveArrayChange, IReactivesArrayChange, IReadonlyReactiveArray, IReadonlyReactivesArray } from "@taedr/reactive";
import { ReactiveUser, User } from "./_model";

export function checkReactivesUsersState(
   array: IReadonlyReactivesArray<User>,
   mirror: IReadonlyReactivesArray<User>,
   IAchange: IReactivesArrayChange<User>,
   IMchange: IReactivesArrayChange<User>,
   Echange: IReactiveArrayChange<User>,
   Evalue: readonly User[],
) {
   for (let i = 0; i < array.size; i++) {
      const wrapper = array.wrappers[i];
      const value = array.value[i];
      expect(value).toBeInstanceOf(User);
      expect(wrapper).toBeInstanceOf(ReactiveUser);
      expect(wrapper).not.toBe(mirror.wrappers[i]);

   }

   checkReactivesArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
}

export function checkReactivesArrayState(
   array: IReadonlyReactivesArray<unknown>,
   mirror: IReadonlyReactivesArray<unknown>,
   IAchange: IReactivesArrayChange<unknown>,
   IMchange: IReactivesArrayChange<unknown>,
   Echange: IReactiveArrayChange<unknown>,
   Evalue: readonly unknown[],
) {
   for (let i = 0; i < array.size; i++) {
      const wrapper = array.wrappers[i];
      const value = array.value[i];
      expect(wrapper.value).toBe(value);
      expect(wrapper.watchers.has(array['_id'])).toBeTruthy();
   }

   for (const { wrappers } of IAchange.deleted) {
      for (const wrapper of wrappers) {
         expect(wrapper.watchers.size).toEqual(0);
      }
   }

   expect(Object.isFrozen(array.wrappers)).toBeTruthy();

   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
}

export function checkReactiveArrayState(
   array: IReadonlyReactiveArray<unknown>,
   mirror: IReadonlyReactiveArray<unknown>,
   IAchange: IReactiveArrayChange<unknown>,
   IMchange: IReactiveArrayChange<unknown>,
   Echange: IReactiveArrayChange<unknown>,
   Evalue: readonly unknown[],
) {
   expect(array.value).toEqual(Evalue);
   expect(mirror.value).toEqual(array.value);

   for (const array of [
      IAchange.added,
      IAchange.deleted,
      IAchange.updated,
      IAchange.moved,
   ]) {
      expect(Object.isFrozen(array)).toBeTruthy();

      for (const value of array) {
         expect(Object.isFrozen(value)).toBeTruthy();
      }
   }

   expect(Echange.moved).toEqual(IAchange.moved);
   expect(IAchange.moved).toEqual(IMchange.moved);

   expect(Echange.added.length).toEqual(IAchange.added.length);
   expect(IMchange.added.length).toEqual(IAchange.added.length);

   for (let i = 0; i < Echange.added.length; i++) {
      const Eadded = Echange.added[i];
      const IAadded = IAchange.added[i];
      const IMadded = IAchange.added[i];

      expect(Eadded.index).toEqual(IAadded.index);
      expect(Eadded.values).toEqual(IAadded.values);
      expect(IAadded.index).toEqual(IMadded.index);
      expect(IAadded.values).toEqual(IMadded.values);
   }

   expect(Echange.deleted.length).toEqual(IAchange.deleted.length);
   expect(IMchange.deleted.length).toEqual(IAchange.deleted.length);

   for (let i = 0; i < Echange.deleted.length; i++) {
      const Eadded = Echange.deleted[i];
      const IAadded = IAchange.deleted[i];
      const IMadded = IAchange.deleted[i];

      expect(Eadded.index).toEqual(IAadded.index);
      expect(Eadded.values).toEqual(IAadded.values);
      expect(Eadded.count).toEqual(IAadded.count);
      expect(IAadded.index).toEqual(IMadded.index);
      expect(IAadded.values).toEqual(IMadded.values);
      expect(IAadded.count).toEqual(IMadded.count);
   }

   expect(Echange.updated.length).toEqual(IAchange.updated.length);
   expect(IMchange.updated.length).toEqual(IAchange.updated.length);

   for (let i = 0; i < Echange.updated.length; i++) {
      const Eadded = Echange.updated[i];
      const IAadded = IAchange.updated[i];
      const IMadded = IAchange.updated[i];

      expect(Eadded.index).toEqual(IAadded.index);
      expect(Eadded.value).toEqual(IAadded.value);
      expect(IAadded.index).toEqual(IMadded.index);
      expect(IAadded.value).toEqual(IMadded.value);
   }

}
