import { ERRORS, ReactiveNumberArray } from '@taedr/reactive';

const message = `WASTED`;

it(`Shutdown`, () => {
   const value = [1, 2];
   const array = new ReactiveNumberArray(value);
   let Ivalue: readonly number[] = [];
   let Imessage = ``;

   array.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });

   array.shutdown = message;

   expect(() => array.send([])).toThrowError(ERRORS.shutdown.change);
   expect(() => array.add([])).toThrowError(ERRORS.shutdown.change);
   expect(() => array.delete([])).toThrowError(ERRORS.shutdown.change);
   expect(() => array.move({ from: 0, to: 1 })).toThrowError(ERRORS.shutdown.change);
   expect(() => array.update(() => 1)).toThrowError(ERRORS.shutdown.change);

   expect(array.shutdown).toEqual(message);
   expect(array.watchers.size).toEqual(0);
   expect(Imessage).toEqual(message);
   expect(Ivalue).toEqual(value);

   let Ivalue_1: readonly number[] = [];
   let Imessage_1 = ``;

   array.watch({
      value: value => Ivalue_1 = value,
      detach: message => Imessage_1 = message
   });

   expect(Imessage_1).toEqual(message);
   expect(Ivalue_1).toEqual(value);
});
