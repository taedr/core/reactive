import { IReactiveArrayChange, IReadonlyReactiveArray, TReactiveArraySorter } from "@taedr/reactive";
import { checkReactiveArrayState } from "../../../state.check";
import { initial, ReactiveUserArray, ReactiveUserIdArray, User } from "../../../_model";

let array: ReactiveUserIdArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;

const sorter: TReactiveArraySorter<User> = (a, b) => b.age - a.age;
const vladlen = new User(`2`, `Vladlen`, 7);
const Evalue = [initial[1], initial[0], vladlen];
const Echange: typeof IAchange = {

   changed: true,
   added: [],
   deleted: [],
   updated: [
      { index: 1, value: vladlen }
   ],
   moved: [
      { from: 1, to: 2 },
   ]
};

beforeEach(() => {
   array = new ReactiveUserIdArray(initial, { sorter });
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(``, () => {
   it(`Index`, () => {
      array.update({ index: 0, value: vladlen });
   });

   it(`Compare`, () => {
      array.update((_, index) => {
         if (index === 0) return vladlen;
      });
   });

   it(`Values`, () => {
      array.update([vladlen]);
   });
});
