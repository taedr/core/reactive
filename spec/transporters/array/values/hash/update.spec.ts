import { IReactiveArrayChange, IReadonlyReactiveArray } from "@taedr/reactive";
import { checkReactiveArrayState } from "../../state.check";
import { initial, ReactiveUserArray, ReactiveUserIdArray, User } from "../../_model";

let array: ReactiveUserIdArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;

const vladlen = new User(`1`, `Vladlen`, 27);
const Echange: typeof IAchange = {
   changed: true,
   added: [],
   deleted: [],
   updated: [{ index: 1, value: vladlen }],
   moved: []
};
let Evalue = [initial[0], vladlen, initial[2]];

beforeEach(() => {
   array = new ReactiveUserIdArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`If trying to update with value which hash already present in array
should redirect this update to index of those existing hash`, () => {
   it(`Index`, () => {
      array.update({ index: 0, value: vladlen });
   });

   it(`Compare`, () => {
      array.update((_, index) => {
         if (index === 0) return vladlen;
      });
   });

   it(`Values`, () => {
      array.update(([vladlen]));
   });
});

/*
During update check if hash already present and redirect update there
Handle sort and hash
send modification overload for array
Process change in one pack

*/
