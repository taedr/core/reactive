import { IReactiveArrayChange, IReadonlyReactiveArray } from "@taedr/reactive";
import { checkReactiveArrayState } from "../../state.check";
import { initial, ReactiveUserArray, ReactiveUserIdArray, User } from "../../_model";

let array: ReactiveUserIdArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;

const addValues = [
   new User(`1`, `Vladlen`, 27),
   new User(`3`, `Motrya`, 19)
];
const Echange: typeof IAchange = {
   changed: true,
   added: [{ index: 3, values: [addValues[1]] }],
   deleted: [],
   updated: [{ index: 1, value: addValues[0] }],
   moved: []
};

let Evalue = [initial[0], addValues[0], initial[2], addValues[1]];

beforeEach(() => {
   array = new ReactiveUserIdArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IAchange = undefined as any;
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`If during add some pack contains value which hash already present should
NOT add it, but update existing value instead`, () => {
   it(`Pack`, () => {
      array.add(addValues);
   });

   it(`Indexes`, () => {
      array.add({ values: addValues });
   });
});
