import { initial, modification, ReactiveUserIdArray, User } from "../../_model";

let array: ReactiveUserIdArray;

beforeEach(() => {
   array = new ReactiveUserIdArray(initial);
});

it(`has`, () => {
   expect(array.has(initial[0].id)).toBeTruthy();
   expect(array.has(modification[0].id)).toBeFalsy();
});

it(`get`, () => {
   expect(array.get(initial[0].id)).toEqual(0);
   expect(array.get(modification[0].id)).toEqual(undefined);
});


it(`send`, () => {
   const newValues = [
      new User(`0`, `A`, 1),
      new User(`1`, `B`, 2),
      new User(`0`, `C`, 3),
      new User(`3`, `D`, 4),
      new User(`1`, `E`, 5),
   ];

   const Evalues = [
      newValues[0],
      newValues[1],
      newValues[3],
   ];

   array.send(newValues);

   expect(array.value).toEqual(Evalues);
});
