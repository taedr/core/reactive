import { IIndexAdded, IReactiveArrayChange, IReadonlyReactiveArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';
import { initial, modification, ReactiveUserArray, User } from '../_model';

let array: ReactiveUserArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;
let Ichange: IReactiveArrayChange<User>;
let Echange: IReactiveArrayChange<User>;
let Evalue: readonly User[];

const spliceIndex = (index: number) => [...initial.slice(0, index), ...modification, ...initial.slice(index)];


beforeEach(() => {
   array = new ReactiveUserArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   expect(Ichange).toBe(IAchange);
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`IF adding array of values without index specification SHOULD add to the end `, () => {
   Evalue = initial.concat(modification);
   Echange.added = [{ index: array.size, values: modification }];

   Ichange = array.add(modification);
});

it(`SHOULD add on specified index`, async () => {
   const index = 1;

   Evalue = spliceIndex(index);
   Echange.added = [{ index, values: modification }];

   Ichange = array.add(modification, index);
});

it(`SHOULD add on index counting from the end`, async () => {
   const index = -1;

   Evalue = spliceIndex(index);
   Echange.added = [{ index, values: modification }];

   Ichange = array.add(modification, index);
});

it(`IF adding pack without index specification SHOULD add to the end and specify index automatically`, () => {
   Evalue = initial.concat(modification);
   Echange.added = [{ index: array.size, values: modification }];

   Ichange = array.add({
      values: modification
   });
});

it(`SHOULD add pack on specified index`, () => {
   const toAdd: IIndexAdded<User>[] = [{
      index: 1,
      values: [modification[0]]
   }, {
      index: 3,
      values: [modification[1]]
   }];

   Evalue = [initial[0], modification[0], initial[1], modification[1], initial[2]];
   Echange.added = toAdd;

   Ichange = array.add(...toAdd);
});

it(`IF packs follow each other without gaps they SHOULD be merged into one`, () => {
   Evalue = initial.concat(modification);
   Echange.added = [{ index: array.size, values: modification }];

   Ichange = array.add({
      values: [modification[0]]
   }, {
      values: [modification[1]]
   });
});

describe(`SHOULD NOT be changed`, () => {
   beforeEach(() => {
      Evalue = initial;
      Echange.changed = false;
   });

   it(`Entries`, () => {
      Ichange = array.add();
   });

   it(`Values`, () => {
      Ichange = array.add([]);
   });
});
