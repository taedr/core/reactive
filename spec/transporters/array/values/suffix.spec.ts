import { ReactiveNumberArray } from '@taedr/reactive';

let array: ReactiveNumberArray;
let mirror: ReactiveNumberArray;

beforeEach(() => {
   array = new ReactiveNumberArray();
   mirror = array.suffix(new ReactiveNumberArray());
});

it(`Source shutdown`, () => {
   array.shutdown = `WASTED`;
   expect(mirror.shutdown).toEqual(`WASTED`);
});

it(`Suffix shutdown`, () => {
   mirror.shutdown = `WASTED`;

   expect(mirror.shutdown).toEqual(`WASTED`);
   expect(array.watchers.size).toEqual(0);
});
