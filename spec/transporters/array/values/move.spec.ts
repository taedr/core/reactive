import { ERRORS, IMove, IReactiveArrayChange, IReadonlyReactiveArray, ReactiveNumberArray, REVERSE } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';

const initial = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

let array: ReactiveNumberArray;
let mirror: IReadonlyReactiveArray<number>;
let IAchange: IReactiveArrayChange<number>;
let IMchange: IReactiveArrayChange<number>;
let Echange: IReactiveArrayChange<number>;
let Ichange: typeof IAchange;
let Evalue = initial;


beforeEach(() => {
   array = new ReactiveNumberArray(initial);
   mirror = array.suffix(new ReactiveNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   expect(Ichange).toEqual(IAchange);
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});


it(`Move`, () => {
   Evalue = [0, 2, 1, 3, 4, 5, 6, 7, 8, 9];
   Echange.moved = [
      { from: 1, to: 2 },
   ];

   Ichange = array.move(...Echange.moved);
});

it(`Swap`, () => {
   Evalue = [0, 8, 2, 3, 4, 5, 6, 7, 1, 9];
   Echange.moved = [
      { from: 1, to: 8, swap: true },
   ];

   Ichange = array.move(...Echange.moved);
});

it(`Move many`, () => {
   Evalue = [9, 0, 2, 3, 4, 6, 1, 8, 7, 5];
   Echange.moved = [
      { from: 1, to: 6 },
      { from: 8, to: 7 },
      { from: 4, to: 8 },
      { from: 9, to: 0 }
   ];

   Ichange = array.move([1, 8, 7, 5], 6);
});

it(`Offset to the right`, () => {
   Evalue = [9, 3, 5, 0, 1, 2, 6, 7, 8, 4];
   Echange.moved = [
      { from: 4, to: 9 },
      { from: 8, to: 9 },
      { from: 3, to: 9 },
      { from: 3, to: 9 },
      { from: 9, to: 0 },
      { from: 9, to: 0 },
      { from: 9, to: 0 }
   ];

   Ichange = array.move([4, 9, 3, 5], 9);
});

it(`Negative index`, () => {
   Evalue = [9, 0, 1, 8, 2, 4, 6, 3, 5, 7];
   Echange.moved = [
      { from: 8, to: 3 },
      { from: 2, to: 3 },
      { from: 5, to: 4 },
      { from: 7, to: 5 },
      { from: 9, to: 0 }
   ];

   Ichange = array.move([8, 2, 4, 6], -7);
});

it(`Errors`, () => {
   Ichange = array.move();
   Evalue = initial;
   Echange.changed = false;
   expect(() => array.move([1, 1], 9)).toThrowError(ERRORS.index.duplicate(1));
});

it(`Reverse`, () => {
   Evalue = [...initial].reverse();
   Echange.moved = [
      { from: 9, to: 0 },
      { from: 9, to: 1 },
      { from: 9, to: 2 },
      { from: 9, to: 3 },
      { from: 9, to: 4 },
      { from: 9, to: 5 },
      { from: 9, to: 6 },
      { from: 9, to: 7 },
      { from: 9, to: 8 }
   ];

   Ichange = array.move(REVERSE);
});

it(`Move clockwise`, () => {
   Evalue = [8, 9, 0, 1, 2, 3, 4, 5, 6, 7];
   Echange.moved = [
      { from: 9, to: 0 },
      { from: 9, to: 0 }
   ];

   Ichange = array.move(2);
});

it(`Move сounterclockwise`, () => {
   Evalue = [2, 3, 4, 5, 6, 7, 8, 9, 0, 1];
   Echange.moved = [
      { from: 0, to: 9 },
      { from: 0, to: 9 }
   ];

   Ichange = array.move(-2);
});

describe(`Should offset left if 'from' is lesser than 'to'`, () => {
   const from = 2;
   const to = 5;
   const EOffsetedvalue = [0, 1, 3, 4, 5, 2, 6, 7, 8, 9];
   const Emoved = [{ from, to }];

   it(`Entry`, () => {
      Evalue = EOffsetedvalue;
      Echange.moved = Emoved;

      Ichange = array.move({ from, to });
   });

   it(`Array`, () => {
      Evalue = EOffsetedvalue;
      Echange.moved = Emoved;

      Ichange = array.move([from], to);
   });
});

describe(`Should offset right if 'from' is bigger than 'to'`, () => {
   const from = 8;
   const to = 5;
   const EOffsetedvalue = [0, 1, 2, 3, 4, 8, 5, 6, 7, 9];
   const Emoved = [{ from, to }];

   it(`Entry`, () => {
      Evalue = EOffsetedvalue;
      Echange.moved = Emoved;

      Ichange = array.move(...Emoved);
   });

   it(`Array`, () => {
      Evalue = EOffsetedvalue;
      Echange.moved = Emoved;

      Ichange = array.move([from], to);
   });
});


/*
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
[1, 3], 7
[0, 2, 4, 5, 6, 1, 3, 7, 8, 9]
[0, 2, 4, 5, 6, 7, 8, 9]

[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
[{from: 1, to: 7}, {from: 3, to: 7}]
[0, 2, 4, 5, 6, 1, 3, 7, 8, 9]


[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
[{from: 0, to: 9}, {from: 9, to: 0}]
[9, 1, 2, 3, 4, 5, 6, 7, 8, 0]
[0, 2, 4, 5, 6, 1, 3, 7, 8, 0]

[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
[2, 7], 5
[0, 1, 3, 4, 2, 7, 5, 6, 8, 9]
[0, 1, 3, 4, 5, 6, 8, 9]

[{from: 2, to: 4}, {from: 7, to: 5}]

*/
