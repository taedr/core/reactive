import { IIndexDeleted, IReactiveArrayChange, IReadonlyReactiveArray, ReactiveNumberArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';

let array: ReactiveNumberArray;
let mirror: IReadonlyReactiveArray<number>;
let IAchange: IReactiveArrayChange<number>;
let IMchange: IReactiveArrayChange<number>;
let Ichange: IReactiveArrayChange<number>;

let remain = [0, 2, 5, 8];
let Evalue: readonly number[];
let Echange: IReactiveArrayChange<number>
const initial = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
const toDelete = initial.filter(v => !remain.includes(v));
const Edeleted: IIndexDeleted<number>[] = [
   { index: 9, count: 1, values: [9] },
   { index: 6, count: 2, values: [6, 7] },
   { index: 3, count: 2, values: [3, 4] },
   { index: 1, count: 1, values: [1] },
];

beforeEach(() => {
   array = new ReactiveNumberArray(initial);
   mirror = array.suffix(new ReactiveNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Evalue = remain;
   Echange = {
      changed: true,
      added: [],
      deleted: Edeleted,
      updated: [],
      moved: []
   };
});

afterEach(() => {
   expect(Ichange).toBe(IAchange);
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`SHOULD delete entry if compare function returns 'true'`, async () => {
   Ichange = array.delete(value => toDelete.includes(value));
});

it(`Delete Indexes`, async () => {
   Ichange = array.delete(...Edeleted);
});

it(`Delete Values`, async () => {
   Ichange = array.delete(toDelete);
});

it(`Delete negative index`, async () => {
   Echange.deleted = [
      { index: 9, count: 1, values: [9] },
      { index: -3, count: 2, values: [6, 7] },
      { index: -4, count: 2, values: [3, 4] },
      { index: -4, count: 1, values: [1] },
   ];

   Ichange = array.delete(...Echange.deleted);
});

describe(`SHOULD NOT be changed`, () => {
   beforeEach(() => {
      Evalue = initial;
      Echange.changed = false;
      Echange.deleted = [];
   });

   it(`Entries`, () => {
      Ichange = array.delete();
   });

   it(`Values`, () => {
      Ichange = array.delete([]);
   });

   it(`Compare`, () => {
      Ichange = array.delete(() => { });
   });
});

it(`Delete counting from the end`, async () => {
   Evalue = [0];
   Echange.deleted = [
      { index: 1, count: 9, values: initial.slice(1, 10) },
   ];

   Ichange = array.delete({
      index: 1, count: -1,
   });
});
