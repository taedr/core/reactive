import { AReactiveObject, AReactivesArray, AReactiveWrapperArray, Bus, ReactiveNumberArray, ReactiveObjectArray, ReactiveString, shutdownOn } from '@taedr/reactive';
import { FN_TO_STRING, getRuntimeId } from '@taedr/utils';

it(`SHOULD keep nulls`, () => {
   const initial = [null, 2, 3, 4, null, null, 3, 1];
   const modification = [null, 2, null, 1];
   const array = new ReactiveNumberArray(initial);

   expect(array.value).toEqual(initial);

   array.send(modification);

   expect(array.value).toEqual(modification);
});

it(`SHOULD filter nulls`, () => {
   const initial = [null, 2, 3, 4, null, null, 3, 1];
   const modification = [null, 2, null, 1];
   const array = new ReactiveNumberArray(initial, {
      getHash: v => `${v}`
   });
   const Evalue = [null, 2, 3, 4, 1];
   const Evalue_2 = [null, 2, 1];

   expect(array.value).toEqual(Evalue);

   array.send(modification);

   expect(array.value).toEqual(Evalue_2);
});

it(`SHOULD keep nulls with unique ids`, () => {
   const initial = [null, 2, 3, 4, null, null, 3, 1];
   const modification = [null, 2, null, 1];
   const array = new ReactiveNumberArray(initial, {
      getHash: v => v === null ? getRuntimeId() : `${v}`
   });
   const Evalue = [null, 2, 3, 4, null, null, 1]; // Without '3'

   expect(array.value).toEqual(Evalue);

   array.send(modification);

   expect(array.value).toEqual(modification);
});

it(`Wrappers`, () => {
   class User {
      constructor(
         public name: string
      ) { }
   }

   class ReactiveUser extends AReactiveObject<User, null> {
      constructor(
         user: User | null
      ) {
         super({
            null: user === null,
            fields: {
               name: new ReactiveString(user?.name ?? ``)
            }
         });
      }

      protected createInstance(): User & object {
         return new (User as any)();
      }
   }

   class ReactiveUserArray extends AReactivesArray<User | null, ReactiveUser> {
      protected createWrapper(value: User): ReactiveUser {
         return new ReactiveUser(value);
      }
   }


   const initial = [
      new ReactiveUser(new User(`A`)),
      new ReactiveUser(null),
      new ReactiveUser(new User(`B`)),
   ];
   const users = new ReactiveUserArray(initial);
   const Evalue = initial.map(({ value }) => value);

   expect(users.value).toEqual(Evalue);

   initial[0].send(null);

   expect(users.value).toEqual([null, null, { name: `B` }]);
});
