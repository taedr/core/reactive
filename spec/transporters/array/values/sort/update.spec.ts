import { IReactiveArrayChange, IReadonlyReactiveArray, IValue, TReactiveArraySorter } from '@taedr/reactive';
import { checkReactiveArrayState } from '../../state.check';
import { ReactiveUserArray, User } from '../../_model';

let array: ReactiveUserArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;
let Echange: IReactiveArrayChange<User>;

const initial = [
   new User(`0`, `A`, 0),
   new User(`1`, `B`, 3),
   new User(`2`, `C`, 5),
   new User(`3`, `D`, 7),
   new User(`4`, `E`, 9),
];

const sorter: TReactiveArraySorter<User> = (a, b) => a.age - b.age;

const modification = [
   new User(`4`, `E`, 8),
   new User(`1`, `B`, 10),
   new User(`0`, `D`, 9),
];

let Evalue = [
   initial[2],
   initial[3],
   modification[0],
   modification[2],
   modification[1]
];

beforeEach(() => {
   array = new ReactiveUserArray(initial, { sorter });
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [
         { index: 0, value: modification[2] },
         { index: 1, value: modification[1] },
         { index: 4, value: modification[0] }
      ],
      moved: [
         { from: 0, to: 3 },
         { from: 0, to: 4 },
         { from: 3, to: 2 }
      ]
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

describe(`IF update changes value weight it should be moved according to sort`, () => {
   it(`function`, () => {
      array.update((value) => {
         return modification.find(v => v.id === value.id);
      });
   });

   it(`entries`, () => {
      array.update(...Echange.updated);
   })
});

/*
[
   new User(`0`, `D`, 9),
   new User(`1`, `B`, 10),
   new User(`2`, `C`, 5),
   new User(`3`, `D`, 7),
   new User(`4`, `E`, 8),
]

[
   new User(`2`, `C`, 5),
   new User(`3`, `D`, 7),
   new User(`4`, `E`, 8),
   new User(`0`, `D`, 9),
   new User(`1`, `B`, 10),
]
------------------------------
[9, 10, 5, 7, 8] - after update
[5, 7, 8, 9, 10] - needed
[8, 10, 9] - update weight order
[10, 9, 8] - sorted update weight
------------------------------
[9, 5, 7, 8, 10] | 10 | 1 - 4

[5, 7, 8, 9, 10] |  9 | 0 - 3
[]

if (next.index > entry.index && next.index > to) continue;
if (next.index < entry.index && next.index < to) continue;

if (next.index < entry.index && next.index > to) next.index++;
if (next.index > entry.index && next.index < to) next.index--;
if (from < next.index && to > next.index) next.index--;
if (from > next.index && to < next.index) next.index++;

*/
