import { IIndexAdd, IReactiveArrayChange, IReadonlyReactiveArray, IValue, ReactiveNumberArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../../state.check';


let array: ReactiveNumberArray;
let mirror: IReadonlyReactiveArray<number>;
let IAchange: IReactiveArrayChange<number>;
let IMchange: IReactiveArrayChange<number>;
let Echange: IReactiveArrayChange<number>;
let Evalue: readonly number[];

const initial = [1, 4, 5, 6, 9];
const sorter = (a: number, b: number) => b - a;

beforeEach(() => {
   array = new ReactiveNumberArray(initial, { sorter });
   mirror = array.suffix(new ReactiveNumberArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: []
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});


it(`SHOULD add values according to sort and determine add indexes after sort`, async () => {
   Evalue = [9, 8, 7, 6, 5, 4, 3, 2, 1];
   Echange.added = [
      { index: 1, values: [8, 7] },
      { index: 6, values: [3, 2] }
   ];

   array.add([8, 3, 7, 2]);
});

it(`SHOULD ignore indexes and reassemble packs depending on insert by sort order`, async () => {
   const toAdd: IIndexAdd<number>[] = [
      { index: 0, values: [2, 12] },
      { index: 1, values: [3, 10, 7] },
      { index: -1, values: [11, 8] },
   ];

   Evalue = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
   Echange.added = [
      { index: 0, values: [12, 11, 10] },
      { index: 4, values: [8, 7] },
      { index: 9, values: [3, 2] }
   ];

   array.add(...toAdd);
});
