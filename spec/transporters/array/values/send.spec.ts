import { IReactiveArrayChange, IReadonlyReactiveArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';
import { initial, modification, ReactiveUserArray, User } from '../_model';


let array: ReactiveUserArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;
let Echange: IReactiveArrayChange<User>;
let Evalue: readonly User[];

beforeEach(() => {
   array = new ReactiveUserArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [],
      moved: [],
   };
});

afterEach(() => {
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`Replace`, async () => {
   Evalue = modification;
   Echange.deleted = [{ index: 0, count: initial.length, values: initial }];
   Echange.added = [{ index: 0, values: modification }];

   array.send(modification);
});

it(`Modify`, async () => {
   Evalue = [modification[1], modification[0], initial[1]];
   Echange.deleted = [{ index: 0, count: 1, values: [initial[0]] }];
   Echange.updated = [{ index: 1, value: modification[0] }];
   Echange.added = [{ index: 2, values: [modification[1]] }];
   Echange.moved = [{ from: 2, to: 0, swap: true }];

   array.send(Echange);
});

describe(`SHOULD reset to empty`, () => {
   beforeEach(() => {
      Evalue = [];
      Echange.deleted = [{ index: 0, count: initial.length, values: initial }];
   });

   it(`null`, () => {
      array.send(null);
   });

   it(`array`, () => {
      array.send([]);
   });
});
