import { IReactiveArrayChange, IReadonlyReactiveArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';
import { initial, modification, ReactiveUserArray, User } from '../_model';

let array: ReactiveUserArray;
let mirror: IReadonlyReactiveArray<User>;
let IAvalue: readonly User[];
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;

beforeEach(() => {
   array = new ReactiveUserArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((value, change) => {
      IAvalue = value;
      IAchange = change
   });
   mirror.watch((_, change) => IMchange = change);
});

it(`Initial`, async () => {
   const Echange: IReactiveArrayChange<User> = {
      initial: true, changed: false,
      added: [{ index: 0, values: initial }],
      deleted: [], updated: [], moved: []
   };

   expect(IAvalue).toBe(array.value);
   expect(IAvalue).toEqual(initial);
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, initial);
});

it(`Get`, async () => {
   for (let i = 0; i < initial.length; i++) {
      const value = array.get(i);
      expect(value).toEqual(initial[i]);
   }

   for (let i = initial.length - 1; i >= 0; i--) {
      const value = array.get(-i);

      expect(value).toEqual(initial.slice(-i)[0]);
   }

   expect(array.get(11)).toBeUndefined();

   expect(array.get(({ id }) => id === `0`)).toEqual(initial[0]);
   expect(array.get(({ id }) => id === `9`)).toEqual(undefined);
});

it(`Has`, async () => {
   for (const user of initial) {
      expect(array.has(({ name }) => name === user.name)).toBeTruthy();
   }

   expect(array.has(({ name }) => name === modification[0].name)).toBeFalsy();
});
