import { IIndexUpdate, IReactiveArrayChange, IReadonlyReactiveArray } from '@taedr/reactive';
import { checkReactiveArrayState } from '../state.check';
import { initial, modification, ReactiveUserArray, User } from '../_model';

let array: ReactiveUserArray;
let mirror: IReadonlyReactiveArray<User>;
let IAchange: IReactiveArrayChange<User>;
let IMchange: IReactiveArrayChange<User>;
let Ichange: typeof IAchange;

let Evalue = [modification[1], initial[1], modification[0]];
let Echange: IReactiveArrayChange<User>;

beforeEach(() => {
   array = new ReactiveUserArray(initial);
   mirror = array.suffix(new ReactiveUserArray());
   array.watch((_, change) => IAchange = change);
   mirror.watch((_, change) => IMchange = change);
   IAchange = undefined as any;
   IMchange = undefined as any;
   Ichange = undefined as any;
   Echange = {
      changed: true,
      added: [],
      deleted: [],
      updated: [
         { value: Evalue[0], index: 0 },
         { value: Evalue[2], index: 2 },
      ],
      moved: []
   };
});

afterEach(() => {
   expect(Ichange).toEqual(IAchange);
   checkReactiveArrayState(array, mirror, IAchange, IMchange, Echange, Evalue);
});

it(`Update Indexes`, async () => {
   Ichange = array.update(...Echange.updated);
});

it(`Update Compare`, async () => {
   Ichange = array.update((_, i) => {
      if (i === 0) return Evalue[0];
      if (i === 2) return Evalue[2];
   });
});


describe(`SHOULD NOT be changed`, () => {
   beforeEach(() => {
      Evalue = initial;
      Echange.changed = false;
      Echange.updated = [];
   });

   it(`Entries`, () => {
      Ichange = array.update({
         index: 0,
         value: initial[0]
      });
   });

   it(`Values`, () => {
      Ichange = array.update((_, i) => {
         if (i === 0) return initial[0];
      });
   });
});
