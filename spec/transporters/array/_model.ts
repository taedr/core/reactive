import { AReactiveObject, AReactivesHashedObjectArray, AReactivesObjectArray, IReactiveArrayCtx, IReactiveWrapperCtx, ReactiveHashedObjectArray, ReactiveNumber, ReactiveObjectArray, ReactiveString } from "@taedr/reactive";
import { FN_GET_ID } from "@taedr/utils";

export class User {
   constructor(
      public id: string,
      public name: string,
      public age: number,
   ) { }
}

export class ReactiveUser extends AReactiveObject<User, never, ReturnType<typeof ReactiveUser.getFields>> {
   constructor(
      user: User
   ) {
      super({
         fields: ReactiveUser.getFields(user)
      });
   }

   protected createInstance() {
      return new (<any>User)();
   }

   protected static getFields(user: User) {
      return {
         id: new ReactiveString(user.id),
         age: new ReactiveNumber(user.age),
         name: new ReactiveString(user.name),
      };
   }
}

export class ReactiveUserArray extends ReactiveObjectArray<User> {
}

export class ReactiveUserIdArray extends ReactiveHashedObjectArray<User> {
   constructor(
      users: User[],
      ctx: Omit<IReactiveArrayCtx<User>, 'getHash'> = {}
   ) {
      super(users, {
         getHash: FN_GET_ID,
         ...ctx
      });
   }
}

export class ReactivesUserArray extends AReactivesObjectArray<User,  ReactiveUser> {
   constructor(
      users: readonly User[] | readonly ReactiveUser[] = [],
      ctx?: IReactiveArrayCtx<User>
   ) {
      super(users, ctx);
   }

   protected createWrapper(value: User): ReactiveUser {
      return new ReactiveUser(value);
   }
}

export class ReactivesUserIdArray extends AReactivesHashedObjectArray<User,  ReactiveUser> {
   constructor(
      users: readonly User[] | readonly ReactiveUser[] = [],
      ctx: Omit<IReactiveArrayCtx<User>, 'getHash'> = {}
   ) {
      super(users, {
         getHash: FN_GET_ID,
         ...ctx
      });
   }

   protected createWrapper(value: User): ReactiveUser {
      return new ReactiveUser(value);
   }
}

export const initial = [
   new User(`0`, `Vasyl`, 8),
   new User(`1`, `Petro`, 36),
   new User(`2`, `Kate`, 17),
];

export const modification = [
   new User(`3`, `Orest`, 56),
   new User(`4`, `Motrya`, 23),
];

export const getInitialWrappers = () => initial.map(user => new ReactiveUser(user));
export const getModificationWrappers = () => modification.map(user => new ReactiveUser(user));
