import { Bus, IBusChange } from '@taedr/reactive';

it(`Basic - should notify watchers when setting the value, undefined should be converted to null`, () => {
   const bus = new Bus<number | null | undefined>();
   let Ivalue: number | null | undefined;
   let Ichange: IBusChange | undefined;

   bus.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   });

   for (const value of [1, null, 2, undefined, 3]) {
      bus.send(value);
      expect(Ivalue).toEqual(value);
      expect(Object.isFrozen(Ichange)).toBeTruthy();
   }
});
