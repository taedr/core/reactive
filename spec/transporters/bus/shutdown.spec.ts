import { Bus, ERRORS } from '@taedr/reactive';

const message = `WASTED`;

it(`Detach`, () => {
   const bus = new Bus<number>();
   let Ivalue = NaN;
   let Imessage = ``;

   const watcherId = bus.watch({
      value: value => Ivalue = value,
      detach: message => Imessage = message
   });
   const watchers = bus.watchers;

   expect(watchers.size).toEqual(1);
   expect(watchers.has(watcherId)).toBeTruthy();

   const first = 1;
   bus.send(first);

   expect(Ivalue).toEqual(first);

   bus.watchers.detach(watcherId, message);
   bus.send(4);

   expect(Imessage).toEqual(message);
   expect(Ivalue).toEqual(first);
   expect(watchers.size).toEqual(0);
});

it(`Shutdown`, () => {
   const bus = new Bus<number>();
   let Imessage = ``;

   bus.watch({
      value: () => { },
      detach: message => Imessage = message
   });

   bus.shutdown = message;

   expect(() => bus.send(4)).toThrowError(ERRORS.shutdown.change);
   expect(Imessage).toEqual(message);
   expect(bus.watchers.size).toEqual(0);
   expect(bus.shutdown).toEqual(message);
});
