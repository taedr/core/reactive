import { Bus, IBusChange } from "@taedr/reactive";


it(`SHOULD call 'value' once without 'prepare'`, () => {
   const bus = new Bus<number>();
   const Ichange: IBusChange[] = [];

   bus.watch((_, change) => Ichange.push(change));

   bus.send(1);

   expect(Ichange).toEqual([{}]);
});


it(`SHOULD call 'value' twice: first - with 'prepare', second - without`, () => {
   const bus = new Bus<number>();
   const Ichange: IBusChange[] = [];

   bus.watch({
      prepare: true,
      value: (_, change) => Ichange.push(change)
   });

   bus.send(1);

   expect(Ichange).toEqual([{ prepare: true }, {}]);
});
