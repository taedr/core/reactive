import { AReactiveObject, AReactivesArray, Bus, passIfChanged, ReactiveNumberArray, ReactiveObjectArray, ReactiveString, shutdownOn } from '@taedr/reactive';
import { FN_DELAY, FN_TO_STRING, getRuntimeId } from '@taedr/utils';

it(`Playground`, async () => {
   const bus = new ReactiveNumberArray([1, 2, 3, 4]);

   const destroy = new Bus();
   const pipe = bus.pipe(shutdownOn(destroy)).suffix();
   pipe

   destroy.pipe() //?

   bus.add()

   const array = new ReactiveNumberArray([null, 2, 3, 4, null, null, 3, 1], {
      getHash: FN_TO_STRING
   });

   class User {
      constructor(
         public name: string
      ) { }
   }

   class ReactiveUser extends AReactiveObject<User, null> {
      constructor(
         user: User | null
      ) {
         super({
            fields: {
               name: new ReactiveString(user?.name ?? ``)
            }
         });
         if (user === null) this.send(null);
      }

      protected createInstance(): User & object {
         return new (User as any)();
      }
   }

   class ReactiveUserArray extends AReactivesArray<User | null, ReactiveUser> {
      protected createWrapper(value: User): ReactiveUser {
         return new ReactiveUser(value);
      }
   }

   const users = new ReactiveObjectArray([
      new User(`A`),
      null,
      new User(`B`)
   ], {
      getHash: user => user?.name ?? getRuntimeId()
   });

   const rusers = new ReactiveUserArray([
      new ReactiveUser(new User(`A`)),
      new ReactiveUser(null),
      new ReactiveUser(new User(`B`)),
   ]);

   rusers //?




   rusers.pipe(
      passIfChanged()
   )

   let resolve: any;
   let reject: any;
   const promise = new Promise((r, rj) => {
      resolve = r;
      reject = rj;
   });

   promise.then(v => {
      v//?
   }).catch(e => {
      e //?
   })

   // reject(`b`)
   resolve(`a`)
reject(`b`)
   await FN_DELAY(1);

   reject(`b`)

   await FN_DELAY(1);
   resolve(`b`)
 
   await FN_DELAY(1);
});


it(`aa`, () => {
   // const user = new FormGroup({
   //    name: new FormControl(`Orest`),
   //    age: new FormControl(12),
   //    address: new FormGroup({
   //       country: new FormControl(`Ukraine`),
   //       city: new FormControl(`Chornomorsk`)
   //    }),
   //    notes: new FormArray([
   //       new FormControl(`A`),
   //       new FormControl(`B`)
   //    ])
   // });

   // user.setValue() //?
})



/*
1. Set nullable pipe values (debounce)
2. Set 'initial' & 'changed' as optional
2. Improve shutdownOn - instant and sequential strategies
2. Shared fuintion for sequent changes for array, record, fields
3. Async fixes for changes  array, record, fields
*/

/*
Form:
value mapper
selction list slected result, value resu;t
*/
