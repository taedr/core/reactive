import { AEmitter } from "./core";

export class Ticker extends AEmitter<number> {
   protected _ticks = 0;
   protected _timerId?: any;

   public get delayMs() { return this._delayMs; }
   public get ticks() { return this._ticks; }

   constructor(
      protected _delayMs: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onFirstWatch(): void {
      this._timerId = setInterval(() => {
         this.send(++this._ticks);
      }, this._delayMs);
   }

   protected onLastDetach(): void {
      clearInterval(this._timerId);
      delete this._timerId;
   }
}
