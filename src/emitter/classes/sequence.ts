import { IBusChange, EDetachMessage, ERRORS, IWatcherCtx, Watcher } from '../../core';
import { AEmitter } from './core';

export class Sequence<T> extends AEmitter<T>{
   protected _values: readonly T[] = Object.freeze([]);

   public get values() { return this._values; }

   constructor(
      values: Iterable<T>,
   ) {
      super();
      this._values = Object.freeze([...values]);
   }
   // ---------------------------------------------
   //                   Interanl
   // ---------------------------------------------
  

   protected createWatcher(ctx: IWatcherCtx<T, IBusChange>, id: string): string {
      const watcher = new Watcher(ctx);

      for (const value of this._values) {
         this.notifySingleWatcher(value, {}, watcher);
      }

      if (watcher.detach) watcher.detach(EDetachMessage.Emittion);

      return id;
   }
}
