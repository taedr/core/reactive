import { TConsumer } from '@taedr/utils';
import { IBusChange, EDetachMessage, IWatcherCtx, Watcher } from '../../core';
import { AEmitter } from './core';


export type TProducer<T> = (send: TConsumer<T>, shutdown: (message?: string) => void) => TConsumer<string> | void;

export class Producer<T> extends AEmitter<T>{
   protected _values: readonly T[] = Object.freeze([]);
   protected _producer: TProducer<T>;
   protected _detacher?: TConsumer<string>;

   public get values() { return this._values; }
   public get shutdown(): string { return this._shutdown ?? ``; }
   public set shutdown(message: string) { this.manualShutdown(message); }

   /** Executes `produce` on instance creation.
    * Saves values which came by `send` call.
    * Shutdowns after `shutdown` call.
    * * The watcher will be provided with all available values at the time of subscription synchronously.
    * * The watcher will be provided with new values asynchronously if it attached during `produce` execution.
    *   */
   constructor(
      producer: TProducer<T>
   ) {
      super();
      this._producer = producer;
      this.init();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected init() {
      const detacher = this._producer(value => {
         if (this._shutdown) return; // #Return#
         this._values = Object.freeze([...this._values, value]);
         this.send(value);
      }, message => {
         this.setShutdown(message || EDetachMessage.Emittion)
      });

      if (detacher) this._detacher = detacher;
   }

   protected createWatcher(ctx: IWatcherCtx<T, IBusChange>, id: string): string {
      const watcher = new Watcher(ctx);

      for (const value of this._values) {
         this.notifySingleWatcher(value, {}, watcher);
      }

      if (this._shutdown) {
         if (watcher.detach) watcher.detach(this._shutdown);
      } else {
         this.addWatcher(id, watcher);
      }

      return id;
   }

   protected manualShutdown(message: string): void {
      if (this._detacher && !this._shutdown) this._detacher(message);
      this.setShutdown(message);
   }
}
