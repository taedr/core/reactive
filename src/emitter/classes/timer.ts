import { IBusChange, EDetachMessage, IWatcherCtx, Watcher } from "../../core";
import { AEmitter } from "./core";


// ==================================================
//                   Watcher
// ==================================================
export class TimerWatcher<T> extends Watcher<T, IBusChange> {
   constructor(
      ctx: IWatcherCtx<T, IBusChange>,
      public timerId: any,
   ) {
      super(ctx);
   }
}
// ==================================================
//                   Timer
// ==================================================
export class Timer<T> extends AEmitter<T> {
   protected _watchers = new Map<string, TimerWatcher<T>>();
   protected _value?: T;

   public get value() { return this._value; }
   public get delayMs() { return this._delayMs; }

   constructor(
      protected _delayMs: number,
      value: T
   ) {
      super();
      if (value) this._value = value as T;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected createWatcher(ctx: IWatcherCtx<T, IBusChange>, id: string) {
      if (this._shutdown) {
         if (ctx.detach) ctx.detach(this._shutdown);
      } else {
         const timerId = setTimeout(() => {
            this.notifySingleWatcher(this._value as T, {}, watcher);
            const deleted = this.deleteWatcher(id, watcher);
            if (deleted && ctx.detach) ctx.detach(EDetachMessage.Emittion);
         }, this._delayMs);
         const watcher = new TimerWatcher(ctx, timerId);

         this._watchers.set(id, watcher);
      }

      return id;
   }

   protected deleteWatcher(id: string, watcher: TimerWatcher<T>) {
      clearTimeout(watcher.timerId);
      return super.deleteWatcher(id, watcher);
   }
}
