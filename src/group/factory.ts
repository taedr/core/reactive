import { IReadonlyBus, TReadonlyBusArray_10, TReadonlyBusArray_2, TReadonlyBusArray_3, TReadonlyBusArray_4, TReadonlyBusArray_5, TReadonlyBusArray_6, TReadonlyBusArray_7, TReadonlyBusArray_8, TReadonlyBusArray_9 } from '../core';
import { Group } from './classes/group';
import { Latest } from './classes/latest';


// ==================================================
//                   Group
// ==================================================
/**
 * Emits latest values from all sources packed into array on change of any source
 */
export function group<A, B>(
   sources: TReadonlyBusArray_2<A, B>
): Group<readonly [A, B], typeof sources>;
export function group<A, B, C>(
   sources: TReadonlyBusArray_3<A, B, C>
): Group<readonly [A, B, C], typeof sources>;
export function group<A, B, C, D>(
   sources: TReadonlyBusArray_4<A, B, C, D>
): Group<readonly [A, B, C, D], typeof sources>;
export function group<A, B, C, D, E>(
   sources: TReadonlyBusArray_5<A, B, C, D, E>
): Group<readonly [A, B, C, D, E], typeof sources>;
export function group<A, B, C, D, E, F>(
   sources: TReadonlyBusArray_6<A, B, C, D, E, F>
): Group<readonly [A, B, C, D, E, F], typeof sources>;
export function group<A, B, C, D, E, F, G>(
   sources: TReadonlyBusArray_7<A, B, C, D, E, F, G>
): Group<readonly [A, B, C, D, E, F, G], typeof sources>;
export function group<A, B, C, D, E, F, G, H>(
   sources: TReadonlyBusArray_8<A, B, C, D, E, F, G, H>
): Group<readonly [A, B, C, D, E, F, G, H], typeof sources>;
export function group<A, B, C, D, E, F, G, H, I>(
   sources: TReadonlyBusArray_9<A, B, C, D, E, F, G, H, I>
): Group<readonly [A, B, C, D, E, F, G, H, I], typeof sources>;
export function group<A, B, C, D, E, F, G, H, I, J>(
   sources: TReadonlyBusArray_10<A, B, C, D, E, F, G, H, I, J>
): Group<readonly [A, B, C, D, E, F, G, H, I, J], typeof sources>;
export function group<T>(
   sources: readonly IReadonlyBus<T>[]
): Group<readonly T[], typeof sources>;
export function group(
   sources: readonly IReadonlyBus<any>[]
): any {
   return new Group(sources);
}
// ==================================================
//                   Latest
// ==================================================
/**
 * Emits each value provided by any source
 */
export function latest<A, B>(
   sources: TReadonlyBusArray_2<A, B>
): Latest<A | B, typeof sources>;
export function latest<A, B, C>(
   sources: TReadonlyBusArray_3<A, B, C>
): Latest<A | B | C, typeof sources>;
export function latest<A, B, C, D>(
   sources: TReadonlyBusArray_4<A, B, C, D>
): Latest<A | B | C | D, typeof sources>;
export function latest<A, B, C, D, E>(
   sources: TReadonlyBusArray_5<A, B, C, D, E>
): Latest<A | B | C | D | E, typeof sources>;
export function latest<A, B, C, D, E, F>(
   sources: TReadonlyBusArray_6<A, B, C, D, E, F>
): Latest<A | B | C | D | E | F, typeof sources>;
export function latest<A, B, C, D, E, F, G>(
   sources: TReadonlyBusArray_7<A, B, C, D, E, F, G>
): Latest<A | B | C | D | E | F | G, typeof sources>;
export function latest<A, B, C, D, E, F, G, H>(
   sources: TReadonlyBusArray_8<A, B, C, D, E, F, G, H>
): Latest<A | B | C | D | E | F | G | H, typeof sources>;
export function latest<A, B, C, D, E, F, G, H, I>(
   sources: TReadonlyBusArray_9<A, B, C, D, E, F, G, H, I>
): Latest<A | B | C | D | E | F | G | H | I, typeof sources>;
export function latest<A, B, C, D, E, F, G, H, I, J>(
   sources: TReadonlyBusArray_10<A, B, C, D, E, F, G, H, I, J>
): Latest<A | B | C | D | E | F | G | H | I | J, typeof sources>;
export function latest<T>(
   sources: readonly IReadonlyBus<T>[]
): Latest<T, typeof sources>;
export function latest(
   sources: readonly IReadonlyBus<any>[]
): any {
   return new Latest(sources);
}
