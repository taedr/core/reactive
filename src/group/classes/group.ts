import { getRuntimeId } from '@taedr/utils';
import { APipe, AReadonlyBus, EDetachMessage, IPipeline, IPossibleChange, IReactiveChange, IReadonlyBus, IWatcherCtx, Pipeline, TOnValue, TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9, TReadonlyBusArray_10, TReadonlyBusArray_2, TReadonlyBusArray_3, TReadonlyBusArray_4, TReadonlyBusArray_5, TReadonlyBusArray_6, TReadonlyBusArray_7, TReadonlyBusArray_8, TReadonlyBusArray_9 } from '../../core';

// ==================================================
//                   Model
// ==================================================
export interface IGroup<T extends readonly unknown[]> extends IReadonlyBus<T> {
   shutdown: string;
   updated: IGroupUpdate;
   watch(watcher: TOnValue<T, IGroupChange>): string;
   watch(watcher: IWatcherCtx<T, IGroupChange>): string;
}

export interface IGroupUpdate {
   sources: readonly ISourceState[];
   rounds: number;
   all: boolean;
   some: boolean;
}

export interface ISourceState {
   detached: boolean;
   updated: boolean;
   updates: number;
}

export interface IGroupChange extends IReactiveChange, IGroupUpdate {
}
// ==================================================
//                   Core
// ==================================================
export class Group<T extends readonly unknown[], S extends readonly IReadonlyBus<unknown>[]>
   extends AReadonlyBus<T> {
   protected _id = getRuntimeId();
   protected _group: unknown[];
   protected _sources: S;
   protected _updated: IGroupUpdate;
   protected _initial?: true;

   public get updated() { return this._updated; }
   public get sources() { return this._sources; }
   public get shutdown(): string { return this._shutdown ?? ``; }
   public set shutdown(message: string) { this.setShutdown(message); }

   constructor(
      sources: S,
   ) {
      super();
      this._sources = Object.freeze([...sources]) as S;
      this._group = new Array(this._sources.length).fill(undefined);
      this._updated = Object.freeze({
         sources: Object.freeze(new Array(this._sources.length).fill(0).map(() => ({
            detached: false,
            updates: 0,
            updated: false,
         }))),
         all: false,
         some: false,
         count: 0,
         rounds: 0,
      });
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public watch(watcher: TOnValue<T, IGroupChange>): string;
   public watch(watcher: IWatcherCtx<T, IGroupChange>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch(): void {
      const initialRef = this._updated;

      this._initial = true;

      if (this._sources.length === 0) {
         this.sendInitial();
         this.setShutdown(EDetachMessage.Sources);
      } else {
         for (let i = 0; i < this._sources.length; i++) {
            this._sources[i].watch({
               id: this._id,
               value: (value, change) => this.onSourceChange(i, value, change),
               detach: message => this.onSourceDetach(i, message,),
            });
         }
      }

      delete this._initial;

      const isHalfFirst = this._updated.rounds === 0 && !this._updated.all;

      if (initialRef !== this._updated && !isHalfFirst) {
         this.sendInitial();
      }

      if (this._updated.sources.every(s => s.detached)) {
         this.setShutdown(EDetachMessage.Sources);
      }
   }

   protected onLastDetach(): void {
      for (const source of this._sources) {
         source.watchers.detach(this._id)
      }
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onSourceChange(index: number, value: unknown, change: IPossibleChange): any {
      const prev = this._group[index];
      const sources = [...this._updated.sources];
      const source = sources[index];

      sources[index] = Object.freeze({
         updated: true,
         updates: source.updates + 1,
         detached: source.detached,
      });

      this._group[index] = value;

      let count = 0;
      let total = 0;

      for (const source of sources) {
         if (source.detached) continue;
         if (source.updated) count += 1;
         total += 1;
      }

      const all = count === total;
      const rounds = this._updated.rounds;
      const changed = change.changed ?? value !== prev;

      if (all) {
         for (let i = 0; i < sources.length; i++) {
            sources[i] = Object.freeze({ ...sources[i], updated: false });
         }

         this._updated = Object.freeze({
            sources: Object.freeze(sources),
            all: true,
            some: false,
            rounds: rounds + 1,
         });
      } else {
         this._updated = Object.freeze({
            sources: Object.freeze(sources),
            all,
            some: count > 0,
            rounds,
         });
      }

      if (this._initial || (this._updated.rounds === 0 && !this._updated.all)) {
         return;
      }

      this.sendValue(changed);
   }

   protected onSourceDetach(index: number, message: string) {
      if (message === this._id) return; // #Return#
      const sources = [...this._updated.sources];
      const source = sources[index];

      sources[index] = Object.freeze({
         updated: source.updated,
         updates: source.updates,
         detached: true,
      });

      this._updated = Object.freeze({ ...this._updated, sources: Object.freeze(sources) });

      if (this._initial) return;

      if (sources.every(s => s.detached)) {
         this.setShutdown(EDetachMessage.Sources);
      } else {
         this.sendValue(false);
      }
   }

   protected sendValue(changed: boolean) {
      const group = Object.freeze([...this._group]) as T;
      const change: IGroupChange = Object.freeze({ changed, ...this._updated });
      this.notifyWatchers(group, change);
   }

   protected sendInitial() {
      const group = Object.freeze([...this._group]) as T;
      const change: IGroupChange = Object.freeze({ initial: true, changed: true, ...this._updated });
      this.notifyWatchers(group, change);
   }
   // ---------------------------------------------
   //                   Pipeline
   // ---------------------------------------------
   public pipe(): IPipeline<T, T, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<T, IGroupChange, A>
   ): IPipeline<T, A, this, typeof pipes>;
   public pipe<A, B>(
      ...pipes: TPipeArray_2<T, IGroupChange, A, B>
   ): IPipeline<T, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IGroupChange, A, B, D>
   ): IPipeline<T, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IGroupChange, A, B, D, E>
   ): IPipeline<T, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IGroupChange, A, B, D, E, F>
   ): IPipeline<T, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IGroupChange, A, B, D, E, F, G>
   ): IPipeline<T, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IGroupChange, A, B, D, E, F, G, H>
   ): IPipeline<T, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IGroupChange, A, B, D, E, F, G, H, I>
   ): IPipeline<T, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IGroupChange, A, B, D, E, F, G, H, I, J>
   ): IPipeline<T, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IGroupChange, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<T, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<T, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
