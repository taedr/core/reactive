import { getRuntimeId } from '@taedr/utils';
import { AReadonlyBus, EDetachMessage, IPossibleChange, IReadonlyBus } from '../../core';

// ==================================================
//                   Latest
// ==================================================
export class Latest<T, S extends readonly IReadonlyBus<T>[]> extends AReadonlyBus<T> {
   protected _id = getRuntimeId();
   protected _initial?: T;

   public get shutdown(): string { return this._shutdown ?? ``; }
   public set shutdown(message: string) { this.setShutdown(message); }

   public get sources(): Readonly<S> { return this._sources; }

   /** Transfers value as it come from any source */
   constructor(
      protected _sources: S
   ) {
      super();
      Object.freeze(_sources);
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch(): void {
      this.send = (value: T) => this._initial = value;

      for (const source of this._sources) {
         source.watch({
            id: this._id,
            value: value => this.send(value),
            detach: mess => this.onSourceDetach(source, mess),
         });
      }

      delete (<any>this).send;
      if (this._initial !== undefined) {
         const change: IPossibleChange = Object.freeze({ initial: true });
         this.notifyWatchers(this._initial, change)
         delete this._initial;
      }
   }

   protected onLastDetach(): void {
      this._sources.forEach(s => s.watchers.detach(this._id));
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onSourceDetach(source: IReadonlyBus<T>, mess: string) {
      if (mess === this._id) return; // #Return#
      if (this._sources.some(s => s.watchers.has(this._id))) return; // #Return#
      this.setShutdown(EDetachMessage.Sources);
   }
}
