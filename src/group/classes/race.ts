import { getRuntimeId } from '@taedr/utils';
import { AReadonlyBus, Reactive, EDetachMessage, IReadonlyBus } from '../../core';


// ==================================================
//                   Race
// ==================================================
export class Race<T> extends AReadonlyBus<T> {
   protected _id = getRuntimeId();
   protected _sources: IReadonlyBus<T>[];

   /** Transfers values only from winner (source which emitted first)  */
   constructor(
      sources: IReadonlyBus<T>[]
   ) {
      super();
      this._sources = [...sources];
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch(): void {
      if (this._sources.length === 1) return this.watchFirst(); // #Return#

      for (const source of this._sources) {
         source.watch({
            id: this._id,
            value: value => this.onSourceValue(source, value),
            detach: message => this.onSourceDetach(source, message),
         });
      }
   }

   protected onLastDetach(): void {
      this._sources.forEach(s => s.watchers.detach(this._id));
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected watchFirst() {
      this._sources[0].watch({
         id: this._id,
         value: value => this.send(value),
         detach: message => this.onWinnerDetach(message),
      });
   }

   protected onSourceValue(source: IReadonlyBus<T>, value: T) {
      this.onLastDetach();
      this._sources = [source];
      this.watchFirst();
      if (!(source instanceof Reactive)) this.send(value);
   }

   protected onWinnerDetach(message: string) {
      if (message === this._id) return; // #Return#
      this.setShutdown(message);
   }

   protected onSourceDetach(source: IReadonlyBus<T>, message: string) {
      if (message === this._id) return; // #Return#
      this._sources = this._sources.filter(s => s !== source);
      if (this._sources.length) return; // #Return#
      this.setShutdown(EDetachMessage.Sources);
   }

   protected setShutdown(message: string) {
      this._sources = [];
      super.setShutdown(message);
   }
}
