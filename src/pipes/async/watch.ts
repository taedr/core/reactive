import { getRuntimeId, TMapper } from '@taedr/utils';
import { AAsyncPipe, IReactiveArray, IReadonlyBus, IReadonlyReactive, IReadonlyReactiveArray, Reactive, ReactiveArray } from "../../core";

// ==================================================
//                   Factory
// ==================================================
/**
 * Converts incoming value to the source, watches it and transfers received values further.
 * New source will cause detaching from the previous source
 */
export function watch<I, O, S extends IReadonlyBus<O>>(
   getSource: TMapper<I, S>,
) {
   return new WatchReplace<I, O, S>(getSource);
}
/**
 * Converts incoming value to the source, watches it and transfers received values further.
 * Starts transferring values from next source when detached from previous
 */
export function watchSequential<I, O, S extends IReadonlyBus<O>>(
   getSource: TMapper<I, S>,
) {
   return new WatchSequential<I, O, S>(getSource);
}
/**
 * Converts incoming value to the source, watches it and transfers received values further.
 * Will transfer values from all received sources parallelly
 */
export function watchParallel<I, O, S extends IReadonlyBus<O>>(
   getSource: TMapper<I, S>,
) {
   return new WatchParallel<I, O, S>(getSource);
}
// ==================================================
//                   Core
// ==================================================
export abstract class AWatch<I, O, S extends IReadonlyBus<O>> extends AAsyncPipe<I, O> {
   protected _id = getRuntimeId();

   constructor(
      protected _getSource: TMapper<I, S>,
   ) {
      super();
   }
}
// ==================================================
//                   Replace
// ==================================================
export interface IWatchEntry<I, O, S extends IReadonlyBus<O>> {
   value: I;
   source: S;
}

export class WatchReplace<I, O, S extends IReadonlyBus<O>> extends AWatch<I, O, S> {
   protected _entry = new Reactive<IWatchEntry<I, O, S> | null>(null);

   public get entry(): IReadonlyReactive<IWatchEntry<I, O, S> | null> { return this._entry; }

   protected transform(value: I) {
      const source = this._getSource(value);
      const prev = this._entry.value;
      const entry = Object.freeze({ value, source });

      if (prev) prev.source.watchers.detach(this._id);

      this.setActive(true);
      this._entry.send(entry);
      source.watch({
         id: this._id,
         value: value => this.send(value),
         detach: message => this.onSourceDetach(message)
      });
   }

   protected onSourceDetach(message: string) {
      if (message === this._id) return;
      this._entry.send(null);
      this.setActive(false);
   }

   protected deactivate(): void {
      if (!this._active.value) return;
      this._entry.value?.source.watchers.detach(this._id);
      this._entry.send(null);
      this.setActive(false);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._entry.shutdown = message;
   }
}
// ==================================================
//                   Group
// ==================================================
export abstract class AWatchGroup<I, O, S extends IReadonlyBus<O>, E extends IWatchEntry<I, O, S>> extends AWatch<I, O, S> {
   protected _entries = new ReactiveArray<E>();

   protected abstract onSourceValue(value: O, entry: E): void;
   protected abstract createEntry(value: I): E;

   protected transform(value: I) {
      const entry = Object.freeze(this.createEntry(value));

      this.setActive(true);
      this._entries.add([entry]);
      entry.source.watch({
         id: this._id,
         value: value => this.onSourceValue(value, entry),
         detach: message => this.onSourceDetach(message, entry)
      });
   }

   protected onSourceDetach(message: string, entry: E) {
      if (message === this._id) return;
      this._entries.delete([entry]);
      if (!this._entries.size) this.setActive(false);
   }

   protected deactivate(): void {
      if (!this._active.value) return;
      for (const entry of this._entries.value) {
         entry.source.watchers.detach(this._id);
      }
      this._entries.send([]);
      this.setActive(false);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._entries.shutdown = message;
   }
}
// ==================================================
//                   Parallel
// ==================================================
export class WatchParallel<I, O, S extends IReadonlyBus<O>> extends AWatchGroup<I, O, S, IWatchEntry<I, O, S>> {
   public get entries(): IReadonlyReactiveArray<IWatchEntry<I, O, S>> {
      return this._entries;
   }

   protected onSourceValue(value: O): void {
      this.send(value)
   }

   protected createEntry(value: I): IWatchEntry<I, O, S> {
      return { value, source: this._getSource(value) };
   }
}
// ==================================================
//                   Sequential
// ==================================================
export interface IReadonlyWatchSequentialEntry<I, O, S extends IReadonlyBus<O>> extends IWatchEntry<I, O, S> {
   buffer: IReadonlyReactiveArray<O>;
}

interface IWatchSequentialEntry<I, O, S extends IReadonlyBus<O>> extends IReadonlyWatchSequentialEntry<I, O, S> {
   buffer: IReactiveArray<O>;
}

export class WatchSequential<I, O, S extends IReadonlyBus<O>> extends AWatchGroup<I, O, S, IWatchSequentialEntry<I, O, S>> {
   public get entries(): IReadonlyReactiveArray<IReadonlyWatchSequentialEntry<I, O, S>> { return this._entries; }

   protected onSourceValue(output: O, entry: IWatchSequentialEntry<I, O, S>) {
      if (entry === this._entries.value[0]) {
         this.send(output);
      } else {
         entry.buffer.add([output]);
      }
   }

   protected createEntry(value: I): IWatchSequentialEntry<I, O, S> {
      return { value, source: this._getSource(value), buffer: new ReactiveArray<O>() };
   }

   protected onSourceDetach(message: string, entry: IWatchSequentialEntry<I, O, S>) {
      if (message === this._id) return;
      if (entry === this._entries.value[0]) {
         for (const entry of this._entries.value) {
            const active = entry.source.watchers.has(this._id);
            this.sendEntryBuffer(entry);
            if (active) break;
            this._entries.delete([entry]);
         }
      } else if (entry.buffer.size === 0) {
         this._entries.delete([entry]);
      }

      if (!this._entries.size) this.setActive(false);
   }

   protected sendEntryBuffer(entry: IWatchSequentialEntry<I, O, S>) {
      while (entry.buffer.size) {
         if (this._shutdown) break;
         const value = entry.buffer.delete({ index: 0 }).deleted[0].values[0];
         this.send(value);
      }
   }
}
