import { getRuntimeId, IS, TMapper } from '@taedr/utils';
import { AAsyncPipe, IReadonlyBus, IReadonlyReactiveArray, ReactiveArray } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** After receiving a value will pack it into an array, wait for specified number of milliseconds before transferring. If during that time a new value will appear, it will be added to the array. */
export function pack<T>(durationMs: number): PackDuration<T>;
/** After receiving value will map it to the source(liberator). This and further received values will be packed into an array and transferred when the liberator emits. */
export function pack<T, L extends IReadonlyBus<unknown>>(
   getLiberator: TMapper<T, L>
): PackLiberator<T, L>;
export function pack<T>(a: number | TMapper<T, IReadonlyBus<unknown>>) {
   if (IS.number(a)) {
      return new PackDuration(a);
   } else {
      return new PackLiberator(a);
   }
}
// ==================================================
//                   Core
// ==================================================
export abstract class APack<T> extends AAsyncPipe<T, readonly T[]> {
   protected _pack = new ReactiveArray<T>();

   public get pack(): IReadonlyReactiveArray<T> { return this._pack; }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._pack.shutdown = message;
   }
}
// ==================================================
//                   Time
// ==================================================
export class PackDuration<T> extends APack<T> {
   protected _timerId?: any;

   public get durationMs() { return this._durationMs; }

   constructor(
      protected _durationMs: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      this.setActive(true);
      this._pack.add([value]);

      if (this._timerId !== undefined) return;

      this._timerId = setTimeout(() => {
         this.send(this._pack.value);
         this.deactivate();
      }, this._durationMs);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected deactivate() {
      if (!this._active.value) return;
      clearTimeout(this._timerId);
      delete this._timerId;
      this._pack.send([]);
      this.setActive(false);
   }
}
// ==================================================
//                   Liberator
// ==================================================
export class PackLiberator<T, L extends IReadonlyBus<unknown>> extends APack<T> {
   protected _liberator: L | null = null;
   protected _id = getRuntimeId();

   public get liberator() { return this._liberator; }

   constructor(
      protected _getLiberator: TMapper<T, L>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      this.setActive(true);
      this._pack.add([value]);

      if (this._liberator) return;

      this._liberator = this._getLiberator(value);
      this._liberator.watch({
         id: this._id,
         value: () => this.release(),
         detach: message => this.releaseByDetach(message),
      });
   }

   protected release() {
      this.send(this._pack.value);
      this.deactivate();
   }

   protected releaseByDetach(message: string) {
      if (message === this._id) return;
      this.release();
   }

   protected deactivate() {
      if (!this._active.value) return;
      this.liberator?.watchers.detach(this._id);
      this._pack.send([]);
      this.setActive(false);
   }
}
