import { getRuntimeId, IS, TMapper } from '@taedr/utils';
import { AAsyncPipe, EDetachMessage, IReactive, IReadonlyBus, IReadonlyReactive, Reactive } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** After receiving value will wait for specified number of milliseconds before emitting it.
   If during that time new value will appear, it will replace the old one and restart the timer */
export function debounce<T>(durationMs: number): DebounceDuration<T>;
/**  After receiving value will wait until mapped source (liberator) emits before transferring value further.
If a new value will appear while a liberator not yet emitted, it will replace the old value and be mapped to the new liberator. */
export function debounce<T, L extends IReadonlyBus<unknown>>(
   getLiberator: TMapper<T, L>
): DebounceLiberator<T, L>;
export function debounce<T>(a: number | TMapper<T, IReadonlyBus<unknown>>) {
   if (IS.number(a)) {
      return new DebounceDuration(a);
   } else {
      return new DebounceLiberator(a);
   }
}
// ==================================================
//                   Core
// ==================================================
export type TDebounceCandidateState = `PENDING` | `COMPLETED` | `ABORTED`;

export abstract class ADebounce<T> extends AAsyncPipe<T, T> {
   protected abstract _candidate: IReactive<unknown>;

   protected setShutdown(message: string): void {
      this._candidate.shutdown = message;
      super.setShutdown(message);
   }
}
// ==================================================
//                   Duration
// ==================================================
export interface IDebounceDurationCandidate<T> {
   state: IReadonlyReactive<TDebounceCandidateState>;
   value: T;
}

interface IInDebounceDurationCandidate<T> extends IDebounceDurationCandidate<T> {
   state: IReactive<TDebounceCandidateState>;
}

export class DebounceDuration<T> extends ADebounce<T> {
   protected _timerId?: any;
   protected _candidate = new Reactive<IInDebounceDurationCandidate<T> | null>(null);

   public get candidate(): IReadonlyReactive<IDebounceDurationCandidate<T> | null> {
      return this._candidate;
   }

   public get durationMs() { return this._durationMs; }

   constructor(
      protected _durationMs: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      const candidate = Object.freeze({
         value,
         state: new Reactive<TDebounceCandidateState>(`PENDING`)
      });

      this.abort();
      this.setActive(true);
      this._candidate.send(candidate)
      clearTimeout(this._timerId);
      this._timerId = setTimeout(() => {
         this.complete();
      }, this._durationMs);
   }

   protected deactivate(): void {
      if (!this._active.value) return;
      clearTimeout(this._timerId);
      delete this._timerId;
      this.abort();
      this._candidate.send(null);
      this.setActive(false);
   }

   protected complete() {
      const candidate = this._candidate.value as IInDebounceDurationCandidate<T>;
      this.send(candidate.value);
      candidate.state.send(`COMPLETED`);
      candidate.state.shutdown = EDetachMessage.Emittion;
      this._candidate.send(null);
      this.setActive(false);
   }

   protected abort() {
      const candidate = this._candidate.value;
      if (!candidate) return;
      candidate.state.send(`ABORTED`);
      candidate.state.shutdown = EDetachMessage.Emittion;
   }
}
// ==================================================
//                   Liberator
// ==================================================
export interface IDebounceLiberatorCandidate<T, L extends IReadonlyBus<unknown>> {
   state: IReadonlyReactive<TDebounceCandidateState>;
   value: T;
   liberator: L;
}

interface IInDebounceLiberatorCandidate<T, L extends IReadonlyBus<unknown>> extends IDebounceLiberatorCandidate<T, L> {
   state: IReactive<TDebounceCandidateState>;
}

export class DebounceLiberator<T, L extends IReadonlyBus<unknown>>
   extends ADebounce<T>{
   protected _id = getRuntimeId();
   protected _candidate = new Reactive<IInDebounceLiberatorCandidate<T, L> | null>(null);

   public get candidate(): IReadonlyReactive<IDebounceLiberatorCandidate<T, L> | null> {
      return this._candidate;
   }

   constructor(
      protected _getLiberator: TMapper<T, L>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      const liberator = this._getLiberator(value);
      const candidate = Object.freeze({
         value,
         state: new Reactive<TDebounceCandidateState>(`PENDING`),
         liberator
      });

      this.abort();
      this.setActive(true);
      this._candidate.send(candidate)
      liberator.watch({
         id: this._id,
         value: () => this.complete(),
         detach: message => this.completeByDetach(message),
      });
   }

   protected complete() {
      const candidate = this._candidate.value as IInDebounceLiberatorCandidate<T, L>;
      this.send(candidate.value);
      candidate.state.send(`COMPLETED`);
      candidate.state.shutdown = EDetachMessage.Emittion;
      candidate.liberator.watchers.detach(this._id);
      this._candidate.send(null);
      this.setActive(false);
   }

   protected abort() {
      const candidate = this._candidate.value;
      if (!candidate) return;
      candidate.state.send(`ABORTED`);
      candidate.state.shutdown = EDetachMessage.Emittion;
      candidate.liberator.watchers.detach(this._id);
   }

   protected completeByDetach(message: string) {
      if (message === this._id) return;
      this.complete();
   }

   protected deactivate() {
      if (!this._active.value) return;
      this.abort();
      this._candidate.send(null);
      this.setActive(false);
   }
}
