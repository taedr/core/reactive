import { getRuntimeId, IS, TMapper } from '@taedr/utils';
import { AAsyncPipe, APipe, IReactiveArray, IReadonlyBus, IReadonlyReactiveArray, ReactiveArray } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** Delays incoming value emition with specified number of milliseconds */
export function suppress<T>(durationMs: number): SuppressDuration<T>;
/** Delays incoming value emition until mapped source (liberator) will emit */
export function suppress<T, L extends IReadonlyBus<boolean>>(
   getLiberator: TMapper<T, IReadonlyBus<boolean>>
): SuppressLiberator<T, L>;
export function suppress<T>(a: number | TMapper<T, IReadonlyBus<boolean>>): APipe<T, T> {
   if (IS.function(a)) {
      return new SuppressLiberator(a);
   } else {
      return new SuppressDuration(a);
   }
}
// ==================================================
//                   Core
// ==================================================
export abstract class ASuppress<T> extends AAsyncPipe<T, T> {
   protected abstract _buffer: IReactiveArray<unknown>;

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
// ==================================================
//                   Duration
// ==================================================
export class SuppressDuration<T> extends ASuppress<T> {
   protected _buffer = new ReactiveArray<T>([]);
   protected _timerId?: any;

   public get durationMs() { return this._durationMs; }
   public get buffer(): IReadonlyReactiveArray<T> { return this._buffer; }

   constructor(
      protected _durationMs: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      this.setActive(true);
      this._buffer.add([value]);
      if (this._timerId === undefined) this.next();
   }

   protected deactivate(): void {
      if (!this._active.value) return;
      clearTimeout(this._timerId);
      delete this._timerId;
      this._buffer.send([]);
      this.setActive(false);
   }

   protected next() {
      this._timerId = setTimeout(this.consume.bind(this), this._durationMs);
   }

   protected consume() {
      const [value] = this._buffer.delete({ index: 0 }).deleted[0].values;
      const active = this._buffer.size > 0;

      delete this._timerId;

      this.send(value as T);

      if (active) {
         this.next();
      } else {
         this.setActive(false);
      }
   }
}
// ==================================================
//                   Liberator
// ==================================================
export interface ISuppressedEntry<T, L extends IReadonlyBus<boolean>> {
   value: T;
   liberator: L;
}

export class SuppressLiberator<T, L extends IReadonlyBus<boolean>> extends ASuppress<T> {
   protected _id = getRuntimeId();
   protected _buffer = new ReactiveArray<ISuppressedEntry<T, L>>();

   public get buffer(): IReadonlyReactiveArray<ISuppressedEntry<T, L>> { return this._buffer; }

   constructor(
      protected _getLiberator: TMapper<T, L>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      const liberator = this._getLiberator(value);
      const entry: ISuppressedEntry<T, L> = Object.freeze({ value, liberator });

      this.setActive(true);
      this._buffer.add([entry]);
      entry.liberator.watch({
         id: this._id,
         value: () => this.liberate(entry),
         detach: message => this.liberateByDetach(message, entry),
      });
   }

   protected liberate(entry: ISuppressedEntry<T, L>) {
      this._buffer.delete([entry]);
      this.detachEntry(entry);
      this.send(entry.value);
      if (!this._buffer.size) this.setActive(false);
   }

   protected liberateByDetach(message: string, entry: ISuppressedEntry<T, L>) {
      if (message === this._id) return;
      this.liberate(entry);
   }

   protected detachEntry(entry: ISuppressedEntry<T, L>) {
      entry.liberator.watchers.detach(this._id);
   }

   protected deactivate(): void {
      if (!this._active.value) return;

      for (const entry of this._buffer.value) {
         this.detachEntry(entry);
      }

      this._buffer.send([]);
      this.setActive(false);
   }
}
