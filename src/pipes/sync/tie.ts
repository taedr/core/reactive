import { getRuntimeId } from '@taedr/utils';
import { IReadonlyBus, TReadonlyBusArray_1, TReadonlyBusArray_10, TReadonlyBusArray_2, TReadonlyBusArray_3, TReadonlyBusArray_4, TReadonlyBusArray_5, TReadonlyBusArray_6, TReadonlyBusArray_7, TReadonlyBusArray_8, TReadonlyBusArray_9 } from '../../core';
import { APipe } from "../../core/pipeline/pipe/sync";

// ==================================================
//                   Factory
// ==================================================
export function tie<A>(
   sources: TReadonlyBusArray_1<A>
): Tier<A>;
export function tie<A, B>(
   sources: TReadonlyBusArray_2<A, B>
): Tier<A | B>;
export function tie<A, B, C>(
   sources: TReadonlyBusArray_3<A, B, C>
): Tier<A | B | C>;
export function tie<A, B, C, D>(
   sources: TReadonlyBusArray_4<A, B, C, D>
): Tier<A | B | C | D>;
export function tie<A, B, C, D, E>(
   sources: TReadonlyBusArray_5<A, B, C, D, E>
): Tier<A | B | C | D | E>;
export function tie<A, B, C, D, E, F>(
   sources: TReadonlyBusArray_6<A, B, C, D, E, F>
): Tier<A | B | C | D | E | F>;
export function tie<A, B, C, D, E, F, G>(
   sources: TReadonlyBusArray_7<A, B, C, D, E, F, G>
): Tier<A | B | C | D | E | F | G>;
export function tie<A, B, C, D, E, F, G, H>(
   sources: TReadonlyBusArray_8<A, B, C, D, E, F, G, H>
): Tier<A | B | C | D | E | F | G | H>;
export function tie<A, B, C, D, E, F, G, H, I>(
   sources: TReadonlyBusArray_9<A, B, C, D, E, F, G, H, I>
): Tier<A | B | C | D | E | F | G | H | I>;
export function tie<A, B, C, D, E, F, G, H, I, J>(
   sources: TReadonlyBusArray_10<A, B, C, D, E, F, G, H, I, J>
): Tier<A | B | C | D | E | F | G | H | I | J>;
export function tie<T>(sources: IReadonlyBus<T>[]): Tier<T> {
   return new Tier<T>(sources);
}
// ==================================================
//                   Pipe
// ==================================================
export class Tier<T> extends APipe<T, T>  {
   protected _id = getRuntimeId();

   public set value(value: T) { this.send(value); }
   public get value(): T { return undefined as any; }
   public set shutdown(message: string) { this.manualShutdown(message); }
   public get shutdown() { return this._shutdown ?? ``; }

   constructor(
      protected _sources: IReadonlyBus<T>[]
   ) {
      super();
      for (const source of this._sources) {
         source.watch({
            id: this._id,
            value: value => this.transform(value)
         });
      }
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      this.send(value);
   }

   protected deactivate(pipelineId: string): void {
      this._watchers.delete(pipelineId);
   }

   /* In order to avoid addition check in pipeline */
   protected setShutdown(message: string): void { }

   protected manualShutdown(message: string) {
      super.setShutdown(message);
      for (const source of this._sources) {
         source.watchers.detach(this._id);
      }
   }
}
