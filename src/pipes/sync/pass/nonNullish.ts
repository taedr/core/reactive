import { IS } from '@taedr/utils';
import { APass } from './core';
import { IPossibleChange } from 'src/core';

// ==================================================
//                   Factory
// ==================================================
/**  Passes value if it is not equal to `null` or `undefined`
 */
export function passNonNullish<T, C extends IPossibleChange = IPossibleChange>() {
   return new PassNonNullish<NonNullable<T>, C>();
}
// ==================================================
//                   Pipe
// ==================================================
export class PassNonNullish<T, C extends IPossibleChange> extends APass<T, C> {
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      if (!IS.null(value)) this.notifyWatchers(value, change);
   }
}
