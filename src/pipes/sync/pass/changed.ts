import { TMapper } from '@taedr/utils';
import { APass } from './core';
import { PassIf } from './if';
import { IPossibleChange } from 'src/core';

// ==================================================
//                   Factory
// ==================================================
/** Passes value if `changed` in `change` is `true` or `undefined` */
export function passIfChanged<T, C extends IPossibleChange = IPossibleChange>(): PassIf<T, C>;
/**
 * Passes value if result of 'getHash' for current value is different from result for the previous
 * @param getHash - function that returns the uniqueness identifier
 */
export function passIfChanged<T, H, C extends IPossibleChange = IPossibleChange>(
   getHash: TMapper<T, H>
): PassIfChanged<T, H, C>
export function passIfChanged<T, H, C extends IPossibleChange = IPossibleChange>(
   getHash?: TMapper<T, H>
): APass<T, C> {
   if (getHash) return new PassIfChanged<T, H, C>(getHash);
   return new PassIf<T, C>((_, { changed }) => changed === undefined || changed === true);
}
// ==================================================
//                   Pipe
// ==================================================
export class PassIfChanged<T, H, C extends IPossibleChange> extends APass<T, C> {
   protected _prev?: H;

   public get prev() { return this._prev; }

   constructor(
      protected _getHash: TMapper<T, H>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      const hash = this._getHash(value);
      const isNotEqual = hash !== this._prev;
      this._prev = hash;

      if (isNotEqual) this.notifyWatchers(value, change);
   }

   protected deactivate(): void {
      delete this._prev;
   }
}
