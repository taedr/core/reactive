import { IBusChange, IPossibleChange } from 'src/core';
import { APass } from './core';

// ==================================================
//                   Factory
// ==================================================
export type TPassCheck<T, C extends IBusChange> = (value: T, change: C) => boolean;


/**  Passes value if check returns `true`
 * @param check - function which checks if the value can be passed further
 */
export function passIf<T, C extends IPossibleChange = IPossibleChange>(
   check: TPassCheck<T, C>
): PassIf<T, C> {
   return new PassIf(check);
}
// ==================================================
//                   Pipe
// ==================================================
export class PassIf<T, C extends IPossibleChange> extends APass<T, C> {
   constructor(
      protected _check: TPassCheck<T, C>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      const passing = this._check(value, change);
      if (passing) this.notifyWatchers(value, change);
   }
}
