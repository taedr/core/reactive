
import { IS, TMapper } from "@taedr/utils";
import { IPossibleChange, IReadonlyReactiveArray, ReactiveArray } from "../../../core";
import { APass } from "./core";

// ==================================================
//                   Factory
// ==================================================
/** Creates lifo `buffer` of specified size, which remembers every passed value.
 * Passes value only if it is not present in the `buffer`
 * @param size - buffer capacity
 * @param getHash - function that returns the uniqueness identifier. By default, returns the incoming value itself
 */
export function passUnique<T, C extends IPossibleChange = IPossibleChange>(
   bufferSize?: number
   ): PassUnique<T, T, C>;
export function passUnique<T, H, C extends IPossibleChange = IPossibleChange>(
   getHash: TMapper<T, H>,
   bufferSize?: number
): PassUnique<T, T, C>;
export function passUnique<T, H, C extends IPossibleChange = IPossibleChange>(
   a: undefined | number | TMapper<T, H>,
   b?: number
): PassUnique<T, H, C> {
   if (a === undefined || IS.number(a)) return new PassUnique(a);
   return new PassUnique(b, a);
}
// ==================================================
//                   Buffer
// ==================================================
export class PassUnique<T, H, C extends IPossibleChange> extends APass<T, C> {
   protected _buffer = new ReactiveArray<H>();

   public get buffer(): IReadonlyReactiveArray<H> { return this._buffer; }
   public get maxSize() { return this._maxSize ?? Infinity; }

   constructor(
      protected _maxSize?: number,
      protected _getHash?: TMapper<T, H>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      const hash = this._getHash ? this._getHash(value) : value as unknown as H;
      const isPass = !this._buffer.has(hash);
      const isFull = this._buffer.size === this._maxSize;

      if (this._maxSize === undefined) {
         if (isPass) {
            this._buffer.add([hash]);
            this.notifyWatchers(value, change);
         }
      } else {
         this._buffer.add([hash]);
         if (isFull) this._buffer.delete({ index: 0 });
         if (isPass) this.notifyWatchers(value, change);
      }
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected deactivate(): void {
      this._buffer.send([]);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
