import { getRuntimeId } from "@taedr/utils";
import { IPossibleChange, IReadonlyReactive } from "../../../core";
import { APass } from "./core";

// ==================================================
//                   Factory
// ==================================================
/** Passes values if `source` last emitted value was `true`.
 * By default passes values if source not yet emitted.
 * @param source - bus which determines passing behavior
 *  */
export function passSwitch<T, S extends IReadonlyReactive<boolean>, C extends IPossibleChange = IPossibleChange>(
   source: S
) {
   return new PassSwitch<T, S, C>(source);
}
// ==================================================
//                   Pipe
// ==================================================
export class PassSwitch<T, S extends IReadonlyReactive<boolean>, C extends IPossibleChange> extends APass<T, C> {
   protected _id = getRuntimeId();

   public get pass() { return this._pass; }

   constructor(
      protected _pass: S
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      if (this._pass.value) this.notifyWatchers(value, change);
   }
}
