import { APipe, IReadonlyReactive, Reactive } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** Pairs previous and incoming values */
export function pair<T>() {
   return new Pair<T>();
}
// ==================================================
//                  Pipe
// ==================================================
export class Pair<T> extends APipe<T, [undefined | T, T]> {
   protected _prev = new Reactive<T | undefined>(undefined);

   public get prev(): IReadonlyReactive<T | undefined> { return this._prev; }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      const prev = this._prev.value;
      this._prev.send(value);
      this.send([prev, value]);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
