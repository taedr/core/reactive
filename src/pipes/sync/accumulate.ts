import { APipe, IReadonlyReactiveArray, ReactiveArray } from "../../core";

// ==================================================
//                   Factory
// ==================================================
/** `BUS` `COLD` Accumulates values produced by previous pipe
 * (by pipeline source if accumulator is a single pipe) into array.
 * Emits once before pipeline shutdown. */
export function accumulate<T>(): Accamulator<T> {
   return new Accamulator<T>();
}
// ==================================================
//                   Pipe
// ==================================================
export class Accamulator<T> extends APipe<T, readonly T[]> {
   protected _buffer = new ReactiveArray<T>();

   public get buffer(): IReadonlyReactiveArray<T> { return this._buffer; }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T): void {
      this._buffer.add([value]);
   }

   protected setShutdown(message: string) {
      this.send(this._buffer.value);
      this._buffer.send([]);
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
