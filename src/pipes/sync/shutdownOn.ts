import { getRuntimeId, IS } from "@taedr/utils";
import { EDetachMessage, IPossibleChange, IReadonlyBus } from "../../core";
import { APipe } from "../../core/pipeline/pipe/sync";

// ==================================================
//                   Factory
// ==================================================
export type TShutdownCheck<T, C extends IPossibleChange> = (value: T, change: C) => boolean | string;

/** Shutdowns pipeline when specified number of values passed through shutdowner
 * * If shutdownOn was triggered while pipeline still processes values asynchronously, no new values will be accepted from pipeline source, will wait until all asynchronous pipes finish processing their values before pipeline shutdown
  */
export function shutdownOn<T>(limit: number, message?: string): ShutdownOnLimit<T>;
/** Shutdowns pipeline if condition returns `true`
 * @include - determines, should value which caused `true` be processed by pipeline
 */
export function shutdownOn<T, C extends IPossibleChange>(check: TShutdownCheck<T, C>, include?: boolean): ShutdownOnTrue<T, C>;
/** Shutdowns pipeline when provided source emits any value   */
export function shutdownOn<T>(source: IReadonlyBus<string | unknown>): ShutdownOnEmit<T>;
export function shutdownOn<T>(
   a: number | TShutdownCheck<T, IPossibleChange> | IReadonlyBus<unknown>,
   b?: boolean | string
): AShutdownOn<T> {
   if (IS.number(a)) {
      return new ShutdownOnLimit(a, b as string);
   } else if (IS.function(a)) {
      return new ShutdownOnTrue(a, b as boolean);
   } else {
      return new ShutdownOnEmit(a);
   }
}
// ==================================================
//                   Core
// ==================================================
export abstract class AShutdownOn<T> extends APipe<T, T> {

}
// ==================================================
//                   Count
// ==================================================
export class ShutdownOnLimit<T> extends AShutdownOn<T> {
   protected _count = 0;
   protected _message?: string;

   public get count() { return this._count; }
   public get limit() { return this._limit; }

   constructor(
      protected _limit: number,
      message?: string
   ) {
      super();
      if (message) this._message = message;
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      const detach = ++this._count === this._limit;
      this.send(value);
      if (detach) this.setShutdown(this._message ?? EDetachMessage.Auto)
   }
}
// ==================================================
//                   True
// ==================================================
export class ShutdownOnTrue<T, C extends IPossibleChange> extends AShutdownOn<T> {
   public get include() { return this._include ?? true; }

   constructor(
      protected _check: TShutdownCheck<T, C>,
      protected _include?: boolean
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      const detach = this._check(value, change);

      if (detach) {
         const message = IS.string(detach) ? detach : EDetachMessage.Auto;
         if (this._include !== false) this.send(value);
         this.setShutdown(message)
      } else {
         this.send(value);
      }
   }
}
// ==================================================
//                   Emit
// ==================================================
export class ShutdownOnEmit<T> extends AShutdownOn<T> {
   protected _id = getRuntimeId();

   constructor(
      protected _source: IReadonlyBus<unknown | string>
   ) {
      super();
      _source.watch({
         id: this._id,
         value: value => this.onSourceChange(value),
         detach: message => this.onSourceDetach(message),
      });
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      this.send(value);
   }

   protected onSourceChange(value: unknown | string) {
      const message = IS.string(value) ? value : EDetachMessage.Auto;
      this._source.watchers.detach(this._id, message)
      this.setShutdown(message);
   }

   protected onSourceDetach(message: string) {
      if (message === this._id) return;
      this.setShutdown(message);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._source.watchers.detach(this._id);
   }
}
