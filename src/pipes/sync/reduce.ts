import { APipe, IReadonlyReactive, Reactive } from '../../core';

// ==================================================
//                   Factory
// ==================================================
export type TReduce<I, O> = (value: I, accumulator: O) => O;

/** Combines previous state with received value in order to create new state */
export function reduce<I, O>(mapper: TReduce<I, O>, initial: O) {
   return new Reducer(initial, mapper);
}
// ==================================================
//                   Core
// ==================================================
export class Reducer<I, O> extends APipe<I, O> {
   protected _state = new Reactive(this._initial);

   public get state(): IReadonlyReactive<O> { return this._state; }
   public get initial() { return this._initial; }

   constructor(
      protected _initial: O,
      protected _reducer: TReduce<I, O>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: I) {
      const reduced = this._reducer(value, this._state.value);
      this._state.send(reduced);
      this.send(reduced);
   }

   protected deactivate(): void {
      this._state.send(this._initial);
   }
}
