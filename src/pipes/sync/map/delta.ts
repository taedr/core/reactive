import { APipe, IBusChange, IReactiveObject, IReactiveObjectChange, IValue } from '../../../core';

// ==================================================
//                   Factory
// ==================================================
/**
 * Merges `value` and `change` into a single object
 */
export function mapDelta<T extends object, C extends IReactiveObjectChange<T>>(

): DeltaMapper<T, C> {
   return new DeltaMapper();
}
// ==================================================
//                   Pipe
// ==================================================
export class DeltaMapper<T extends object, C extends IReactiveObjectChange<object>> extends APipe<T, C['delta'], C> {
   constructor(
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(_: T, change: C) {
      this.send(change.delta);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
