import { TMapper, TNew } from '@taedr/utils';
import { AAsyncPipe, APipe, EDetachMessage, IKeyValue, IReactiveSuffix, IReadonlyBus, IReadonlyReactive, IReadonlyReactiveArray, Reactive, ReactiveArray } from '../../../core';
import { group } from '../../../group';
import { shutdownOn } from '../shutdownOn';
import { map } from './core';


// ==================================================
//                   Facctory
// ==================================================
export type TFieldsMappersRecord<I extends object, O extends object> = {
   [K in (keyof I & keyof O)]?: TMapper<I[K], O[K]>;
}

export type TFieldsMappersRecordAsync<I extends object, O extends object> = {
   [K in (keyof I & keyof O)]?: TMapper<IReadonlyReactive<I[K]>, IReadonlyBus<O[K]>>;
}

export function mapFields<I extends object, O extends object>(
   mappers: TFieldsMappersRecord<Required<I>, O>,
   buider?: TNew<O>
): FieldsMapper<I, O> {
   return new FieldsMapper(
      mappers as any,
      buider
   );
}

export function mapFieldsAsync<I extends object, O extends object>(
   mappers: TFieldsMappersRecordAsync<Required<I>, O>,
   buider?: TNew<O>
): AsyncFieldsMapper<I, O> {
   return new AsyncFieldsMapper(
      mappers as any,
      buider
   );
}
// ==================================================
//                   Sync
// ==================================================
export class FieldsMapper<I extends object, O extends object> extends APipe<I, O> {
   protected _builder?: TNew<O>;

   constructor(
      protected _mappers: TFieldsMappersRecord<I, O>,
      builder?: TNew<O>
   ) {
      super();
      if (builder) this._builder = builder;
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(input: I) {
      const output: any = new (this._builder ?? Object)();

      for (const key in input) {
         const mapper = (this._mappers as any)[key];
         const value = input[key];
         output[key] = mapper ? mapper(value) : value;
      }

      this.send(output);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
// ==================================================
//                   Async
// ==================================================
export interface IReadonlyAsyncFieldsModification<I extends object, O extends object> {
   input: I;
   mapped: IReadonlyReactive<O>;
}

export interface IAsyncFieldsModification<I extends object, O extends object>
   extends IReadonlyAsyncFieldsModification<I, O> {
   mapped: IReactiveSuffix<O>;
}


export class AsyncFieldsMapper<I extends object, O extends object> extends AAsyncPipe<I, O> {
   protected _buffer = new ReactiveArray<IAsyncFieldsModification<I, O>>();
   protected _builder?: TNew<O>;

   public get buffer(): IReadonlyReactiveArray<IReadonlyAsyncFieldsModification<I, O>> { return this._buffer; }

   constructor(
      protected _mappers: TFieldsMappersRecord<IReadonlyReactive<Required<I>>, IReadonlyBus<O>>,
      builder?: TNew<O>
   ) {
      super();
      if (builder) this._builder = builder;
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(input: I) {
      const output: any = new (this._builder ?? Object)();
      const buses: IReadonlyBus<IKeyValue<unknown>>[] = [];

      for (const key in input) {
         const mapper = (this._mappers as any)[key];
         const value = input[key];
         const reative = new Reactive(value);

         if (mapper) {
            const bus: IReadonlyBus<unknown> = mapper(reative);
            const pipe = bus.pipe(
               map(value => ({ key, value }))
            );
            buses.push(pipe);
         } else {
            output[key] = value
         }
      }

      const mapped = group(buses).pipe(
         shutdownOn(1),
         map(entries => {
            for (const { key, value } of entries) {
               output[key] = value;
            }
            return output;
         })
      ).suffix();

      const entry = Object.freeze({ input, mapped });

      this.setActive(true);
      this._buffer.add([entry]);
      mapped.watch(() => this.onChangeMapped(entry));
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onChangeMapped(
      entry: IAsyncFieldsModification<I, O>,
   ) {
      if (entry !== this._buffer.value[0]) return;

      this._buffer.delete({ index: 0 });
      this.send(entry.mapped.value);

      while (this._buffer.size) {
         const next = this._buffer.value[0];
         if (!next.mapped.shutdown) break;
         this._buffer.delete({ index: 0 });
         this.send(next.mapped.value);
      }

      if (!this._buffer.size) this.setActive(false);
   }

   protected deactivate(): void {
      if (!this._active.value) return;

      for (const entry of this._buffer.value) {
         entry.mapped.shutdown = EDetachMessage.Auto;
      }

      this._buffer.send([]);
      this.setActive(false);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
