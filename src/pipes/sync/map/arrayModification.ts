import { TMapper } from '@taedr/utils';
import { AAsyncPipe, APipe, EDetachMessage, IIndexAdd, IIndexUpdate, IReactiveArrayModification, IReactiveSuffix, IReadonlyBus, IReadonlyReactive, IReadonlyReactiveArray, Reactive, ReactiveArray } from '../../../core';
import { group } from '../../../group';
import { shutdownOn } from '../shutdownOn';
import { map } from './core';

// ==================================================
//                   Factory
// ==================================================
/**
 * Converts values in `added` and `updated` fields into new shape
 */
export function mapArrayModification<I, O>(
   mapper: TMapper<I, O>,
) {
   return new ArrayModificationMapper(mapper);
}
/**
 * * Converts values inside `added` and `deleted` to reactive
 * * `mapper` should return bus which emits converted value
 * * waits untill all buses emit their values and emits change afterwards
 */
export function mapArrayModificationAsync<I, O>(
   mapper: TMapper<IReadonlyReactive<I>, IReadonlyBus<O>>,
) {
   return new AsyncArrayModificationMapper(mapper);
}
// ==================================================
//                   Sync
// ==================================================
export class ArrayModificationMapper<I, O> extends APipe<IReactiveArrayModification<I>, IReactiveArrayModification<O>> {
   constructor(
      protected _mapper: TMapper<I, O>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(change: IReactiveArrayModification<I>) {
      const added: IIndexAdd<O>[] = [];
      const updated: IIndexUpdate<O>[] = [];

      for (let i = 0; i < change.added.length; i++) {
         const { index, values: origin } = change.added[i];
         const values: O[] = [];

         for (let j = 0; j < origin.length; j++) {
            const converted = this._mapper(origin[j]);
            values.push(converted);
         }

         added.push({ index, values });
      }

      for (let i = 0; i < change.updated.length; i++) {
         const { index, value: origin } = change.updated[i];
         updated.push({ index, value: this._mapper(origin) });
      }

      this.send({
         added,
         deleted: change.deleted,
         updated,
         moved: change.moved
      });
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
// ==================================================
//                   Async
// ==================================================
export interface IReadonlyAsyncArrayModification<I, O, S extends IReadonlyBus<O>> {
   mapped: IReadonlyReactive<IReactiveArrayModification<O>>;
   input: IReactiveArrayModification<I>;
   add: readonly IAsyncArrayModificationAddEntry<I, O, S>[];
   update: readonly IAsyncArrayModificationUpdateEntry<I, O, S>[];
}

export interface IAsyncArrayModification<I, O, S extends IReadonlyBus<O>>
   extends IReadonlyAsyncArrayModification<I, O, S> {
   mapped: IReactiveSuffix<IReactiveArrayModification<O>>;
}

export interface IAsyncArrayModificationAddEntry<I, O, S extends IReadonlyBus<O>> {
   index?: number;
   input: I;
   output: S;
}

export interface IAsyncArrayModificationUpdateEntry<I, O, S extends IReadonlyBus<O>>
   extends IAsyncArrayModificationAddEntry<I, O, S> {
   index: number;
}

export class AsyncArrayModificationMapper<I, O, S extends IReadonlyBus<O>>
   extends AAsyncPipe<IReactiveArrayModification<I>, IReactiveArrayModification<O>> {
   protected _buffer = new ReactiveArray<IAsyncArrayModification<I, O, S>>();

   public get buffer(): IReadonlyReactiveArray<IReadonlyAsyncArrayModification<I, O, S>> {
      return this._buffer;
   }

   constructor(
      protected _mapper: TMapper<IReadonlyReactive<I>, S>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(change: IReactiveArrayModification<I>) {
      const addBuses: IReadonlyBus<IIndexAdd<O>>[] = [];
      const updateBuses: IReadonlyBus<IIndexUpdate<O>>[] = [];
      const addEntries: IAsyncArrayModificationAddEntry<I, O, S>[] = [];
      const updateEntries: IAsyncArrayModificationUpdateEntry<I, O, S>[] = [];

      for (let i = 0; i < change.added.length; i++) {
         const { index, values: origin } = change.added[i];
         const buses: IReadonlyBus<O>[] = [];

         for (let j = 0; j < origin.length; j++) {
            const input = origin[j];
            const reactive = new Reactive(input);
            const output = this._mapper(reactive);
            const knownIndex = index ? index + j : index;
            const entry = Object.freeze({ index: knownIndex, input, output });

            buses.push(output);
            addEntries.push(entry)
         }

         const grouped = group(buses).pipe(
            map(values => ({ index, values })),
         );

         addBuses.push(grouped);
      }

      for (let i = 0; i < change.updated.length; i++) {
         const { index, value: input } = change.updated[i];
         const reactive = new Reactive(input);
         const output = this._mapper(reactive)
         const entry = Object.freeze({ index, input, output });
         const bus = output.pipe(
            map(value => ({ index, value }))
         );

         updateEntries.push(entry)
         updateBuses.push(bus);
      }

      const mapped = group([
         group(addBuses),
         group(updateBuses)
      ]).pipe(
         shutdownOn(1),
         map(([added, updated]) => ({
            added,
            deleted: change.deleted,
            updated,
            moved: change.moved
         }))
      ).suffix();

      const entry = Object.freeze({
         input: change,
         mapped,
         add: addEntries,
         update: updateEntries
      });


      this.setActive(true);
      this._buffer.add([entry]);
      mapped.watch(() => this.onChangeMapped(entry));
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onChangeMapped(
      entry: IAsyncArrayModification<I, O, S>,
   ) {
      if (entry !== this._buffer.value[0]) return;

      this._buffer.delete({ index: 0 });
      this.send(entry.mapped.value);

      while (this._buffer.size) {
         const next = this._buffer.value[0];
         if (!next.mapped.shutdown) break;
         this._buffer.delete({ index: 0 });
         this.send(next.mapped.value);
      }

      if (!this._buffer.size) this.setActive(false);
   }

   protected deactivate(): void {
      if (!this._active.value) return;

      for (const entry of this._buffer.value) {
         entry.mapped.shutdown = EDetachMessage.Auto;
      }

      this._buffer.send([]);
      this.setActive(false);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
