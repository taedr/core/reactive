import { APipe, IReactiveArrayModification } from '../../../../core';

// ==================================================
//                   Factory
// ==================================================
/* Wraps incoming value into array addition modification */
export function mapArrayAdd<T>(index?: number): ArrayAddMapper<T> {
   return new ArrayAddMapper(index);
}
/* Wraps incoming values into array addition modification */
export function mapArrayAddMultiple<T>(index?: number): ArrayAddMultipleMapper<T> {
   return new ArrayAddMultipleMapper(index);
}
// ==================================================
//                   Pipe
// ==================================================
export class ArrayAddMapper<T> extends APipe<T, IReactiveArrayModification<T>> {
   public get index() { return this._index; }

   constructor(
      protected _index?: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      this.send({
         added: [{ index: this._index, values: [value] }],
         updated: [],
         deleted: [],
         moved: []
      });
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
// ==================================================
//                   Pipe
// ==================================================
export class ArrayAddMultipleMapper<T> extends APipe<readonly T[], IReactiveArrayModification<T>> {
   public get index() { return this._index; }

   constructor(
      protected _index?: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(values: readonly T[]) {
      this.send({
         added: [{ index: this._index, values }],
         updated: [],
         deleted: [],
         moved: []
      });
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
