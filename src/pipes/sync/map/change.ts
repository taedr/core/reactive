import { APipe, IBusChange, IValue } from '../../../core';

// ==================================================
//                   Factory
// ==================================================
/**
 * Merges `value` and `change` into a single object
 */
export function mergeChange<T, C extends IBusChange>(): MergeChange<T, C> {
   return new MergeChange();
}
// ==================================================
//                   Pipe
// ==================================================
export class MergeChange<T, C extends IBusChange> extends APipe<T, C & IValue<T>, C> {
   constructor(
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      this.send({ value, ...change });
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
