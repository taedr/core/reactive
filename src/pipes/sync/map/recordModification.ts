import { TMapper } from '@taedr/utils';
import { AAsyncPipe, APipe, EDetachMessage, IKeyValue, IReactiveRecordModification, IReactiveSuffix, IReadonlyBus, IReadonlyReactive, IReadonlyReactiveArray, Reactive, ReactiveArray } from '../../../core';
import { group } from '../../../group';
import { shutdownOn } from '../shutdownOn';
import { map } from './core';

// ==================================================
//                   Factory
// ==================================================
/**
 * Converts values inside `added` and `deleted` to the new shape
 */
export function mapRecordModification<I, O>(
   mapper: TMapper<I, O>,
) {
   return new RecordModificationMapper(mapper);
}
/**
 * * Converts values inside `added` and `deleted` to reactive
 * * `mapper` should return bus which emits converted value
 * * waits untill all buses emit their values and emits change afterwards
 */
export function mapRecordModificationAsync<I, O>(
   mapper: TMapper<IReadonlyReactive<I>, IReadonlyBus<O>>,
) {
   return new AsyncRecordModificationMapper(mapper);
}
// ==================================================
//                   Sync
// ==================================================
export class RecordModificationMapper<I, O> extends APipe<IReactiveRecordModification<I>, IReactiveRecordModification<O>> {
   constructor(
      protected _mapper: TMapper<I, O>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(change: IReactiveRecordModification<I>) {
      const added: IKeyValue<O>[] = [];
      const updated: IKeyValue<O>[] = [];

      for (let i = 0; i < change.added.length; i++) {
         const { key, value: origin } = change.added[i];
         added.push({ key, value: this._mapper(origin) });
      }

      for (let i = 0; i < change.updated.length; i++) {
         const { key, value: origin } = change.updated[i];
         updated.push({ key, value: this._mapper(origin) });
      }

      this.send({
         added,
         deleted: change.deleted,
         updated,
      });
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
// ==================================================
//                   Async
// ==================================================
export interface IReadonlyAsyncRecordModification<I, O, S extends IReadonlyBus<O>> {
   input: IReactiveRecordModification<I>;
   mapped: IReadonlyReactive<IReactiveRecordModification<O>>;
   add: readonly IAsyncRecordModificationEntry<I, O, S>[];
   update: readonly IAsyncRecordModificationEntry<I, O, S>[];
}

export interface IAsyncRecordModification<I, O, S extends IReadonlyBus<O>>
   extends IReadonlyAsyncRecordModification<I, O, S> {
   mapped: IReactiveSuffix<IReactiveRecordModification<O>>;
}

export interface IAsyncRecordModificationEntry<I, O, S extends IReadonlyBus<O>> {
   key: string;
   input: I;
   output: S;
}


export class AsyncRecordModificationMapper<I, O, S extends IReadonlyBus<O>>
   extends AAsyncPipe<IReactiveRecordModification<I>, IReactiveRecordModification<O>> {
   protected _buffer = new ReactiveArray<IAsyncRecordModification<I, O, S>>();

   public get buffer(): IReadonlyReactiveArray<IReadonlyAsyncRecordModification<I, O, S>> {
      return this._buffer;
   }

   constructor(
      protected _mapper: TMapper<IReadonlyReactive<I>, IReadonlyBus<O>>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(change: IReactiveRecordModification<I>) {
      const addBuses: IReadonlyBus<IKeyValue<O>>[] = [];
      const updateBuses: IReadonlyBus<IKeyValue<O>>[] = [];
      const addEntries: IAsyncRecordModificationEntry<I, O, S>[] = [];
      const updateEntries: IAsyncRecordModificationEntry<I, O, S>[] = [];

      for (let i = 0; i < change.added.length; i++) {
         const { key, value: origin } = change.added[i];
         const reactive = new Reactive(origin);
         const bus = this._mapper(reactive).pipe(
            map(value => ({ key, value })),
         );

         addBuses.push(bus);
      }

      for (let i = 0; i < change.updated.length; i++) {
         const { key, value: origin } = change.updated[i];
         const reactive = new Reactive(origin);
         const bus = this._mapper(reactive).pipe(
            map(value => ({ key, value }))
         );

         updateBuses.push(bus);
      }

      const mapped = group([
         group(addBuses),
         group(updateBuses)
      ]).pipe(
         shutdownOn(1),
         map(([added, updated]) => ({
            added,
            deleted: change.deleted,
            updated,
         }))
      ).suffix();

      const entry = Object.freeze({
         input: change,
         mapped,
         add: addEntries,
         update: updateEntries
      });


      this.setActive(true);
      this._buffer.add([entry]);
      mapped.watch(() => this.onChangeMapped(entry));
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected onChangeMapped(
      entry: IAsyncRecordModification<I, O, S>,
   ) {
      if (entry !== this._buffer.value[0]) return;

      this._buffer.delete({ index: 0 });
      this.send(entry.mapped.value);

      while (this._buffer.size) {
         const next = this._buffer.value[0];
         if (!next.mapped.shutdown) break;
         this._buffer.delete({ index: 0 });
         this.send(next.mapped.value);
      }

      if (!this._buffer.size) this.setActive(false);
   }

   protected deactivate(): void {
      if (!this._active.value) return;

      for (const entry of this._buffer.value) {
         entry.mapped.shutdown = EDetachMessage.Auto;
      }

      this._buffer.send([]);
      this.setActive(false);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
