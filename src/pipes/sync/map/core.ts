import { APipe, IBusChange, IPossibleChange } from '../../../core';

// ==================================================
//                   Factory
// ==================================================
export type TMapperPipe<I, O, C extends IBusChange> = (value: I, change: C) => O;

/**
 * Converts received value to different shape and emits this shape.
 */
export function map<I, O, C extends IPossibleChange = IPossibleChange>(
   mapper: TMapperPipe<I, O, C>
): Mapper<I, O, C> {
   return new Mapper(mapper);
}
// ==================================================
//                   Pipe
// ==================================================
export class Mapper<I, O, C extends IPossibleChange> extends APipe<I, O, C> {
   constructor(
      protected _mapper: TMapperPipe<I, O, C>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: I, change: C) {
      const mapped = this._mapper(value, change);
      this.send(mapped);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
