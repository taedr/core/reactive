import { APipe } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** Receives an iterable and emits its values one by one */
export function iterate<T>() {
   return new Iterator<T>();
}
// ==================================================
//                  Pipe
// ==================================================
export class Iterator<T> extends APipe<Iterable<T>, T> {
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(values: Iterable<T>) {
      for (const value of values) {
         if (!this._shutdown) this.send(value);
      }
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
}
