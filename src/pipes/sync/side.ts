import { APipe, IBusChange } from '../../core';

// ==================================================
//                   Factory
// ==================================================
export type TSideFn<T, C extends IBusChange> = (value: T, change: C) => void;

/**
 * Performs a side effect
 */
export function side<T, C extends IBusChange>(fn: TSideFn<T, C>) {
   return new Side<T, C>(fn);
}
// ==================================================
//                   Pipe
// ==================================================
export class Side<T, C extends IBusChange> extends APipe<T, T, C> {
   constructor(
      protected _fn: TSideFn<T, C>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      this._fn(value, change);
      this.notifyWatchers(value, change);
   }
}
