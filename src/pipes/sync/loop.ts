import { getRuntimeId } from '@taedr/utils';
import { APipe, IBus, IPipeline } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** integrates incoming value and values emitted by provided sources into an array.
 * Starts emitting when first value from pipeline received.
 * After that will emit on any change of provided source.
 */
export function loop<I, O, S extends IBus<I>>(
   pipeline: IPipeline<I, O, S>
): Loop<I, O, S> {
   return new Loop(pipeline);
}
// ==================================================
//                   Core
// ==================================================
export class Loop<I, O, S extends IBus<I>> extends APipe<I, O> {
   protected _id = getRuntimeId();

   constructor(
      protected _pipeline: IPipeline<I, O, S>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: I) {
     this._pipeline.origin.send(value);
   }

   protected activate(): void {
      this._pipeline.watch({
         id: this._id,
         value: value => this.send(value),
      });
   }

   protected deactivate(): void {
      this._pipeline.watchers.detach(this._id);
   }
}
