import { getRuntimeId, IS, TMapper } from '@taedr/utils';
import { APipe, IReadonlyBus } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** After receiving value will emit it instantly and start timer for specified number of milliseconds.
 * Until timer ends all new incoming values will be discarded. */
export function discard<T>(durationMs: number): DiscardDuration<T>;
/**  After receiving value will emit it instantly and map it to the source (liberator).
 * Until the liberator emits, all new incoming values will be discarded. */
export function discard<T>(getLiberator: TMapper<T, IReadonlyBus<unknown>>): DiscardLiberator<T>;
export function discard<T>(a: number | TMapper<T, IReadonlyBus<unknown>>): ADiscard<T> {
   if (IS.number(a)) {
      return new DiscardDuration(a);
   } else {
      return new DiscardLiberator(a);
   }
}
// ==================================================
//                   Core
// ==================================================
export abstract class ADiscard<T> extends APipe<T, T> {
   protected _activatedAt?: number;

   public get activatedAt() { return this._activatedAt; }
}
// ==================================================
//                   Duration
// ==================================================
export class DiscardDuration<T> extends ADiscard<T> {
   protected _timerId?: any;

   public get durationMs() { return this._durationMs; }

   constructor(
      protected _durationMs: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      if (this._timerId !== undefined) return;

      this._activatedAt = Date.now();
      this._timerId = setTimeout(() => this.clear(), this._durationMs);
      this.send(value);
   }

   protected deactivate(): void {
      clearTimeout(this._timerId);
      this.clear();
   }

   protected clear() {
      delete this._timerId;
      delete this._activatedAt;
   }
}
// ==================================================
//                   Liberator
// ==================================================
export class DiscardLiberator<T> extends ADiscard<T> {
   protected _id = getRuntimeId();
   protected _source?: IReadonlyBus<unknown>;

   constructor(
      protected _getLiberator: TMapper<T, IReadonlyBus<unknown>>
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      if (this._activatedAt !== undefined) return;

      this._activatedAt = Date.now();
      this._source = this._getLiberator(value);
      this._source.watch({
         id: this._id,
         value: () => this.deactivate(),
         detach: message => this.releaseByDetach(message),
      });
      this.send(value);
   }

   protected releaseByDetach(message: string) {
      if (message === this._id) return;
      this.deactivate();
   }

   protected clear() {
      this._source?.watchers.detach(this._id);
      delete this._source;
      delete this._activatedAt;
   }

   protected deactivate() {
      this.clear();
   }
}
