import { getRuntimeId } from '@taedr/utils';
import { APipe, IReadonlyBus, IReadonlyReactive, Reactive, TReadonlyBusArray_1, TReadonlyBusArray_10, TReadonlyBusArray_2, TReadonlyBusArray_3, TReadonlyBusArray_4, TReadonlyBusArray_5, TReadonlyBusArray_6, TReadonlyBusArray_7, TReadonlyBusArray_8, TReadonlyBusArray_9 } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** integrates incoming value and values emitted by provided sources into an array.
 * After that will emit on any change of provided source.
 */
export function integrate<I, A>(
   sources: TReadonlyBusArray_1<A>,
): Integrator<I, [I, A], typeof sources>;
export function integrate<I, A, B>(
   sources: TReadonlyBusArray_2<A, B>,
): Integrator<I, [I, A, B], typeof sources>;
export function integrate<I, A, B, C>(
   sources: TReadonlyBusArray_3<A, B, C>,
): Integrator<I, [I, A, B, C], typeof sources>;
export function integrate<I, A, B, C, D>(
   sources: TReadonlyBusArray_4<A, B, C, D>,
): Integrator<I, [I, A, B, C, D], typeof sources>;
export function integrate<I, A, B, C, D, E>(
   sources: TReadonlyBusArray_5<A, B, C, D, E>,
): Integrator<I, [I, A, B, C, D, E], typeof sources>;
export function integrate<I, A, B, C, D, E, F>(
   sources: TReadonlyBusArray_6<A, B, C, D, E, F>,
): Integrator<I, [I, A, B, C, D, E, F], typeof sources>;
export function integrate<I, A, B, C, D, E, F, G>(
   sources: TReadonlyBusArray_7<A, B, C, D, E, F, G>,
): Integrator<I, [I, A, B, C, D, E, F, G], typeof sources>;
export function integrate<I, A, B, C, D, E, F, G, H>(
   sources: TReadonlyBusArray_8<A, B, C, D, E, F, G, H>,
): Integrator<I, [I, A, B, C, D, E, F, G, H], typeof sources>;
export function integrate<I, A, B, C, D, E, F, G, H, J>(
   sources: TReadonlyBusArray_9<A, B, C, D, E, F, G, H, J>,
): Integrator<I, [I, A, B, C, D, E, F, G, H, J], typeof sources>;
export function integrate<I, A, B, C, D, E, F, G, H, J, K>(
   sources: TReadonlyBusArray_10<A, B, C, D, E, F, G, H, J, K>,
): Integrator<I, [I, A, B, C, D, E, F, G, H, J, K], typeof sources>;
export function integrate(
   sources: readonly IReadonlyBus<any>[],
): Integrator<any, any, typeof sources> {
   return new Integrator(sources);
}
// ==================================================
//                   Core
// ==================================================
export class Integrator<I, O extends readonly any[], S extends readonly IReadonlyBus<unknown>[]>
   extends APipe<I, O> {
   protected _id = getRuntimeId();
   protected _group: Reactive<O>;
   protected _sources: IReadonlyBus<unknown>[];
   protected _initiated?: boolean[];

   public get sources() { return this._sources; }
   public get group(): IReadonlyReactive<O> { return this._group; }

   constructor(
      sources: S,
   ) {
      super();
      this._sources = [...sources];
      this._initiated = new Array(sources.length + 1).fill(false);
      this._group = new Reactive(new Array(sources.length + 1) as any);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: I) {
      this.onValue(0, value);
   }

   protected onValue(index: number, value: unknown) {
      const merged = [...this._group.value];
      merged[index] = value;
      this._group.send(Object.freeze(merged as any));

      if (this._initiated !== undefined) {
         this._initiated[index] = true;
         if (this._initiated.some(v => !v)) return;
         delete this._initiated;
      }

      this.send(this._group.value);
   }

   protected activate(): void {
      for (let i = 0; i < this._sources.length; i++) {
         this._sources[i].watch({
            id: this._id,
            value: value => this.onValue(i + 1, value),
         });
      }
   }

   protected deactivate(): void {
      for (const source of this._sources) {
         source.watchers.detach(this._id);
      }
      this._group.send(new Array(this._sources.length + 1) as any);
   }
}
