import { IS, ToBoolean } from "@taedr/utils";
import { APipe, IPossibleChange } from "../../core";

// ==================================================
//                   Factory
// ==================================================
export type TSkipCkeck<T, C extends IPossibleChange> = (value: T, change: C) => boolean;

/** Skips specified number of values */
export function skip<T>(limit: number): SkipLimit<T>;
/** Skips values until function returns `false` */
export function skip<T, C extends IPossibleChange>(check: TSkipCkeck<T, C>): SkipCheck<T, C>;
export function skip<T, C extends IPossibleChange>(a: number | TSkipCkeck<T, C>) {
   if (IS.function(a)) return new SkipCheck(a);
   return new SkipLimit<T>(a);
}

/* Passes value if `initial` in `change` is falsy */
export function skipInitial<T>(): SkipCheck<T, IPossibleChange> {
   return new SkipCheck((_, { initial }) => !!initial);
}
// ==================================================
//                   Core
// ==================================================
export abstract class ASkip<T> extends APipe<T, T> {

}
// ==================================================
//                   Limit
// ==================================================
export class SkipLimit<T> extends ASkip<T> {
   protected _count = 0;

   public get count() { return this._count; }
   public get limit() { return this._limit; }
   public get passed() { return this._count > this._limit; }

   constructor(
      protected _limit: number
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T) {
      if (++this._count > this._limit) this.send(value);
   }
}
// ==================================================
//                   Check
// ==================================================
export class SkipCheck<T, C extends IPossibleChange> extends ASkip<T> {
   protected _passed = false;

   public get passed() { return this._passed; }

   constructor(
      protected _check: TSkipCkeck<T, C>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   protected transform(value: T, change: C) {
      if (this._passed) {
         this.send(value);
      } else if (!this._check(value, change)) {
         this._passed = true;
         this.send(value);
      }
   }
}
