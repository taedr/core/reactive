import { APipe, IReadonlyReactiveArray, ReactiveArray } from '../../core';

// ==================================================
//                   Factory
// ==================================================
/** Collects elements
 * @param capacity - number of elements which should be collected
 * @param strategy
 * * `FULL` (default) - Emits when specified number of elements is collected. Clears values afterwards.
 * * `PARTIAL` - Emits on each new value income. Clears values after specified number of elements is collected.
 * * `FILL` - Initially fill array with undefined. Replaces undefined from end to start on each new value income. Clears values when no undefined left.
 * @param lifo - Replaces 'Clears values' with 'Removes first value and adds incoming value to the end'.
 */
export function collect<T>(capacity: number, strategy?: TCollectStrategy, lifo?: boolean) {
   return new Collector<T>(capacity, strategy, lifo);
}
// ==================================================
//                   Pipe
// ==================================================
export type TCollectStrategy = `FULL` | `PARTIAL` | `FILL`;

export class Collector<T> extends APipe<T, readonly T[]> {
   protected _buffer = new ReactiveArray<T>();
   protected _lifo?: true;
   protected _starategy?: TCollectStrategy;

   public get buffer(): IReadonlyReactiveArray<T> { return this._buffer; }
   public get maxSize() { return this._maxSize; }

   constructor(
      protected _maxSize: number,
      strategy: TCollectStrategy = `FULL`,
      lifo?: boolean
   ) {
      super();
      this._starategy = strategy;
      if (lifo) this._lifo = true;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected transform(value: T) {
      const fill = this._starategy === `FILL`;
      const partial = this._starategy === `PARTIAL`;

      if (this._maxSize === this._buffer.size) {
         if (this._lifo) {
            this._buffer.delete({ index: 0 });
            this._buffer.add([value]);
         } else if (fill) {
            if (this._buffer.value.some(v => v === undefined)) {
               this._buffer.delete({ index: 0 });
               this._buffer.add([value]);
            } else {
               this._buffer.send([...new Array(this._maxSize - 1), value]);
            }
         } else {
            this._buffer.send([value]);
         }
      } else {
         const isFillEmpty = !this._buffer.size && fill;
         const curr = isFillEmpty ? new Array(this._maxSize - 1).fill(undefined) : this._buffer.value;
         this._buffer.send([...curr, value]);
      }

      if (partial) {
         this.send(this._buffer.value);
      } else {
         if (this._maxSize === this._buffer.size) this.send(this._buffer.value);
      }
   }

   protected deactivate(): void {
      this._buffer.send([]);
   }

   protected setShutdown(message: string): void {
      super.setShutdown(message);
      this._buffer.shutdown = message;
   }
}
