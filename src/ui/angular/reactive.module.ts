import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WatchPipe } from './watch.pipe';

@NgModule({
   imports: [
      CommonModule,
   ],
   declarations: [
      WatchPipe,
   ],
   exports: [
      WatchPipe,
   ]
})
export class TaedrReactiveNgModule {
   constructor(
   ) {
   }
}
