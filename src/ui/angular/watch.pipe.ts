import { ChangeDetectorRef, Pipe, PipeTransform } from "@angular/core";
import { IReadonlyBus } from "@taedr/reactive";
import { getRuntimeId } from "@taedr/utils";

@Pipe({
   name: 'watch',
   pure: false,
})
export class WatchPipe<T> implements PipeTransform {
   protected _id = getRuntimeId();
   protected _source: IReadonlyBus<T> | undefined;
   protected _value: T | undefined;

   constructor(
      protected _cdr: ChangeDetectorRef
   ) {
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public send(source: IReadonlyBus<T>): T | undefined {
      if (this._source !== source) {
         this.watch(source);
      }

      return this._value;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   private watch(bus: IReadonlyBus<T>) {
      this._source?.watchers.detach(this._id);
      this._value = undefined;
      this._source = bus;
      bus.watch({
         id: this._id,
         value: value => {
            this._value = value;
            this._cdr.markForCheck();
         },
      })
   }
}
