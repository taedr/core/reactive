import { IS, TConsumer } from '@taedr/utils';
import { EDetachMessage } from '../../core';
import { AAdapterBus } from './core';

// ==================================================
//                   Model
// ==================================================
export interface ISubscription {
   unsubscribe(): void;
}

export interface IObservable<T> {
   subscribe(
      next: TConsumer<T>,
      error?: TConsumer<unknown>,
      complete?: TConsumer<void>,
   ): ISubscription;
}
// ==================================================
//                   Adapter
// ==================================================
export class ObservableBus<T> extends AAdapterBus<T> {
   protected _subscription?: ISubscription;

   constructor(
      protected _source: IObservable<T>,
   ) {
      super();
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch(): void {
      this.subscribe();
   }

   protected onLastDetach(): void {
      this._subscription?.unsubscribe();
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected subscribe() {
      this._subscription = this._source.subscribe(
         value => this.send(value),
         e => this.onError(e),
         () => this.setShutdown(EDetachMessage.Emittion),
      );
   }

   protected onError(e: any) {
      let mess: string = EDetachMessage.Error;
      if (IS.string(e) && e.length) mess = e;
      if (e instanceof Error && e.message.length) mess = e.message;
      this.setShutdown(mess)
   }

   protected setShutdown(message: string) {
      super.setShutdown(message);
      this._source = null as any;
   }


}
