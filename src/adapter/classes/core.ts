import { AReadonlyBus } from "../../core";

export abstract class AAdapterBus<T> extends AReadonlyBus<T> {
   public get shutdown(): string { return this._shutdown ?? ``; }
   public set shutdown(message: string) { this.setShutdown(message); }
}
