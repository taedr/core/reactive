import { TConsumer } from '@taedr/utils';
import { AAdapterBus } from './core';

// ==================================================
//                   Model
// ==================================================
export interface IEventSource<T> {
   addEventListener(event: keyof DocumentEventMap, handler: TConsumer<T>, options?: boolean | AddEventListenerOptions): void;
   removeEventListener(event: keyof DocumentEventMap, handler: TConsumer<T>, options?: boolean | AddEventListenerOptions): void;
}
// ==================================================
//                   Adapter
// ==================================================
export class EventBus<T> extends AAdapterBus<T> {
   protected _callback = (value: T) => this.send(value);
   protected _options?: boolean | AddEventListenerOptions

   constructor(
      protected _source: IEventSource<T>,
      protected _event: keyof DocumentEventMap,
      options?: boolean | AddEventListenerOptions
   ) {
      super();
      if (options) this._options = options;
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch() {
      this._source.addEventListener(this._event, this._callback, this._options);
   }

   protected onLastDetach(): void {
      this._source.removeEventListener(this._event, this._callback);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected setShutdown(message: string) {
      this._source.removeEventListener(this._event, this._callback);
      super.setShutdown(message);
      this._source = null as any;
   }
}
