import { IS } from '@taedr/utils';
import { EDetachMessage, IBusChange, IWatcherCtx, TOnValue, Watcher } from '../../core';
import { AAdapterBus } from './core';

export class PromiseBus<T> extends AAdapterBus<T> {
   protected _value?: T;

   public get value() { return this._value; }

   constructor(
      protected _promise: Promise<T>,
   ) {
      super();
      this.init();
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public watch(watcher: TOnValue<T, IBusChange>): string;
   public watch(watcher: IWatcherCtx<T, IBusChange>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected init() {
      this._promise
         .then(value => this.onResolve(value))
         .catch((e) => this.onReject(e));
   }

   protected onResolve(value: T) {
      this._value = value;
      this.send(value);
      this.setShutdown(EDetachMessage.Emittion);
   }

   protected onReject(e: unknown) {
      const message = IS.string(e) ? e : JSON.stringify(e);
      this.setShutdown(message);
   }

   protected rejectWatcher(id: string, watcher: Watcher<T, IBusChange>): void {
      if (this._shutdown === EDetachMessage.Emittion) {
         this.notifySingleWatcher(this._value as T, {}, watcher);
      }
      super.rejectWatcher(id, watcher);
   }

   protected setShutdown(message: string) {
      super.setShutdown(message);
      this._promise = null as any;
   }
   // ---------------------------------------------
   //                   Static
   // ---------------------------------------------
   public static resolve<T>(value: T, delayMs?: number) {
      const promise: Promise<T> = !IS.number(delayMs) ? Promise.resolve(value) :
         new Promise(r => setTimeout(() => r(value), delayMs));
      return new PromiseBus(promise);
   }

   public static reject(value: any, delayMs?: number) {
      const promise = !IS.number(delayMs) ? Promise.reject(value) :
         new Promise((_, r) => setTimeout(() => r(value), delayMs));
      return new PromiseBus(promise);
   }
}
