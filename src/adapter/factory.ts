import { IS } from '@taedr/utils';
import { IReadonlyBus, Reactive } from '../core';
import { Producer, Sequence, TProducer } from '../emitter';
import { EventBus, IEventSource } from './classes/event';
import { IObservable, ObservableBus } from './classes/observable';
import { PromiseBus } from './classes/promise';


export function adapt<T>(source: Promise<T>): PromiseBus<T>;
export function adapt<T>(source: IEventSource<T>, event: keyof DocumentEventMap): EventBus<T>;
export function adapt<T>(source: IObservable<T>): ObservableBus<T>;
export function adapt<T>(iterable: Iterable<T>): Sequence<T>;
export function adapt<T>(produce: TProducer<T>): Producer<T>;
export function adapt<T>(value: T): Reactive<T>;
export function adapt(source: any, second?: any): IReadonlyBus<any> {
   if (`then` in source) {
      return new PromiseBus(source);
   } else if (IS.iterable(source)) {
      return new Sequence(source);
   } else if (IS.function(source)) {
      return new Producer(source);
   } else if (`addEventListener` in source) {
      return new EventBus(source, second);
   } else if (`subscribe` in source) {
      return new ObservableBus(source);
   } else {
      return new Reactive(source);
   }
}
