import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IIndexAdd, IIndexDelete, IIndexDeleted, IIndexUpdate, IPipeline, IReactive, IReadonlyReactive, IValue, IWatcherCtx, TOnValue } from '../../circle';
import { IReactiveArrayActions, IReactiveArrayChange, IReactiveArrayCtx, IReactiveArrayModification, IReadonlyReactiveArray, ISortable, TReactiveArrayDeleter, TReactiveArrayComparer, TReactiveArrayUpdater } from '../values/interface';


// ==================================================
//                   Incapsulation
// ==================================================
export interface IReadonlyReactiveWrapperArray<T, W> extends IReadonlyReactiveArray<T> {
   readonly wrappers: readonly W[];
   getWrapper(index: number): W | undefined;
   hasWrapper(wrapper: W): boolean;
   hasWrapper(comparer: TReactiveArrayComparer<W>): boolean;
   watch(watcher: TOnValue<readonly T[], IReactiveWrapperArrayChange<T, W>>): string;
   watch(watcher: IWatcherCtx<readonly T[], IReactiveWrapperArrayChange<T, W>>): string;
   pipe(): IPipeline<readonly T[], readonly T[], this>;
   pipe<A>(
      ...pipes: TPipeArray_1<readonly T[], IReactiveWrapperArrayChange<T, W>, A>
   ): IPipeline<readonly T[], A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B>
   ): IPipeline<readonly T[], B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D>
   ): IPipeline<readonly T[], D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E>
   ): IPipeline<readonly T[], E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F>
   ): IPipeline<readonly T[], F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G>
   ): IPipeline<readonly T[], G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H>
   ): IPipeline<readonly T[], H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I>
   ): IPipeline<readonly T[], I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<readonly T[], J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<readonly T[], K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<readonly T[], unknown, this>;
}

export interface IReactiveWrapperArray<T, W> extends IReadonlyReactiveWrapperArray<T, W>, IWrappersArrayActions<T, W> {
   sorter: TReactiveWrapperArraySorter<T, W> | undefined;
}

export interface IReadonlyReactivesArray<T, R extends IReadonlyReactive<T> = IReadonlyReactive<T>> extends IReadonlyReactiveWrapperArray<T, R> {
}

export interface IReactivesArray<T, R extends IReactive<T> = IReactive<T>> extends IReadonlyReactivesArray<T, R>, IReactiveWrapperArray<T, R> {
}
// ==================================================
//                Ctx
// ==================================================
export interface IReactiveWrapperArrayCtx<T, W> extends IReactiveArrayCtx<T> {
   sorter?: TReactiveWrapperArraySorter<T, W>;
}
// ==================================================
//                   Actions
// ==================================================
export interface IWrappersArrayActions<T, W> extends IReactiveArrayActions<T> {
   sendWrappers(wrappers: readonly W[]): IReactiveWrapperArrayChange<T, W>;
   sendWrappers(modification: IReactiveArrayModification<W>): IReactiveWrapperArrayChange<T, W>;
   add(values: Iterable<T>, index?: number): IReactiveWrapperArrayChange<T, W>;
   add(...packs: IIndexAdd<T>[]): IReactiveWrapperArrayChange<T, W>;
   addWrappers(values: Iterable<W>, index?: number): IReactiveWrapperArrayChange<T, W>;
   addWrappers(...packs: IIndexAdd<W>[]): IReactiveWrapperArrayChange<T, W>;
   update(comparer: TReactiveArrayUpdater<T>): IReactiveWrapperArrayChange<T, W>;
   update(...values: IIndexUpdate<T>[]): IReactiveWrapperArrayChange<T, W>;
   updateWrappers(comparer: TReactiveArrayUpdater<W>): IReactiveWrapperArrayChange<T, W>;
   updateWrappers(...values: IIndexUpdate<W>[]): IReactiveWrapperArrayChange<T, W>;
   delete(comparer: TReactiveArrayDeleter<T>): IReactiveWrapperArrayChange<T, W>;
   delete(values: Iterable<T>): IReactiveWrapperArrayChange<T, W>;
   delete(...ranges: IIndexDelete[]): IReactiveWrapperArrayChange<T, W>;
   deleteWrappers(comparer: TReactiveArrayDeleter<W>): IReactiveWrapperArrayChange<T, W>;
   deleteWrappers(values: Iterable<W>): IReactiveWrapperArrayChange<T, W>;
}

export type TReactiveWrapperArraySorter<T, W> = (a: T, b: T, a_wrapper: W, b_wrapper: W) => number;

export interface ISortableWrapper<T, W> extends ISortable<W> {
   exactValue: T;
}
// ==================================================
//                   Change
// ==================================================
export interface IReactiveWrapperArrayChange<T, W> extends IReactiveArrayChange<T> {
   added: readonly IIndexAddedWrappers<T, W>[];
   deleted: readonly IIndexDeletedWrappers<T, W>[];
   updated: readonly IIndexUpdatedWrapper<T, W>[];
}

export interface IReactivesArrayChange<T, R extends IReadonlyReactive<T> = IReadonlyReactive<T>> extends IReactiveWrapperArrayChange<T, R> {
}
// ==================================================
//                   Index
// ==================================================
export interface IIndexAddedWrappers<T, W> extends IIndexAdd<T> {
   index: number;
   wrappers: readonly W[];
}

export interface IIndexDeletedWrappers<T, W> extends IIndexDeleted<T> {
   wrappers: readonly W[];
}

export interface IIndexUpdatedWrapper<T, W> extends IIndexUpdate<T> {
   wrapper: W;
}
