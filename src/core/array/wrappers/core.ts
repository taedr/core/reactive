import { getRuntimeId, IS, TMapper } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, ERRORS, IIndexAdd, IIndexAdded, IIndexAddedWrappers, IIndexDelete, IIndexDeleted, IIndexDeletedWrappers, IIndexUpdate, IIndexUpdatedWrapper, IMove, IPipeline, IReactive, IReactiveArrayInternalChange, IReactiveArrayModification, IReactiveChange, IReactiveWrapperArray, IReactiveWrapperArrayChange, IReactiveWrapperArrayCtx, ISortableWrapper, IWatcherCtx, Pipeline, Reactive, SReverse, TOnValue, TReactiveArrayComparer, TReactiveArrayDeleter, TReactiveArraySorter, TReactiveArrayUpdater, TReactiveWrapperArraySorter } from '../../circle';
import { ReactiveArray } from '../values/core';

export abstract class AReactiveWrapperArray<T, W> extends ReactiveArray<T> implements IReactiveWrapperArray<T, W> {
   protected _id = getRuntimeId();
   protected _wrappers: readonly W[] = Object.freeze([]);
   protected _sorter?: TReactiveWrapperArraySorter<T, W>;
   protected _changes?: IReactiveWrapperArrayChange<T, W>[];

   public get wrappers(): readonly W[] { return this._wrappers; }
   public get sorter(): TReactiveWrapperArraySorter<T, W> | undefined { return this._sorter; }
   public set sorter(sorter: TReactiveWrapperArraySorter<T, W> | undefined) { this.setSorter(sorter); }

   constructor(
      initial: readonly T[] | readonly W[] = [],
      ctx?: IReactiveWrapperArrayCtx<T, W>,
   ) {
      super([]);
      this.setCtx(ctx);
      if (initial.length) this.setValues(initial);
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ==================================================
   //                   Abstract
   // ==================================================
   protected abstract isWrappers(value: readonly T[] | readonly W[]): value is readonly W[];
   protected abstract getReactive(wrapper: W): IReactive<T>;
   protected abstract createWrapper(value: T): W;
   // ==================================================
   //                   Initial
   // ==================================================
   protected setValues(_initial: readonly T[] | readonly W[]) {
      const initial = this.isWrappers(_initial) ? _initial : this.valuesToWrappers(_initial);
      const wrappers = this.getInitial(initial);
      const values: T[] = [];

      this._processing = true;

      for (let i = 0; i < wrappers.length; i++) {
         values.push(this.watchWrapper(wrappers[i]));
      }

      this._value = Object.freeze(values);
      this._wrappers = Object.freeze(wrappers);
      delete this._processing;
   }

   protected getHash(wrapper: any): string {
      const reactive = this.getReactive(wrapper);
      const value = reactive.value;
      const getHash = this._getHash as TMapper<T, string>;
      return getHash(value);
   }
   // ==================================================
   //                   Watch
   // ==================================================
   public watch(watcher: TOnValue<readonly T[], IReactiveWrapperArrayChange<T, W>>): string;
   public watch(watcher: IWatcherCtx<readonly T[], IReactiveWrapperArrayChange<T, W>>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ==================================================
   //                   Send
   // ==================================================
   public send(values: readonly T[] | null): IReactiveWrapperArrayChange<T, W>;
   public send(modification: IReactiveArrayModification<T>): IReactiveWrapperArrayChange<T, W>;
   public send(a: readonly T[] | IReactiveArrayModification<T> | null) {
      const change = this.createInternalChange(this._wrappers);

      if (IS.readonlyArray(a)) {
         const wrappers = this.valuesToWrappers(a);
         this.replace(change, wrappers);
      } else if (!!a) {
         this.modifyValues(change, a);
      } else {
         this.replace(change, []);
      }

      return this.addWrappersChange(change);
   }

   public sendWrappers(wrappers: readonly W[] | null): IReactiveWrapperArrayChange<T, W>;
   public sendWrappers(modification: IReactiveArrayModification<W>): IReactiveWrapperArrayChange<T, W>;
   public sendWrappers(a: readonly W[] | IReactiveArrayModification<W> | null) {
      const change = this.createInternalChange(this._wrappers);
      this.replaceRouter(change, a);
      return this.addWrappersChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected modifyValues(
      change: IReactiveArrayInternalChange<W>,
      modification: IReactiveArrayModification<T>
   ) {
      super.modify(change, {
         added: modification.added.map(({ index, values }) => ({ index, values: this.valuesToWrappers(values) })),
         updated: modification.updated.map(({ index, value }) => ({ index, value: this.createWrapper(value) })),
         deleted: modification.deleted,
         moved: modification.moved
      });
   }

   protected prepareToChange(value: readonly T[]) {
      const internal = this.createInternalChange(this._wrappers);
      const wrappers = this.valuesToWrappers(value);
      this.replace(internal, wrappers);
      const change = this.createWrappersChange(internal);
      this.notifyWatchersPrepare(this._value, change);

      return change;
   }
   // ==================================================
   //                   Utils
   // ==================================================
   public getWrapper(index: number): W | undefined;
   public getWrapper(comaparer: TReactiveArrayComparer<W>): W | undefined;
   public getWrapper(a: number | TReactiveArrayComparer<W>): W | undefined {
      return this.getValue(this._wrappers, a);
   }

   public hasWrapper(wrapper: W): boolean;
   public hasWrapper(comaparer: TReactiveArrayComparer<W>): boolean;
   public hasWrapper(a: W | TReactiveArrayComparer<W>) {
      return this.hasValue(this._wrappers, a);
   }
   // ==================================================
   //                   Add
   // ==================================================
   public add(values: readonly T[], index?: number): IReactiveWrapperArrayChange<T, W>;
   public add(...packs: IIndexAdd<T>[]): IReactiveWrapperArrayChange<T, W>;
   public add(
      a: readonly T[] | IIndexAdd<T>,
      b?: number | IIndexAdd<T>,
      ...c: IIndexAdd<T>[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      if (a === undefined) return this.addWrappersChange(change);
      const packs = this.createPacks(a, b, ...c);
      const wrapperPacks = this.convertPacks(packs);
      this.addRouter(change, ...wrapperPacks);
      return this.addWrappersChange(change);
   }

   public addWrappers(values: readonly W[], index?: number): IReactiveWrapperArrayChange<T, W>;
   public addWrappers(...packs: IIndexAdd<W>[]): IReactiveWrapperArrayChange<T, W>;
   public addWrappers(
      a: readonly W[] | IIndexAdd<W>,
      b?: number | IIndexAdd<W>,
      ...c: IIndexAdd<W>[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.addRouter(change, a as any, b as any, ...c);
      return this.addWrappersChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected convertPacks(packs: readonly IIndexAdd<T>[]) {
      const converted: IIndexAdd<W>[] = [];

      for (let i = 0; i < packs.length; i++) {
         const { index, values } = packs[i];
         converted.push({ index, values: this.valuesToWrappers(values) });
      }

      return converted;
   }
   // ==================================================
   //                   Delete
   // ==================================================
   public delete(comparer: TReactiveArrayDeleter<T>): IReactiveWrapperArrayChange<T, W>;
   public delete(values: readonly T[]): IReactiveWrapperArrayChange<T, W>;
   public delete(...ranges: IIndexDelete[]): IReactiveWrapperArrayChange<T, W>;
   public delete(
      a: TReactiveArrayDeleter<T> | readonly T[] | IIndexDelete,
      ...b: IIndexDelete[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);

      if (IS.readonlyArray(a)) {
         this.deleteCompare(change, (_, i) => a.includes(this._value[i]));
      } else if (IS.function(a)) {
         this.deleteCompare(change, (_, i) => a(this._value[i], i));
      } else {
         const packs: IIndexDelete[] = [];
         if (a) packs.push(a as IIndexDelete);
         packs.push(...b);
         this.deleteRanges(change, packs);
      }

      return this.addWrappersChange(change);
   }

   public deleteWrappers(comparer: TReactiveArrayDeleter<W>): IReactiveWrapperArrayChange<T, W>;
   public deleteWrappers(values: readonly W[]): IReactiveWrapperArrayChange<T, W>;
   public deleteWrappers(
      a: TReactiveArrayDeleter<W> | readonly W[],
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.deleteRouter(change, a as any);
      return this.addWrappersChange(change);
   }
   // ==================================================
   //                   Update
   // ==================================================
   public update(comparer: TReactiveArrayUpdater<T>): IReactiveWrapperArrayChange<T, W>;
   public update(...values: IIndexUpdate<T>[]): IReactiveWrapperArrayChange<T, W>;
   public update(
      a: TReactiveArrayUpdater<T> | IIndexUpdate<T>,
      ...b: IIndexUpdate<T>[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.setUpdateChange = this.setWrapperUpdateChange as any;
      this.getHash = this.getValueHash as any;
      this.updateCompare = this.updateValuesCompare as any;
      this.updateRouterPreSort(change, a as any, ...b as any);
      delete (this as any).getHash;
      delete (this as any).getUpdateChange;
      delete (this as any).updateCompare;
      if (this._sorter && change.updated.length) this.sortUpdated(change);
      return this.addWrappersChange(change);
   }

   public updateWrappers(comparer: TReactiveArrayUpdater<W>): IReactiveWrapperArrayChange<T, W>;
   public updateWrappers(...values: IIndexUpdate<W>[]): IReactiveWrapperArrayChange<T, W>;
   public updateWrappers(
      a: TReactiveArrayUpdater<W> | IIndexUpdate<W>,
      ...b: IIndexUpdate<W>[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.updateRouter(change, a as any, ...b);
      return this.addWrappersChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected updateValuesCompare(
      change: IReactiveArrayInternalChange<W>,
      comparer: TReactiveArrayUpdater<T>
   ) {
      for (let i = 0; i < this.size; i++) {
         const prev = this._value[i];
         const newValue = comparer(prev, i);
         if (newValue === undefined || newValue === prev) continue;
         this.setWrapperUpdateChange(change, i, newValue);
      }
   }

   protected getValueHash(value: T) {
      return (this._getHash as TMapper<T, string>)(value);
   }

   protected setWrapperUpdateChange(
      change: IReactiveArrayInternalChange<W>,
      index: number,
      value: T
   ) {
      const wrapper = this._wrappers[index];
      const reactive = this.getReactive(wrapper) as Reactive<T>;

      this.addPrepared(reactive, value);
      change.updated.push(Object.freeze({ index, value: wrapper }));
   }
   // ==================================================
   //                   Move
   // ==================================================
   public move(indexes: Iterable<number>, to: number): IReactiveWrapperArrayChange<T, W>;
   public move(steps: number): IReactiveWrapperArrayChange<T, W>;
   public move(reverse: SReverse): IReactiveWrapperArrayChange<T, W>;
   public move(...movements: IMove[]): IReactiveWrapperArrayChange<T, W>;
   public move(
      a: Iterable<number> | number | SReverse | IMove,
      b?: number | IMove,
      ...c: IMove[]
   ): IReactiveWrapperArrayChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.moveRouter(change, a, b, ...c);
      return this.addWrappersChange(change);
   }
   // ==================================================
   //                   Sort
   // ==================================================
   protected setSorter(sorter: TReactiveArraySorter<T> | undefined) {
      if (sorter === undefined) {
         delete this._sorter;
         return;
      }

      const change = this.createInternalChange(this._wrappers);

      this._sorter = sorter;
      this.sortCurrent(change);
      this.addWrappersChange(change);
   }

   protected newValueToSortable<M>(wrapper: M): ISortableWrapper<T, M> {
      const reactive = this.getReactive(wrapper as unknown as W);
      const value = reactive.value as unknown as T;
      return { value: wrapper, index: -1, exactValue: value };
   }

   protected valueToSortable<M>(wrapper: M, index: number): ISortableWrapper<T, M> {
      const value = this._value[index] as unknown as T;
      return { value: wrapper, index, exactValue: value };
   }

   protected getSorter() {
      return (a: any, b: any) => (this._sorter as any)(
         a.exactValue,
         b.exactValue,
         a.value,
         b.value
      ) || a.index - b.index;
   }
   // ==================================================
   //                   Source
   // ==================================================
   protected onSourceModification(modification: IReactiveArrayModification<T>) {
      const change = this.createInternalChange(this._wrappers);
      this.modifyValues(change, modification);
      this.addWrappersChange(change);
   }
   // ==================================================
   //                   Change
   // ==================================================
   protected createInternalChange<M>(value: readonly M[]): IReactiveArrayInternalChange<M> {
      const change = super.createInternalChange(value);
      this._processing = true;
      return change;
   }

   protected addWrappersChange(
      change: IReactiveArrayInternalChange<W>
   ) {
      const converted = this.createWrappersChange(change);
      this.notifyWatchers(this._value, converted);
      return converted;
   }

   protected createWrappersChange(
      change: IReactiveArrayInternalChange<W>
   ) {
      const converted: IReactiveWrapperArrayChange<T, W> = Object.freeze({
         changed: this.isChanged(change),
         deleted: Object.freeze(this.convertDeleted(change.deleted)),
         added: Object.freeze(this.convertAdded(change.added)),
         updated: Object.freeze(this.convertUpdated(change.updated)),
         moved: Object.freeze(change.moved),
      });

      this._value = Object.freeze(this.wrappersToValues(change.value));
      this._wrappers = Object.freeze(change.value);
      delete this._processing;
      return converted;
   }

   protected wrappersToValues(wrappers: readonly W[]): T[] {
      const values: T[] = [];

      if (this._getHash) {
         const hashIndex = this._hashIndex = {} as Record<string, number>;

         for (let i = 0; i < wrappers.length; i++) {
            const value = this.getReactive(wrappers[i]).value;
            const hash = this._getHash(value);
            if (hash !== null) hashIndex[hash] = i;
            values.push(value);
         }
      } else {
         for (let i = 0; i < wrappers.length; i++) {
            values.push(this.getReactive(wrappers[i]).value);
         }
      }

      return values;
   }

   protected valuesToWrappers(values: readonly T[]): W[] {
      const wrappers: W[] = [];
      for (let i = 0; i < values.length; i++) {
         wrappers.push(this.createWrapper(values[i]));
      }
      return wrappers;
   }

   protected convertDeleted(deleted: readonly IIndexDeleted<W>[]): IIndexDeletedWrappers<T, W>[] {
      const converted: IIndexDeletedWrappers<T, W>[] = [];

      for (let i = 0; i < deleted.length; i++) {
         const { values: wrappers, index, count } = deleted[i];
         const values: T[] = [];

         for (let j = 0; j < wrappers.length; j++) {
            const wrapper = wrappers[j];
            const value = this.detachWrapper(wrapper);
            values.push(value);
         }

         converted.push(Object.freeze({ index, count, values, wrappers }));
      }

      return converted;
   }

   protected detachWrapper(wrapper: W) {
      const reactive = this.getReactive(wrapper);
      reactive.watchers.detach(this._id);
      return reactive.value;
   }

   protected convertAdded(added: readonly IIndexAdded<W>[]): IIndexAddedWrappers<T, W>[] {
      const converted: IIndexAddedWrappers<T, W>[] = [];

      for (let i = 0; i < added.length; i++) {
         const { values: wrappers, index } = added[i];
         const values: T[] = [];

         for (let j = 0; j < wrappers.length; j++) {
            const wrapper = wrappers[j];
            const value = this.watchWrapper(wrapper);
            values.push(value);
         }

         converted.push(Object.freeze({ index, values, wrappers }));
      }

      return converted;
   }

   protected watchWrapper(wrapper: W) {
      const reactive = this.getReactive(wrapper)
      reactive.watch({
         id: this._id,
         prepare: true,
         value: (value, change) => this.onWrapperChange(value, wrapper, change)
      });
      return reactive.value;
   }

   protected onWrapperChange(value: T, wrapper: W, { changed, prepare }: IReactiveChange) {
      if (this._processing) return; // #Return#
      if (!prepare) return this.notifyAboutChange();

      const internal = this.createInternalChange(this._wrappers);
      const index = this._wrappers.indexOf(wrapper);

      if (changed) {
         if (this._getHash && this._hashIndex) {
            const hash = this._getHash(value);
            if (hash !== null && !!this._hashIndex[hash]) {
               throw new Error(ERRORS.sameHash(index));
            }
         }
         internal.updated.push({ index, value: wrapper });
         if (this._sorter) this.sortUpdated(internal);

      }

      const change = this.createWrappersChange(internal);
      this.notifyWatchersPrepare(this._value, change);
   }

   protected convertUpdated(updated: readonly IIndexUpdate<W>[]): IIndexUpdatedWrapper<T, W>[] {
      const converted: IIndexUpdatedWrapper<T, W>[] = [];

      for (let i = 0; i < updated.length; i++) {
         const { value: wrapper, index } = updated[i];
         const prev = this._wrappers[index];
         const reactive = this.getReactive(wrapper);
         const value = reactive.value;

         if (wrapper !== prev) {
            this.detachWrapper(prev);
            this.watchWrapper(wrapper);
         }

         converted.push(Object.freeze({ index, value, wrapper }));
      }

      return converted;
   }

   protected createInitialChange(): IReactiveWrapperArrayChange<T, W> {
      const value = this._value;
      const wrappers = this._wrappers;
      const added = Object.freeze(this.size ? [Object.freeze({ index: 0, values: value, wrappers })] : []);
      const deleted = Object.freeze([]);
      const updated = Object.freeze([]);
      const moved = Object.freeze([]);
      return Object.freeze({ initial: true, changed: true, added, deleted, updated, moved });
   }
   // ==================================================
   //                   Shutdown
   // ==================================================
   protected setShutdown(message: string) {
      super.setShutdown(message);
      for (let i = 0; i < this._wrappers.length; i++) {
         this.detachWrapper(this._wrappers[i]);
      }
   }
   // ==================================================
   //                   Pipeline
   // ==================================================
   public pipe(): IPipeline<readonly T[], readonly T[], this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<readonly T[], IReactiveWrapperArrayChange<T, W>, A>
   ): IPipeline<readonly T[], A, this, typeof pipes>;
   public pipe<A, B,>(
      ...pipes: TPipeArray_2<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B>
   ): IPipeline<readonly T[], B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D>
   ): IPipeline<readonly T[], D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E>
   ): IPipeline<readonly T[], E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F>
   ): IPipeline<readonly T[], F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G>
   ): IPipeline<readonly T[], G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H>
   ): IPipeline<readonly T[], H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I>
   ): IPipeline<readonly T[], I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<readonly T[], J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<readonly T[], IReactiveWrapperArrayChange<T, W>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<readonly T[], K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<readonly T[], unknown, this>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
