import { IS } from "@taedr/utils";
import { Reactive, AReactiveObject, IReactive, IReactivesArray, ReactiveBigint, ReactiveBoolean, ReactiveNumber, ReactiveString, TRB, TRI, TRN, TRO, TRS, TReactiveArrayComparer, IIndexUpdatedWrapper, IIndexUpdate, TReactiveArrayUpdater } from "../../circle";
import { AReactiveWrapperArray } from './core';

// ==================================================
//                   Core
// ==================================================
export abstract class AReactivesArray<T, R extends IReactive<T>> extends AReactiveWrapperArray<T, R> implements IReactivesArray<T, R> {
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected getReactive(wrapper: R) {
      return wrapper;
   }

   protected isWrappers(values: readonly T[] | readonly R[]): values is readonly R[] {
      return values[0] instanceof Reactive;
   }
}
// ==================================================
//                   Primitive
// ==================================================
export abstract class AReactivesPrimitiveArray<T extends string | number | boolean | bigint | null | undefined, R extends IReactive<T>> extends AReactivesArray<T, R> {

}

export class ReactivesStringArray<T = string> extends AReactivesPrimitiveArray<TRS<T>, ReactiveString<T>> {
   protected createWrapper(value: TRS<T>): ReactiveString<T> {
      return new ReactiveString(value);
   }
}

export class ReactivesNumberArray<T = number> extends AReactivesPrimitiveArray<TRN<T>, ReactiveNumber<T>> {
   protected createWrapper(value: TRN<T>): ReactiveNumber<T> {
      return new ReactiveNumber(value);
   }
}

export class ReactivesBooleanArray<T = boolean> extends AReactivesPrimitiveArray<TRB<T>, ReactiveBoolean<T>> {
   protected createWrapper(value: TRB<T>): ReactiveBoolean<T> {
      return new ReactiveBoolean(value);
   }
}

export class ReactivesBigintArray<T = bigint> extends AReactivesPrimitiveArray<TRI<T>, ReactiveBigint<T>> {
   protected createWrapper(value: TRI<T>): ReactiveBigint<T> {
      return new ReactiveBigint(value);
   }
}
// ==================================================
//                   Object
// ==================================================
export abstract class AReactivesObjectArray<T extends object, R extends AReactiveObject<T>>
   extends AReactivesArray<T, R> {
}

export abstract class AReactivesHashedObjectArray<T extends object, R extends AReactiveObject<T>>
   extends AReactivesObjectArray<T, R> {

   public get(hash: string): number | undefined
   public get(index: number): T | undefined
   public get(a: number | string): T | number | undefined {
      if (IS.string(a)) {
         const hashIndex = this._hashIndex as Record<string, number>;
         return hashIndex[a];
      } else {
         return this.getValueByIndex(this._value, a);
      }
   }

   public has(hash: string): boolean;
   public has(value: T): boolean;
   public has(comaparer: TReactiveArrayComparer<T>): boolean;
   public has(a: T | string | TReactiveArrayComparer<T>) {
      if (IS.string(a)) {
         const hashIndex = this._hashIndex as Record<string, number>;
         return hashIndex[a] !== undefined;
      } else {
         return this.hasValue(this._value, a);
      }
   }

   public update(values: readonly T[]): readonly IIndexUpdatedWrapper<T, R>[];
   public update(comparer: TReactiveArrayUpdater<T>): readonly IIndexUpdatedWrapper<T, R>[];
   public update(...values: IIndexUpdate<T>[]): readonly IIndexUpdatedWrapper<T, R>[];
   public update(
      a: readonly T[] | TReactiveArrayUpdater<T> | IIndexUpdate<T>,
      ...b: IIndexUpdate<T>[]
   ): readonly IIndexUpdate<T>[] {
      return super.update(a as any, ...b);
   }
}


// ==================================================
//                   Dimensional
// ==================================================
// export class ReactiveDimensionalArray<T extends readonly unknown[]> extends ReactiveObjectArray<T> {
// }
