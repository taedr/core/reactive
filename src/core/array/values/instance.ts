import { IS } from '@taedr/utils';
import { IIndexUpdate, IReactiveArrayChange, IReactiveArrayCtx, TRB, TReactiveArrayComparer, TReactiveArrayUpdater, TRI, TRN, TRO, TRS } from "../../circle";
import { ReactiveArray } from "./core";

// ==================================================
//                   Primitive
// ==================================================
export abstract class AReactivePrimitiveArray<T extends string | number | boolean | bigint | null> extends ReactiveArray<T> {
   constructor(
      initial: T[] = [],
      ctx?: IReactiveArrayCtx<T>
   ) {
      super(initial, ctx);
   }
}
// ==================================================
//                   String
// ==================================================
export class ReactiveStringArray<T = string> extends AReactivePrimitiveArray<TRS<T>> {
}
// ==================================================
//                   Number
// ==================================================
export class ReactiveNumberArray<T = number> extends AReactivePrimitiveArray<TRN<T>> {
}
// ==================================================
//                   Boolean
// ==================================================
export class ReactiveBooleanArray<T = boolean> extends AReactivePrimitiveArray<TRB<T>> {
}
// ==================================================
//                   Bigint
// ==================================================
export class ReactiveBigintArray<T = bigint> extends AReactivePrimitiveArray<TRI<T>> {
}
// ==================================================
//                   Object
// ==================================================
export class ReactiveObjectArray<T> extends ReactiveArray<TRO<T>> {
}

export class ReactiveHashedObjectArray<T extends object> extends ReactiveArray<TRO<T>> {
   public get(hash: string): number | undefined;
   public get(index: number): T | undefined;
   public get(comaparer: TReactiveArrayComparer<T>): T | undefined;
   public get(a: number | string | TReactiveArrayComparer<T>): T | number | undefined {
      if (IS.string(a)) {
         const hashIndex = this._hashIndex as Record<string, number>;
         return hashIndex[a];
      } else {
         return this.getValue(this._value, a);
      }
   }

   public has(hash: string): boolean;
   public has(value: T): boolean;
   public has(comaparer: TReactiveArrayComparer<T>): boolean;
   public has(a: T | string | TReactiveArrayComparer<T>) {
      if (IS.string(a)) {
         const hashIndex = this._hashIndex as Record<string, number>;
         return hashIndex[a] !== undefined;
      } else {
         return this.hasValue(this._value, a);
      }
   }

   public update(values: readonly T[]): IReactiveArrayChange<T>;
   public update(comparer: TReactiveArrayUpdater<T>): IReactiveArrayChange<T>;
   public update(...values: IIndexUpdate<T>[]): IReactiveArrayChange<T>;
   public update(a: readonly T[] | TReactiveArrayUpdater<T> | IIndexUpdate<T>, ...b: IIndexUpdate<T>[]): IReactiveArrayChange<T> {
      const internal = this.createInternalChange(this._value);
      this.updateRouter(internal, a as any, ...b);
      return this.addValuesChange(internal);
   }
}
// ==================================================
//                   Dimensional
// ==================================================
export class ReactiveDimensionalArray<T extends readonly unknown[]> extends ReactiveObjectArray<T> {
}
