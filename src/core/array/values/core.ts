import { IS } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, ERRORS, IIndexAdd, IIndexAdded, IIndexDelete, IIndexDeleted, IIndexUpdate, IMove, IPipeline, IPossibleChange, IReactiveArray, IReactiveArrayChange, IReactiveArrayCtx, IReactiveArrayInternalChange, IReactiveArrayModification, ISortable, IWatcherCtx, Pipeline, REVERSE, Reactive, SReverse, TMapValueToHash, TOnValue, TReactiveArrayComparer, TReactiveArrayDeleter, TReactiveArraySorter, TReactiveArrayUpdater } from '../../circle';



export class ReactiveArray<T> extends Reactive<readonly T[]> implements IReactiveArray<T> {
   protected _sorter?: TReactiveArraySorter<T>;
   protected _changes?: IReactiveArrayChange<T>[];
   protected _getHash?: TMapValueToHash<T>;
   protected _hashIndex?: Record<string, number>;

   public get size() { return this._value.length; }
   public get sorter(): TReactiveArraySorter<T> | undefined { return this._sorter; }
   public set sorter(sorter: TReactiveArraySorter<T> | undefined) { this.setSorter(sorter); }

   constructor(
      initial: readonly T[] = [],
      ctx?: IReactiveArrayCtx<T>
   ) {
      super(Object.freeze([]));
      this.setCtx(ctx);
      if (initial.length) this.setValues(initial);
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ==================================================
   //                Initial
   // ==================================================
   protected setCtx(ctx?: IReactiveArrayCtx<T>) {
      if (ctx?.getHash) this._getHash = ctx.getHash;
      if (ctx?.sorter) this._sorter = ctx.sorter;
   }

   protected setValues(initial: readonly T[]) {
      this._value = Object.freeze(this.getInitial(initial));
   }

   protected getInitial<M>(initial: readonly M[]): M[] {
      const sorted = this._sorter ? this.sortInitial(initial) : [...initial];

      if (this._getHash === undefined) return sorted;

      const values: M[] = [];
      const hashIndex = this._hashIndex = {} as Record<string, number>;
      let compensator = 0;

      for (let i = 0; i < sorted.length; i++) {
         const value = sorted[i];
         const hash = this.getHash(value);

         if (IS.null(hash)) continue;
         if (hashIndex[hash] !== undefined) {
            compensator += 1;
         } else {
            hashIndex[hash] = i - compensator;
            values.push(value);
         }
      }

      return values;
   }

   protected getHash<M>(value: M): string {
      return (this._getHash as any)(value);
   }
   // ==================================================
   //                Watch
   // ==================================================
   public watch(watcher: TOnValue<readonly T[], IReactiveArrayChange<T>>): string;
   public watch(watcher: IWatcherCtx<readonly T[], IReactiveArrayChange<T>>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ==================================================
   //                Send
   // ==================================================
   public send(values: readonly T[] | null): IReactiveArrayChange<T>;
   public send(modification: IReactiveArrayModification<T>): IReactiveArrayChange<T>;
   public send(a: readonly T[] | IReactiveArrayModification<T> | null): IReactiveArrayChange<T> {
      const internal = this.createInternalChange(this._value);
      this.replaceRouter(internal, a);
      const change = this.addValuesChange(internal);
      return change;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected replaceRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      a: readonly M[] | IReactiveArrayModification<M> | null
   ) {
      if (IS.readonlyArray(a)) {
         this.replace(change, a);
      } else if (!!a) {
         this.modify(change, a);
      } else {
         this.replace(change, []);
      }
   }

   protected replace<M>(
      change: IReactiveArrayInternalChange<M>,
      newValues: readonly M[]
   ) {
      if (this.size) change.deleted = [this.createDeleteChange(0, change.value)];
      change.value = this.getInitial(newValues);
      if (change.value.length) change.added = [this.getAddChange(0, change.value)];
   }

   protected modify<M>(
      change: IReactiveArrayInternalChange<M>,
      { added, deleted, updated, moved }: IReactiveArrayModification<M>
   ) {
      if (deleted.length) this.deleteRouter(change, ...deleted);
      if (added.length) this.addRouter(change, ...added);
      if (updated.length) this.updateRouter(change, ...updated);
      if (moved.length && !this._sorter) this.moveValues(change, moved);
   }

   protected prepareToChange(value: readonly T[]) {
      const internal = this.createInternalChange(this._value);
      this.replace(internal, value);
      const change = this.createValuesChange(internal);
      this.notifyWatchersPrepare(this._value, change);
      return change;
   }
   // ==================================================
   //                Utils
   // ==================================================
   public get(index: number): T | undefined;
   public get(comaparer: TReactiveArrayComparer<T>): T | undefined;
   public get(a: number | TReactiveArrayComparer<T>): T | undefined {
      return this.getValue(this._value, a);
   }

   public has(value: T): boolean;
   public has(comaparer: TReactiveArrayComparer<T>): boolean;
   public has(a: T | TReactiveArrayComparer<T>) {
      return this.hasValue(this._value, a);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected getValue<M>(
      array: readonly M[],
      a: number | TReactiveArrayComparer<M>
   ): M | undefined {
      if (IS.number(a)) {
         const index = a < 0 ? array.length + a : a;
         const value = array[index];
         return value;
      } else {
         for (let i = 0; i < array.length; i++) {
            const value = array[i];
            if (a(value, i)) return value;
         }
      }

      return;
   }

   protected hasValue<M>(array: readonly M[], a: M | TReactiveArrayComparer<M>): boolean {
      if (IS.function(a)) {
         for (let i = 0; i < array.length; i++) {
            const isValue = a(array[i], i);
            if (isValue) return true;
         }
         return false;
      } else {
         return array.includes(a);
      }
   }
   // ==================================================
   //                Add
   // ==================================================
   public add(values: readonly T[], index?: number): IReactiveArrayChange<T>;
   public add(...packs: IIndexAdd<T>[]): IReactiveArrayChange<T>;
   public add(
      a: readonly T[] | IIndexAdd<T>,
      b?: IIndexAdd<T> | number,
      ...c: IIndexAdd<T>[]
   ): IReactiveArrayChange<T> {
      const change = this.createInternalChange(this._value);
      this.addRouter(change, a as any, b as any, ...c);
      return this.addValuesChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected addRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      ...packs: IIndexAdd<M>[]
   ): void;
   protected addRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      a: readonly M[] | IIndexAdd<M>,
      b?: IIndexAdd<M> | number,
      ...c: IIndexAdd<M>[]
   ) {
      if (a === undefined) return;

      const packs = this.createPacks(a, b, ...c);

      if (this._getHash) {
         if (this._sorter) {
            this.addSortedHashPacks(change, packs);
         } else {
            this.addHashPacks(change, packs);
         }
      } else if (this._sorter) {
         this.sortAdd(change, this.mergePacksAndValues(change, packs));
      } else {
         this.addPacks(change, packs);
      }
   }

   protected createPacks<M>(
      a: readonly M[] | IIndexAdd<M>,
      b?: IIndexAdd<M> | number,
      ...c: IIndexAdd<M>[]
   ) {
      const packs: IIndexAdd<M>[] = [];

      if (IS.readonlyArray(a)) {
         packs.push({ index: b as number ?? this.size, values: a });
      } else if (a !== undefined) {
         packs.push(a as IIndexAdd<M>);
         if (b) packs.push(b as IIndexAdd<M>);
         packs.push(...c);
      }

      return packs;
   }

   protected addPacks<M>(
      change: IReactiveArrayInternalChange<M>,
      packs: readonly IIndexAdd<M>[]
   ) {
      let packStartIndex = packs[0].index ?? change.value.length;
      let packValues = [...packs[0].values];

      for (let i = 1; i < packs.length; i++) {
         const pack = packs[i];
         const index = pack.index ?? change.value.length;
         const prevPackEndIndex = packStartIndex + packValues.length - 1;

         if (prevPackEndIndex === index) {
            packValues.push(...pack.values);
         } else {
            this.setAddChange(change, packStartIndex, packValues);
            packStartIndex = pack.index === undefined ? index + packValues.length : index;
            packValues = [...pack.values];
         }
      }

      if (packValues.length) this.setAddChange(change, packStartIndex, packValues);
   }

   protected addHashPacks<M>(
      change: IReactiveArrayInternalChange<M>,
      packs: readonly IIndexAdd<M>[]
   ) {
      const newPacks: IIndexAdd<M>[] = [];
      const hashIndex = this._hashIndex as Record<string, number>;

      for (let i = 0; i < packs.length; i++) {
         const pack = packs[i];
         const newPack: M[] = [];

         for (let j = 0; j < pack.values.length; j++) {
            const value = pack.values[j];
            const hash = this.getHash(value);
            const index = hashIndex[hash];

            if (index === undefined) {
               newPack.push(value);
            } else {
               this.setUpdateChange(change, index, value);
            }
         }

         newPacks.push({ index: pack.index, values: newPack });
      }

      this.addPacks(change, newPacks);
   }

   protected addSortedHashPacks<M>(
      change: IReactiveArrayInternalChange<M>,
      packs: readonly IIndexAdd<M>[]
   ) {
      const hashIndex = this._hashIndex as Record<string, number>;
      const sortables = this.valuesToSortables(change.value);
      const updateWeights = new Map<ISortable<M>, M>();

      for (let i = 0; i < packs.length; i++) {
         for (let j = 0; j < packs[i].values.length; j++) {
            const value = packs[i].values[j];
            const hash = this.getHash(value)
            const index = hashIndex[hash];

            if (index === undefined) {
               sortables.push(this.newValueToSortable(value));
            } else if (value !== change.value[index]) {
               updateWeights.set(sortables[index], value);
            }
         }
      }

      this.sortAdd(change, sortables);

      if (!updateWeights.size) return;

      for (const [weight, value] of updateWeights) {
         const newIndex = sortables.indexOf(weight);
         this.setUpdateChange(change, newIndex, value);
      }

      this.sortUpdated(change);
   }

   protected getAddChange<M>(index: number, values: readonly M[]): IIndexAdded<M> {
      return Object.freeze({ index, values });
   }

   protected mergePacksAndValues<M>(
      change: IReactiveArrayInternalChange<M>,
      packs: readonly IIndexAdd<M>[]
   ) {
      const sortables = this.valuesToSortables(change.value);

      for (let i = 0; i < packs.length; i++) {
         for (let j = 0; j < packs[i].values.length; j++) {
            sortables.push(this.newValueToSortable(packs[i].values[j]));
         }
      }

      return sortables;
   }

   protected setAddChange<M>(
      change: IReactiveArrayInternalChange<M>,
      index: number,
      values: readonly M[]
   ) {
      change.value.splice(index, 0, ...values);
      change.added.push(Object.freeze({ index, values }));
   }
   // ==================================================
   //                Delete
   // ==================================================
   public delete(comparer: TReactiveArrayDeleter<T>): IReactiveArrayChange<T>;
   public delete(...ranges: IIndexDelete[]): IReactiveArrayChange<T>;
   public delete(values: readonly T[]): IReactiveArrayChange<T>;
   public delete(
      a: readonly T[] | TReactiveArrayDeleter<T> | IIndexDelete,
      ...b: IIndexDelete[]
   ): IReactiveArrayChange<T> {
      const change = this.createInternalChange(this._value);
      this.deleteRouter(change, a as any, ...b);
      return this.addValuesChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected deleteRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      ...ranges: IIndexDelete[]
   ): void;
   protected deleteRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      a: readonly M[] | TReactiveArrayDeleter<M> | IIndexDelete,
      ...b: IIndexDelete[]
   ) {
      if (IS.readonlyArray(a)) {
         this.deleteCompare(change, value => a.includes(value));
      } else if (IS.function(a)) {
         this.deleteCompare(change, a);
      } else {
         const packs: IIndexDelete[] = [];
         if (a) packs.push(a as IIndexDelete);
         packs.push(...b);
         this.deleteRanges(change, packs);
      }
   }

   protected deleteRanges<M>(
      change: IReactiveArrayInternalChange<M>,
      ranges: readonly IIndexDelete[]
   ) {
      const sorted = [...ranges].sort((a, b) => b.index - a.index);

      for (const { index, count = 1 } of sorted) {
         const end = count < 0 ? change.value.length + count + 1 : count;
         const values = change.value.splice(index, end);
         change.deleted.push(this.createDeleteChange(index, values));
      }
   }

   protected deleteCompare<M>(
      change: IReactiveArrayInternalChange<M>,
      comparer: TReactiveArrayDeleter<M>
   ) {
      const newValue: M[] = [];
      let index = 0;
      let pack: M[] = [];

      for (let i = 0; i < change.value.length; i++) {
         const value = change.value[i]

         if (comparer(value, i) === true) {
            index = pack.length === 0 ? i : index;
            pack.push(value);
         } else {
            if (pack.length) change.deleted.unshift(this.createDeleteChange(index, pack));
            pack = [];
            newValue.push(value)
         }
      }

      if (pack.length) change.deleted.unshift(this.createDeleteChange(index, pack));
      change.value = newValue;
   }

   protected createDeleteChange<M>(index: number, values: readonly M[]): IIndexDeleted<M> {
      return Object.freeze({ index, count: values.length, values: Object.freeze(values) });
   }
   // ==================================================
   //                Update
   // ==================================================
   public update(comparer: TReactiveArrayUpdater<T>): IReactiveArrayChange<T>;
   public update(...values: IIndexUpdate<T>[]): IReactiveArrayChange<T>;
   public update(a: TReactiveArrayUpdater<T> | IIndexUpdate<T>, ...b: IIndexUpdate<T>[]): IReactiveArrayChange<T> {
      const change = this.createInternalChange(this._value);
      this.updateRouter(change, a as any, ...b)
      return this.addValuesChange(change);
   }
   // ---------------------------------------------
   //                   Interanl
   // ---------------------------------------------
   protected updateRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      ...values: IIndexUpdate<M>[]
   ): void;
   protected updateRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      a: readonly M[] | TReactiveArrayUpdater<M> | IIndexUpdate<M>,
      ...b: IIndexUpdate<M>[]
   ) {
      this.updateRouterPreSort(change, a, ...b);
      if (this._sorter && change.updated.length) this.sortUpdated(change);
   }

   protected updateRouterPreSort<M>(
      change: IReactiveArrayInternalChange<M>,
      a: readonly M[] | TReactiveArrayUpdater<M> | IIndexUpdate<M>,
      ...b: IIndexUpdate<M>[]
   ) {
      if (IS.function(a)) {
         if (this._getHash) {
            this.updateCompareHashes(change, a);
         } else {
            this.updateCompare(change, a);
         }
      } else if (IS.readonlyArray(a)) {
         this.updateValuesHashes(change, a);
      } else if (a !== undefined) {
         const indexes = [a, ...b as IIndexUpdate<M>[]];

         if (this._getHash) {
            this.updateIndexesHashes(change, indexes);
         } else {
            this.updateIndexes(change, indexes);
         }
      }
   }

   protected updateIndexes<M>(
      change: IReactiveArrayInternalChange<M>,
      indexes: readonly IIndexUpdate<M>[]
   ) {
      for (const { value, index } of indexes) {
         const valueIndex = index < 0 ? this.size + index : index;
         this.setUpdateChange(change, valueIndex, value);
      }
   }

   protected updateIndexesHashes<M>(
      change: IReactiveArrayInternalChange<M>,
      indexes: readonly IIndexUpdate<M>[]
   ) {
      const hashIndex = this._hashIndex as Record<string, number>;
      for (const { value, index } of indexes) {
         const valueIndex = index < 0 ? this.size + index : index;
         const hash = this.getHash(value);
         const newIndex = hashIndex[hash] ?? valueIndex;
         this.setUpdateChange(change, newIndex, value);
      }
   }

   protected updateCompare<M>(
      change: IReactiveArrayInternalChange<M>,
      comparer: TReactiveArrayUpdater<M>
   ) {
      for (let i = 0; i < this.size; i++) {
         const prev = change.value[i];
         const newValue = comparer(prev, i);
         if (newValue === undefined || newValue === prev) continue;
         this.setUpdateChange(change, i, newValue);
      }
   }

   protected updateCompareHashes<M>(
      change: IReactiveArrayInternalChange<M>,
      comparer: TReactiveArrayUpdater<M>
   ) {
      const hashIndex = this._hashIndex as Record<string, number>;

      for (let i = 0; i < this.size; i++) {
         const prev = change.value[i];
         const newValue = comparer(prev, i);
         if (newValue === undefined || newValue === prev) continue;
         const hash = this.getHash(newValue);
         const newIndex = hashIndex[hash] ?? i;
         this.setUpdateChange(change, newIndex, newValue);
      }
   }

   protected updateValuesHashes<M>(
      change: IReactiveArrayInternalChange<M>,
      updates: readonly M[]
   ) {
      const hashIndex = this._hashIndex as Record<string, number>;

      for (const value of updates) {
         const hash = this.getHash(value);
         const index = hashIndex[hash];
         if (index === undefined) continue;
         this.setUpdateChange(change, index, value);
      }
   }

   protected setUpdateChange<M>(
      change: IReactiveArrayInternalChange<M>,
      index: number,
      value: M
   ) {
      if (value === change.value[index]) return;
      change.value[index] = value;
      change.updated.push(Object.freeze({ index, value }));
   }
   // ==================================================
   //                Move
   // ==================================================
   public move(indexes: Iterable<number>, to: number): IReactiveArrayChange<T>;
   public move(steps: number): IReactiveArrayChange<T>;
   public move(reverse: SReverse): IReactiveArrayChange<T>;
   public move(...movements: IMove[]): IReactiveArrayChange<T>;
   public move(
      a: Iterable<number> | number | SReverse | IMove,
      b?: number | IMove,
      ...c: IMove[]
   ): IReactiveArrayChange<T> {
      const change = this.createInternalChange(this._value);
      this.moveRouter(change, a, b, ...c);
      return this.addValuesChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected moveRouter<M>(
      change: IReactiveArrayInternalChange<M>,
      a: Iterable<number> | number | SReverse | IMove,
      b?: number | IMove,
      ...c: IMove[]
   ) {
      if (this._sorter) throw new Error(ERRORS.moveSort); // #Error#

      if (IS.iterable(a)) {
         this.moveIndexes(change, a, b as number);
      } else if (IS.number(a)) {
         this.moveSteps(change, a);
      } else if (a === REVERSE) {
         this.moveReverse(change);
      } else if (a !== undefined) {
         const values = [a];
         if (b) values.push(b as IMove, ...c as IMove[]);
         this.moveValues(change, values);
      }
   }

   protected moveValues<M>(
      change: IReactiveArrayInternalChange<M>,
      moves: readonly IMove[]
   ) {
      const size = change.value.length;
      const array = change.value;

      for (const move of moves) {
         const from = move.from < 0 ? size + move.from : move.from;
         const to = move.to < 0 ? size + move.to : move.to;

         if (move.swap) {
            const temp = array[to];
            array[to] = array[from];
            array[from] = temp;
         } else {
            const [value] = array.splice(from, 1);
            array.splice(to, 0, value);
         }

         Object.freeze(move);
      }

      change.moved = moves as IMove[];
   }

   protected moveIndexes<M>(
      change: IReactiveArrayInternalChange<M>,
      _indexes: Iterable<number>,
      _to: number
   ) {
      const indexesCheck = new Set<number>();
      const indexAnchors = new Map<number, number>();
      const offsetAnchors = new Map<number, number>();
      const endIndex = change.value.length - 1;
      const insert = _to < 0 ? this.size + _to : _to;
      let offset = 0;

      for (const index of _indexes) {
         if (indexesCheck.has(index)) throw new Error(ERRORS.index.duplicate(index));
         indexesCheck.add(index);
      }

      const indexes = [...indexesCheck];

      for (let i = 0; i < indexes.length; i++) {
         const index = indexes[i];
         const from = indexAnchors.get(index) ?? index;
         let to = insert + i - offset;

         if (from < insert && insert < to) {
            offset++;
            to--;
         }
         if (from === to) continue;

         const [value] = change.value.splice(from, 1);
         change.moved.push(this.getMoveChange(from, to));
         change.value.splice(to, 0, value);

         if (from < to) {
            for (let i = from + 1; i < to + 1; i++) {
               const index = indexAnchors.get(i) ?? i;
               const newIndex = index - 1;
               indexAnchors.set(i, newIndex);
            }
         } else {
            const start = offsetAnchors.get(to) ?? to;
            const end = offsetAnchors.get(from) ?? from;

            for (let i = start; i < end; i++) {
               const index = indexAnchors.get(i) ?? i;
               const newIndex = index + 1;
               indexAnchors.set(i, newIndex);
               offsetAnchors.set(newIndex, i);
            }
         }

         indexAnchors.set(index, to);
      }

      for (let i = 0; i < offset; i++) {
         change.moved.push(this.getMoveChange(endIndex, 0));
         const [value] = change.value.splice(endIndex, 1);
         change.value.splice(0, 0, value);
      }
   }

   protected moveSteps<M>(
      change: IReactiveArrayInternalChange<M>,
      _steps: number
   ) {
      const array = change.value;
      const steps = Math.abs(_steps);
      const last = array.length - 1;
      const from = _steps < 0 ? 0 : last;
      const to = _steps < 0 ? last : 0;

      for (let i = 0; i < steps; i++) {
         const [value] = array.splice(from, 1);
         array.splice(to, 0, value);
         change.moved.push(this.getMoveChange(from, to));
      }
   }

   protected moveReverse<M>(
      change: IReactiveArrayInternalChange<M>
   ) {
      const array = change.value;
      const from = array.length - 1;

      for (let i = 0; i < from; i++) {
         const to = i;
         const [value] = array.splice(from, 1);
         array.splice(to, 0, value);
         change.moved.push(this.getMoveChange(from, to));
      }
   }

   protected getMoveChange(from: number, to: number): IMove {
      return Object.freeze({ from, to });
   }
   // ==================================================
   //                Sort
   // ==================================================
   protected setSorter(sorter: TReactiveArraySorter<T> | undefined) {
      if (sorter === undefined) {
         delete this._sorter;
         return;
      }

      const change = this.createInternalChange(this._value);

      this._sorter = sorter;
      this.sortCurrent(change);
      this.addValuesChange(change);
   }

   protected sortCurrent<M>(change: IReactiveArrayInternalChange<M>) {
      const sortables = this.valuesToSortables(change.value);
      const sorted = [...sortables].sort(this.getSorter());
      const values: M[] = [];
      const moved: IMove[] = [];

      for (let to = 0; to < sorted.length; to++) {
         const sortable = sorted[to];
         const from = sortable.index;
         const prevSortable = sortables[to];

         values.push(sortable.value);

         if (from === to) continue;
         prevSortable.index = from;
         sortables[from] = prevSortable;
         moved.push({ from, to, swap: true });
      }

      change.value = values;
      change.moved = moved;
   }

   protected sortUpdated<M>(change: IReactiveArrayInternalChange<M>) {
      const sortables = this.valuesToSortables(change.value);
      const updated: ISortable<M>[] = [];
      const values: M[] = [];
      const moves: IMove[] = [];

      for (let i = 0; i < change.updated.length; i++) {
         updated.push(sortables[change.updated[i].index])
      }

      sortables.sort(this.getSorter());

      for (let i = 0; i < updated.length; i++) {
         const update = updated[i];
         const from = update.index;
         const to = sortables.indexOf(update);
         if (from === to) continue;
         moves.push({ from, to });

         for (let j = i + 1; j < updated.length; j++) {
            const next = updated[j];
            if (from < next.index && to >= next.index) next.index--;
            if (from > next.index && to <= next.index) next.index++;
         }
      }

      for (let i = 0; i < sortables.length; i++) {
         values.push(sortables[i].value);
      }

      change.moved = moves;
      change.value = values;
   }

   protected sortInitial<M>(array: readonly M[]) {
      const sortables: ISortable<M>[] = [];
      const values: M[] = [];

      for (let i = 0; i < array.length; i++) {
         sortables.push(this.newValueToSortable(array[i]));
      }

      sortables.sort(this.getSorter());

      for (let i = 0; i < sortables.length; i++) {
         values.push(sortables[i].value);
      }

      return values;
   }

   protected sortAdd<M>(
      change: IReactiveArrayInternalChange<M>,
      sortables: ISortable<M>[],
   ) {
      const added: IIndexAdded<M>[] = [];
      const values: M[] = [];

      sortables.sort(this.getSorter());

      const firstAddIndex = sortables.findIndex(({ index }) => index === -1);
      let prev = firstAddIndex - 1;
      let packStart = firstAddIndex;
      let pack: M[] = [];

      for (let i = 0; i < sortables.length; i++) {
         const weight = sortables[i];
         const value = sortables[i].value;

         if (weight.index === -1) {
            if ((i - prev) === 1) {
               pack.push(value);
            } else {
               added.push(this.getAddChange(packStart, pack));
               pack = [value];
               packStart = i;
            }

            prev = i;
         }

         values.push(value);
      }

      if (pack.length) added.push(this.getAddChange(packStart, pack));

      change.value = values;
      change.added = added;
   }

   protected valuesToSortables<M>(array: readonly M[]) {
      const sortables: ISortable<M>[] = [];
      for (let i = 0; i < array.length; i++) {
         sortables.push(this.valueToSortable(array[i], i));
      }
      return sortables;
   }

   protected newValueToSortable<M>(value: M): ISortable<M> {
      return { value, index: -1 };
   }

   protected valueToSortable<M>(value: M, index: number): ISortable<M> {
      return { value, index };
   }

   protected getSorter<M>() {
      return (a: ISortable<M>, b: ISortable<M>) =>
         (this._sorter as any)(a.value, b.value) || a.index - b.index;
   }
   // ==================================================
   //                Source
   // ==================================================
   protected onSourceValue(
      value: readonly T[] | IReactiveArrayModification<T>, change: IPossibleChange
   ) {
      let handler: (...args: any[]) => void;
      if (this.isModification(change)) {
         handler = this.onSourceChange.bind(this);
      } else if (this.isModification(value)) {
         handler = this.onSourceModification.bind(this);
      } else {
         handler = this.onSourceReplace.bind(this);
      }

      const watcher = (<any>this._source)['_watchers'].get(this._id);
      watcher.value = handler;
      handler(value, change);
   }

   protected isModification(change: unknown): change is IReactiveArrayModification<T> {
      return IS.object(change) && `added` in change && `updated` in change && `deleted` in change && `moved` in change;
   }

   protected onSourceReplace(values: readonly T[]) {
      this.send(values);
   }

   protected onSourceChange(_: readonly T[], change: IReactiveArrayModification<T>) {
      this.onSourceModification(change);
   }

   protected onSourceModification(modification: IReactiveArrayModification<T>) {
      const change = this.createInternalChange(this._value);
      this.modify(change, modification);
      this.addValuesChange(change);
   }
   // ==================================================
   //                Change
   // ==================================================
   protected addValuesChange(
      internal: IReactiveArrayInternalChange<T>
   ) {
      const converted = this.createValuesChange(internal);
      this.notifyWatchers(this._value, converted);
      return converted;
   }

   protected createValuesChange(
      internal: IReactiveArrayInternalChange<T>
   ) {
      const converted: IReactiveArrayChange<T> = Object.freeze({
         changed: this.isChanged(internal),
         added: Object.freeze(internal.added),
         deleted: Object.freeze(internal.deleted),
         updated: Object.freeze(internal.updated),
         moved: Object.freeze(internal.moved),
      });
      if (this._getHash) this.setHashes(internal.value);
      this._value = Object.freeze(internal.value);
      return converted;
   }

   protected isChanged<M>({ added, deleted, updated, moved }: IReactiveArrayInternalChange<M>) {
      return !!(moved.length || added.length || deleted.length || updated.length);
   }

   protected createInternalChange<M>(value: readonly M[]): IReactiveArrayInternalChange<M> {
      this.checkErrors();
      return { value: [...value], added: [], deleted: [], updated: [], moved: [] };
   }

   protected createInitialChange(): IReactiveArrayChange<T> {
      return Object.freeze({
         initial: true,
         changed: true,
         added: Object.freeze(this.size ? [this.getAddChange(0, this._value)] : []),
         deleted: Object.freeze([]),
         updated: Object.freeze([]),
         moved: Object.freeze([])
      });
   }

   protected setHashes(values: readonly T[]) {
      this._hashIndex = {};

      for (let i = 0; i < values.length; i++) {
         const hash = this.getHash(values[i]);
         if (hash === null) continue;
         this._hashIndex[hash] = i;
      }
   }
   // ==================================================
   //                Pipeline
   // ==================================================
   public pipe(): IPipeline<readonly T[], readonly T[], this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<readonly T[], IReactiveArrayChange<T>, A>
   ): IPipeline<readonly T[], A, this, typeof pipes>;
   public pipe<A, B,>(
      ...pipes: TPipeArray_2<readonly T[], IReactiveArrayChange<T>, A, B>
   ): IPipeline<readonly T[], B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<readonly T[], IReactiveArrayChange<T>, A, B, D>
   ): IPipeline<readonly T[], D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<readonly T[], IReactiveArrayChange<T>, A, B, D, E>
   ): IPipeline<readonly T[], E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F>
   ): IPipeline<readonly T[], F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G>
   ): IPipeline<readonly T[], G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<readonly T[], H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<readonly T[], I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<readonly T[], J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<readonly T[], K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<readonly T[], unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
