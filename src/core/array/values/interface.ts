import { TMapper } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IChanged, IIndex, IPipeline, IReactiveChange, IReactiveCtx, IReadonlyReactive, IValue, IWatcherCtx, SReverse, TOnValue } from '../../circle';

// ==================================================
//                   Incapsulation
// ==================================================
export interface IReadonlyReactiveArray<T> extends IReadonlyReactive<readonly T[]> {
   readonly size: number;
   has(value: T): boolean;
   has(comparer: TReactiveArrayComparer<T>): boolean;
   get(index: number): T | undefined;
   watch(watcher: TOnValue<readonly T[], IReactiveArrayChange<T>>): string;
   watch(watcher: IWatcherCtx<readonly T[], IReactiveArrayChange<T>>): string;
   pipe(): IPipeline<readonly T[], readonly T[], this>;
   pipe<A>(
      ...pipes: TPipeArray_1<readonly T[], IReactiveArrayChange<T>, A>
   ): IPipeline<readonly T[], A, this, typeof pipes>;
   pipe<A, B>(
      ...pipes: TPipeArray_2<readonly T[], IReactiveArrayChange<T>, A, B>
   ): IPipeline<readonly T[], B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<readonly T[], IReactiveArrayChange<T>, A, B, D>
   ): IPipeline<readonly T[], D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<readonly T[], IReactiveArrayChange<T>, A, B, D, E>
   ): IPipeline<readonly T[], E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F>
   ): IPipeline<readonly T[], F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G>
   ): IPipeline<readonly T[], G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<readonly T[], H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<readonly T[], I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<readonly T[], J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<readonly T[], IReactiveArrayChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<readonly T[], K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<readonly T[], unknown, this>;
}

export interface IReactiveArray<T> extends IReadonlyReactiveArray<T>, IReactiveArrayActions<T> {
   shutdown: string;
}
// ==================================================
//                Ctx
// ==================================================
export type TMapValueToHash<T> = (value: T) => string | null;

export interface IReactiveArrayCtx<T> extends IReactiveCtx {
   sorter?: TReactiveArraySorter<T>;
   getHash?: TMapValueToHash<T>;
}
// ==================================================
//                   Change
// ==================================================
export interface IReactiveArrayModification<T> {
   added: readonly IIndexAdd<T>[];
   deleted: readonly IIndexDelete[];
   updated: readonly IIndexUpdate<T>[];
   moved: readonly IMove[];
}

export interface IReactiveArrayChange<T> extends IReactiveChange, IReactiveArrayModification<T> {
   added: readonly IIndexAdded<T>[];
   deleted: readonly IIndexDeleted<T>[];
   updated: readonly IIndexUpdate<T>[];
   moved: readonly IMove[];
}

export interface IReactiveArrayInternalChange<T> {
   value: T[];
   added: IIndexAdded<T>[];
   deleted: IIndexDeleted<T>[];
   updated: IIndexUpdate<T>[];
   moved: IMove[];
}
// ==================================================
//                   Actions
// ==================================================
export interface IReactiveArrayActions<T> {
   send(values: readonly T[] | null): IReactiveArrayChange<T>;
   send(modification: IReactiveArrayModification<T>): IReactiveArrayChange<T>;
   add(values: readonly T[], index?: number): IReactiveArrayChange<T>;
   add(...packs: IIndexAdd<T>[]): IReactiveArrayChange<T>;
   update(comparer: TReactiveArrayUpdater<T>): IReactiveArrayChange<T>;
   update(...values: IIndexUpdate<T>[]): IReactiveArrayChange<T>;
   delete(comparer: TReactiveArrayDeleter<T>): IReactiveArrayChange<T>;
   delete(...ranges: IIndexDelete[]): IReactiveArrayChange<T>;
   delete(values: Iterable<T>): IReactiveArrayChange<T>;
   move(indexes: Iterable<number>, to: number): IReactiveArrayChange<T>;
   move(steps: number): IReactiveArrayChange<T>;
   move(reverse: SReverse): IReactiveArrayChange<T>;
   move(...moves: IMove[]): IReactiveArrayChange<T>;
}

export type TReactiveArrayDeleter<T> = (value: T, index: number) => boolean | void;
export type TReactiveArrayComparer<T> = (value: T, index: number) => boolean;
export type TReactiveArrayUpdater<T> = (value: T, index: number) => T | void;
export type TReactiveArraySorter<T> = (a: T, b: T, ...args: any[]) => number;

export interface ISortable<T> extends IValue<T>, IIndex {
}
// ==================================================
//                   Index
// ==================================================
export interface IIndexAdd<T> {
   values: readonly T[];
   index?: number;
}

export interface IIndexAdded<T> extends IIndex, IIndexAdd<T> {
   index: number;
}

export interface IIndexDelete extends IIndex {
   count?: number;
}

export interface IIndexDeleted<T> extends IIndexDelete {
   count: number;
   values: readonly T[];
}

export interface IIndexUpdate<T> extends IIndex, IValue<T> {
}

export interface IMove {
   from: number;
   to: number;
   swap?: boolean;
}
