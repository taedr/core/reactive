import { Reactive } from "./core";
import { TRB, TRI, TRN, TRS } from "./type";

// ==================================================
//                   String
// ==================================================
export class ReactiveString<T = string> extends Reactive<TRS<T>> {
}
// ==================================================
//                   Number
// ==================================================
export class ReactiveNumber<T = number> extends Reactive<TRN<T>> {
}
// ==================================================
//                   Boolean
// ==================================================
export class ReactiveBoolean<T = boolean> extends Reactive<TRB<T>> {
}
// ==================================================
//                   Bigint
// ==================================================
export class ReactiveBigint<T = bigint> extends Reactive<TRI<T>> {
}
