import { TStringKeys } from "@taedr/utils";
import { IReactive, IReactiveArray, IReactiveArrayModification, IReactiveChange, IReactiveObject, IReactiveObjectChange, IReactiveRecord, IReactiveRecordModification, IReadonlyReactive, IReadonlyReactiveArray, IReadonlyReactiveObject, IReadonlyReactiveRecord, IReadonlyRecord } from "../circle";

export type TRV<V, T> = T & (V | null);
export type TRS<T> = TRV<string, T>;
export type TRB<T> = boolean | TRV<boolean, T>;
export type TRN<T> = TRV<number, T>;
export type TRI<T> = TRV<bigint, T>;
export type TRO<T> = TRV<object, T>;

export type TNewWrapper<T, W> = new (value: T, ...args: any[]) => W;
export type TReactiveValue<T> =
   T extends readonly unknown[] ? Readonly<T> :
   T extends object ? T extends Record<string, T[TStringKeys<T>]> ? IReadonlyRecord<T[string]> :
   Readonly<T> : T;

export type TReadonlyReactive<T> =
   T extends readonly unknown[] ? IReadonlyReactiveArray<T> :
   T extends object ? T extends Record<string, T[TStringKeys<T>]> ?
   IReadonlyReactiveRecord<T[string]> : IReadonlyReactiveObject<T> : IReadonlyReactive<T>;

export type TReactive<T> =
   T extends readonly unknown[] ? IReactiveArray<T> :
   T extends object ? T extends Record<string, T[TStringKeys<T>]> ?
   IReactiveRecord<T[string]> : IReactiveObject<T> : IReactive<T>;

export type TReactiveChange<T> =
   T extends readonly unknown[] ? IReactiveArrayModification<T[number]> :
   T extends object ? T extends Record<string, T[TStringKeys<T>]> ?
   IReactiveRecordModification<T[string]> : IReactiveObjectChange<T> : IReactiveChange;
