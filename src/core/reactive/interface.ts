import { APipe, IBusChange, IChanged, IInitial, IPipeline, IReadonlyBus, ITransporter, IWatcherCtx, TOnValue, TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../circle';

// ==================================================
//                Incapsulation
// ==================================================
export interface IReadonlyReactive<T> extends IReadonlyBus<T> {
   readonly value: T;
   watch(watcher: TOnValue<T, IReactiveChange>): string;
   watch(watcher: IWatcherCtx<T, IReactiveChange>): string;
   pipe(): IPipeline<T, T, this>;
   pipe<A>(
      ...pipes: TPipeArray_1<T, IReactiveChange, A>
   ): IPipeline<T, A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IReactiveChange, A, B>
   ): IPipeline<T, B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IReactiveChange, A, B, D>
   ): IPipeline<T, D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IReactiveChange, A, B, D, E>
   ): IPipeline<T, E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IReactiveChange, A, B, D, E, F>
   ): IPipeline<T, F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IReactiveChange, A, B, D, E, F, G>
   ): IPipeline<T, G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IReactiveChange, A, B, D, E, F, G, H>
   ): IPipeline<T, H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IReactiveChange, A, B, D, E, F, G, H, I>
   ): IPipeline<T, I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IReactiveChange, A, B, D, E, F, G, H, I, J>
   ): IPipeline<T, J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IReactiveChange, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<T, K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<T, unknown, this, typeof pipes>;
}

export interface IReactiveSuffix<T, S extends IReadonlyBus<T> = IReadonlyBus<T>>
   extends IReadonlyReactive<T> {
   readonly source: S;
   readonly origin: S extends ITransporter<any> ? S['origin'] : S;
   shutdown: string;
}

export interface IReactive<T> extends IReadonlyReactive<T> {
   shutdown: string;
   send(value: T): void;
}
// ==================================================
//                Ctx
// ==================================================
export interface IReactiveCtx {
   shutdown?: boolean;
}
// ==================================================
//                Watcher
// ==================================================
export interface IReactiveChange extends IBusChange, IChanged, IInitial {
}
