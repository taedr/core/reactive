import { getRuntimeId } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '..';
import { APipe, Bus, EDetachMessage, ERRORS, IBusChange, IPipeline, IReactive, IReactiveChange, IReactiveCtx, IReactiveSuffix, IReadonlyBus, IWatcherCtx, Pipeline, TOnValue, Watcher } from '../circle';

// ==================================================
//                Core
// ==================================================
export class Reactive<T> extends Bus<T> implements IReactive<T> {
   protected _value: T;
   protected _source?: IReadonlyBus<T> | null;
   protected _id?: string;

   public get value(): T { return this._value; }

   protected get source() { return this._source; }

   constructor(
      initial: T,
      ctx?: IReactiveCtx,
   ) {
      super();
      this._value = initial;
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public watch(watcher: TOnValue<T, IReactiveChange>): string;
   public watch(watcher: IWatcherCtx<T, IReactiveChange>): string;
   public watch(a: any): string {
      return super.watch(a);
   }

   public send(value: T): IReactiveChange {
      const change = this.prepareToChange(value);
      this.notifyAboutChange();
      return change;
   }

   protected prepareToChange(value: T): IReactiveChange {
      const change: IReactiveChange = Object.freeze({ changed: value !== this._value });
      this.checkErrors();
      this._value = value;
      this.notifyWatchersPrepare(value, change);
      return change;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected addWatcher(id: string, watcher: Watcher<T, IBusChange>) {
      super.addWatcher(id, watcher);
      this.initialNotify(watcher, id);
   }

   protected initialNotify(watcher: Watcher<T, IBusChange>, id: string) {
      const change = Object.freeze(this.createInitialChange());
      this.notifySingleWatcher(this._value, change, watcher);
   }

   protected createInitialChange(): IReactiveChange {
      return Object.freeze({ initial: true, changed: true });
   }

   protected createWatcher(ctx: IWatcherCtx<T, IBusChange>, id: string) {
      const watcher = new Watcher(ctx);

      if (this._shutdown) {
         this.rejectWatcher(id, watcher);
      } else {
         this.addWatcher(id, watcher);
      }

      return id;
   }

   protected rejectWatcher(id: string, watcher: Watcher<T, IBusChange>): void {
      this._watchers.set(id, watcher);
      this.initialNotify(watcher, id);
      const deleted = this._watchers.delete(id);
      if (deleted && watcher.detach) watcher.detach(this._shutdown as string);
   }

   protected checkInitialShutdown(ctx: IReactiveCtx) {
      if (ctx.shutdown) this.setShutdown(EDetachMessage.Emittion);
   }
   // ---------------------------------------------
   //                   Source
   // ---------------------------------------------
   protected setSource(source: IReadonlyBus<T>) {
      if (!!this._source) throw new Error(ERRORS.source); // #Error#

      const id = this._id = this._id ?? getRuntimeId();
      this._source = source;

      Object.defineProperty(this, `origin`, {
         get() { return (source as any)['origin'] ?? source; },
      })

      source.watch({
         id,
         value: this.onSourceValue.bind(this),
         detach: this.setShutdown.bind(this),
      });
   }

   protected onSourceValue(value: T, _: IBusChange) {
      this.send(value);
   }
   // ---------------------------------------------
   //                   Sutdown
   // ---------------------------------------------
   protected setShutdown(message: string) {
      if (this._shutdown || message === this._id) return; // #Return#
      if (!message) throw new Error(ERRORS.shutdown.message); // #Error#
      if (this._source) {
         this._source.watchers.detach(this._id as string);
         this._source = null;
      }
      this._shutdown = message;
      this.notifyDetachAll(message);
   }
   // ---------------------------------------------
   //                   Pipeline
   // ---------------------------------------------
   public pipe(): IPipeline<T, T, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<T, IReactiveChange, A>
   ): IPipeline<T, A, this, typeof pipes>;
   public pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IReactiveChange, A, B>
   ): IPipeline<T, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IReactiveChange, A, B, D>
   ): IPipeline<T, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IReactiveChange, A, B, D, E>
   ): IPipeline<T, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IReactiveChange, A, B, D, E, F>
   ): IPipeline<T, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IReactiveChange, A, B, D, E, F, G>
   ): IPipeline<T, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IReactiveChange, A, B, D, E, F, G, H>
   ): IPipeline<T, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IReactiveChange, A, B, D, E, F, G, H, I>
   ): IPipeline<T, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IReactiveChange, A, B, D, E, F, G, H, I, J>
   ): IPipeline<T, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IReactiveChange, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<T, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<T, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
// ==================================================
//                Suffix
// ==================================================
export class ReactiveSuffix<T, S extends IReadonlyBus<T>> extends Reactive<T> implements IReactiveSuffix<T, S> {
   protected _sourceNotYetEmitted?: true = true;
   protected _id = getRuntimeId();

   public get origin() { return (<any>this._source)['origin'] ?? this._source; }
   public get source() { return this._source; }

   constructor(
      protected _source: S
   ) {
      super(undefined as any);
      _source.watch({
         id: this._id,
         value: this.onSourceValue.bind(this),
         detach: this.setShutdown.bind(this),
      });
   }

   protected initialNotify(watcher: Watcher<T, IBusChange>, id: string) {
      if (this._sourceNotYetEmitted) return;
      super.initialNotify(watcher, id);
   }

   protected onSourceValue(value: T, _: IBusChange) {
      delete this._sourceNotYetEmitted;
      this.send(value);
   }
}
