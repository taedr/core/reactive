import { IPipe, IReadonlyBus, ITransporter } from '../..';

export interface IPipeline<I, O, S extends IReadonlyBus<I>, P extends readonly IPipe<any, any>[] = IPipe<any, any>[]>
   extends IReadonlyBus<O> {
   readonly pipes: Readonly<P>;
   readonly origin: S extends ITransporter<any> ? S['origin'] : S;
   readonly source: S;
   shutdown: string;
}
