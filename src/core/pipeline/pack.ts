import { IBusChange } from "../circle";
import { APipe } from "./pipe/sync";

export type TPipeArray_1<T, C extends IBusChange, A> = [APipe<T, A, C>];
export type TPipeArray_2<T, C extends IBusChange, A, B> = [...TPipeArray_1<T, C, A>, APipe<A, B>];
export type TPipeArray_3<T, C extends IBusChange, A, B, D> = [...TPipeArray_2<T, C, A, B>, APipe<B, D>];
export type TPipeArray_4<T, C extends IBusChange, A, B, D, E> = [...TPipeArray_3<T, C, A, B, D>, APipe<D, E>];
export type TPipeArray_5<T, C extends IBusChange, A, B, D, E, F> = [...TPipeArray_4<T, C, A, B, D, E>, APipe<E, F>];
export type TPipeArray_6<T, C extends IBusChange, A, B, D, E, F, G> = [...TPipeArray_5<T, C, A, B, D, E, F>, APipe<F, G>];
export type TPipeArray_7<T, C extends IBusChange, A, B, D, E, F, G, H> = [...TPipeArray_6<T, C, A, B, D, E, F, G>, APipe<G, H>];
export type TPipeArray_8<T, C extends IBusChange, A, B, D, E, F, G, H, I> = [...TPipeArray_7<T, C, A, B, D, E, F, G, H>, APipe<H, I>];
export type TPipeArray_9<T, C extends IBusChange, A, B, D, E, F, G, H, I, J> = [...TPipeArray_8<T, C, A, B, D, E, F, G, H, I>, APipe<I, J>];
export type TPipeArray_10<T, C extends IBusChange, A, B, D, E, F, G, H, I, J, K> = [...TPipeArray_9<T, C, A, B, D, E, F, G, H, I, J>, APipe<J, K>];
