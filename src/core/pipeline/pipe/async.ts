import { APipe, IPossibleChange, IReadonlyReactive, Reactive } from '../../circle';


export abstract class AAsyncPipe<I, O, C extends IPossibleChange = IPossibleChange> extends APipe<I, O, C>{
   protected _active = new Reactive(false);

   public get active(): IReadonlyReactive<boolean> { return this._active; }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------

   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected setShutdown(message: string) {
      super.setShutdown(message);
      this._active.shutdown = message;
   }

   protected setActive(active: boolean) {
      if (active === this._active.value) return;
      this._active.send(active);
   }
}
