import { AReadonlyBus, IPossibleChange, IReadonlyBus } from '../../circle';

export interface IPipe<I, O, C extends IPossibleChange = IPossibleChange>
   extends IReadonlyBus<O> {

}

export abstract class APipe<I, O, C extends IPossibleChange = IPossibleChange>
   extends AReadonlyBus<O> implements IPipe<I, O, C>{
   // ---------------------------------------------
   //                   Abstract
   // ---------------------------------------------
   protected abstract transform(value: I, change: C): void;
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected activate() { }
   protected deactivate(pipelineId: string) { }
}
