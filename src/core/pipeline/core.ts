import { getRuntimeId } from '@taedr/utils';
import { AShutdownOn } from '../../pipes/sync/shutdownOn';
import { Tier } from '../../pipes/sync/tie';
import { AAsyncPipe, APipe, AReadonlyBus, IBusChange, IPipeline, IPossibleChange, IReadonlyBus, IWatcherCtx, PIPELINE } from '../circle';

// ==================================================
//                   Pipeline
// ==================================================
export class Pipeline<I, O, S extends IReadonlyBus<I>, P extends APipe<unknown, unknown>[]>
   extends AReadonlyBus<O> implements IPipeline<I, O, S, P> {
   protected _id = getRuntimeId();
   protected _shutdownMark?: string;
   protected _lastActivePipeIndex?: number;

   public set shutdown(message: string) { this.manualShutdown(message); }
   public get shutdown() { return this._shutdown ?? ``; }
   public get pipes() { return this._pipes; }
   public get origin() { return (<any>this._source)['origin'] ?? this._source; }
   public get source() { return this._source; }

   constructor(
      protected _source: S,
      protected _pipes: P,
   ) {
      super();
      this._pipes = Object.freeze([..._pipes]) as P;
      this.init();
   }
   // ---------------------------------------------
   //                   Init
   // ---------------------------------------------
   protected init() {
      for (let i = 0; i < this._pipes.length; i++) {
         if (!(this._pipes[i] instanceof Tier)) this.watchPipe(i);
      }
   }
   // ---------------------------------------------
   //                   Lifecycle
   // ---------------------------------------------
   protected onFirstWatch() {
      const [first] = this._pipes;
      const onValue = first ? first['transform'].bind(first) : this.notifyWatchers.bind(this);
      const ctx: IWatcherCtx<unknown, IPossibleChange> = {
         id: this._id,
         value: onValue,
         detach: this.sourceShutdown.bind(this),
      };

      for (let i = 0; i < this._pipes.length; i++) {
         const pipe = this._pipes[i];

         if (pipe instanceof Tier) {
            this.watchPipe(i);
         } else {
            pipe['activate']();
         }
      }

      this._source.watch(ctx);
   }

   protected onLastDetach(): void {
      if (this._shutdownMark) {
         this.setShutdown(this._shutdownMark);
      } else {
         this.detachFromSource(this._id);
         this._pipes.forEach(p => p['deactivate'](this._id));
      }
   }
   // ---------------------------------------------
   //                   Pipe
   // ---------------------------------------------
   protected watchPipe(index: number) {
      const pipe = this._pipes[index] as APipe<any, any>;
      const next = this._pipes[index + 1];
      const isLastIndex = index === this._pipes.length - 1;
      const value = isLastIndex ? this.notifyWatchers.bind(this) : next['transform'].bind(next);
      const ctx: IWatcherCtx<unknown, IBusChange> = { id: PIPELINE, value };

      if (pipe instanceof Tier) ctx.id = this._id;
      if (pipe instanceof AShutdownOn) ctx.detach = message => this.autoShutdown(message, index);

      pipe.watch(ctx);
   }

   protected shutdownPipes(index: number, message: string) {
      const start = this._lastActivePipeIndex as number;
      for (let i = start; i < index; i++) {
         const pipe = this._pipes[i];
         (<number>this._lastActivePipeIndex) += 1;
         pipe.watchers.detach(PIPELINE);
         pipe['deactivate'](this._id);
         pipe['setShutdown'](message);
      }
   }
   // ---------------------------------------------
   //                   Shutdown
   // ---------------------------------------------
   protected checkShutdown() {
      const start = this._lastActivePipeIndex as number;
      const message = this._shutdownMark as string;

      for (let i = start; i < this._pipes.length; i++) {
         const pipe = this._pipes[i] as APipe<any, any>;
         const active = pipe instanceof AAsyncPipe && pipe.active.value;

         if (active) {
            pipe.active.watch(this.onAsyncPipeState.bind(this));
            return;
         }

         (<number>this._lastActivePipeIndex) += 1;
         pipe['deactivate'](this._id);
         pipe['setShutdown'](message);
      }

      this.setShutdown(message);
   }

   protected onAsyncPipeState(active: boolean) {
      if (!active) this.checkShutdown();
   }

   protected manualShutdown(message: string) {
      this._lastActivePipeIndex = this._lastActivePipeIndex ?? 0;
      this.detachFromSource(message);
      this.shutdownPipes(this._pipes.length, message);
      this.setShutdown(message);
   }

   protected autoShutdown(message: string, index: number) {
      if (this._shutdownMark !== undefined) return; // #Return#
      this._shutdownMark = message;
      this._lastActivePipeIndex = 0;
      this.detachFromSource(message);

      if (index === this._pipes.length - 1) {
         this.setShutdown(message);
      } else {
         this.shutdownPipes(index, message);
         this.checkShutdown();
      }
   }

   protected sourceShutdown(message: string) {
      if (this._shutdownMark !== undefined || message === this._id) return; // #Return#
      this._shutdownMark = message;
      this._lastActivePipeIndex = 0;
      this.checkShutdown();
   }

   protected setShutdown(message: string) {
      super.setShutdown(message);
      delete this._shutdownMark;
      delete this._lastActivePipeIndex;
   }

   protected detachFromSource(message: string) {
      this._source.watchers.detach(this._id, message);
   }
}
