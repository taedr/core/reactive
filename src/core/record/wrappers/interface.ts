import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IKey, IKeyValue, IPipeline, IReactive, IReactiveRecordActions, IReactiveRecordChange, IReactiveRecordModification, IReadonlyReactive, IReadonlyReactiveRecord, IReadonlyRecord, IWatcherCtx, IWrapper, TOnValue, TReactiveRecordDeleter, TReactiveRecordIncluder, TReactiveRecordUpdater } from '../../circle';

// ==================================================
//                   Incapsulation
// ==================================================
export interface IReadonlyReactiveWrapperRecord<T, W> extends IReadonlyReactiveRecord<T> {
   readonly wrappers: IReadonlyRecord<W>;
   hasWrapper(wrapper: W): boolean;
   hasWrapper(comparer: TReactiveRecordIncluder<W>): boolean;
   watch(watcher: TOnValue<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>>): string;
   watch(watcher: IWatcherCtx<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>>): string;
   pipe(): IPipeline<IReadonlyRecord<T>, IReadonlyRecord<T>, this>;
   pipe<A>(
      ...pipes: TPipeArray_1<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A>
   ): IPipeline<IReadonlyRecord<T>, A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B>
   ): IPipeline<IReadonlyRecord<T>, B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D>
   ): IPipeline<IReadonlyRecord<T>, D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E>
   ): IPipeline<IReadonlyRecord<T>, E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F>
   ): IPipeline<IReadonlyRecord<T>, F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G>
   ): IPipeline<IReadonlyRecord<T>, G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H>
   ): IPipeline<IReadonlyRecord<T>, H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I>
   ): IPipeline<IReadonlyRecord<T>, I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<IReadonlyRecord<T>, J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<IReadonlyRecord<T>, K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<IReadonlyRecord<T>, unknown, this>;
}

export interface IReactiveWrapperRecord<T, W> extends IReadonlyReactiveWrapperRecord<T, W>, IReactiveWrapperRecordActions<T, W> {

}

export interface IReadonlyReactivesRecord<T, R extends IReadonlyReactive<T> = IReadonlyReactive<T>> extends IReadonlyReactiveWrapperRecord<T, R> {
}

export interface IReactivesRecord<T, R extends IReactive<T> = IReactive<T>> extends IReadonlyReactivesRecord<T, R>, IReactiveWrapperRecord<T, R> {
}
// ==================================================
//                   Actions
// ==================================================
export interface IReactiveWrapperRecordActions<T, W> extends IReactiveRecordActions<T> {
   sendWrappers(value: IReadonlyRecord<W>): void;
   sendWrappers(modification: IReactiveRecordModification<W>): void;
   add(values: IReadonlyRecord<T>): IReactiveWrapperRecordChange<T, W>;
   add(values: readonly IKeyValue<T>[]): IReactiveWrapperRecordChange<T, W>;
   addWrappers(values: IReadonlyRecord<W>): IReactiveWrapperRecordChange<T, W>;
   addWrappers(values: readonly IKeyValue<W>[]): IReactiveWrapperRecordChange<T, W>;
   update(values: readonly IKeyValue<T>[]): IReactiveWrapperRecordChange<T, W>;
   update(values: IReadonlyRecord<T>): IReactiveWrapperRecordChange<T, W>;
   update(comparer: TReactiveRecordUpdater<T>): IReactiveWrapperRecordChange<T, W>;
   updateWrappers(values: readonly IKeyValue<W>[]): IReactiveWrapperRecordChange<T, W>;
   updateWrappers(values: IReadonlyRecord<W>): IReactiveWrapperRecordChange<T, W>;
   updateWrappers(comparer: TReactiveRecordUpdater<W>): IReactiveWrapperRecordChange<T, W>;
   delete(comparer: TReactiveRecordDeleter<T>): IReactiveWrapperRecordChange<T, W>;
   delete(values: readonly T[]): IReactiveWrapperRecordChange<T, W>;
   delete(...values: IKey[]): IReactiveWrapperRecordChange<T, W>;
   deleteWrappers(comparer: TReactiveRecordDeleter<W>): IReactiveWrapperRecordChange<T, W>;
   deleteWrappers(values: readonly W[]): IReactiveWrapperRecordChange<T, W>;
}
// ==================================================
//                   Change
// ==================================================
export interface IReactiveWrapperRecordChange<T, W> extends IReactiveRecordChange<T> {
   deleted: readonly IKeyWrapper<T, W>[];
   updated: readonly IKeyWrapper<T, W>[];
   added: readonly IKeyWrapper<T, W>[];
}

export interface IReactivesRecordChange<T, R extends IReadonlyReactive<T> = IReadonlyReactive<T>> extends IReactiveWrapperRecordChange<T, R> {
}
// ==================================================
//                   Key
// ==================================================
export interface IKeyWrapper<T, W> extends IKeyValue<T>, IWrapper<W> {
}
