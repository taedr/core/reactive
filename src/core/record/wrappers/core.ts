import { getRuntimeId, IS } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IKey, IKeyValue, IKeyWrapper, IPipeline, IReactive, IReactiveChange, IReactiveRecordCtx, IReactiveRecordInternalChange, IReactiveRecordModification, IReactiveWrapperRecord, IReactiveWrapperRecordChange, IReadonlyRecord, IWatcherCtx, Pipeline, Reactive, TOnValue, TReactiveRecordDeleter, TReactiveRecordIncluder, TReactiveRecordUpdater } from '../../circle';
import { ReactiveRecord } from '../values/core';

// ==================================================
//                   Core
// ==================================================
export abstract class AReactiveWrapperRecord<T, W> extends ReactiveRecord<T> implements IReactiveWrapperRecord<T, W>{
   protected _id = getRuntimeId();
   protected _wrappers: IReadonlyRecord<W> = {};
   protected _changes?: IReactiveWrapperRecordChange<T, W>[];

   public get wrappers(): IReadonlyRecord<W> { return this._wrappers; }

   constructor(
      initial: IReadonlyRecord<T> | IReadonlyRecord<W> = {},
      ctx?: IReactiveRecordCtx,
   ) {
      super({});
      this._size = Object.keys(initial).length;
      if (this.size) this.setInitial(initial);
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ---------------------------------------------
   //                   Abstract
   // ---------------------------------------------
   protected abstract isWrappers(value: IReadonlyRecord<T> | IReadonlyRecord<W>): value is IReadonlyRecord<W>;
   protected abstract getReactive(wrapper: W): IReactive<T>;
   protected abstract createWrapper(value: T): W;
   // ==================================================
   //                Initial
   // ==================================================
   protected setInitial(initial: IReadonlyRecord<T> | IReadonlyRecord<W>) {
      const values: Record<string, T> = {};
      const wrappers: Record<string, W> = {};

      this._processing = true;

      if (this.isWrappers(initial)) {
         for (const key in initial) {
            wrappers[key] = initial[key];
            values[key] = this.watchWrapper(key, wrappers[key]);
         }
      } else {
         for (const key in initial) {
            const wrapper = this.createWrapper(initial[key]);
            const reactive = this.getReactive(wrapper);
            values[key] = this.watchReactive(key, reactive);
            wrappers[key] = wrapper;
         }
      }

      this._value = Object.freeze(values);
      this._wrappers = Object.freeze(wrappers);
      delete this._processing;
   }
   // ==================================================
   //                Watch
   // ==================================================
   public watch(watcher: TOnValue<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>>): string;
   public watch(watcher: IWatcherCtx<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ==================================================
   //                Utils
   // ==================================================
   public hasWrapper(value: W): boolean;
   public hasWrapper(comaparer: TReactiveRecordIncluder<W>): boolean;
   public hasWrapper(a: W | TReactiveRecordIncluder<W>) {
      return this.hasValue(this._wrappers, a);
   }
   // ==================================================
   //                Send
   // ==================================================
   public send(values: IReadonlyRecord<T> | null): IReactiveWrapperRecordChange<T, W>;
   public send(modification: IReactiveRecordModification<T>): IReactiveWrapperRecordChange<T, W>;
   public send(a: IReadonlyRecord<T> | IReactiveRecordModification<T> | null): IReactiveWrapperRecordChange<T, W> {
      const change = this.createInternalChange(this._wrappers);

      if (a === null) {
         this.replace(change, {});
      } else if (this.isModification(a)) {
         this.modifyValues(change, a);
      } else {
         const wrappers = this.valuesToWrappers(a);
         this.replace(change, wrappers);
      }

      return this.addWrappersChange(change);
   }

   public sendWrappers(wrappers: IReadonlyRecord<W> | null): IReactiveWrapperRecordChange<T, W>;
   public sendWrappers(modification: IReactiveRecordModification<W>): IReactiveWrapperRecordChange<T, W>;
   public sendWrappers(a: IReadonlyRecord<W> | IReactiveRecordModification<W> | null) {
      const change = this.createInternalChange(this._wrappers);
      this.replaceRouter(change, a);
      return this.addWrappersChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected modifyValues(
      change: IReactiveRecordInternalChange<W>,
      { added, updated, deleted }: IReactiveRecordModification<T>
   ) {
      if (deleted.length) this.deleteEntries(change, deleted);
      if (added.length) this.addValueRouter(change, added);
      if (updated.length) this.updateValueRouter(change, updated);
   }

   protected prepareToChange(value: IReadonlyRecord<T>) {
      const internal = this.createInternalChange(this._wrappers);
      const wrappers = this.valuesToWrappers(value);
      this.replace(internal, wrappers);
      const change = this.createWrappersChange(internal);
      this.notifyWatchersPrepare(this._value, change);
      return change;
   }
   // ==================================================
   //                Add
   // ==================================================
   public add(values: IReadonlyRecord<T>): IReactiveWrapperRecordChange<T, W>;
   public add(values: readonly IKeyValue<T>[]): IReactiveWrapperRecordChange<T, W>;
   public add(
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[],
   ): IReactiveWrapperRecordChange<T, W> {
      const internal = this.createInternalChange(this._wrappers);
      this.addValueRouter(internal, a);
      return this.addWrappersChange(internal);
   }

   public addWrappers(wrappers: IReadonlyRecord<W>): IReactiveWrapperRecordChange<T, W>;
   public addWrappers(wrappers: readonly IKeyValue<W>[]): IReactiveWrapperRecordChange<T, W>;
   public addWrappers(
      a: IReadonlyRecord<W> | readonly IKeyValue<W>[],
   ): IReactiveWrapperRecordChange<T, W> {
      const internal = this.createInternalChange(this._wrappers);
      this.addRouter(internal, a);
      return this.addWrappersChange(internal);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected addValueRouter(
      change: IReactiveRecordInternalChange<W>,
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[],
   ) {
      const values = IS.readonlyArray(a) ? this.entriesToWrappers(a) : this.valuesToWrappers(a);
      this.addRouter(change, values);
   }



   // ==================================================
   //                Delete
   // ==================================================
   public delete(comparer: TReactiveRecordDeleter<T>): IReactiveWrapperRecordChange<T, W>;
   public delete(values: readonly T[]): IReactiveWrapperRecordChange<T, W>;
   public delete(...values: IKey[]): IReactiveWrapperRecordChange<T, W>;
   public delete(
      a: readonly T[] | TReactiveRecordDeleter<T> | IKey,
      ...b: IKey[]
   ): IReactiveWrapperRecordChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.deleteValueRouter(change, a, ...b);
      return this.addWrappersChange(change);
   }

   public deleteWrappers(comparer: TReactiveRecordDeleter<W>): IReactiveWrapperRecordChange<T, W>;
   public deleteWrappers(values: readonly W[]): IReactiveWrapperRecordChange<T, W>;
   public deleteWrappers(a: readonly W[] | TReactiveRecordDeleter<W>): IReactiveWrapperRecordChange<T, W> {
      const internal = this.createInternalChange(this._wrappers);
      this.deleteRouter(internal, a);
      return this.addWrappersChange(internal);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected deleteValueRouter(
      change: IReactiveRecordInternalChange<W>,
      a: readonly T[] | TReactiveRecordDeleter<T> | IKey,
      ...b: IKey[]
   ) {
      if (IS.readonlyArray(a)) {
         this.deleteCompare(change, (_, i) => a.includes(this._value[i]));
      } else if (IS.function(a)) {
         this.deleteCompare(change, (_, i) => a(this._value[i], i));
      } else {
         const packs: IKey[] = [];
         if (a) packs.push(a as IKey);
         packs.push(...b);
         this.deleteEntries(change, packs);
      }
   }
   // ==================================================
   //                Update
   // ==================================================
   public update(values: readonly IKeyValue<T>[]): IReactiveWrapperRecordChange<T, W>;
   public update(values: IReadonlyRecord<T>): IReactiveWrapperRecordChange<T, W>;
   public update(comparer: TReactiveRecordUpdater<T>): IReactiveWrapperRecordChange<T, W>;
   public update(
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[] | TReactiveRecordUpdater<T>
   ): IReactiveWrapperRecordChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.updateValueRouter(change, a);
      return this.addWrappersChange(change);
   }

   public updateWrappers(values: IReadonlyRecord<W>): IReactiveWrapperRecordChange<T, W>;
   public updateWrappers(values: readonly IKeyValue<W>[]): IReactiveWrapperRecordChange<T, W>;
   public updateWrappers(comparer: TReactiveRecordUpdater<W>): IReactiveWrapperRecordChange<T, W>;
   public updateWrappers(
      a: IReadonlyRecord<W> | readonly IKeyValue<W>[] | TReactiveRecordUpdater<W>
   ): IReactiveWrapperRecordChange<T, W> {
      const change = this.createInternalChange(this._wrappers);
      this.updateRouter(change, a);
      return this.addWrappersChange(change);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected updateValueRouter(
      change: IReactiveRecordInternalChange<W>,
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[] | TReactiveRecordUpdater<T>
   ) {
      this.setUpdateChange = this.setWrapperUpdateChange as any;
      this.updateCompare = this.updateValueCompare as any;
      this.updateRouter(change, a as any);
      delete (this as any).getUpdateChange;
      delete (this as any).updateCompare;
   }

   protected updateValueCompare(
      change: IReactiveRecordInternalChange<W>,
      comparer: TReactiveRecordUpdater<T>
   ) {
      for (const key in this._value) {
         const prev = this._value[key];
         const newValue = comparer(prev, key);
         if (newValue === undefined || newValue === prev) continue;
         this.setWrapperUpdateChange(change, key, newValue);
      }
   }

   protected updateValueEntries(
      change: IReactiveRecordInternalChange<W>,
      entries: readonly IKeyValue<T>[]
   ) {
      for (let i = 0; i < entries.length; i++) {
         const { key, value } = entries[i];
         const prev = change.value[key];
         if (prev === undefined) continue;
         this.setWrapperUpdateChange(change, key, value);
      }
   }

   protected updateValueRecord<M>(
      change: IReactiveRecordInternalChange<M>,
      record: IReadonlyRecord<M>
   ) {
      for (const key in record) {
         const value = record[key];
         const prev = change.value[key];
         if (prev === undefined) continue;
         this.setUpdateChange(change, key, value);
      }
   }

   protected setWrapperUpdateChange(
      change: IReactiveRecordInternalChange<W>,
      key: string,
      value: T
   ) {
      const wrapper = this._wrappers[key];
      const reactive = this.getReactive(wrapper) as unknown as Reactive<any>;

      this.addPrepared(reactive, value);
      change.updated.push(this.createValueChange(key, wrapper));
   }
   // ==================================================
   //                Wrapper
   // ==================================================
   protected watchWrapper(key: string, wrapper: W) {
      const reactive = this.getReactive(wrapper)
      return this.watchReactive(key, reactive);
   }

   protected watchReactive(key: string, reactive: IReactive<T>) {
      reactive.watch({
         id: this._id,
         prepare: true,
         value: (_, change) => this.onWrapperChange(key, change)
      });
      return reactive.value;
   }

   protected detachWrapper(key: string): IKeyWrapper<T, W> {
      const wrapper = this._wrappers[key];
      this.getReactive(wrapper).watchers.detach(this._id);
      return Object.freeze({ key, value: this._value[key], wrapper });
   }
   // ==================================================
   //                   Source
   // ==================================================
   protected onSourceModification(modification: IReactiveRecordModification<T>) {
      const change = this.createInternalChange(this._wrappers);
      this.modifyValues(change, modification);
      this.addWrappersChange(change);
   }
   // ==================================================
   //                Change
   // ==================================================
   protected addWrappersChange(
      internal: IReactiveRecordInternalChange<W>
   ) {
      const change = this.createWrappersChange(internal);
      this.notifyWatchers(this._value, change);
      delete this._processing;
      return change;
   }

   protected createWrappersChange(
      internal: IReactiveRecordInternalChange<W>
   ) {
      const values = this.wrappersToValues(internal.value);
      const change: IReactiveWrapperRecordChange<T, W> = {
         changed: this.isChanged(internal),
         added: Object.freeze(this.convertAdded(internal.added)),
         deleted: Object.freeze(this.convertDeleted(internal.deleted)),
         updated: Object.freeze(this.convertUpdated(internal.updated))
      };

      this._value = Object.freeze(values);
      this._wrappers = Object.freeze(internal.value);
      this._size = Object.keys(this._value).length;

      return Object.freeze(change);
   }

   protected createInternalChange<M>(value: IReadonlyRecord<M>): IReactiveRecordInternalChange<M> {
      this.checkErrors();
      this._processing = true;
      return super.createInternalChange(value);
   }

   protected createInitialChange(): IReactiveWrapperRecordChange<T, W> {
      const added: IKeyWrapper<T, W>[] = [];
      const deleted = Object.freeze([]);
      const updated = Object.freeze([]);

      for (const key in this._wrappers) {
         added.push(this.createAddWrapperChange(key, this._value[key], this._wrappers[key]));
      }

      Object.freeze(added);

      return Object.freeze({ initial: true, changed: true, added, deleted, updated });
   }

   protected createWrapperChange(key: string, wrapper: W): IKeyWrapper<T, W> {
      return Object.freeze({ key, value: this.getReactive(wrapper).value, wrapper });
   }
   // ==================================================
   //                Mappers
   // ==================================================
   protected valuesToWrappers(values: IReadonlyRecord<T>) {
      const wrappers: Record<string, W> = {};
      for (const key in values) {
         wrappers[key] = this.createWrapper(values[key]);
      }
      return wrappers;
   }

   protected wrappersToValues(wrappers: IReadonlyRecord<W>) {
      const values: Record<string, T> = {};
      for (const key in wrappers) {
         values[key] = this.getReactive(wrappers[key]).value;
      }
      return values;
   }

   protected entriesToWrappers(entries: readonly IKeyValue<T>[]) {
      const converted: IKeyValue<W>[] = [];
      for (let i = 0; i < entries.length; i++) {
         const { key, value } = entries[i];
         converted.push({ key, value: this.createWrapper(value) });
      }
      return converted;
   }

   protected createAddWrapperChange(key: string, value: T, wrapper: W) {
      return Object.freeze({ key, value, wrapper });
   }
   // ==================================================
   //                Converters
   // ==================================================
   protected convertDeleted(deleted: readonly IKeyValue<W>[]): IKeyWrapper<T, W>[] {
      const converted: IKeyWrapper<T, W>[] = [];

      for (let i = 0; i < deleted.length; i++) {
         converted.push(this.detachWrapper(deleted[i].key));
      }

      return converted;
   }

   protected convertAdded(added: readonly IKeyValue<W>[]): IKeyWrapper<T, W>[] {
      const converted: IKeyWrapper<T, W>[] = [];
      for (let i = 0; i < added.length; i++) {
         const { key, value: wrapper } = added[i];
         const value = this.watchWrapper(key, wrapper);
         converted.push(Object.freeze({ key, value, wrapper }));
      }
      return converted;
   }

   protected onWrapperChange(key: string, { prepare }: IReactiveChange) {
      if (this._processing) return; // #Return#
      if (!prepare) return this.notifyAboutChange();

      const internal = this.createInternalChange(this._wrappers);
      delete this._processing;
      internal.updated = [this.createValueChange(key, this._wrappers[key])];
      const change = this.createWrappersChange(internal);
      this.notifyWatchersPrepare(this._value, change);
   }

   protected convertUpdated(updated: readonly IKeyValue<W>[]): IKeyWrapper<T, W>[] {
      const converted: IKeyWrapper<T, W>[] = [];

      for (let i = 0; i < updated.length; i++) {
         const { key, value: wrapper } = updated[i];
         const prev = this._wrappers[key];
         const reactive = this.getReactive(wrapper);
         const value = reactive.value;

         if (wrapper !== prev) {
            this.detachWrapper(key);
            this.watchWrapper(key, wrapper);
         }

         converted.push(Object.freeze({ key, value, wrapper }));
      }

      return converted;
   }
   // ==================================================
   //                Shutdown
   // ==================================================
   protected setShutdown(message: string) {
      super.setShutdown(message);
      for (const key in this._wrappers) {
         this.detachWrapper(key)
      }
   }
   // ==================================================
   //                Pipeline
   // ==================================================
   public pipe(): IPipeline<IReadonlyRecord<T>, IReadonlyRecord<T>, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A>
   ): IPipeline<IReadonlyRecord<T>, A, this, typeof pipes>;
   public pipe<A, B>(
      ...pipes: TPipeArray_2<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B>
   ): IPipeline<IReadonlyRecord<T>, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D>
   ): IPipeline<IReadonlyRecord<T>, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E>
   ): IPipeline<IReadonlyRecord<T>, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F>
   ): IPipeline<IReadonlyRecord<T>, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G>
   ): IPipeline<IReadonlyRecord<T>, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H>
   ): IPipeline<IReadonlyRecord<T>, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I>
   ): IPipeline<IReadonlyRecord<T>, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<IReadonlyRecord<T>, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<IReadonlyRecord<T>, IReactiveWrapperRecordChange<T, W>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<IReadonlyRecord<T>, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<IReadonlyRecord<T>, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
