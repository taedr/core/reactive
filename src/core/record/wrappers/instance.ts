import { Reactive, AReactiveObject, IReactive, IReactivesRecord, IReadonlyRecord, ReactiveBigint, ReactiveBoolean, ReactiveNumber, ReactiveString, TRB, TRI, TRN, TRO, TRS } from "../../circle";
import { AReactiveWrapperRecord } from './core';

// ==================================================
//                   Core
// ==================================================
export abstract class AReactivesRecord<T, R extends IReactive<T>> extends AReactiveWrapperRecord<T, R> implements IReactivesRecord<T, R> {
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected getReactive(wrapper: R) {
      return wrapper;
   }

   protected isWrappers(record: IReadonlyRecord<T> | IReadonlyRecord<R>): record is IReadonlyRecord<R> {
      for (const key in record) {
         return record[key] instanceof Reactive;
      }
      return false;
   }
}
// ==================================================
//                   Primitive
// ==================================================
export abstract class AReactivesPrimitiveRecord<T extends string | number | boolean | bigint | null, R extends IReactive<T>> extends AReactivesRecord<T, R> {

}

export class ReactivesStringRecord<T = string> extends AReactivesPrimitiveRecord<TRS<T>, ReactiveString<T>> {
   protected createWrapper(value: TRS<T>): ReactiveString<T> {
      return new ReactiveString(value);
   }
}

export class ReactivesNumberRecord<T = number> extends AReactivesPrimitiveRecord<TRN<T>, ReactiveNumber<T>> {
   protected createWrapper(value: TRN<T>): ReactiveNumber<T> {
      return new ReactiveNumber(value);
   }
}

export class ReactivesBooleanRecord<T = boolean> extends AReactivesPrimitiveRecord<TRB<T>, ReactiveBoolean<T>> {
   protected createWrapper(value: TRB<T>): ReactiveBoolean<T> {
      return new ReactiveBoolean(value);
   }
}

export class ReactivesBigintRecord<T = bigint> extends AReactivesPrimitiveRecord<TRI<T>, ReactiveBigint<T>> {
   protected createWrapper(value: TRI<T>): ReactiveBigint<T> {
      return new ReactiveBigint(value);
   }
}
// ==================================================
//                   Object
// ==================================================
export abstract class AReactivesObjectRecord<T extends object, N extends null, R extends AReactiveObject<T, N>> extends AReactivesRecord<T | N, R> {
}
// ==================================================
//                   Dimensional
// ==================================================
// export class ReactiveDimensionalRecord<T extends readonly unknown[]> extends ReactiveObjectRecord<T> {
// }
