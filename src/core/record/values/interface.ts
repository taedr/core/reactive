import { TMapper } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IChanged, IKey, IPipeline, IReactiveChange, IReactiveCtx, IReadonlyReactive, IValue, IWatcherCtx, TOnValue } from '../../circle';


// ==================================================
//                   Incapsulation
// ==================================================
export interface IReadonlyReactiveRecord<T> extends IReadonlyReactive<IReadonlyRecord<T>> {
   readonly size: number;
   has(value: T): boolean;
   has(comaparer: TReactiveRecordIncluder<T>): boolean;
   watch(watcher: TOnValue<IReadonlyRecord<T>, IReactiveRecordChange<T>>): string;
   watch(watcher: IWatcherCtx<IReadonlyRecord<T>, IReactiveRecordChange<T>>): string;
   pipe(): IPipeline<IReadonlyRecord<T>, IReadonlyRecord<T>, this>;
   pipe<A>(
      ...pipes: TPipeArray_1<IReadonlyRecord<T>, IReactiveRecordChange<T>, A>
   ): IPipeline<IReadonlyRecord<T>, A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B>
   ): IPipeline<IReadonlyRecord<T>, B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D>
   ): IPipeline<IReadonlyRecord<T>, D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E>
   ): IPipeline<IReadonlyRecord<T>, E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F>
   ): IPipeline<IReadonlyRecord<T>, F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G>
   ): IPipeline<IReadonlyRecord<T>, G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<IReadonlyRecord<T>, H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<IReadonlyRecord<T>, I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<IReadonlyRecord<T>, J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<IReadonlyRecord<T>, K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<IReadonlyRecord<T>, unknown, this>;
}

export interface IReactiveRecord<T> extends IReadonlyReactiveRecord<T>, IReactiveRecordActions<T> {
   shutdown: string;
}
// ==================================================
//                Ctx
// ==================================================
export interface IReactiveRecordCtx extends IReactiveCtx {
}
// ==================================================
//                   Value
// ==================================================
export interface IReadonlyRecord<T> extends Readonly<Record<string, T>> {
}
// ==================================================
//                   Actions
// ==================================================
export interface IReactiveRecordActions<T> {
   send(values: IReadonlyRecord<T> | null): IReactiveRecordChange<T>;
   send(modification: IReactiveRecordModification<T>): IReactiveRecordChange<T>;
   add(values: IReadonlyRecord<T>): IReactiveRecordChange<T>;
   add(values: readonly IKeyValue<T>[]): IReactiveRecordChange<T>;
   update(values: readonly IKeyValue<T>[]): IReactiveRecordChange<T>;
   update(values: IReadonlyRecord<T>): IReactiveRecordChange<T>;
   update(comparer: TReactiveRecordUpdater<T>): IReactiveRecordChange<T>;
   delete(comparer: TReactiveRecordDeleter<T>): IReactiveRecordChange<T>;
   delete(values: readonly T[]): IReactiveRecordChange<T>;
   delete(...values: IKey[]): IReactiveRecordChange<T>;
}

export type TReactiveRecordDeleter<T> = (value: T, key: string) => boolean | void;
export type TReactiveRecordIncluder<T> = (value: T, key: string) => boolean;
export type TReactiveRecordUpdater<T> = (value: T, key: string) => T | void;
// ==================================================
//                   Change
// ==================================================
export interface IReactiveRecordModification<T> {
   added: readonly IKeyValue<T>[];
   deleted: readonly IKey[];
   updated: readonly IKeyValue<T>[];
}

export interface IReactiveRecordChange<T> extends IReactiveChange, IReactiveRecordModification<T> {
   added: readonly IKeyValue<T>[];
   deleted: readonly IKeyValue<T>[];
   updated: readonly IKeyValue<T>[];
}

export interface IReactiveRecordInternalChange<T> {
   value: Record<string, T>;
   added: IKeyValue<T>[];
   deleted: IKeyValue<T>[];
   updated: IKeyValue<T>[];
}
// ==================================================
//                   Key
// ==================================================
export interface IKeyValue<T> extends IValue<T>, IKey {
}
