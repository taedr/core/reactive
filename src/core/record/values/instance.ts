import { TRB, TRI, TRN, TRO, TRS } from "../../circle";
import { ReactiveRecord } from "./core";

// ==================================================
//                   Primitive
// ==================================================
export abstract class AReactivePrimitiveRecord<T extends string | number | boolean | bigint | null | undefined> extends ReactiveRecord<T> {
}
// ==================================================
//                   String
// ==================================================
export class ReactiveStringRecord<T = string> extends AReactivePrimitiveRecord<TRS<T>> {
}
// ==================================================
//                   Number
// ==================================================
export class ReactiveNumberRecord<T = number> extends AReactivePrimitiveRecord<TRN<T>> {
}
// ==================================================
//                   Boolean
// ==================================================
export class ReactiveBooleanRecord<T = boolean> extends AReactivePrimitiveRecord<TRB<T>> {
}
// ==================================================
//                   Bigint
// ==================================================
export class ReactiveBigintRecord<T = bigint> extends AReactivePrimitiveRecord<TRI<T>> {
}
// ==================================================
//                   Object
// ==================================================
export class ReactiveObjectRecord<T extends object> extends ReactiveRecord<TRO<T>> {
}
// ==================================================
//                   Dimensional
// ==================================================
export class ReactiveDimensionalRecord<T extends readonly unknown[]> extends ReactiveObjectRecord<T> {
}
