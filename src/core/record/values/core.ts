import { IS } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IKey, IKeyValue, IPipeline, IPossibleChange, IWatcherCtx, Pipeline, Reactive, TOnValue } from '../../circle';
import { IReactiveRecord, IReactiveRecordChange, IReactiveRecordCtx, IReactiveRecordInternalChange, IReactiveRecordModification, IReadonlyRecord, TReactiveRecordDeleter, TReactiveRecordIncluder, TReactiveRecordUpdater } from './interface';

export class ReactiveRecord<T> extends Reactive<IReadonlyRecord<T>> implements IReactiveRecord<T> {
   protected _size = 0;
   protected _changes?: IReactiveRecordChange<T>[];

   public get size() { return this._size; }

   constructor(
      initial: IReadonlyRecord<T> = {},
      ctx?: IReactiveRecordCtx
   ) {
      super(Object.freeze({}));
      this._size = Object.keys(initial).length;
      if (this._size) this.setInitial(initial);
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ==================================================
   //                Initial
   // ==================================================
   protected setInitial(initial: IReadonlyRecord<T>) {
      this._value = Object.freeze({ ...initial });
   }
   // ==================================================
   //                Watch
   // ==================================================
   public watch(watcher: TOnValue<IReadonlyRecord<T>, IReactiveRecordChange<T>>): string;
   public watch(watcher: IWatcherCtx<IReadonlyRecord<T>, IReactiveRecordChange<T>>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ==================================================
   //                Utils
   // ==================================================
   public has(value: T): boolean;
   public has(comaparer: TReactiveRecordIncluder<T>): boolean;
   public has(a: T | TReactiveRecordIncluder<T>) {
      return this.hasValue(this._value, a);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected hasValue<M>(
      record: IReadonlyRecord<M>,
      a: M | TReactiveRecordIncluder<M>
   ) {
      const comaparer = IS.function(a) ? a : (value: M) => value === a;
      for (const key in record) {
         if (comaparer(record[key], key)) return true;
      }
      return false;
   }
   // ==================================================
   //                Send
   // ==================================================
   public send(values: IReadonlyRecord<T> | null): IReactiveRecordChange<T>;
   public send(modification: IReactiveRecordModification<T>): IReactiveRecordChange<T>;
   public send(a: IReadonlyRecord<T> | IReactiveRecordModification<T> | null): IReactiveRecordChange<T> {
      const internal = this.createInternalChange(this._value);
      this.replaceRouter(internal, a);
      const change = this.addValuesChange(internal);
      return change;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected replaceRouter<M>(
      change: IReactiveRecordInternalChange<M>,
      a: IReadonlyRecord<M> | IReactiveRecordModification<M> | null
   ) {
      if (a === null) {
         this.replace(change, {});
      } else if (this.isModification(a)) {
         this.modify(change, a);
      } else {
         this.replace(change, a);
      }
   }

   protected replace<M>(
      change: IReactiveRecordInternalChange<M>,
      newValues: IReadonlyRecord<M>
   ) {
      if (this.size) change.deleted = this.recordToEntries(change.value);
      change.value = { ...newValues };
      change.added = this.recordToEntries(change.value);
   }

   protected modify<M>(
      change: IReactiveRecordInternalChange<M>,
      { added, deleted, updated }: IReactiveRecordModification<M>
   ) {
      if (deleted.length) this.deleteEntries(change, deleted);
      if (added.length) this.addEntries(change, added);
      if (updated.length) this.updateEntries(change, updated);
   }

   protected prepareToChange(value: IReadonlyRecord<T>) {
      const internal = this.createInternalChange(this._value);
      this.replace(internal, value);
      const change = this.createValuesChange(internal);
      this.notifyWatchersPrepare(this._value, change);
      return change;
   }
   // ==================================================
   //                Add
   // ==================================================
   public add(values: IReadonlyRecord<T>): IReactiveRecordChange<T>;
   public add(values: readonly IKeyValue<T>[]): IReactiveRecordChange<T>;
   public add(
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[]
   ): IReactiveRecordChange<T> {
      const internal = this.createInternalChange(this._value);
      this.addRouter(internal, a);
      return this.addValuesChange(internal);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected addRouter<M>(
      change: IReactiveRecordInternalChange<M>,
      a: IReadonlyRecord<M> | readonly IKeyValue<M>[],
   ) {
      if (IS.readonlyArray(a)) {
         return this.addEntries(change, a);
      } else {
         return this.addRecord(change, a);
      }
   }

   protected addEntries<M>(
      change: IReactiveRecordInternalChange<M>,
      entries: readonly IKeyValue<M>[]
   ) {
      for (let i = 0; i < entries.length; i++) {
         const { key, value } = entries[i];
         const prev = change.value[key];

         if (prev !== undefined) {
            this.setUpdateChange(change, key, value);
         } else {
            this.setAddChange(change, key, value);
         }
      }
   }

   protected addRecord<M>(
      change: IReactiveRecordInternalChange<M>,
      record: IReadonlyRecord<M>
   ) {
      for (const key in record) {
         const prev = change.value[key];
         const value = record[key];

         if (prev !== undefined) {
            this.setUpdateChange(change, key, value);
         } else {
            this.setAddChange(change, key, value);
         }
      }
   }

   protected setAddChange<M>(
      change: IReactiveRecordInternalChange<M>,
      key: string,
      value: M
   ) {
      change.value[key] = value;
      change.added.push(this.createValueChange(key, value));
   }
   // ==================================================
   //                Delete
   // ==================================================
   public delete(comparer: TReactiveRecordDeleter<T>): IReactiveRecordChange<T>;
   public delete(values: readonly T[]): IReactiveRecordChange<T>;
   public delete(...values: IKey[]): IReactiveRecordChange<T>;
   public delete(
      a: readonly T[] | TReactiveRecordDeleter<T> | IKey,
      ...b: IKey[]
   ): IReactiveRecordChange<T> {
      const internal = this.createInternalChange(this._value);
      this.deleteRouter(internal, a as any, ...b);
      return this.addValuesChange(internal);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected deleteRouter<M>(
      change: IReactiveRecordInternalChange<M>,
      a: readonly M[] | TReactiveRecordDeleter<M> | IKey,
      ...b: IKey[]
   ) {
      if (IS.readonlyArray(a)) {
         this.deleteCompare(change, value => a.includes(value));
      } else if (IS.function(a)) {
         this.deleteCompare(change, a);
      } else if (a !== undefined) {
         this.deleteEntries(change, [a, ...b]);
      }
   }

   protected deleteCompare<M>(
      change: IReactiveRecordInternalChange<M>,
      comparer: TReactiveRecordDeleter<M>
   ) {
      for (const key in change.value) {
         if (comparer(change.value[key], key)) {
            this.setDeleteChange(change, key);
         }
      }
   }

   protected deleteEntries<M>(
      change: IReactiveRecordInternalChange<M>,
      entries: readonly IKey[]
   ) {
      for (let i = 0; i < entries.length; i++) {
         const { key } = entries[i];
         if (change.value[key] === undefined) continue;
         this.setDeleteChange(change, key);
      }
   }

   protected setDeleteChange<M>(
      change: IReactiveRecordInternalChange<M>,
      key: string,
   ) {
      change.deleted.push(this.createValueChange(key, change.value[key]));
      delete change.value[key];
   }
   // ==================================================
   //                Update
   // ==================================================
   public update(values: readonly IKeyValue<T>[]): IReactiveRecordChange<T>;
   public update(values: IReadonlyRecord<T>): IReactiveRecordChange<T>;
   public update(comparer: TReactiveRecordUpdater<T>): IReactiveRecordChange<T>;
   public update(
      a: IReadonlyRecord<T> | readonly IKeyValue<T>[] | TReactiveRecordUpdater<T>
   ): IReactiveRecordChange<T> {
      const internal = this.createInternalChange(this._value);
      this.updateRouter(internal, a);
      return this.addValuesChange(internal);
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected updateRouter<M>(
      change: IReactiveRecordInternalChange<M>,
      a: IReadonlyRecord<M> | readonly IKeyValue<M>[] | TReactiveRecordUpdater<M>
   ) {
      if (IS.readonlyArray(a)) {
         this.updateEntries(change, a);
      } else if (IS.function(a)) {
         this.updateCompare(change, a);
      } else {
         this.updateRecord(change, a);
      }
   }

   protected updateCompare<M>(
      change: IReactiveRecordInternalChange<M>,
      comparer: TReactiveRecordUpdater<M>
   ) {
      for (const key in change.value) {
         const prev = change.value[key];
         const value = comparer(prev, key);
         if (value === undefined) continue;
         this.setUpdateChange(change, key, value);
      }
   }

   protected updateEntries<M>(
      change: IReactiveRecordInternalChange<M>,
      entries: readonly IKeyValue<M>[]
   ) {
      for (let i = 0; i < entries.length; i++) {
         const { key, value } = entries[i];
         const prev = change.value[key];
         if (prev === undefined) continue;
         this.setUpdateChange(change, key, value);
      }
   }

   protected updateRecord<M>(
      change: IReactiveRecordInternalChange<M>,
      record: IReadonlyRecord<M>
   ) {
      for (const key in record) {
         const value = record[key];
         const prev = change.value[key];
         if (prev === undefined) continue;
         this.setUpdateChange(change, key, value);
      }
   }

   protected setUpdateChange<M>(
      change: IReactiveRecordInternalChange<M>,
      key: string,
      value: M
   ) {
      change.value[key] = value;
      change.updated.push(this.createValueChange(key, value));
   }
   // ==================================================
   //                Notify
   // ==================================================
   protected saveChange(value: IReadonlyRecord<T>, change: IReactiveRecordChange<T>): void {
      this._changes = [...this._changes ?? [], change];
      this._value = value;
   }

   protected notifyChanges(): void {
      if (this._changes === undefined) return;
      const [last] = this._changes.slice(-1);

      for (let i = 0; i < this._changes.length - 1; i++) {
         this.notifyWatchers({}, this._changes[i]);
      }

      this.notifyWatchers(this._value, last);
      delete this._changes;
   }
   // ==================================================
   //                Source
   // ==================================================
   protected onSourceValue(
      value: IReadonlyRecord<T> | IReactiveRecordModification<T>, change: IPossibleChange
   ) {
      let handler: (...args: any[]) => void;
      if (this.isModification(change)) {
         handler = this.onSourceChange.bind(this);
      } else if (this.isModification(value)) {
         handler = this.onSourceModification.bind(this);
      } else {
         handler = this.onSourceReplace.bind(this);
      }

      const watcher = (<any>this._source)['_watchers'].get(this._id);
      watcher.value = handler;
      handler(value, change);
   }

   protected isModification(change: unknown): change is IReactiveRecordModification<unknown> {
      return IS.object(change) && `added` in change && IS.array(change.added) && `updated` in change && `deleted` in change;
   }

   protected onSourceReplace(values: IReadonlyRecord<T>) {
      this.send(values);
   }

   protected onSourceChange(_: IReadonlyRecord<T>, change: IReactiveRecordModification<T>) {
      this.onSourceModification(change);
   }

   protected onSourceModification(change: IReactiveRecordModification<T>) {
      const internal = this.createInternalChange(this._value);
      this.modify(internal, change);
      this.addValuesChange(internal);
   }
   // ==================================================
   //                Change
   // ==================================================
   protected addValuesChange(
      internal: IReactiveRecordInternalChange<T>
   ) {
      const change = this.createValuesChange(internal);
      this.notifyWatchers(this._value, change);
      return change;
   }

   protected createValuesChange(
      internal: IReactiveRecordInternalChange<T>
   ): IReactiveRecordChange<T> {
      const change: IReactiveRecordChange<T> = {
         changed: this.isChanged(internal),
         added: Object.freeze(internal.added),
         deleted: Object.freeze(internal.deleted),
         updated: Object.freeze(internal.updated)
      };

      this._value = Object.freeze(internal.value);
      this._size = Object.keys(this._value).length;
      return change;
   }

   protected createInternalChange<M>(value: IReadonlyRecord<M>): IReactiveRecordInternalChange<M> {
      this.checkErrors();
      return { value: { ...value }, added: [], deleted: [], updated: [] };
   }

   protected createInitialChange(): IReactiveRecordChange<T> {
      return Object.freeze({ 
         initial: true, 
         changed: true, 
         added: Object.freeze(this.recordToEntries(this._value)), 
         deleted: Object.freeze([]), 
         updated: Object.freeze([]) 
      });
   }

   protected createValueChange<M>(key: string, value: M): IKeyValue<M> {
      return Object.freeze({ key, value });
   }

   protected isChanged<M>({ added, deleted, updated }: IReactiveRecordInternalChange<M>) {
      return !!(added.length || deleted.length || updated.length);
   }

   protected recordToEntries<M>(record: IReadonlyRecord<M>) {
      const entries: IKeyValue<M>[] = [];
      for (const key in record) {
         entries.push(Object.freeze({ key, value: record[key] }));
      }
      return entries;
   }
   // ==================================================
   //                Pipeline
   // ==================================================
   public pipe(): IPipeline<IReadonlyRecord<T>, IReadonlyRecord<T>, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<IReadonlyRecord<T>, IReactiveRecordChange<T>, A>
   ): IPipeline<IReadonlyRecord<T>, A, this, typeof pipes>;
   public pipe<A, B>(
      ...pipes: TPipeArray_2<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B>
   ): IPipeline<IReadonlyRecord<T>, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D>
   ): IPipeline<IReadonlyRecord<T>, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E>
   ): IPipeline<IReadonlyRecord<T>, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F>
   ): IPipeline<IReadonlyRecord<T>, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G>
   ): IPipeline<IReadonlyRecord<T>, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<IReadonlyRecord<T>, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<IReadonlyRecord<T>, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<IReadonlyRecord<T>, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<IReadonlyRecord<T>, IReactiveRecordChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<IReadonlyRecord<T>, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<IReadonlyRecord<T>, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
