import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';
import { APipe, IPipeline, IReactiveArrayModification, IReactiveRecordModification, IReactiveSuffix, IReadonlyReactive, IReadonlyReactiveArray, IReadonlyReactiveRecord } from '../circle';

// ==================================================
//                Incapsulation
// ==================================================
export interface IReadonlyBus<T> {
   readonly watchers: IWatchers;
   readonly shutdown: string;
   watch(watcher: TOnValue<T, IBusChange>): string;
   watch(watcher: IWatcherCtx<T, IBusChange>): string;
   suffix(): IReactiveSuffix<T, this>;
   suffix<R extends IReadonlyReactive<T>>(
      reactive: R,
   ): R & ITransporter<this>;
   suffix<R extends IReadonlyReactiveArray<T extends IReactiveArrayModification<infer I> ? I : never>>(
      array: R,
   ): R & ITransporter<this>;
   suffix<R extends IReadonlyReactiveRecord<T extends IReactiveRecordModification<infer I> ? I : never>>(
      record: R,
   ): R & ITransporter<this>;
   pipe(): IPipeline<T, T, this>;
   pipe<A>(
      ...pipes: TPipeArray_1<T, IBusChange, A>
   ): IPipeline<T, A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IBusChange, A, B>
   ): IPipeline<T, B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IBusChange, A, B, D>
   ): IPipeline<T, D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IBusChange, A, B, D, E>
   ): IPipeline<T, E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IBusChange, A, B, D, E, F>
   ): IPipeline<T, F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IBusChange, A, B, D, E, F, G>
   ): IPipeline<T, G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IBusChange, A, B, D, E, F, G, H>
   ): IPipeline<T, H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IBusChange, A, B, D, E, F, G, H, I>
   ): IPipeline<T, I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IBusChange, A, B, D, E, F, G, H, I, J>
   ): IPipeline<T, J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IBusChange, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<T, K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<T, unknown, this, typeof pipes>;
}

export interface IBus<T> extends IReadonlyBus<T> {
   shutdown: string;
   send(value: T): void;
}

export interface ITransporter<S extends IReadonlyBus<unknown>> {
   origin: S;
}
// ==================================================
//                Change
// ==================================================
export interface IBusChange {
   prepare?: true;
}

export interface IPendingChange<T> {
   value: T;
   change: IBusChange;
}
// ==================================================
//                Watcher
// ==================================================
export type TOnDeatch = (message: string) => void;
export type TOnValue<T, C extends IBusChange> = (value: T, change: C) => void;

export interface IWatcherCtx<T, C extends IBusChange> {
   id?: string;
   value: TOnValue<T, C>;
   prepare?: boolean;
   detach?: TOnDeatch;
}

export interface IWatchers {
   readonly size: number;
   detach(id: string, message?: string): boolean;
   has(id: string): boolean;
}
