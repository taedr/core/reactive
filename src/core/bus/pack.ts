import { IReadonlyBus } from "./interface";

export type TReadonlyBusArray_1<A> = readonly [IReadonlyBus<A>];
export type TReadonlyBusArray_2<A, B> = readonly [IReadonlyBus<A>, IReadonlyBus<B>];
export type TReadonlyBusArray_3<A, B, C> = readonly [IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>];
export type TReadonlyBusArray_4<A, B, C, D> = readonly [IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>, IReadonlyBus<D>];
export type TReadonlyBusArray_5<A, B, C, D, E> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>,
   IReadonlyBus<D>, IReadonlyBus<E>
];
export type TReadonlyBusArray_6<A, B, C, D, E, F> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>,
   IReadonlyBus<D>, IReadonlyBus<E>, IReadonlyBus<F>
];
export type TReadonlyBusArray_7<A, B, C, D, E, F, G> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>, IReadonlyBus<D>,
   IReadonlyBus<E>, IReadonlyBus<F>, IReadonlyBus<G>
];
export type TReadonlyBusArray_8<A, B, C, D, E, F, G, H> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>, IReadonlyBus<D>,
   IReadonlyBus<E>, IReadonlyBus<F>, IReadonlyBus<G>, IReadonlyBus<H>
];
export type TReadonlyBusArray_9<A, B, C, D, E, F, G, H, I> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>, IReadonlyBus<D>, IReadonlyBus<E>,
   IReadonlyBus<F>, IReadonlyBus<G>, IReadonlyBus<H>, IReadonlyBus<I>
];
export type TReadonlyBusArray_10<A, B, C, D, E, F, G, H, I, J> = readonly [
   IReadonlyBus<A>, IReadonlyBus<B>, IReadonlyBus<C>, IReadonlyBus<D>, IReadonlyBus<E>,
   IReadonlyBus<F>, IReadonlyBus<G>, IReadonlyBus<H>, IReadonlyBus<I>, IReadonlyBus<J>
];
