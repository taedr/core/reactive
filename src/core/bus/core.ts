import { getRuntimeId, IS } from '@taedr/utils';
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '..';
import { APipe, ERRORS, IBus, IBusChange, IPendingChange, IPipeline, IPossibleChange, IReactive, IReactiveArrayModification, IReactiveRecordModification, IReactiveSuffix, IReadonlyBus, IReadonlyReactive, IReadonlyReactiveArray, IReadonlyReactiveRecord, IWatcherCtx, IWatchers, Pipeline, Reactive, ReactiveSuffix, TOnValue, Watcher } from '../circle';


// ==================================================
//                    Core
// ==================================================
export abstract class AReadonlyBus<T> implements IReadonlyBus<T>  {
   protected _watchers = new Map<string, Watcher<T, IBusChange>>();
   protected _shutdown?: string;
   protected _prepared?: AReadonlyBus<any>[];
   protected _processing?: true;
   protected _pending?: IPendingChange<T>;

   public get watchers() { return this.getWatchers(); }
   public get shutdown() { return this._shutdown ?? ``; }
   // ---------------------------------------------
   //                   Watch
   // ---------------------------------------------
   public watch(watcher: TOnValue<T, IBusChange>): string;
   public watch(watcher: IWatcherCtx<T, IBusChange>): string;
   public watch(a: TOnValue<T, IBusChange> | IWatcherCtx<T, IBusChange>): string {
      const ctx: IWatcherCtx<T, IBusChange> = IS.function(a) ? { value: a } : a;
      const id = ctx.id ?? getRuntimeId();

      if (this._watchers.has(id)) {
         throw new Error(ERRORS.id(id));
      }

      return this.createWatcher(ctx, id);
   }

   protected createWatcher(ctx: IWatcherCtx<T, IBusChange>, id: string) {
      const watcher = new Watcher(ctx);

      if (this._shutdown) {
         this.rejectWatcher(id, watcher);
      } else {
         this.addWatcher(id, watcher);
      }

      return id;
   }

   protected rejectWatcher(id: string, watcher: Watcher<T, IBusChange>) {
      if (watcher.detach) watcher.detach(this._shutdown as string);
   }

   protected detach(id: string, message: string): boolean {
      const watcher = this._watchers.get(id);
      if (!watcher) return false;
      const deleted = this.deleteWatcher(id, watcher);
      if (deleted && watcher.detach) watcher.detach(message ?? '');
      if (this._watchers.size === 0) this.onLastDetach();
      return true;
   }

   protected deleteWatcher(id: string, watcher: Watcher<T, IBusChange>) {
      return this._watchers.delete(id);
   }

   protected addWatcher(id: string, watcher: Watcher<T, IBusChange>) {
      this._watchers.set(id, watcher);
      if (this._watchers.size === 1) this.onFirstWatch();
   }
   // ---------------------------------------------
   //                   Hooks
   // ---------------------------------------------
   protected onFirstWatch() { }
   protected onLastDetach() { }
   // ---------------------------------------------
   //                   Send
   // ---------------------------------------------
   protected send(value: T) {
      this.checkErrors();
      this.notifyWatchers(value, {});
   }

   protected prepareToChange(value: T): IPossibleChange {
      const change: IBusChange = {};
      this.checkErrors();
      this.notifyWatchersPrepare(value, change);
      return change;
   }

   protected notifyWatchers(value: T, change: IBusChange) {
      this.notifyWatchersPrepare(value, change);
      this.notifyAboutChange();
   }

   protected notifyWatchersPrepare(value: T, change: IBusChange) {
      if (this._pending) throw new Error(ERRORS.recursive);
      const prepareChange: IBusChange = Object.freeze({ ...change, prepare: true });

      this._pending = { value, change };

      for (const [_, watcher] of this._watchers) {
         if (watcher.prepare) watcher.value(value, prepareChange);
      }
   }

   protected notifyAboutChange() {
      const { value, change } = this._pending as IPendingChange<T>;

      if (this._prepared) {
         this._processing = true;
         for (let i = 0; i < this._prepared.length; i++) {
            this._prepared[i]['notifyAboutChange']();
         }
         delete this._processing;
         delete this._prepared;
      }

      Object.freeze(change);

      for (const [_, watcher] of this._watchers) {
         if (!watcher.prepare) watcher.value(value, change);
      }

      for (const [_, watcher] of this._watchers) {
         if (watcher.prepare) watcher.value(value, change);
      }

      delete this._pending;
   }

   protected addPrepared<M>(
      reactive: AReadonlyBus<M>,
      value: M
   ) {
      this._prepared = this._prepared ?? [];
      this._prepared.push(reactive);
      return reactive['prepareToChange'](value);
   }

   protected notifySingleWatcher(value: T, change: IBusChange, watcher: Watcher<T, IBusChange>,): void {
      this._pending = { value, change };

      if (watcher.prepare) {
         const prepareChange: IBusChange = Object.freeze({ ...change, prepare: true });
         watcher.value(value, prepareChange);
      }

      Object.freeze(change);

      watcher.value(value, change);
      delete this._pending;
   }

   protected checkErrors() {
      if (this._shutdown) throw new Error(ERRORS.shutdown.change); // #Error#
   }

   protected getWatchers(): IWatchers {
      const watchers: IWatchers = {
         size: NaN,
         has: id => this._watchers.has(id),
         detach: (id, message) => this.detach(id, message ?? id),
      }
      Object.defineProperty(watchers, `size`, {
         get: () => this._watchers.size,
      });
      return watchers;
   }
   // ---------------------------------------------
   //                   Shutdown
   // ---------------------------------------------
   protected setShutdown(message: string) {
      if (!message) throw new Error(ERRORS.shutdown.message); // #Error#
      this._shutdown = message;
      this.notifyDetachAll(message);
   }

   protected notifyDetachAll(message: string) {
      const someWatchers = this._watchers.size > 0;

      for (const [id, watcher] of [...this._watchers]) {
         if (watcher.prepare && watcher.detach) {
            this.deleteWatcher(id, watcher);
            watcher.detach(message);
         }
      }

      for (const [id, watcher] of [...this._watchers]) {
         this.deleteWatcher(id, watcher);
         if (watcher.detach) watcher.detach(message);
      }

      if (someWatchers) this.onLastDetach();
   }
   // ---------------------------------------------
   //                   Suffix
   // ---------------------------------------------
   public suffix(): IReactiveSuffix<T, this>;
   public suffix<R extends IReadonlyReactive<T>>(
      reactive: R,
   ): R;
   public suffix<R extends IReadonlyReactiveArray<T extends IReactiveArrayModification<infer I> ? I : never>>(
      array: R,
   ): R & IReactiveSuffix<T, this>;
   public suffix<R extends IReadonlyReactiveRecord<T extends IReactiveRecordModification<infer I> ? I : never>>(
      record: R,
   ): R & IReactiveSuffix<T, this>;
   public suffix(
      reactive?: IReactive<T>,
   ): any {
      const suffix = reactive ?? new ReactiveSuffix<T, this>();
      if (!(suffix instanceof Reactive)) throw new Error(ERRORS.suffix);

      suffix['setSource'](this);
      return suffix;
   }
   // ---------------------------------------------
   //                   Terminal
   // ---------------------------------------------
  
   // ---------------------------------------------
   //                   Pipeline
   // ---------------------------------------------
   public pipe(): IPipeline<T, T, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<T, IBusChange, A>
   ): IPipeline<T, A, this, typeof pipes>;
   public pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IBusChange, A, B>
   ): IPipeline<T, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IBusChange, A, B, D>
   ): IPipeline<T, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IBusChange, A, B, D, E>
   ): IPipeline<T, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IBusChange, A, B, D, E, F>
   ): IPipeline<T, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IBusChange, A, B, D, E, F, G>
   ): IPipeline<T, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IBusChange, A, B, D, E, F, G, H>
   ): IPipeline<T, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IBusChange, A, B, D, E, F, G, H, I>
   ): IPipeline<T, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IBusChange, A, B, D, E, F, G, H, I, J>
   ): IPipeline<T, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IBusChange, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<T, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<T, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
// ==================================================
//                   Instance
// ==================================================
export class Bus<T> extends AReadonlyBus<T> implements IBus<T> {
   public get shutdown(): string { return this._shutdown ?? ``; }
   public set shutdown(message: string) { this.setShutdown(message); }

   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public send(value: T, change: IBusChange = {}) {
      this.checkErrors();
      this.notifyWatchers(value, change);
   }
}
