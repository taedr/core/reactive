import { IS, TDeepPartial, TStringKeys } from "@taedr/utils";
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '..';
import { APipe, IPipeline, IReactiveChange, IReactiveObjectChange, IReactiveWrapperCtx, IWatcherCtx, Pipeline, Reactive, TObjectSymbols, TOnValue, TReactiveWrapperFields } from "../circle";


export abstract class AReactiveWrapper<T extends object, W, F extends TReactiveWrapperFields<T, W>, N extends null = never> extends Reactive<Readonly<T> | N> {
   protected _fields: F;

   public get value() { return this._value; }
   public get fields() { return this._fields; }

   constructor(
      ctx: IReactiveWrapperCtx<T, W, F>,
   ) {
      super({} as any);
      this._fields = Object.freeze(ctx.fields);
      this._value = this.watchFields(ctx.symbols);
      if (ctx.null) this.clearFields();
      if (ctx) this.checkInitialShutdown(ctx);
   }
   // ---------------------------------------------
   //                   Abstract
   // ---------------------------------------------
   protected abstract getReactive(wrapper: W): Reactive<any>;
   protected abstract createInstance(): T;
   // ---------------------------------------------
   //                   Watch
   // ---------------------------------------------
   public watch(watcher: TOnValue<T, IReactiveObjectChange<T>>): string;
   public watch(watcher: IWatcherCtx<T, IReactiveObjectChange<T>>): string;
   public watch(a: any): string {
      return super.watch(a);
   }
   // ---------------------------------------------
   //                   Send
   // ---------------------------------------------
   public send(value: T | TDeepPartial<T> | N) {
      const change = this.prepareToChange(value);
      this.notifyAboutChange();
      return change;
   }

   protected prepareToChange(value: T | TDeepPartial<T> | N) {
      const change = IS.null(value) ? this.clearFields() : this.updateFields(value);
      this.notifyWatchersPrepare(this._value, change);
      return change;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected watchFields(symbols?: TObjectSymbols<T>) {
      const initial: any = this.createInstance();

      this._processing = true;

      for (const key in this._fields) {
         const reactive = this.getReactive(this._fields[key]);

         reactive.watch({
            id: `OBJECT`,
            prepare: true,
            value: (value, change) => this.onFieldChange(key as any, value, change),
         });
         initial[key] = reactive.value;
      }

      for (const s of Object.getOwnPropertySymbols(symbols ?? {})) {
         initial[s] = (<any>symbols)[s];
      }

      delete this._processing;

      return Object.freeze(initial);
   }

   protected onFieldChange(
      key: TStringKeys<T>,
      value: unknown,
      { changed, prepare }: IReactiveChange,
   ) {
      if (this._processing) return; // #Return#
      if (!prepare) return this.notifyAboutChange();

      this.checkErrors();

      const updated = this.getCurretCopy();
      const delta = { [key]: value } as TDeepPartial<T>;
      const change = this.createObjectChange(changed, delta);

      updated[key] = value;
      this._value = Object.freeze(updated);
      this.notifyWatchersPrepare(this._value, change);
   }


   protected updateFields(
      instance: T | TDeepPartial<T>
   ) {
      const curr = this._value ?? {};
      const delta: any = {};
      const fields: any = {};
      let updated = this.getCurretCopy();
      let changed = false;

      this.checkErrors();
      this._processing = true;

      for (const _key in instance) {
         const key: TStringKeys<T> = _key as any;
         const field = this._fields[key];
         if (!field) continue;
         const reactive = this.getReactive(field);
         const value: any = (instance as any)[key];
         const prev = updated[key];

         if (prev === value || reactive.shutdown) continue;

         const fieldChange = this.addPrepared(reactive, value);

         changed = changed || !!fieldChange.changed;
         updated[key] = reactive.value;
         fields[key] = fieldChange;
         delta[key] = (<any>fieldChange)['delta'] ?? reactive.value;
      }

      for (const s of Object.getOwnPropertySymbols(curr)) {
         updated[s] = (<any>instance)[s] ?? (<any>curr)[s];
      }

      const change = this.createObjectChange(changed, delta);

      this._value = Object.freeze(updated);
      delete this._processing;

      return change;
   }

   protected getCurretCopy(): any {
      const newValue: any = this.createInstance();

      for (const key in this._fields) {
         newValue[key] = this.getReactive(this._fields[key]).value;
      }

      return newValue;
   }

   protected clearFields() {
      const delta = {} as TDeepPartial<T>;
      const changed = this._value === null;
      const change = this.createObjectChange(changed, delta);

      this.checkErrors();
      this._value = null as N;

      return change;
   }

   protected createObjectChange(
      changed: boolean,
      delta: TDeepPartial<T>,
   ): IReactiveObjectChange<T> {
      return { changed, delta: Object.freeze(delta) };
   }

   protected createInitialChange(): IReactiveObjectChange<T> {
      const delta: any = {};

      for (const [key, field] of Object.entries(this._fields)) {
         const reactive = this.getReactive(field as W);
         delta[key] = reactive.value;
      }

      return { initial: true, changed: true, delta };
   }
   // ---------------------------------------------
   //                   Shutdown
   // ---------------------------------------------
   protected setShutdown(message: string) {
      if (message === this._id) return; // #Return#
      super.setShutdown(message);
      for (const key in this._fields) {
         this.getReactive(this._fields[key]).shutdown = message;
      }
   }
   // ---------------------------------------------
   //                   Pipeline
   // ---------------------------------------------
   public pipe(): IPipeline<Readonly<T> | N, T, this>;
   public pipe<A>(
      ...pipes: TPipeArray_1<T, IReactiveObjectChange<T>, A>
   ): IPipeline<Readonly<T> | N, A, this, typeof pipes>;
   public pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IReactiveObjectChange<T>, A, B>
   ): IPipeline<Readonly<T> | N, B, this, typeof pipes>;
   public pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IReactiveObjectChange<T>, A, B, D>
   ): IPipeline<Readonly<T> | N, D, this, typeof pipes>;
   public pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IReactiveObjectChange<T>, A, B, D, E>
   ): IPipeline<Readonly<T> | N, E, this, typeof pipes>;
   public pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IReactiveObjectChange<T>, A, B, D, E, F>
   ): IPipeline<Readonly<T> | N, F, this, typeof pipes>;
   public pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IReactiveObjectChange<T>, A, B, D, E, F, G>
   ): IPipeline<Readonly<T> | N, G, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<Readonly<T> | N, H, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<Readonly<T> | N, I, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<Readonly<T> | N, J, this, typeof pipes>;
   public pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<Readonly<T> | N, K, this, typeof pipes>;
   public pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<Readonly<T> | N, unknown, this, typeof pipes>;
   public pipe(
      ...args: any[]
   ): any {
      return new Pipeline(this, [...args]);
   }
}
