import { TDeepPartial, TStringKeys } from "@taedr/utils";
import { APipe, IPipeline, IReactive, IReactiveChange, IReactiveCtx, IReadonlyReactive, IWatcherCtx, IWatchers, TOnValue, TReactiveChange, TReactiveValue } from "../circle";
import { TPipeArray_1, TPipeArray_10, TPipeArray_2, TPipeArray_3, TPipeArray_4, TPipeArray_5, TPipeArray_6, TPipeArray_7, TPipeArray_8, TPipeArray_9 } from '../..';

// ==================================================
//                   Change
// ==================================================
export interface IReactiveObjectChange<T extends object> extends IReactiveChange {
   delta: Readonly<TDeepPartial<T>>;
}
// ==================================================
//                   Fields
// ==================================================
export type TReactiveWrapperFields<T extends object, W> = {
   [K in keyof Pick<T, Extract<keyof T, string>>]: W;
}

export type TObjectSymbols<T extends object> = {
   [K in keyof Pick<T, Extract<keyof T, string>>]: T[K];
}
// ==================================================
//                   Incapsulation
// ==================================================
export interface IReadonlyReactiveObject<
   T extends object,
   N extends null = never,
   F extends TReadonlyReactiveObjectFields<T> = TReadonlyReactiveObjectFields<T>,
> extends IReadonlyReactive<Readonly<T> | N> {
   readonly fields: F;
   watch(watcher: TOnValue<T, IReactiveObjectChange<T>>): string;
   watch(watcher: IWatcherCtx<T, IReactiveObjectChange<T>>): string;
   watch(watcher: TOnValue<T, IReactiveChange>): string;
   watch(watcher: IWatcherCtx<T, IReactiveChange>): string;
   pipe(): IPipeline<Readonly<T> | N, T, this>;
   pipe<A>(
      ...pipes: TPipeArray_1<T, IReactiveObjectChange<T>, A>
   ): IPipeline<Readonly<T> | N, A, this, typeof pipes>;
   pipe<A, B,>(
      ...pipes: TPipeArray_2<T, IReactiveObjectChange<T>, A, B>
   ): IPipeline<Readonly<T> | N, B, this, typeof pipes>;
   pipe<A, B, D>(
      ...pipes: TPipeArray_3<T, IReactiveObjectChange<T>, A, B, D>
   ): IPipeline<Readonly<T> | N, D, this, typeof pipes>;
   pipe<A, B, D, E>(
      ...pipes: TPipeArray_4<T, IReactiveObjectChange<T>, A, B, D, E>
   ): IPipeline<Readonly<T> | N, E, this, typeof pipes>;
   pipe<A, B, D, E, F>(
      ...pipes: TPipeArray_5<T, IReactiveObjectChange<T>, A, B, D, E, F>
   ): IPipeline<Readonly<T> | N, F, this, typeof pipes>;
   pipe<A, B, D, E, F, G>(
      ...pipes: TPipeArray_6<T, IReactiveObjectChange<T>, A, B, D, E, F, G>
   ): IPipeline<Readonly<T> | N, G, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H>(
      ...pipes: TPipeArray_7<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H>
   ): IPipeline<Readonly<T> | N, H, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I>(
      ...pipes: TPipeArray_8<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I>
   ): IPipeline<Readonly<T> | N, I, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J>(
      ...pipes: TPipeArray_9<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I, J>
   ): IPipeline<Readonly<T> | N, J, this, typeof pipes>;
   pipe<A, B, D, E, F, G, H, I, J, K>(
      ...pipes: TPipeArray_10<T, IReactiveObjectChange<T>, A, B, D, E, F, G, H, I, J, K>
   ): IPipeline<Readonly<T> | N, K, this, typeof pipes>;
   pipe(
      ...pipes: APipe<unknown, unknown>[]
   ): IPipeline<Readonly<T> | N, unknown, this>;
}

export interface IReactiveObject<
   T extends object,
   N extends null = never,
   F extends TReactiveObjectFields<T> = TReactiveObjectFields<T>,
> extends IReadonlyReactiveObject<T, N, F> {
   readonly watchers: IWatchers;
   shutdown: string;
   send(value: T | TDeepPartial<T>): void;
}
// ==================================================
//                   Ctx
// ==================================================
export interface IReactiveWrapperCtx<T extends object, W, F extends TReactiveWrapperFields<T, W>> extends IReactiveCtx {
   fields: F;
   symbols?: TObjectSymbols<T>;
   null?: boolean;
}
// ==================================================
//                   Fields
// ==================================================
export type TReadonlyReactiveObjectFields<T extends object> = {
   [K in keyof Pick<T, TStringKeys<T>>]: IReadonlyReactive<TReactiveValue<T[K]>>;
}

export type TReactiveObjectFields<T extends object> = {
   [K in keyof Pick<T, TStringKeys<T>>]: IReactive<TReactiveValue<T[K]>>;
}
