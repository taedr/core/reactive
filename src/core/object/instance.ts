import { Reactive, AReactiveWrapper, IReactive, IReactiveObject, IReadonlyRecord, TReactive, TReactiveObjectFields, TRO } from "../circle";

// ==================================================
//                   Core
// ==================================================
export abstract class AReactiveObject<T extends object, N extends null = never, F extends TReactiveObjectFields<T> = TReactiveObjectFields<T>>
   extends AReactiveWrapper<T, TReactive<any>, F, N> implements IReactiveObject<T, N, F> {

   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------

   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected getReactive(field: Reactive<any>) {
      return field;
   }
}
// ==================================================
//                   Instance
// ==================================================
export class ReactiveObject<F extends IReadonlyRecord<IReactive<any>>, N extends null = never>
   extends AReactiveObject<TReactiveObjectValue<F>, N, F> {
   constructor(
      fields: F,
      nullable?: N
   ) {
      super({ fields });
   }

   protected createInstance(): TReactiveObjectValue<F> {
      return {} as any;
   }
}


export type TReactiveObjectValue<F extends IReadonlyRecord<IReactive<any>>> = {
   [K in keyof F]: F[K] extends IReactive<infer V> ? V : never;
}
