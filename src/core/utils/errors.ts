
export const ERRORS = Object.freeze({
   recursive: `Some watcher trying to update reactive during notification process.
   This will lead to endless watcher calls. Reconsider your algorithm`,
   suffix: `Only 'Reactive' inheritant can be used as suffix`,
   source: `Reactive already has a source`,
   moveSort: `Can't move values while 'sort' is present`,
   notUsed: `NOT USED`,
   sameHash: (index: number) => `Value with similiar hash is already present on index '${index}'`,
   id: (id: string) => `Watcher with id '${id}' is already added`,
   shutdown: {
      message: `Message can't be an empty string`,
      change: `Value can not be changed after shutdown`,
   },
   index: {
      greater: (index: number, lastIndex: number) => `Index '${index}' is greater than last '${lastIndex}'`,
      lesser: (origin: number, index: number) => `Index '${origin}' after convertion '${index}' is lesser than 0`,
      duplicate: (index: number) => `Index '${index}' duplicated. Indexes array should contain only unique numbers`,
   },
});

export enum EDetachMessage {
   Auto = `Shutdown automatically`,
   Race = `Race finished`,
   Manual = `Manual detach by source`,
   Error = `Source threw an error`,
   Emittion = `Emittion complete`,
   Sources = `Detached from all sources`,
}
