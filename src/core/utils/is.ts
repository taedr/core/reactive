import { IS, TNew } from "@taedr/utils";
import { Reactive, AReactiveArray, AReactiveObject, AReactiveRecord, IReadonlyRecord, ReactiveBigint, ReactiveBigintArray, ReactiveBigintRecord, ReactiveBoolean, ReactiveBooleanArray, ReactiveBooleanRecord, ReactiveNumber, ReactiveNumberArray, ReactiveNumberRecord, ReactiveObjectArray, ReactivesBigintArray, ReactivesBooleanArray, ReactivesNumberArray, ReactivesStringArray, ReactiveString, ReactiveStringArray, ReactiveStringRecord } from "../circle";
import { ReactivesBigintRecord, ReactivesBooleanRecord, ReactivesNumberRecord, ReactivesStringRecord } from "../record/wrappers/instance";

export const IS_REACTIVE = Object.freeze({
   core(value: unknown): value is Reactive<unknown> {
      return value instanceof Reactive;
   },
   string(value: unknown): value is ReactiveString {
      return value instanceof ReactiveString;
   },
   number(value: unknown): value is ReactiveNumber {
      return value instanceof ReactiveNumber;
   },
   boolean(value: unknown): value is ReactiveBoolean {
      return value instanceof ReactiveBoolean;
   },
   bigint(value: unknown): value is ReactiveBigint {
      return value instanceof ReactiveBigint;
   },
   object<T extends object = object>(value: unknown, builder?: TNew<T>): value is AReactiveObject<T> {
      const isObject = value instanceof AReactiveObject;
      if (!isObject) return false;
      return builder ? value.value instanceof builder : true;
   },
   array: Object.freeze({
      core(value: unknown): value is AReactiveArray<readonly unknown[]> {
         return value instanceof AReactiveArray;
      },
      string(value: unknown): value is AReactiveArray<readonly string[]> {
         return value instanceof ReactiveStringArray || value instanceof ReactivesStringArray;
      },
      number(value: unknown): value is AReactiveArray<readonly number[]> {
         return value instanceof ReactiveNumberArray || value instanceof ReactivesNumberArray;
      },
      boolean(value: unknown): value is AReactiveArray<readonly boolean[]> {
         return value instanceof ReactiveBooleanArray || value instanceof ReactivesBooleanArray;
      },
      bigint(value: unknown): value is AReactiveArray<readonly bigint[]> {
         return value instanceof ReactiveBigintArray || value instanceof ReactivesBigintArray;
      },
      // object(value: unknown): value is AReactiveArray<readonly object[]> {
      //    return value instanceof ReactiveObjectArray || value instanceof ReactivesObjectArray<object>;
      // },
   }),
   record: Object.freeze({
      core(value: unknown): value is AReactiveRecord<IReadonlyRecord<unknown>> {
         return value instanceof AReactiveRecord;
      },
      string(value: unknown): value is AReactiveRecord<IReadonlyRecord<string>> {
         return value instanceof ReactiveStringRecord || value instanceof ReactivesStringRecord;
      },
      number(value: unknown): value is AReactiveRecord<IReadonlyRecord<number>> {
         return value instanceof ReactiveNumberRecord || value instanceof ReactivesNumberRecord;
      },
      boolean(value: unknown): value is AReactiveRecord<IReadonlyRecord<boolean>> {
         return value instanceof ReactiveBooleanRecord || value instanceof ReactivesBooleanRecord;
      },
      bigint(value: unknown): value is AReactiveRecord<IReadonlyRecord<bigint>> {
         return value instanceof ReactiveBigintRecord || value instanceof ReactivesBigintRecord;
      },
      // object(value: unknown): value is AReactiveArray<readonly object[]> {
      //    return value instanceof ReactiveObjectArray || value instanceof ReactivesObjectArray;
      // },
   }),
});
