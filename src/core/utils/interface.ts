
export interface IValue<T> {
   value: T;
}

export interface IKey {
   key: string;
}

export interface IIndex {
   index: number;
}

export interface IChanged {
   changed: boolean;
}

export interface IInitial {
   initial?: true;
}

export interface IWrapper<W> {
   wrapper: W;
}

export interface IPossibleChange {
   prepare?: true;
   initial?: boolean;
   changed?: boolean;
   added?: readonly unknown[];
   deleted?: readonly unknown[];
   updated?: readonly unknown[];
   moved?: readonly unknown[];
   wrappers?: readonly unknown[];
}
