import { IWatcherCtx, TOnDeatch, TOnValue } from "../bus/interface";
import { IBusChange } from "../circle";


export class Watcher<T, C extends IBusChange>  {
   readonly value: TOnValue<T, C>;
   readonly detach?: TOnDeatch;
   readonly prepare?: boolean;

   constructor(
      ctx: IWatcherCtx<T, C>,
   ) {
      if (ctx.prepare) this.prepare = ctx.prepare;
      this.value = ctx.value;
      if (ctx.detach) this.detach = ctx.detach;
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------

}
