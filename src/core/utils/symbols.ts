
export const PIPELINE = `PIPELINE`;
export const OBJECT = `OBJECT`;
export const SAMPLE = `_SAMPLE_`;

export const REACTIVE = Symbol(`REACTIVE`);
export type SReactive = typeof REACTIVE;

export const REVERSE = Symbol(`REVERSE`);
export type SReverse = typeof REVERSE;
