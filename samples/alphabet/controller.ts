import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Letter } from './model';


@Component({
   selector: 'app-alphabet',
   template: ``
   // template: `
   //    <button (click)="start()">Start</button>
   //    <section>
   //       <span>Score: {{ state.score }}</span>
   //       <span>Level: {{ state.level }}</span>
   //    </section>
   //    <section>
   //       <div *ngFor="let letter of state.letters; trackBy: track">
   //          {{ letter.line }}
   //       </div>
   //    </section>
   //    <section>
   //       <br *ngFor="let letter of state.border">
   //       {{ bottom }}
   //    </section>
   //    <section *ngIf="isOver">
   //       GAME OVER!
   //    </section>
   // `,
})
export class AlphabetComponent implements OnInit, OnDestroy, AfterViewInit {
   // public tickDelayMs: number;
   // public state: State;
   // public game: Pipeline<unknown, IPair<State>>;
   // public isOver: boolean;
   // readonly bottom = '-'.repeat(gameWidth);
   // readonly hub = new WatchersHub();


   constructor(
      readonly hostDom: ElementRef,
      readonly chDec: ChangeDetectorRef

   ) {

   }

   track(_: any, letter: Letter) {
      return letter.id;
   }


   ngOnInit() {
      this.chDec.detach();
      this.setInitial();
   }


   ngOnDestroy() {
      // if (this.game) this.game.shutdown = `Destroyed.`;
   }


   ngAfterViewInit() {
      this.chDec.detectChanges();
   }



   setInitial() {
      // this.state = new State(null, null);
      // this.tickDelayMs = initialTickDelayMs;
      // this.isOver = false;
   }

   start() {
      // this.setInitial();

      // this.game = latest(
      //    adapter('keydown', document.body),
      //    interval({
      //       delayMs: () => this.tickDelayMs,
      //       value: () => new Letter(),
      //    }),
      // ).pipe(
      //    state(State),
      //    pair()
      //);

      // this.game.node(this.hub).watch(({ prev, curr }) => {
      //    if (curr.letters.length > endThreshold - 1) {
      //       this.game.shutdown = `Game over.`;
      //       this.isOver = true;
      //       console.log(this.game);
      //    } else if (curr.level - prev?.level === 1) {
      //       this.tickDelayMs -= speedAdjust;
      //    }

      //    this.state = curr;
      //    this.chDec.detectChanges();
      // });
   }
}
