import { FN_RANDOM_NUM, FN_RUNTIME_ID, IS, FN_PACK } from '@taedr/utils';


// ==================================================
//                   Constants
// ==================================================
export const newLevelOnScore = 20;
export const speedAdjust = 50;
export const endThreshold = 15;
export const gameWidth = 30;
export const initialTickDelayMs = 1000;
// ==================================================
//                   States
// ==================================================
export class Letter {
   readonly line: string;
   readonly char: string;
   readonly id = FN_RUNTIME_ID();

   constructor() {
      const chars = FN_RANDOM_NUM('a'.charCodeAt(0), 'z'.charCodeAt(0));
      const OY = FN_RANDOM_NUM(0, gameWidth);

      this.char = String.fromCharCode(chars);
      this.line = String.fromCharCode(160).repeat(OY) + this.char;
   }
}


export class State {
   readonly score: number = 0;
   readonly letters: Letter[] = [];
   readonly level: number = 1;
   readonly border: number[] = FN_PACK(endThreshold);


   constructor(
      input: KeyboardEvent | Letter,
      state: State | null
   ) {
      if (IS.null(state)) return; // #Return#
      const { score, level, letters } = state;

      this.score = score;
      this.level = level;

      if (input instanceof Letter) {
         this.letters = [...letters, input];
      } else if (input instanceof KeyboardEvent) {
         this.letters = letters.filter(({ char }) => char !== input.key);
         this.score += letters.length - this.letters.length;

         if (score > 0 && score % newLevelOnScore === 0) {
            this.level += 1;
            this.letters = [];
         }
      }

      this.border = FN_PACK(endThreshold - this.letters.length);
   }
}