import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveComponent } from './reactive.controller';
import { AlphabetComponent } from '../samples/alphabet/controller';
import { ReactiveRoutingModule } from './reactive.routing';
import { TaedrReactiveNgModule } from "@taedr/reactive/ui/angular";

@NgModule({
   imports: [
      CommonModule,
      ReactiveRoutingModule,
      TaedrReactiveNgModule,
   ],
   declarations: [
      ReactiveComponent,
      AlphabetComponent
   ],
   exports: [
      ReactiveComponent
   ]
})
export class ReactiveModule {

}
