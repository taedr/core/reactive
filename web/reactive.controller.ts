import { AfterViewInit, ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { AReadonlyBus, Bus, IReadonlyBus, map, passIf, Reactive, ReactiveArray, ReactiveRecord, ReactiveObject, ReactivesArray, ReactivesMap, Sequence, side, suppress, Timer, Watcher } from '@taedr/reactive';

@Component({
   selector: 'app-reactive',
   templateUrl: './reactive.template.html',
   styleUrls: ['./reactive.styles.scss']
})
export class ReactiveComponent implements OnInit, AfterViewInit, OnDestroy {

   constructor(
      private _ngZone: NgZone,
      private _cdr: ChangeDetectorRef,
   ) { }

   ngOnInit() {
      this.init();
      this.types();
      this.ready();
   }

   ngOnDestroy() {

   }

   types() {
      class User {
         constructor(
            public name: string,
            public age: number,
         ) { }
      }

      const rmap = new ReactiveRecord({ 1: 1, 2: 2 });

      console.log(new Bus());
      console.log(new Reactive(1));
      console.log(new ReactiveArray([1, 2, 3]));
      console.log(new ReactivesArray([new Reactive(1), new Reactive(2)]));
      console.log(rmap);
      console.log({ ...rmap.value });
      console.log(new ReactivesMap({ 1: new Reactive(1), 2: new Reactive(2) }));
      console.log(new ReactiveObject(new User(`Vasya`, 32)));
      console.log(new Bus<number>().pipe(
         map(value => value),
         passIf(value => value > 1),
         suppress(10),
         side(() => { })
      ));
   }

   public timer: IReadonlyBus<string> | undefined;
   public message = `ФФФФ`;

   ready() {
      // this.timer = new Sequence([`Ready!`, `3`, `2`, `1`, `GO!`, ``]).pipe(
      //    suppress(value => new Timer(value ? 1000 : 2000))
      // );

      // this.timer.watch(value => {
      //    // this._cdr.markForCheck();
      //    // setTimeout(() => {
      //    //    this.message = value;
      //    // })
      //    this._ngZone.runOutsideAngular(() => {


      //    })
      //    this._ngZone.run(() => {
      //       console.log(this.message);
      //    });
      //    // this.message = value;
      //    // console.log(this.message);
      //    // document.dispatchEvent(new MouseEvent(`click`))

      // })
   }

   ngAfterViewInit() {

   }

   protected init() {
      return
      console.log(AReadonlyBus.prototype)
      const notifyWatcher = AReadonlyBus.prototype['notifyWatcher'];
      const zone = this._ngZone;
      // const cdr =this._cdr;
      console.log(zone);

      AReadonlyBus.prototype['notifyWatcher'] = function <T>(watcher: Watcher<T>, value: T) {
         // notifyWatcher(watcher, value)
         // cdr.markForCheck();
         zone.run(() => {
            notifyWatcher(watcher, value)

         });
      }
      // this._ngZone.run()
   }

   progress: number = 0;
   label: string = ``;


   // Loop inside the Angular zone
   // so the UI DOES refresh after each setTimeout cycle
   processWithinAngularZone() {
      this.label = 'inside';
      this.progress = 0;
      this._increaseProgress(() => console.log('Inside Done!'));
   }

   // Loop outside of the Angular zone
   // so the UI DOES NOT refresh after each setTimeout cycle
   processOutsideOfAngularZone() {
      this.label = 'outside';
      this.progress = 0;
      this._ngZone.runOutsideAngular(() => {
         this._increaseProgress(() => {
            // reenter the Angular zone and display done
            this._ngZone.run(() => { console.log('Outside Done!'); });
         });
      });
   }

   _increaseProgress(doneCallback: () => void) {
      this.progress += 1;
      console.log(`Current progress: ${this.progress}%`);

      if (this.progress < 100) {
         window.setTimeout(() => this._increaseProgress(doneCallback), 10);
      } else {
         doneCallback();
      }
   }


   user() {
      class Address {
         constructor(
            public city: string,
            public country: string,
         ) { }
      }

      class Note {
         constructor(
            public text: string,
         ) { }
      }

      class User {
         constructor(
            public name: string,
            public age: number,
            public address: Address,
            public notes: Note[],
            public marks: number[],
         ) { }
      }

      const user = new User(`Andrii`, 27, new Address(`Chornomorsk`, `Ukraine`), [new Note(`Hey`)], [0, 1]);
      const object = new ReactiveObject(user);

      console.log(object);
   }
}
