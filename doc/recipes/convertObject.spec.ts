import { accumulate, AReactiveObject, Bus, group, groupPacks, IReadonlyBus, map, Mapper, Reactive, ReactiveNumber, ReactiveObject, ReactiveObjectArray, ReactiveString, ReadonlyReactive, Sequence, WatchSequential, watchSequential } from '@taedr/reactive';
import { TDeepPartial, TMapper, TStringKeys } from '@taedr/utils';

it(`Convert object's fields asynchronously and reassemble in the new instance`, async () => {
   class User {
      constructor(
         readonly name: string,
         readonly city: string,
      ) { }
   }

   const users = [
      new User(`Andrii`, `Chornomorsk`),
      new User(`Vesta`, `Odesa`)
   ];

   const mapped = users.map(user => new User(
      `|Verified| ${user.name}`,
      `|Ukraine| ${user.city}`
   ));

   const convertName = (name: string) => new Reactive(`|Verified| ${name}`, { shutdown: true });
   const convertCity = (city: string) => new Reactive(`|Ukraine| ${city}`, { shutdown: true });

   const result = new Sequence(users).pipe(
      watchSequential(({ name, city }) => group([
         convertName(name),
         convertCity(city),
      ])),
      map(([name, city]) => new User(name, city)),
      accumulate()
   ).suffix(new ReactiveObjectArray<User>([]));

   console.log(JSON.stringify(result.value) === JSON.stringify(mapped)); // true
   expect(result.value).toEqual(mapped);
});

it(`qq`, async () => {
   class User {
      constructor(
         readonly name: string,
         readonly age: number,
         readonly sex: `MALE` | `FEMALE`,
         readonly city: string,
      ) { }
   }

   class ReactiveUser extends AReactiveObject<User> {
      constructor(
         user: User
      ) {
         super({
            fields: {
               name: new ReactiveString(user.name),
               age: new ReactiveNumber(user.age),
               sex: new ReactiveString(user.sex),
               city: new ReactiveString(user.city),
            }
         });
      }

      protected createInstance(): User {
         return new (User as any)();
      }
   }

   const changes = new Bus<TDeepPartial<User>>();

   const fieldMappers: {
      [K in keyof User]: TMapper<User[K], User[K]>
   } = {
      name: name => `Garo | ${name}`,
      age: age => age,
      city: city => `Best | ${city}`,
      sex: sex => sex,
   };



   const fieldBuses = Object.fromEntries(Object.entries(fieldMappers)
      .map(([key, mapper]) => [key, (value: User[keyof User]) => new ReadonlyReactive([key, mapper(value)])])
   );

   const initial = new User(`Andrii`, 27, `MALE`, `Chornomorsk`);
   // const result = changes.pipe(
   //    watchSequential(delta => groupPacks(
   //       Object.entries(delta).map(([key, value]) => fieldBuses[key](value)),
   //    )),
   //    map((entries: any[]) => ({ ...Object.fromEntries(entries) }))
   // ).suffix(new ReactiveUser(initial));
   const result = changes.pipe(
      ...mapDelta({
         name: name => new Reactive(`Garo | ${name}`, { shutdown: true }),
         city: city => new Reactive(`Best | ${city}`, { shutdown: true }),
      })
   ).suffix(new ReactiveUser(initial));

   changes.send({
      age: 18,
      name: `Orest`
   });

   expect(result.value).toEqual(new User(`Garo | Orest`, 18, `MALE`, `Chornomorsk`));
});


export type TObjectFieldsMappers<T extends object> = {
   [K in keyof Pick<T, TStringKeys<T>>]?: TMapper<T[K], IReadonlyBus<T[K]>>;
};

function mapDelta<T extends object>(mappers: TObjectFieldsMappers<T>): [
   WatchSequential<T | TDeepPartial<T>, readonly [string, any][]>,
   Mapper<readonly [string, any][], TDeepPartial<T>>
] {
   return [
      watchSequential((delta: T | TDeepPartial<T>) => groupPacks(
         Object.entries(delta).map(([key, value]) => {
            const mapper: TMapper<any, IReadonlyBus<any>> | undefined = mappers[key];
            if (mapper) {
               return mapper(value).pipe(
                  map(value => [key, value])
               );
            }
            return new ReadonlyReactive([key, value])
         }),
      )),
      map<any, any>((entries: any[]) => ({ ...Object.fromEntries(entries) }))
   ]
}
