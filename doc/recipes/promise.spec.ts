import { accumulate, adapt, debounce, Sequence, watchSequential } from '@taedr/reactive';
import { PromiseAdapter } from '@taedr/reactive/adapter/classes/promise';
import { FN_DELAY } from '@taedr/utils';

const getPromise = (value: number, delayMs: number) => {
   return new Promise<number>(resolve => setTimeout(() => resolve(value), delayMs));
}


it(`Get promises results in specified order`, async () => {
   const results = new Sequence([
      getPromise(1, 30),
      getPromise(2, 20),
      getPromise(3, 10)
   ]).pipe(
      watchSequential(p => new PromiseAdapter(p)),
      accumulate(),
      debounce(1)
   ).suffix();

   await FN_DELAY(60);

   expect(results.value).toEqual([1, 2, 3]);
});

it(`Default value for rejected promise`, async () => {
   const results = new Sequence([
      Promise.resolve(1),
      Promise.reject().catch(() => null),
   ]).pipe(
      watchSequential(adapt),
      accumulate()
   ).suffix();

   await FN_DELAY(1);

   expect(results.value).toEqual([1, null]);
});
