import { accumulate, EDetachMessage, Sequence, suppress, watchParallel, watchSequential } from "@taedr/reactive";
import { FN_DELAY } from "@taedr/utils";

/* Accumulates values in order of their income */
it(`Parallel`, async () => {
   const accumulator = new Sequence([
      new Sequence([1, 2]).pipe(suppress(1)),
      new Sequence([3, 4]).pipe(suppress(1)),
   ]).pipe(
      watchParallel(v => v),
      accumulate(),
   ).suffix();

   await FN_DELAY(20);

   console.log(accumulator.value); // [1, 3, 2, 4]
   console.log(accumulator.shutdown === EDetachMessage.Emittion); // true
   expect(accumulator.value).toEqual([1, 3, 2, 4]);
   expect(accumulator.shutdown).toEqual(EDetachMessage.Emittion);
});

/* Accumulates values in order of their sources */
it(`Sequential`, async () => {
   const accumulator = new Sequence([
      new Sequence([1, 2]).pipe(suppress(1)),
      new Sequence([3, 4]).pipe(suppress(1)),
   ]).pipe(
      watchSequential(v => v),
      accumulate(),
   ).suffix();

   await FN_DELAY(20);

   console.log(accumulator.value); // [1, 2, 3, 4]
   console.log(accumulator.shutdown === EDetachMessage.Emittion); // true
   expect(accumulator.value).toEqual([1, 2, 3, 4]);
   expect(accumulator.shutdown).toEqual(EDetachMessage.Emittion);
});
