import { IReactivesArrayChange, Reactive, ReactiveNumber, ReactivesNumberArray } from '@taedr/reactive';

/*
ReactiveіArray is an observable which will supply watchers with newly
created frozen array after any modification.
*/
it(`ReactivesArray`, async () => {
   const array = new ReactivesNumberArray([0, 1, 2]);

   let Ivalue: readonly number[] | undefined;
   let Ichange: IReactivesArrayChange<number> | undefined;

   array.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   });

   for (let i = 0; i < array.wrappers.length; i++) {
      const wrapper = array.wrappers[i];
      expect(wrapper).toBeInstanceOf(ReactiveNumber);
      expect(wrapper.value).toEqual(array.value[i])
   }

   expect(Ivalue).toEqual([0, 1, 2]);
   expect(Ichange).toEqual({
      initial: true,
      changed: true,
      added: [
         { index: 0, values: [0, 1, 2], wrappers: array.wrappers },
      ],
      updated: [],
      deleted: [],
      moved: []
   });
});


describe(`Add`, () => {
   it(`Reactive`, () => {
      const array = new ReactivesNumberArray([
         new ReactiveNumber(0),
         new ReactiveNumber(1),
         new ReactiveNumber(2),
      ]);

      const modification = [
         new ReactiveNumber(3),
         new ReactiveNumber(4)
      ];

      array.addWrappers(modification);

      expect(array.value).toEqual([0, 1, 2, 3, 4]);
      expect(array.wrappers[3]).toBe(modification[0]);
      expect(array.wrappers[4]).toBe(modification[1]);

      array.addWrappers([
         new ReactiveNumber(-2),
         new ReactiveNumber(-1),
      ], 0);

      expect(array.value).toEqual([-2, -1, 0, 1, 2, 3, 4]);

      array.addWrappers([new ReactiveNumber(3.5)], -1);

      expect(array.value).toEqual([-2, -1, 0, 1, 2, 3, 3.5, 4]);
   });

   it(`Indexes`, () => {
      const array = new ReactivesNumberArray([0, 1, 2]);

      array.addWrappers(
         { index: 1, values: [new ReactiveNumber(5)] },
         { index: 3, values: [new ReactiveNumber(6)] }
      );

      expect(array.value).toEqual([0, 5, 1, 6, 2]);
   });

   it(`Values`, () => {
      const array = new ReactivesNumberArray([0, 1, 2]);

      array.add([3]);

      expect(array.value).toEqual([0, 1, 2, 3]);
      expect(array.wrappers[3]).toBeInstanceOf(ReactiveNumber);
      expect(array.wrappers[3].value).toEqual(3);
   });
});

describe(`Update`, () => {
   it(`Index`, () => {
      const zero = new ReactiveNumber(0);
      const one = new ReactiveNumber(1);
      const two = new ReactiveNumber(2);
      const array = new ReactivesNumberArray([zero, one]);

      array.updateWrappers({
         index: 0, value: two
      });

      expect(array.value).toEqual([2, 1]);
      expect(array.wrappers[0]).toEqual(two);
   });

   it(`Compare`, () => {
      const array = new ReactivesNumberArray([0, 1, 2]);

      array.update((value, index) => {
         if (value === 0) return 5;
         if (index === 2) return 6;
      });

      expect(array.value).toEqual([5, 1, 6]);
   });

   it(`Entry`, () => {
      const array = new ReactivesNumberArray([0, 1, 2]);

      array.wrappers[0].send(3);

      expect(array.value).toEqual([3, 1, 2]);
   });
});

describe(`Delete`, () => {
   it(`Compare`, () => {
      const array = new ReactivesNumberArray([0, 1, 2]);

      array.deleteWrappers(({ value }, index) => {
         if (value === 0) return true;
         if (index === 2) return true;
      });

      expect(array.value).toEqual([1]);
   });

   it(`Values`, () => {
      const initial = [
         new ReactiveNumber(0),
         new ReactiveNumber(1),
         new ReactiveNumber(2),
      ];
      const array = new ReactivesNumberArray(initial);

      array.deleteWrappers([initial[0], initial[2], new ReactiveNumber(3)]);

      expect(array.value).toEqual([1]);
   });
});

describe(`Move`, () => {
   /* Same as for ReactiveArray */
});

describe(`Map`, () => {
   /* Same as for ReactiveArray */
});

it(`Sort`, () => {
   /* Same as for ReactiveArray */
});
