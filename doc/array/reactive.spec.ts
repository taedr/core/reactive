import { Bus, EDetachMessage, IIndex, IIndexAdd, IIndexUpdate, IMove, IReactiveArrayChange, REVERSE, ReactiveArray, ReactiveNumberArray, ReactiveStringArray, mapArrayModification, mergeChange, shutdownOn } from '@taedr/reactive';

/*
ReactiveArray is an observable which will supply watchers with newly created frozen array after any modification.
*/
it(`ReactiveArray`, async () => {
   const array = new ReactiveArray([0, 1, 2, 3, 4, 5]);

   let Ivalue: readonly number[] | undefined;
   let Ichange: IReactiveArrayChange<number> | undefined;

   expect(() => (<any>array.value).push(1)).toThrowError(`Cannot add property 6, object is not extensible`);

   /* Each time array got modified watchers will be notified */
   array.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   });

   expect(Ivalue).toEqual([0, 1, 2, 3, 4, 5]);
   expect(Ivalue).toBe(array.value);
   expect(Ichange).toEqual({
      initial: true,
      changed: true,
      added: [{ index: 0, values: [0, 1, 2, 3, 4, 5] }],
      updated: [],
      deleted: [],
      moved: []
   });
});

it(`Replace`, () => {
   const array = new ReactiveArray([4, 5]);

   let Ivalue: readonly number[] | undefined;
   let Ichange: IReactiveArrayChange<number> | undefined;

   array.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   });

   array.send([3, 2, 1]);

   expect(array.value).toEqual([3, 2, 1]);
   expect(array.value).toBe(Ivalue);
   expect(Ichange).toEqual({
      changed: true,
      added: [{ index: 0, values: [3, 2, 1] }],
      updated: [],
      deleted: [{ index: 0, count: 2, values: [4, 5] }],
      moved: []
   })
});

it(`Utils`, () => {
   const array = new ReactiveArray([0, 1, 2, 3, 4]);

   // Shortcut for array.value.length
   expect(array.size).toBe(5);
   // Checks if value present in array
   expect(array.has(2)).toBeTruthy();
   expect(array.has((value) => value > 3)).toBeTruthy();
   expect(array.has((_, index) => index === 6)).toBeFalsy();
   // Gets value from index
   expect(array.get(1)).toEqual(1);
   expect(array.get(-1)).toEqual(4);
});

describe(`Add`, () => {
   it(`Values`, () => {
      const array = new ReactiveArray([0, 1, 2]);
      let Ichange: IReactiveArrayChange<number> = null as any;

      array.watch((_, change) => { Ichange = change; });

      /* Adds values from iterable to the spefied index (to the end if omitted) */
      const change = array.add([3, 4]);

      expect(array.value).toEqual([0, 1, 2, 3, 4]);
      expect(change).toEqual({
         changed: true,
         added: [{ index: 3, values: [3, 4] }],
         updated: [],
         deleted: [],
         moved: []
      });
      expect(change).toBe(Ichange);

      array.add([-2, -1], 0);

      expect(array.value).toEqual([-2, -1, 0, 1, 2, 3, 4]);

      array.add([3.5], -1);

      expect(array.value).toEqual([-2, -1, 0, 1, 2, 3, 3.5, 4]);
   });

   it(`Indexes`, () => {
      const array = new ReactiveArray([0, 3, 6]);

      /* Adds values to the specified indexes */
      const modification: IIndexAdd<number>[] = [
         { index: 1, values: [1, 2] },
         { index: 4, values: [4, 5] }
      ];

      const change = array.add(...modification);

      expect(array.value).toEqual([0, 1, 2, 3, 4, 5, 6]);
      expect(change).toEqual({
         changed: true,
         added: modification,
         updated: [],
         deleted: [],
         moved: []
      });
   });
});

describe(`Update`, () => {
   it(`Index`, () => {
      const array = new ReactiveArray([0, 1, 2]);
      const modification: IIndexUpdate<number>[] = [
         { index: 0, value: 5 },
         { index: 2, value: 6 }
      ];

      /* Updates specified indexes with new values */
      const change = array.update(...modification);

      expect(array.value).toEqual([5, 1, 6]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [{ index: 0, value: 5 }, { index: 2, value: 6 }],
         deleted: [],
         moved: []
      });
   });

   it(`Compare`, () => {
      const array = new ReactiveArray([0, 1, 2]);

      /* Updates value on provided index if function returned something */
      const change = array.update((value, index) => {
         if (value === 0) return 5;
         if (index === 2) return 6;
      });

      expect(array.value).toEqual([5, 1, 6]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [{ index: 0, value: 5 }, { index: 2, value: 6 }],
         deleted: [],
         moved: []
      });
   });
});

describe(`Delete`, () => {
   it(`Compare`, () => {
      const array = new ReactiveArray([0, 1, 2]);

      /* Delets value on provided index if returns true */
      const change = array.delete((value, index) => {
         if (value === 0) return true;
         if (index === 2) return true;
      });

      expect(array.value).toEqual([1]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [
            { index: 2, count: 1, values: [2] },
            { index: 0, count: 1, values: [0] }
         ],
         moved: []
      });
   });

   it(`Indexes`, () => {
      const array = new ReactiveArray([0, 1, 2]);
      const modification: IIndex[] = [{ index: 0 }, { index: 2 }];

      /* Delets value on provided indexes */
      const change = array.delete(...modification);

      expect(array.value).toEqual([1]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [
            { index: 2, count: 1, values: [2] },
            { index: 0, count: 1, values: [0] }
         ],
         moved: []
      });
   });

   it(`Values`, () => {
      const array = new ReactiveArray([0, 1, 2]);

      /* Deletes provided values if they are present is the array */
      const change = array.delete([0, 2, 5]);

      expect(array.value).toEqual([1]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [
            { index: 2, count: 1, values: [2] },
            { index: 0, count: 1, values: [0] }
         ],
         moved: []
      });
   });
});

describe(`Move`, () => {
   it(`From`, () => {
      const array = new ReactiveArray([0, 1, 2, 3, 4]);
      const moves: IMove[] = [{ from: 0, to: 2 }];

      /* Moves value from specified index to another index */
      const change = array.move(...moves);

      expect(array.value).toEqual([1, 2, 0, 3, 4]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [],
         moved: moves
      });
   });

   it(`Swap`, () => {
      const array = new ReactiveArray([0, 1, 2, 3, 4]);
      const moves: IMove[] = [{ from: 0, to: -1, swap: true }];

      /* Swaps values beetwen indexes */
      const change = array.move(...moves);

      expect(array.value).toEqual([4, 1, 2, 3, 0]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [],
         moved: moves
      });
   });

   it(`Indexes`, () => {
      const array = new ReactiveArray([0, 1, 2, 3, 4]);

      /* Moves values to specified index  */
      const change = array.move([1, 3], 0);

      expect(array.value).toEqual([1, 3, 0, 2, 4]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [],
         moved: [{ from: 1, to: 0 }, { from: 3, to: 1 }]
      });
   });

   it(`Steps`, () => {
      const array = new ReactiveArray([0, 1, 2, 3, 4]);

      /* Moves values to left specified number of times */
      array.move(2);

      expect(array.value).toEqual([3, 4, 0, 1, 2]);

      /* Moves values to right specified number of times */
      array.move(-2);

      expect(array.value).toEqual([0, 1, 2, 3, 4]);
   });

   it(`Reverse`, () => {
      const array = new ReactiveArray([0, 1, 2, 3, 4]);

      /* Moves values reverse */
      const change = array.move(REVERSE);

      expect(array.value).toEqual([4, 3, 2, 1, 0]);
      expect(change).toEqual({
         changed: true,
         added: [],
         updated: [],
         deleted: [],
         moved: [
            { from: 4, to: 0 },
            { from: 4, to: 1 },
            { from: 4, to: 2 },
            { from: 4, to: 3 }
         ]
      });
   });
});

it(`Sort`, () => {
   const array = new ReactiveArray([0, 1, 2, 3, 4]);

   array.sorter = (a, b) => b - a;

   expect(array.value).toEqual([4, 3, 2, 1, 0]);

   /* During changes sort will be maintained. */
   /* Specific add indexes will be ignored while sort is active */
   array.add([5], 3);

   expect(array.value).toEqual([5, 4, 3, 2, 1, 0]);

   /* Updated value will be moved */
   array.update({ index: -1, value: 6 });

   expect(array.value).toEqual([6, 5, 4, 3, 2, 1]);

   /* Sended value will be sorted */
   array.send([7, 9, 5]);

   console.log(array.value); // [9, 7, 5]
   expect(array.value).toEqual([9, 7, 5]);

   /* After sorter removal action will work in default way */
   array.sorter = undefined;

   array.add([10], 2);

   expect(array.value).toEqual([9, 7, 10, 5]);
});

it(`map`, () => {
   const array = new ReactiveNumberArray([0, 1, 2]);
   const shutdowner = new Bus<null>();

   /* Creates new array which will be change along with source array */
   const suffix = array.pipe(
      mergeChange(),
      mapArrayModification(v => `!${v}`),
      shutdownOn(shutdowner)
   ).suffix(new ReactiveStringArray());

   expect(array.watchers.size).toEqual(1);
   expect(suffix.origin).toEqual(array);

   expect(suffix.value).toEqual(['!0', '!1', '!2']);

   array.add([3]);

   expect(suffix.value).toEqual(['!0', '!1', '!2', '!3']);

   shutdowner.send(null);

   array.add([4]);

   expect(suffix.value).toEqual(['!0', '!1', '!2', '!3']);

   expect(array.watchers.size).toEqual(0);
   expect(suffix.shutdown).toEqual(EDetachMessage.Auto);
});
