import { IKeyValue,  Reactive, ReactivesNumberRecord } from "@taedr/reactive";
import { IMap } from "@taedr/utils";

/*
ReactiveMap is an observable which will supply watchers with newly
created frozen object after any modification.
*/
it(`ReactivesMap`, () => {
   const map = new ReactivesNumberRecord({
      one: new Reactive(1),
      two: new Reactive(2),
      three: new Reactive(3),
   });
   const initial = map.value;

   let newValue: IMap<number> = null as any;
   let newChange: IReactivesMapChange<number> = null as any;

   try {
      (<any>map.value)['four'] = 3;
   } catch (e) {
      console.log(e); // [TypeError: Cannot add property four, object is not extensible]
   }

   /* Each time map got modified watchers will be notified */
   map.watch(value => { newValue = value; });
   map.changes.watch(change => { newChange = change; });

   map.set({ four: new Reactive(4) });

   console.log(newValue); // { one: 1, two: 2, three: 3, four: 4 }
   console.log(newValue === map.value); // true
   console.log(newChange.value === newValue); // true
   console.log(newChange.prev === initial); // true
   console.log(newChange.wrappers === map.wrappers); // true
   console.log(map.wrappers['one'].value === map.value['one']); // true
   console.log(map.size); // 4

   expect(newValue).toEqual({ one: 1, two: 2, three: 3, four: 4 });
   expect(newValue).toBe(map.value);
   expect(newValue).toBe(newChange.value);
   expect(newChange.prev).toBe(initial);
   expect(newChange.wrappers).toBe(map.wrappers);
   expect(map.wrappers['one'].value).toBe(map.value['one']);
   expect(map.size).toBe(4);
});

it(`Set`, () => {
   const map = new ReactivesMap({
      one: new Reactive(1),
      two: new Reactive(2),
      three: new Reactive(3),
   });
   const initial = map.wrappers;
   const modification: IMap<Reactive<number>> = { four: new Reactive(4), one: new Reactive(11) };

   /* Adds new value under specified key or replaces value if key already present */
   const setted = map.set(modification);

   console.log(map.value); // { one: 11, two: 2, three: 3, four: 4 }
   console.log(setted.added[0].value === modification['four']); // true
   console.log(setted.added[1].value === modification['one']); // true
   console.log(setted.deleted[0].value === initial['one']); // true

   expect(map.value).toEqual({ one: 11, two: 2, three: 3, four: 4 });
   expect(setted.added[0].value).toBe(modification['four']);
   expect(setted.added[1].value).toBe(modification['one']);
   expect(setted.deleted[0].value).toBe(initial['one']);
});

describe(`Delete`, () => {
   it(`Keys`, () => {
      const map = new ReactivesMap({
         one: new Reactive(1),
         two: new Reactive(2),
         three: new Reactive(3),
      });

      /* Deletes values under provided keys */
      map.delete({ key: `one` }, { key: `three` });

      console.log(map.value); // { two: 2 }

      expect(map.value).toEqual({ two: 2 });
   });

   it(`Values`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const map = new ReactivesMap({ a, b, c });

      /* Deletes each entry if it corresponds with any provided array value */
      const deleted = map.delete([a, c]);

      console.log(map.value); // { b: 2 }
      console.log(deleted[0].value === a); // true
      console.log(deleted[1].value === c); // true

      expect(map.value).toEqual({ b: 2 });
      expect(deleted[0].value).toBe(a);
      expect(deleted[1].value).toBe(c);
   });

   it(`Compare`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const d = new Reactive(1);
      const map = new ReactivesMap({ a, b, c, d });

      /* Deletes entry if function returns true */
      const deleted = map.delete((value, key) => {
         if (value === 1) return true;
         if (key === `c`) return true;
      });

      console.log(map.value); // { b: 2 }
      console.log(deleted[0].value === a); // true
      console.log(deleted[1].value === c); // true
      console.log(deleted[2].value === d); // true

      expect(map.value).toEqual({ b: 2 });
      expect(deleted[0].value).toBe(a);
      expect(deleted[1].value).toBe(c);
      expect(deleted[2].value).toBe(d);
   });
});


describe(`Update`, () => {
   it(`Entries`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const map = new ReactivesMap({ a, b, c });

      let newA: number = null as any;

      a.watch(value => newA = value);

      /* Updates values under specified keys */
      const updated = map.update({
         a: 11,
         c: 33,
         d: 44,
      });

      /* [ { key: 'a', value: 11, prev: 1 }, { key: 'c', value: 33, prev: 3 } ] */
      console.log(updated);
      console.log(map.value); // { a: 11, b: 2, c: 33 }
      console.log(newA); // 11

      expect(map.value).toEqual({ a: 11, b: 2, c: 33 });
      expect(updated).toEqual([{ key: 'a', value: 11, prev: 1 }, { key: 'c', value: 33, prev: 3 }]);
      expect(newA).toBe(11);
   });

   it(`Key`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const map = new ReactivesMap({ a, b, c });

      const modification: readonly IKeyValue<number>[] = [
         { key: `a`, value: 11 },
         { key: `c`, value: 33 },
         { key: `d`, value: 44 },
      ];

      /* Updates values under specified keys */
      const updated = map.update(modification);

      /* [ { key: 'a', value: 11, prev: 1 }, { key: 'c', value: 33, prev: 3 } ] */
      console.log(updated);
      console.log(map.value); // { a: 11, b: 2, c: 33 }

      expect(map.value).toEqual({ a: 11, b: 2, c: 33 });
      expect(updated).toEqual([{ key: 'a', value: 11, prev: 1 }, { key: 'c', value: 33, prev: 3 }]);
   });

   it(`Compare`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const d = new Reactive(1);
      const map = new ReactivesMap({ a, b, c, d });

      /* Updates entry with returned value */
      const updated = map.update((value, key) => {
         if (value === 1) return 11;
         if (key === `c`) return 33;
      });

      /* [
            { key: 'a', value: 11, prev: 1 },
            { key: 'c', value: 33, prev: 3 },
            { key: 'd', value: 11, prev: 1 }
         ] */
      console.log(updated);
      console.log(map.value); // { a: 11, b: 2, c: 33 }

      expect(map.value).toEqual({ a: 11, b: 2, c: 33, d: 11 });
      expect(updated).toEqual([
         { key: 'a', value: 11, prev: 1 },
         { key: 'c', value: 33, prev: 3 },
         { key: 'd', value: 11, prev: 1 }
      ]);
   });

   it(`Entry`, () => {
      const a = new Reactive(1);
      const b = new Reactive(2);
      const c = new Reactive(3);
      const map = new ReactivesMap({ a, b, c });

      /* Update on any entry will cause update of whole map */
      a.send(11);

      console.log(map.value); // { a: 11, b: 2, c: 3 }

      expect(map.value).toEqual({ a: 11, b: 2, c: 3 });
   });
});
