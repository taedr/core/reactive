import { IReactiveMapChange, ReactiveMap } from "@taedr/reactive";
import { IMap } from "@taedr/utils";

/*
ReactiveMap is an observable which will supply watchers with newly
created frozen object after any modification.
*/
it(`ReactiveMap`, () => {
   const map = new ReactiveMap({
      one: 1,
      two: 2,
      three: 3,
   });
   const initial = map.value;

   let newValue: IMap<number> = null as any;
   let newChange: IReactiveMapChange<number> = null as any;

   try {
      (<any>map.value)['four'] = 3;
   } catch (e) {
      console.log(e); // [TypeError: Cannot add property four, object is not extensible]
   }

   /* Each time map got modified watchers will be notified */
   map.watch(value => { newValue = value; });
   map.changes.watch(change => { newChange = change; });

   map.set({ four: 4 });

   console.log(newValue); // { one: 1, two: 2, three: 3, four: 4 }
   console.log(newValue === map.value); // true
   console.log(newChange.value === newValue); // true
   console.log(newChange.prev === initial); // true
   console.log(map.size); // 4

   expect(newValue).toEqual({ one: 1, two: 2, three: 3, four: 4 });
   expect(newValue).toBe(map.value);
   expect(newValue).toBe(newChange.value);
   expect(newChange.prev).toBe(initial);
   expect(map.size).toBe(4);
});

it(`Set`, () => {
   const map = new ReactiveMap({
      one: 1,
      two: 2,
      three: 3,
   });

   /* Adds new value under specified key or updates value if key already present */
   const setted = map.set({ four: 4, one: 11 });

   console.log(map.value); // { one: 11, two: 2, three: 3, four: 4 }
   /* {
      added: [ { key: 'four', value: 4 } ],
      updated: [ { key: 'one', value: 11, prev: 1 } ],
      deleted: []
     } */
   console.log(setted);

   expect(map.value).toEqual({ one: 11, two: 2, three: 3, four: 4 });
   expect(setted).toEqual({
      added: [{ key: 'four', value: 4 }],
      updated: [{ key: 'one', value: 11, prev: 1 }],
      deleted: []
   });
});

describe(`Delete`, () => {
   it(`Keys`, () => {
      const map = new ReactiveMap({
         one: 1,
         two: 2,
         three: 3,
      });

      /* Deletes values under provided keys */
      const deleted = map.delete({ key: `one` }, { key: `three` });

      console.log(map.value); // { two: 2 }
      console.log(deleted); // [ { key: 'one', value: 1 }, { key: 'three', value: 3 } ]

      expect(map.value).toEqual({ two: 2 });
      expect(deleted).toEqual([{ key: 'one', value: 1 }, { key: 'three', value: 3 }]);
   });

   it(`Values`, () => {
      const map = new ReactiveMap({
         a: 1,
         b: 2,
         c: 3,
         d: 1,
      });

      /* Deletes each entry if it corresponds with any provided array value */
      const deleted = map.delete([1, 3, 5]);

      console.log(map.value); // { b: 2 }
      /*
      [  { key: 'a', value: 1 },
         { key: 'c', value: 3 },
         { key: 'd', value: 1 } ]
      */
      console.log(deleted);

      expect(map.value).toEqual({ b: 2 });
      expect(deleted).toEqual([
         { key: 'a', value: 1 },
         { key: 'c', value: 3 },
         { key: 'd', value: 1 }
      ]);
   });

   it(`Compare`, () => {
      const map = new ReactiveMap({
         a: 1,
         b: 2,
         c: 3,
         d: 1,
      });

      /* Deletes entry if function returns true */
      const deleted = map.delete((value, key) => {
         if (value === 1) return true;
         if (key === `c`) return true;
      });

      console.log(map.value); // { b: 2 }
      /*
      [  { key: 'a', value: 1 },
         { key: 'c', value: 3 },
         { key: 'd', value: 1 } ]
      */
      console.log(deleted);

      expect(map.value).toEqual({ b: 2 });
      expect(deleted).toEqual([
         { key: 'a', value: 1 },
         { key: 'c', value: 3 },
         { key: 'd', value: 1 }
      ]);
   });
});
