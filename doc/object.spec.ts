import { IReactiveObjectChange, Reactive, ReactiveObject } from '@taedr/reactive';
import { TDeepPartial } from '@taedr/utils';

/*
ReactiveObject is observable which will notify watchers with newly
created frozen object if any of its root or nested fields chaged.
*/
it(`ReactiveObject`, async () => {
   class User {
      constructor(
         public name: string,
         public age: number,
         public address: Address,
         public notes: string[],
      ) { }
   }

   class Address {
      constructor(
         public country: string,
         public city: string,
      ) { }
   }

   const instance = new User(`Andrii`, 27, new Address(`Ukraine`, `Chornomorsk`), [`Hello`]);
   const object = new ReactiveObject(instance);
   const initial = object.value;

   let newValue: User = null as any;
   let newAge: number = null as any;
   let newCity: string = null as any;
   let newChange: IReactiveObjectChange<User> = null as any;

   /* Provided object will be recreated during initialization */
   console.log(object.value === instance); // false
   console.log(JSON.stringify(object.value) === JSON.stringify(instance)); // true

   /* Value itself is freezed */
   try {
      (<any>object.value).age = 20;
   } catch (e) {
      console.log(e); // [TypeError: Cannot assign to read only property 'age' of object '#<User>']
   }

   expect(object.value).not.toBe(instance);
   expect(object.value).toEqual(instance);
   expect(() => { (<any>object.value).age = 20 }).toThrowError();

   /* Each time field got changed watchers will be notified with new value */
   object.watch(value => { newValue = value; });

   /* There are possibility to watch and change specific fields.
      Each field will be represented with observables of appropriate type.
    */
   object.fields.get(`age`).watch(age => { newAge = age; });
   object.fields.get(`address`).fields.get(`city`).watch(city => { newCity = city; });

   /* Also you can watch for the full changes description   */
   object.changes.watch(change => { newChange = change; });

   /* In order to modify specific fields you can provide them by assigning new partial object */
   const modification = {
      age: 20,
      address: {
         city: `Odesa`
      }
   };

   object.send(modification);

   /*
   User {
      name: 'Andrii',
      age: 20,
      address: Address { country: 'Ukraine', city: 'Odesa' },
      notes: [ 'Hello' ]
   }*/
   console.log(newValue);
   console.log(object.value === newValue); // true
   console.log(newValue instanceof User); // true
   console.log(newAge); // 20
   console.log(newCity); // Odesa

   console.log(JSON.stringify(newChange.delta) === JSON.stringify(modification)); // true
   console.log(newChange.prev === initial); // true
   console.log(newChange.value === newValue); // true

   /*
      Map { 'age' => { prev: 27, value: 20 },
      'address' => { delta: { city: 'Odesa' },
      fields: Map { 'city' => { prev: 'Chornomorsk', value: 'Odesa' } },
      value: Address { country: 'Ukraine', city: 'Odesa' },
      prev: Address { country: 'Ukraine', city: 'Chornomorsk' } } }
   */
   console.log(newChange.fields);

   expect(object.value).toBe(newValue);
   expect(newValue).toBeInstanceOf(User);
   expect(newValue.age).toBe(modification.age);
   expect(newValue.address.city).toBe(modification.address.city);
   expect(newChange.delta).toEqual(modification);
   expect(newChange.prev).toBe(initial);
   expect(newChange.value).toBe(newValue);

   object.fields.get(`address`).fields.get(`city`).send(`Kyiv`);

   /*
   User {
      name: 'Andrii',
      age: 20,
      address: Address { country: 'Ukraine', city: 'Kyiv' },
      notes: [ 'Hello' ]
   }*/
   console.log(newValue);
   console.log(newCity); // Kyiv

   object.fields.get(`notes`).add([new Reactive(`world!`)]);

   /*
   User {
      name: 'Andrii',
      age: 20,
      address: Address { country: 'Ukraine', city: 'Kyiv' },
      notes: [ 'Hello', 'world!' ]
   }*/
   console.log(newValue);

   expect(newValue.address.city).toEqual(`Kyiv`);
   expect(newValue.notes).toHaveLength(2);
   expect(newValue.notes[1]).toEqual(`world!`);
});

it(`Update`, () => {
   class User {
      constructor(
         public name: string,
         public age: number,
      ) { }
   }

   const user = new User(`Andrii`, 27);
   const delta: TDeepPartial<User> = {
      age: 20
   };

   /* Updates object with delta */
   const updated = ReactiveObject.update(user, delta);

   console.log(updated); // User { name: 'Andrii', age: 20 }

   expect(updated).toBeInstanceOf(User);
   expect(updated.age).toEqual(delta.age);
   expect(updated.name).toEqual(user.name);
});

it(`Clone`, () => {
   class User {
      constructor(
         public name: string,
         public age: number,
         public address: Address,
      ) { }
   }

   class Address {
      constructor(
         public city: string,
      ) { }
   }

   const user = new User(`Andrii`, 27, new Address(`Chornomorsk`));

   /* Deeply clones provided object */
   const cloned = ReactiveObject.clone(user);

   console.log(JSON.stringify(cloned) === JSON.stringify(user)); // true
   console.log(cloned === user); // false

   expect(cloned).toBeInstanceOf(User);
   expect(cloned.address).toBeInstanceOf(Address);
   expect(cloned).toEqual(user);
   expect(cloned).not.toBe(user);
});
