import { Bus, reduce } from '@taedr/reactive';

it(`Combines previous state with received value in order to create new state`, async () => {
   const bus = new Bus<number>();
   const reducer = reduce<number, string>((value, acc) => `${acc}${value}`, `!`);
   const pipe = bus.pipe(reducer);
   let Ivalue: string | null = null;

   const watcherId = pipe.watch(value => Ivalue = value);

   expect(reducer.state.value).toEqual(`!`);
   expect(reducer.state.value).toEqual(reducer.initial);

   bus.send(1);

   expect(Ivalue).toEqual(`!1`);
   expect(reducer.state.value).toEqual(Ivalue);

   bus.send(2);

   expect(Ivalue).toEqual(`!12`);

   pipe.watchers.detach(watcherId);

   expect(reducer.state.value).toEqual(reducer.initial);
});
