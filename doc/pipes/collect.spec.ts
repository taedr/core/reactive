import { Bus, collect } from '@taedr/reactive';
import { WASTED } from '@taedr/utils';

/* Collects specified number of elements and emits them. Clears collected values after emit */
it(`Clear`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(collector.buffer.value).toEqual([1]);
   expect(collector.maxSize).toEqual(2);
   expect(newValues).toBeUndefined();

   bus.send(2);

   expect(newValues).toEqual([1, 2]);
   expect(collector.buffer.value).toEqual(newValues);

   bus.send(3);

   expect(collector.buffer.value).toEqual([3]);
   expect(newValues).toEqual([1, 2]);

   pipe.shutdown = WASTED;

   expect(collector.buffer.value).toEqual([]);
   expect(collector.buffer.shutdown).toEqual(WASTED);
});
/* Emits collected values on each new income. Clears collected values when capacity reached. */
it(`Partial`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2, `PARTIAL`);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(newValues).toEqual([1]);

   bus.send(2);

   expect(newValues).toEqual([1, 2]);

   bus.send(3);

   expect(newValues).toEqual([3]);
});
/* Initially fill array with undefined. Replaces undefined from end to start on each new value income. Clears values when no undefined left. */
it(`fill`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2, `FILL`);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(newValues).toEqual([undefined, 1]);

   bus.send(2);

   expect(newValues).toEqual([1, 2]);

   bus.send(3);

   expect(newValues).toEqual([undefined, 3]);
});

/* After capacity reached each new value will be added to the end while first value will be removed */
it(`lifo`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2, `FULL`, true);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(newValues).toBeUndefined();

   bus.send(2);

   expect(newValues).toEqual([1, 2]);

   bus.send(3);

   expect(newValues).toEqual([2, 3]);
});

it(`fill lifo`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2, `FILL`, true);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(newValues).toEqual([undefined, 1]);

   bus.send(2);

   expect(newValues).toEqual([1, 2]);

   bus.send(3);

   expect(newValues).toEqual([2, 3]);
});

it(`Partial LIFO`, () => {
   const bus = new Bus<number>();
   const collector = collect<number>(2, `PARTIAL`, true);
   const pipe = bus.pipe(collector);
   let newValues: readonly number[] | undefined;

   pipe.watch(value => newValues = value);

   bus.send(1);

   expect(newValues).toEqual([1]);

   bus.send(2);

   expect(newValues).toEqual([1, 2]);

   bus.send(3);

   expect(newValues).toEqual([2, 3]);
});
