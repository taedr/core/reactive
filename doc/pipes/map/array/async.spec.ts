import { EDetachMessage, map, mapArrayModificationAsync, mergeChange, ReactiveNumberArray, ReactiveStringArray, shutdownOn, watch } from "@taedr/reactive";
import { PromiseAdapter } from '@taedr/reactive/adapter/classes/promise';
import { FN_DELAY, WASTED } from '@taedr/utils';

it(`Converts values in 'added' and 'updated' fields into new shape `, () => {
   const array = new ReactiveNumberArray([1]);
   const suffix = array.pipe(
      mergeChange(),
      mapArrayModificationAsync(
         r => r.pipe(
            map(v => `${v}`)
         )
      )
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([`1`]);

   array.add([2]);

   expect(suffix.value).toEqual([`1`, `2`]);
});

it(`Converts values in 'added' and 'updated' fields into new shape `, async () => {
   const array = new ReactiveNumberArray([1]);
   const suffix = array.pipe(
      mergeChange(),
      mapArrayModificationAsync(
         value => value.pipe(
            watch(v => PromiseAdapter.resolve(`${v}`))
         )
      )
   ).suffix(new ReactiveStringArray());

   await FN_DELAY(1);

   expect(suffix.value).toEqual([`1`]);

   array.add([2]);

   await FN_DELAY(1);

   expect(suffix.value).toEqual([`1`, `2`]);
});

it(`SHOULD emit mapped changes in their income order`, async () => {
   const array = new ReactiveNumberArray([1]);
   const delays = [20, 10, 40];
   const mapper = mapArrayModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, delays.shift()))
      )
   );
   const suffix = array.pipe(
      mergeChange(),
      mapper
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([]);

   array.add([2]);
   array.add([3]);

   await FN_DELAY(10);

   expect(suffix.value).toEqual([]);

   await FN_DELAY(10);

   expect(suffix.value).toEqual([`1`, `2`]);

   array.update({ index: 0, value: 4 })

   await FN_DELAY(20);

   expect(suffix.value).toEqual([`4`, `2`, `3`]);
});


it(`SHOULD clear values on shutdown`, async () => {
   const array = new ReactiveNumberArray([1]);
   const delays = [20, 10, 40];
   const mapper = mapArrayModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, delays.shift()))
      )
   );
   const suffix = array.pipe(
      mergeChange(),
      mapper
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([]);

   array.add([2]);
   array.add([3]);

   await FN_DELAY(10);

   expect(suffix.value).toEqual([]);

   suffix.shutdown = WASTED;

   expect(array.watchers.size).toEqual(0);
   expect(mapper.buffer.size).toEqual(0);
});

it(`SHOULD shutdown atomatically`, async () => {
   const array = new ReactiveNumberArray([1]);
   const mapper = mapArrayModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, 10))
      )
   );
   const suffix = array.pipe(
      mergeChange(),
      shutdownOn(1),
      mapper
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([]);

   array.add([2]);
   array.add([3]);

   await FN_DELAY(10);

   expect(suffix.value).toEqual([`1`]);

   expect(array.watchers.size).toEqual(0);
   expect(suffix.shutdown).toEqual(EDetachMessage.Auto);
   expect(mapper.buffer.shutdown).toEqual(EDetachMessage.Auto);
});
