import { mapArrayAdd, mapArrayAddMultiple, Reactive, ReactiveNumberArray } from "@taedr/reactive";

it(`SHOULD wraps incoming value into array addition modification `, () => {
   const add = new Reactive(1);
   const suffix = add.pipe(
      mapArrayAdd()
   ).suffix(new ReactiveNumberArray());

   expect(suffix.value).toEqual([1]);

   add.send(2);

   expect(suffix.value).toEqual([1, 2]);
});

it(`SHOULD wraps incoming values into array addition modification `, () => {
   const add = new Reactive([1, 2]);
   const suffix = add.pipe(
      mapArrayAddMultiple()
   ).suffix(new ReactiveNumberArray());

   expect(suffix.value).toEqual([1, 2]);

   add.send([3, 4]);

   expect(suffix.value).toEqual([1, 2, 3, 4]);
});
