import { Bus, IReactiveArrayModification, mapArrayModification, mergeChange, ReactiveNumberArray, ReactiveStringArray } from "@taedr/reactive";

it(`Converts values in 'added' and 'updated' from the source observable 'change' into new shape`, () => {
   const array = new ReactiveNumberArray([1]);
   const suffix = array.pipe(
      mergeChange(),
      mapArrayModification(value => `${value}`)
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([`1`]);

   array.add([2]);

   expect(suffix.value).toEqual([`1`, `2`]);
});

it(`Converts values in 'added' and 'updated' fields into new shape `, () => {
   const bus = new Bus<IReactiveArrayModification<number>>();
   const suffix = bus.pipe(
      mapArrayModification(value => `${value}`)
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([]);

   bus.send({
      added: [{ index: 0, values: [1, 2] }],
      deleted: [],
      updated: [],
      moved: []
   });

   expect(suffix.value).toEqual([`1`, `2`]);
});
