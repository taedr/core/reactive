import { Bus, map, mergeChange, ReactiveNumberArray } from '@taedr/reactive';

/* Converts value from one form to another */
it(`Function`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      map(value => `!${value}`)
   );
   let Ivalue: string | undefined;

   pipe.watch(value => Ivalue = value);

   bus.send(1);

   expect(Ivalue).toEqual(`!1`);
});

/* Extracts specified key from the object */
it(`Key`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      mergeChange(),
      map(`value`)
   );
   let Ivalue: number | undefined;

   pipe.watch(value => Ivalue = value);

   bus.send(1);

   expect(Ivalue).toEqual(1);
});

/* Extracts specified keys from object into the new one*/
it(`Keys`, async () => {
   const bus = new ReactiveNumberArray();
   const pipe = bus.pipe(
      mergeChange(),
      map([`value`, `changed`])
   );
   let Ivalue: { value: readonly number[], changed: boolean } | undefined;

   pipe.watch(value => Ivalue = value);

   const Evalue: typeof Ivalue = {
      value: [1],
      changed: true,
   };

   bus.send(Evalue.value);

   expect(Ivalue).toEqual(Evalue);
});
