import { Bus, IReactiveRecordModification, mapRecordModification, mergeChange, ReactiveNumberRecord, ReactiveStringRecord } from "@taedr/reactive";

it(`Converts values in 'added' and 'updated' from the source observable 'change' into new shape`, () => {
   const array = new ReactiveNumberRecord({ 1: 1 });
   const suffix = array.pipe(
      mergeChange(),
      mapRecordModification(value => `${value}`)
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({ 1: `1` });

   array.add({ 2: 2 });

   expect(suffix.value).toEqual({ 1: `1`, 2: `2` });
});

it(`Converts values in 'added' and 'updated' fields into new shape `, () => {
   const bus = new Bus<IReactiveRecordModification<number>>();
   const suffix = bus.pipe(
      mapRecordModification(value => `${value}`)
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({});

   bus.send({
      added: [
         { key: `1`, value: 1 },
         { key: `2`, value: 2 }
      ],
      deleted: [],
      updated: [],
   });

   expect(suffix.value).toEqual({ 1: `1`, 2: `2` });
});
