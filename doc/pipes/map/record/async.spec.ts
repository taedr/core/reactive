import { adapt, EDetachMessage, map, mapRecordModificationAsync, mergeChange, ReactiveNumberRecord, ReactiveStringRecord, shutdownOn, watch } from "@taedr/reactive";
import { PromiseAdapter } from '@taedr/reactive/adapter/classes/promise';
import { FN_DELAY, WASTED } from '@taedr/utils';

it(`Converts values in 'added' and 'updated' fields into new shape `, () => {
   const record = new ReactiveNumberRecord({ 1: 1 });
   const suffix = record.pipe(
      mergeChange(),
      mapRecordModificationAsync(
         r => r.pipe(
            map(v => `${v}`)
         )
      )
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({ 1: `1` });

   record.add({ 2: 2 });

   expect(suffix.value).toEqual({ 1: `1`, 2: `2` });
});

it(`Converts values in 'added' and 'updated' fields into new shape `, async () => {
   const record = new ReactiveNumberRecord({ 1: 1 });
   const suffix = record.pipe(
      mergeChange(),
      mapRecordModificationAsync(
         value => value.pipe(
            map(v => Promise.resolve(`${v}`)),
            watch(v => adapt(v))
         )
      )
   ).suffix(new ReactiveStringRecord());

   await FN_DELAY(1);

   expect(suffix.value).toEqual({ 1: `1` });

   record.add({ 2: 2 });

   await FN_DELAY(1);

   expect(suffix.value).toEqual({ 1: `1`, 2: `2` });
});

it(`SHOULD emit mapped changes in their income order `, async () => {
   const record = new ReactiveNumberRecord({ 1: 1 });
   const delays = [20, 10, 40];
   const mapper = mapRecordModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, delays.shift()))
      )
   )
   const suffix = record.pipe(
      mergeChange(),
      mapper
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({});

   record.add({ 2: 2 });
   record.add({ 3: 3 });

   await FN_DELAY(10);

   expect(suffix.value).toEqual({});

   await FN_DELAY(10);

   expect(suffix.value).toEqual({ 1: `1`, 2: `2` });

   await FN_DELAY(20);

   expect(suffix.value).toEqual({ 1: `1`, 2: `2`, 3: `3` });
});

it(`SHOULD clear values on shutdown`, async () => {
   const record = new ReactiveNumberRecord({ 1: 1 });
   const delays = [20, 10, 40];
   const mapper = mapRecordModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, delays.shift()))
      )
   );
   const suffix = record.pipe(
      mergeChange(),
      mapper
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({});

   record.add({ 2: 2 });
   record.add({ 3: 3 });

   await FN_DELAY(10);

   expect(suffix.value).toEqual({});

   suffix.shutdown = WASTED;

   expect(record.watchers.size).toEqual(0);
   expect(mapper.buffer.size).toEqual(0);
});

it(`SHOULD shutdown atomatically`, async () => {
   const record = new ReactiveNumberRecord({ 1: 1 });
   const mapper = mapRecordModificationAsync<number, string>(
      value => value.pipe(
         watch(v => PromiseAdapter.resolve(`${v}`, 10))
      )
   );
   const suffix = record.pipe(
      mergeChange(),
      shutdownOn(1),
      mapper
   ).suffix(new ReactiveStringRecord());

   expect(suffix.value).toEqual({});

   record.add({ 2: 2 });
   record.add({ 3: 3 });

   await FN_DELAY(10);

   expect(suffix.value).toEqual({ 1: `1` });


   expect(record.watchers.size).toEqual(0);
   expect(suffix.shutdown).toEqual(EDetachMessage.Auto);
   expect(mapper.buffer.shutdown).toEqual(EDetachMessage.Auto);
});
