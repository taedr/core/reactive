import { AReactiveObject, EDetachMessage, map, mapDelta, mapFieldsAsync, ReactiveNumber, ReactiveString, shutdownOn, watch } from '@taedr/reactive';
import { PromiseAdapter } from '@taedr/reactive/adapter/classes/promise';
import { FN_DELAY, TDeepPartial, WASTED } from '@taedr/utils';

class User {
   constructor(
      public name: string,
      public age: number
   ) { }
}

class ReactiveUser extends AReactiveObject<User> {
   constructor(
      user: User
   ) {
      super({
         fields: {
            name: new ReactiveString(user.name),
            age: new ReactiveNumber(user.age)
         }
      });
   }

   protected createInstance(): User {
      return new (User as any)();
   }
}

interface IMappedUserDelta {
   name: number;
   age: string;
}

it(`SHOULD map all fields`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      name: 6,
      age: `330`
   }

   const pipe = object.pipe(
      mapFieldsAsync<User, IMappedUserDelta>({
         name: bus => bus.pipe(
            map(name => name.length)
         ),
         age: bus => bus.pipe(
            map(age => `${age * 10}`)
         ),
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

it(`IF mapper specified SHOULD map field, IF not SHOUD use original value`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      name: `Andrii`,
      age: `330`
   }

   const pipe = object.pipe(
      mapFieldsAsync<User, IMappedUserDelta>({
         age: bus => bus.pipe(
            map(age => `${age * 10}`)
         ),
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

it(`SHOULD call mappers only for fields which present in the incoming object`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      age: `330`
   }

   const pipe = object.pipe(
      mapDelta(),
      mapFieldsAsync<TDeepPartial<User>, IMappedUserDelta>({
         name: bus => bus.pipe(
            map(name => name.length)
         ),
         age: bus => bus.pipe(
            map(age => `${age * 10}`)
         ),
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

it(`SHOULD emit mapped changes in their income order`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   const Ivalues: IMappedUserDelta[] = [];
   const delays = [50, 10, 30, 20];
   const Einitial = {
      name: 6,
      age: `220`
   };

   const Evalue = {
      name: 1,
      age: `330`
   };

   const pipe = object.pipe(
      mapFieldsAsync<TDeepPartial<User>, IMappedUserDelta>({
         name: bus => bus.pipe(
            watch(name => PromiseAdapter.resolve(name.length, delays.shift()))
         ),
         age: bus => bus.pipe(
            watch(age => PromiseAdapter.resolve(`${age * 10}`, delays.shift()))
         ),
      })
   );

   pipe.watch(value => Ivalues.push(value));

   expect(Ivalues).toEqual([]);

   await FN_DELAY(20);

   object.send({
      name: `S`,
      age: 33
   });

   expect(Ivalues).toEqual([]);

   await FN_DELAY(30);

   expect(Ivalues).toEqual([Einitial, Evalue]);
});

it(`SHOULD clear values on shutdown`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   const Evalue = {
      name: `Andrii`,
      age: `330`
   }

   const mapper = mapFieldsAsync<User, IMappedUserDelta>({
      age: bus => bus.pipe(
         watch(age => PromiseAdapter.resolve(`${age * 10}`, 10))
      ),
   });
   const suffix = object.pipe(
      mapper
   ).suffix();

   object.send({
      age: 33
   });

   expect(suffix.value).toEqual(null);

   await FN_DELAY(20);

   expect(suffix.value).toEqual(Evalue);

   suffix.shutdown = WASTED;

   expect(object.watchers.size).toEqual(0);
   expect(mapper.buffer.size).toEqual(0);
});

it(`SHOULD shutdown atomatically`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   const Evalue = {
      name: `Andrii`,
      age: `220`
   }

   const mapper = mapFieldsAsync<User, IMappedUserDelta>({
      age: bus => bus.pipe(
         watch(age => PromiseAdapter.resolve(`${age * 10}`, 10))
      ),
   });
   const suffix = object.pipe(
      shutdownOn(1),
      mapper
   ).suffix();

   object.send({
      age: 33
   });

   await FN_DELAY(10);

   expect(suffix.value).toEqual(Evalue);

   expect(object.watchers.size).toEqual(0);
   expect(suffix.shutdown).toEqual(EDetachMessage.Auto);
   expect(mapper.buffer.shutdown).toEqual(EDetachMessage.Auto);
});
