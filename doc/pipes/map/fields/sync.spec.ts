import { AReactiveObject, mapDelta, mapFields, ReactiveNumber, ReactiveString } from '@taedr/reactive';
import { TDeepPartial } from '@taedr/utils';

class User {
   constructor(
      public name: string,
      public age: number
   ) { }
}

class ReactiveUser extends AReactiveObject<User> {
   constructor(
      user: User
   ) {
      super({
         fields: {
            name: new ReactiveString(user.name),
            age: new ReactiveNumber(user.age)
         }
      });
   }

   protected createInstance(): User {
      return new (User as any)();
   }
}

interface IMappedUserDelta {
   name: number;
   age: string;
}

it(`SHOULD map all fields`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      name: 6,
      age: `330`
   }

   const pipe = object.pipe(
      mapFields<User, IMappedUserDelta>({
         name: name => name.length,
         age: age => `${age * 10}`,
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

it(`IF mapper specified SHOULD map field, IF not SHOUD use original value`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      name: `Andrii`,
      age: `330`
   }

   const pipe = object.pipe(
      mapFields<User, IMappedUserDelta>({
         age: age => `${age * 10}`,
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

it(`SHOULD call mappers only for fields which present in the incoming object`, async () => {
   const object = new ReactiveUser(new User(`Andrii`, 22));
   let Ivalue: IMappedUserDelta | undefined;
   const Evalue = {
      age: `330`
   }

   const pipe = object.pipe(
      mapDelta(),
      mapFields<TDeepPartial<User>, IMappedUserDelta>({
         name: name => name.length,
         age: age => `${age * 10}`,
      })
   );

   pipe.watch(value => Ivalue = value);

   object.send({
      age: 33
   });

   expect(Ivalue).toEqual(Evalue);
});

