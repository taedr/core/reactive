import { Bus, side } from '@taedr/reactive';

/* Performs a side effect */
it(`Side`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(side(value => sideValue = value));
   let sideValue: number | undefined;
   let newValue: number | undefined;

   pipe.watch(value => newValue = value);

   expect(sideValue).toBeUndefined();
   expect(newValue).toBeUndefined();

   bus.send(1);

   console.log(sideValue); // 1
   console.log(newValue === sideValue); // true
   expect(sideValue).toEqual(1);
   expect(newValue).toEqual(sideValue);
});
