import { accumulate, Bus, EDetachMessage, IBusChange, iterate, map, shutdownOn, suppress } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

/* Accumulates values produced by previous pipe
 (by pipeline source if accumulator is a single pipe) into array.
 Emits once before pipeline shutdown. */
it(`accumulate`, async () => {
   const bus = new Bus<number>();
   const accumulator = accumulate<number>();
   const pipe = bus.pipe(accumulator);
   let newValue: readonly number[] | undefined;

   pipe.watch(value => newValue = value);

   console.log(newValue); // undefined
   console.log(accumulator.buffer.value); // []
   expect(newValue).toBeUndefined();
   expect(accumulator.buffer.value).toEqual([]);

   bus.send(1);
   bus.send(2);
   bus.send(3);

   console.log(newValue); // undefined
   console.log(accumulator.buffer.value); // [1, 2, 3]
   expect(newValue).toBeUndefined();
   expect(accumulator.buffer.value).toEqual([1, 2, 3]);

   bus.shutdown = `WASTED`;

   console.log(newValue); // [1, 2, 3]
   console.log(accumulator.buffer.value); // []
   console.log(pipe.shutdown); // WASTED
   expect(newValue).toEqual([1, 2, 3]);
   expect(accumulator.buffer.value).toEqual([]);
   expect(pipe.shutdown).toEqual(`WASTED`);
});

/*  */
it(`accumulate multiple`, async () => {
   const bus = new Bus<number>();
   const shutdowner = shutdownOn(3);
   const accumulator_1 = accumulate<number>();
   const iterator = iterate<number>();
   const suppresser = suppress<number>(1);
   const mapper = map<number, string, IBusChange>(value => `!${value}`);
   const accumulator_2 = accumulate<string>();
   const pipe = bus.pipe(
      shutdowner,
      accumulator_1,
      iterator,
      suppresser,
      mapper,
      accumulator_2
   );
   let newValue: readonly string[] | undefined;
   let accValue: readonly number[] | undefined;

   pipe.watch(value => newValue = value);
   accumulator_1.watch(value => accValue = value);

   console.log(accValue); // undefined
   expect(accValue).toBeUndefined();

   bus.send(1);
   bus.send(2);
   bus.send(3);

   await FN_DELAY(50);

   console.log(newValue); // [ '!1', '!2', '!3' ]
   console.log(accumulator_1.buffer.value); // []
   console.log(pipe.shutdown === EDetachMessage.Auto);
   console.log(accValue); // [1, 2, 3]
   expect(accValue).toEqual([1, 2, 3]);
   expect(newValue).toEqual(['!1', '!2', '!3']);
   expect(accumulator_1.buffer.value).toEqual([]);
   expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
});
