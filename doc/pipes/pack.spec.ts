import { Bus, IPipeline, pack, PackDuration, PackLiberator, Timer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';
import { checkAsyncShutdown, WASTED } from './_utils/async';

it(`After receiving a value will pack it into an array, wait for specified number of milliseconds before transferring.
If during that time a new value will appear, it will be added to the array.`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      pack(10)
   ) as IPipeline<readonly number[], Bus<number>,[
      PackDuration<number>
   ]>;

   const [packer] = pipe.pipes;
   let Ivalue: readonly number[] | undefined;

   pipe.watch(value => Ivalue = value);

   expect(packer.pack.value).toEqual([]);
   expect(packer.durationMs).toEqual(10);
   expect(packer.active.value).toBeFalsy();

   bus.send(1);

   expect(packer.active.value).toBeTruthy();
   expect(Ivalue).toBeUndefined();
   expect(packer.pack.value).toEqual([1]);

   await FN_DELAY(10);

   expect(Ivalue).toEqual([1]);
   expect(packer.pack.value).toEqual([]);
   expect(packer.active.value).toBeFalsy();

   bus.send(2);
   bus.send(3);

   expect(packer.pack.value).toEqual([2, 3]);

   await FN_DELAY(10);

   expect(Ivalue).toEqual([2, 3]);
   expect(packer.pack.value).toEqual([]);

   pipe.shutdown = WASTED;

   expect(packer.pack['shutdown']).toEqual(WASTED);
});

it(`After receiving value will map it to the source(liberator). This and further received values will be packed into an array and transferred when the liberator emits.`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      pack(value => new Timer(value * 10, 1))
   ) as IPipeline<readonly number[], Bus<number>,[
      PackLiberator<number, Timer<1>>
   ]>;
   const [packer] = pipe.pipes;

   let Ivalue: readonly number[] | undefined;

   pipe.watch(value => Ivalue = value);

   expect(packer.pack.value).toEqual([]);
   expect(packer.active.value).toBeFalsy();

   bus.send(3);
   bus.send(2);

   expect(packer.pack.value).toEqual([3, 2]);
   expect(packer.active.value).toBeTruthy();

   await FN_DELAY(15);

   expect(Ivalue).toBeUndefined();

   await FN_DELAY(15);

   expect(Ivalue).toEqual([3, 2]);
   expect(packer.active.value).toBeFalsy();

   bus.send(1);

   pipe.shutdown = WASTED;

   expect(packer.pack['shutdown']).toEqual(WASTED);
});

describe(`WHEN shutdowned automatically SHOULD shutdown pipeline only afrer value will be released`, () => {
   it(`Time`, async () => {
      await checkAsyncShutdown(
         () => pack(10),
         () => FN_DELAY(10).then(() => [1])
      );
   });

   it(`Emit`, async () => {
      await checkAsyncShutdown(
         () => pack(value => new Timer(value * 10, 1)),
         () => FN_DELAY(10).then(() => [1])
      );
   });
});
