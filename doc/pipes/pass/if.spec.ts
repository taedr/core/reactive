import { Bus, IBusChange, passIf } from '@taedr/reactive';
import { checkOriginChange } from './_passSameChange';

it(`Passes value if funtion returns 'true'`, async () => {
   const bus = new Bus<number>();
   const passer = passIf<number, IBusChange>(value => value < 1 || value > 2);
   const pipe = bus.pipe(passer);
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   bus.send(0);
   bus.send(1);
   bus.send(2);
   bus.send(3);

   expect(newValues).toEqual([0, 3]);
});

it(`SHOULD pass same change as recieved`, async () => {
   checkOriginChange(passIf(() => true));
});
