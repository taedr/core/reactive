import { Bus, IBusChange, Reactive, passSwitch } from '@taedr/reactive';
import { checkOriginChange } from '../_utils/passSameChange';

it(`Passes values if 'source' last emitted value was 'true'.
By default passes values if source not yet emitted.`, async () => {
   const bus = new Bus<number>();
   const passer = passSwitch<number, Reactive<boolean>, IBusChange>(new Reactive(true));
   const pipe = bus.pipe(
      passer
   );
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   bus.send(1);
   passer.pass.send(false);
   bus.send(2);
   passer.pass.send(true);
   bus.send(3);
   passer.pass.send(false);
   bus.send(4);

   expect(newValues).toEqual([1, 3]);
});

it(`SHOULD pass same change as recieved`, async () => {
   checkOriginChange(passSwitch(new Reactive(true)));
});
