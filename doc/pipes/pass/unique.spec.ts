import { Bus, IBusChange, passUnique } from "@taedr/reactive";
import { WASTED } from "@taedr/utils";
import { checkOriginChange } from "../_utils/passSameChange";

it(`SHOULD pass only unique values`, async () => {
   const values = [1, 2, 1, 3, 2, 3, 4, 5, 4];
   const Evalues = [1, 2, 3, 4, 5];
   const bus = new Bus<number>();
   const passer = passUnique<number, IBusChange>();
   const pipe = bus.pipe(passer);
   const Ivalues: number[] = [];
   const Ebuffer: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   for (const value of values) {
      bus.send(value);
      if (!Ebuffer.includes(value)) Ebuffer.push(value);
      expect(passer.buffer.value).toEqual(Ebuffer);
   }

   expect(Ivalues).toEqual(Evalues);
   expect(passer.buffer.value).toEqual(Evalues);
   expect(passer.maxSize).toEqual(Infinity);

   pipe.shutdown = WASTED;

   expect(passer.buffer.value).toEqual([]);
   expect(passer.buffer.shutdown).toEqual(WASTED);
});


it(`SHOULD remember two last values.
If incoming value equal some present in the buffer it SHOULD be skipped`, async () => {
   const bus = new Bus<number>();
   const passer = passUnique<number, IBusChange>(2);
   const pipe = bus.pipe(passer);
   const Ivalues: number[] = [];

   const watcherId = pipe.watch(value => Ivalues.push(value));

   expect(Ivalues).toEqual([]);
   expect(passer.buffer.value).toEqual([]);
   expect(passer.maxSize).toEqual(2);

   bus.send(1);

   expect(Ivalues).toEqual([1]);
   expect(passer.buffer.value).toEqual([1]);

   bus.send(2);

   expect(Ivalues).toEqual([1, 2]);
   expect(passer.buffer.value).toEqual([1, 2]);

   bus.send(1);

   expect(Ivalues).toEqual([1, 2]);
   expect(passer.buffer.value).toEqual([2, 1]);

   bus.send(2);

   expect(Ivalues).toEqual([1, 2]);
   expect(passer.buffer.value).toEqual([1, 2]);

   bus.send(3);

   expect(Ivalues).toEqual([1, 2, 3]);
   expect(passer.buffer.value).toEqual([2, 3]);

   bus.send(1);
   expect(Ivalues).toEqual([1, 2, 3, 1]);
   expect(passer.buffer.value).toEqual([3, 1]);

   pipe.watchers.detach(watcherId);

   expect(passer.buffer.value).toEqual([]);
});

it(`SHOULD pass same change as recieved`, async () => {
   checkOriginChange(passUnique());
});
