import { Bus, IBusChange, IReactiveChange, passIfChanged, ReactiveNumber } from '@taedr/reactive';
import { checkOriginChange } from '../_utils/passSameChange';

it(`Passes value if 'changed' in 'change' is 'true'`, async () => {
   const reactive = new ReactiveNumber(1);
   const passer = passIfChanged<number, IReactiveChange>();
   const pipe = reactive.pipe(passer);
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   reactive.send(1);
   reactive.send(0);
   reactive.send(1);
   reactive.send(1);
   reactive.send(0);
   reactive.send(0);
   reactive.send(1);

   expect(newValues).toEqual([1, 0, 1, 0, 1]);
});

it(`Passes value if result of 'getHash' for current value is different from result for the previous`, async () => {
   class User {
      constructor(
         readonly name: string,
         readonly age: number
      ) { }
   }

   const bus = new Bus<User>();
   const passer = passIfChanged<User, number, IBusChange>(({ age }) => age);
   const pipe = bus.pipe(passer);
   const newValues: User[] = [];

   pipe.watch(value => newValues.push(value));

   bus.send(new User(`Adnrii`, 18));

   expect(passer.prev).toEqual(18);

   bus.send(new User(`Volodimir`, 18));

   expect(passer.prev).toEqual(18);

   bus.send(new User(`Orest`, 21));

   expect(passer.prev).toEqual(21);

   bus.send(new User(`Jaroslav`, 21));

   expect(passer.prev).toEqual(21);

   expect(newValues).toEqual([{ name: 'Adnrii', age: 18 }, { name: 'Orest', age: 21 }]);
});

it(`SHOULD pass same change as recieved`, async () => {
   checkOriginChange(passIfChanged(), []);
   checkOriginChange(passIfChanged(() => true));
});
