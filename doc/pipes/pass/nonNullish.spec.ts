import { Bus, IBusChange, passNonNullish } from '@taedr/reactive';
import { checkOriginChange } from '../_utils/passSameChange';

it(`Passes value if it is not equal to null or undefined`, async () => {
   const bus = new Bus<number | null>();
   const passer = passNonNullish<number, IBusChange>();
   const pipe = bus.pipe(passer);
   const Ivalues: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   bus.send(null);
   bus.send(1);
   bus.send(null);
   bus.send(2);

   expect(Ivalues).toEqual([1, 2]);
});

it(`SHOULD pass same change as recieved`, async () => {
   checkOriginChange(passNonNullish());
});
