import { Bus, IPipeline, IReadonlyBus, Producer, watch, WatchParallel, watchParallel, WatchReplace, watchSequential, Reactive, IWatchEntry, Timer, Ticker, shutdownOn } from '@taedr/reactive';
import { FN_DELAY, FN_MIRROR } from '@taedr/utils';
import { checkAsyncShutdown } from './_utils/async';

const getSources = () => [
   new Producer<number>(async (send, shutdown) => {
      send(1); // 1
      await FN_DELAY(60);
      send(2); // 5
      shutdown();
   }),
   new Producer<number>(async (send, shutdown) => {
      send(3); // 2
      await FN_DELAY(40);
      send(4); // 3
      shutdown();
   }),
   new Producer<number>(async (send, shutdown) => {
      send(5); // 4
      await FN_DELAY(20);
      send(6); // 6
      shutdown();
   }),
];

it(`All sources will be watched instantly and produce values parallelly`, async () => {
   const bus = new Bus<IReadonlyBus<number>>();
   const pipe = bus.pipe(
      watchParallel(FN_MIRROR)
   ) as IPipeline<number, Bus<IReadonlyBus<number>>, [
      WatchParallel<IReadonlyBus<number>, number, IReadonlyBus<number>>
   ]>;
   const [watcher] = pipe.pipes;

   const Ivalues: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   expect(Ivalues).toEqual([]);
   expect(watcher.entries.size).toEqual(0);
   expect(watcher.active.value).toBeFalsy();

   getSources().forEach(s => bus.send(s));

   expect(Ivalues).toEqual([1, 3, 5]);
   expect(watcher.entries.size).toEqual(3);
   expect(watcher.active.value).toBeTruthy();

   await FN_DELAY(100);

   expect(Ivalues).toEqual([1, 3, 5, 6, 4, 2]);
   expect(watcher.entries.size).toEqual(0);
   expect(watcher.active.value).toBeFalsy();
});

it(`New source will cause detaching from the previous source`, async () => {
   const bus = new Bus<IReadonlyBus<number>>();
   const pipe = bus.pipe(
      watch(FN_MIRROR)
   ) as IPipeline<number, Bus<IReadonlyBus<number>>, [
      WatchReplace<IReadonlyBus<number>, number, IReadonlyBus<number>>
   ]>;
   const [watcher] = pipe.pipes;
   const Ivalues: number[] = [];
   const sources = getSources();

   pipe.watch(value => Ivalues.push(value));

   expect(watcher.active.value).toBeFalsy();
   expect(watcher.entry.value).toEqual(null);

   let entry: IWatchEntry<IReadonlyBus<number>, number, IReadonlyBus<number>> | null = null;

   for (const source of sources) {
      bus.send(source);
      entry = watcher.entry.value;
      expect(entry).toEqual({ value: source, source });
   }

   expect(watcher.active.value).toBeTruthy();

   await FN_DELAY(20);

   expect(Ivalues).toEqual([1, 3, 5, 6]);
   expect(watcher.active.value).toBeFalsy();
});

it(`Start watching next source when detached from previous`, async () => {
   const bus = new Bus<IReadonlyBus<number>>();
   const watcher = watchSequential<IReadonlyBus<number>, number, IReadonlyBus<number>>(FN_MIRROR);
   const pipe = bus.pipe(watcher);
   const Ivalues: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   expect(watcher.active.value).toBeFalsy();

   getSources().forEach(s => bus.send(s));

   expect(watcher.entries.size).toEqual(3);
   expect(Ivalues).toEqual([1]);
   expect(watcher.active.value).toBeTruthy();

   await FN_DELAY(100);

   expect(Ivalues).toEqual([1, 2, 3, 4, 5, 6]);
   expect(watcher.active.value).toBeFalsy();
});

describe(`WHEN shutdowned automatically SHOULD shutdown pipeline only afrer value will be released`, () => {
   it(`Replace`, async () => {
      await checkAsyncShutdown(
         () => watch(() => new Ticker(10).pipe(shutdownOn(2))),
         () => FN_DELAY(40).then(() => 2)
      );
   });

   it(`Parallel`, async () => {
      await checkAsyncShutdown(
         () => watchParallel(() => new Ticker(10).pipe(shutdownOn(2))),
         () => FN_DELAY(40).then(() => 2)
      );
   });

   it(`Sequential`, async () => {
      await checkAsyncShutdown(
         () => watchSequential(() => new Ticker(10).pipe(shutdownOn(2))),
         () => FN_DELAY(40).then(() => 2)
      );
   });
});
