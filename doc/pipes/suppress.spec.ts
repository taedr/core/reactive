import { Bus, IPipeline, suppress, SuppressLiberator, Timer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';
import { checkAsyncShutdown } from './_utils/async';


it(`Duration`, async () => {
   const bus = new Bus<number>();
   const suppresser = suppress<number>(10);
   const pipe = bus.pipe(suppresser);
   const Ivalues: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   expect(suppresser.buffer.value).toEqual([]);
   expect(suppresser.durationMs).toEqual(10);
   expect(suppresser.active.value).toBeFalsy();

   bus.send(1);
   bus.send(2);

   expect(Ivalues).toEqual([]);
   expect(suppresser.buffer.value).toEqual([1, 2]);
   expect(suppresser.active).toBeTruthy();

   await FN_DELAY(10);

   expect(Ivalues).toEqual([1]);
   expect(suppresser.buffer.value).toEqual([2]);
   expect(suppresser.active).toBeTruthy();

   await FN_DELAY(10);

   expect(Ivalues).toEqual([1, 2]);
   expect(suppresser.buffer.value).toEqual([]);
   expect(suppresser.active.value).toBeFalsy();

   pipe.shutdown = 'WASTED';

   expect(suppresser.buffer['shutdown']).toEqual('WASTED');
});

it(`Liberation`, async () => {
   const bus = new Bus<number>();
   const suppresser = suppress<number, Timer<true>>(value => new Timer(value * 10, true));
   const pipe = bus.pipe(suppresser);
   const Ivalues: number[] = [];

   pipe.watch(value => Ivalues.push(value));

   expect(suppresser.buffer.value).toEqual([]);
   expect(suppresser.active.value).toBeFalsy();

   bus.send(1);
   bus.send(2);

   expect(Ivalues).toEqual([]);

   expect(suppresser.buffer.size).toEqual(2);
   expect(suppresser.active.value).toBeTruthy();

   await FN_DELAY(10);

   expect(Ivalues).toEqual([1]);
   expect(suppresser.buffer.size).toEqual(1);
   expect(suppresser.active).toBeTruthy();

   await FN_DELAY(20);

   expect(Ivalues).toEqual([1, 2]);
   expect(suppresser.buffer.value).toEqual([]);
   expect(suppresser.active.value).toBeFalsy();

   pipe.shutdown = 'WASTED';

   expect(suppresser.buffer['shutdown']).toEqual('WASTED');
});

it(`Manual pass control`, async () => {
   const bus = new Bus<number>();
   const pipe = (bus.pipe(
      suppress(() => new Bus<boolean>())
   ) as IPipeline<number, number, Bus<number>, [
      SuppressLiberator<number, Bus<boolean>>
   ]>).suffix();
   const Ivalues: number[] = [];
   pipe.origin
   const watcherId = pipe.watch(value => Ivalues.push(value));
   const [suppresser] = pipe.origin.pipes;

   bus.send(1);
   bus.send(2);
   bus.send(3);

   const [first] = suppresser.buffer.value;

   first.liberator.send(true);

   expect(Ivalues).toEqual([1]);
   expect(first.liberator.watchers.size).toEqual(0);
   expect(suppresser.buffer.size).toEqual(2);

   const [second] = suppresser.buffer.value;

   second.liberator.shutdown = `WASTED`;

   expect(second.liberator.watchers.size).toEqual(0);
   expect(suppresser.buffer.size).toEqual(1);

   const [third] = suppresser.buffer.value;

   pipe.watchers.detach(watcherId);

   expect(third.liberator.watchers.size).toEqual(0);
   expect(suppresser.buffer.size).toEqual(0);

   pipe.shutdown = `WASTED`;

   expect(suppresser.buffer['shutdown']).toEqual(`WASTED`);
});

describe(`WHEN shutdowned automatically SHOULD shutdown pipeline only afrer value will be released`, () => {
   it(`Time`, async () => {
      await checkAsyncShutdown(
         () => suppress(20),
         () => FN_DELAY(20).then(() => 1)
      );
   });

   it(`Emit`, async () => {
      await checkAsyncShutdown(
         () => suppress(value => new Timer(value * 20, true)),
         () => FN_DELAY(20).then(() => 1)
      );
   });
});
