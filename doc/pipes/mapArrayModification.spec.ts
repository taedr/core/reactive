import { mapArrayModification, mergeChange, ReactiveNumberArray, ReactiveStringArray } from "@taedr/reactive";

it(`Map from another array`, () => {
   const array = new ReactiveNumberArray([1]);
   const suffix = array.pipe(
      mergeChange(),
      mapArrayModification(value => `${value}`)
   ).suffix(new ReactiveStringArray());

   expect(suffix.value).toEqual([`1`]);

   array.add([2]);

   expect(suffix.value).toEqual([`1`, `2`]);
});
