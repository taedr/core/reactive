import { Bus, integrate, Reactive } from '@taedr/reactive';

/* integrates incoming value and values emitted by provided sources into an array.
After that will emit on any change of provided source.*/
it(`integrate`, async () => {
   const number = new Bus<number>();
   const string = new Reactive(``);
   const integrated = integrate<number, string>([string]);
   const pipe = number.pipe(integrated);
   let Ivalue: [number, string] | null = null;

   const watcherId = pipe.watch(value => { Ivalue = value });

   expect(Ivalue).toBe(null)

   number.send(1);

   expect(Ivalue).toEqual([1, ``]);
   expect(integrated.group.value).toEqual(Ivalue);

   string.send(`a`);

   expect(Ivalue).toEqual([1, `a`]);

   pipe.watchers.detach(watcherId);

   expect(integrated.group.value).toEqual([undefined, undefined]);
});

it(`integrate`, async () => {
   const number = new Bus<number>();
   const string = new Reactive(``);
   const integrated = integrate<number, string>([string]);
   const pipe = number.pipe(integrated);
   let Ivalue: [number, string] | null = null;

   const watcherId = pipe.watch(value => { Ivalue = value });

   expect(Ivalue).toBe(null)

   number.send(1);

   expect(Ivalue).toEqual([1, ``]);
   expect(integrated.group.value).toEqual(Ivalue);

   string.send(`a`);

   expect(Ivalue).toEqual([1, `a`]);

   pipe.watchers.detach(watcherId);

   expect(integrated.group.value).toEqual([undefined, undefined]);
});
