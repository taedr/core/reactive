import { Bus, iterate } from '@taedr/reactive';

/* Receives an iterable and emits its values one by one */
it(`Iterate`, async () => {
   const bus = new Bus<number[]>();
   const pipe = bus.pipe(iterate());
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   bus.send([1, 2]);
   bus.send([3, 4]);

   console.log(newValues); // [ 1, 2, 3, 4 ]
   expect(newValues).toEqual([1, 2, 3, 4]);
});
