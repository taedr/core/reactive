import { APipe, Bus, IPossibleChange, side } from "@taedr/reactive";

export function checkOriginChange(
    pipe: APipe<number, any>,
    toEmit: number[] = [],
) {
    const bus = new Bus<number>();
    let IBchange: IPossibleChange | undefined;
    let ISchange: IPossibleChange | undefined;

    bus.watch((_, change) => IBchange = change);

    bus.pipe(
        pipe,
        side((_, change) => ISchange = change)
    ).watch(() => { });

    bus.send(1);

    for (const value of toEmit) {
        bus.send(value);
    }

    expect(IBchange).toBe(ISchange);
}
