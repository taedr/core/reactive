import { AAsyncPipe, EDetachMessage, Reactive, shutdownOn } from "@taedr/reactive";

export async function checkAsyncShutdown<T>(
   createPipe: () => AAsyncPipe<number, T>,
   waitPipe: () => Promise<T>
) {
   const reactive = new Reactive(1);
   const pipe = reactive.pipe(
      shutdownOn(1),
      createPipe(),
   );

   let Ivalue: any;

   pipe.watch(value => Ivalue = value);
   expect(Ivalue).toBeUndefined();

   const Evalue = await waitPipe();

   expect(Ivalue).toEqual(Evalue);
   expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
};
