import { Bus, IBusChange, ReactiveNumber, skip, skipInitial } from '@taedr/reactive';

it(`Skips specified number of values`, async () => {
   const bus = new Bus<number>();
   const skipper = skip<number>(2);
   const pipe = bus.pipe(skipper);
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   console.log(skipper.count); // 0
   console.log(skipper.limit); // 2
   console.log(skipper.passed); // false
   expect(skipper.count).toEqual(0);
   expect(skipper.limit).toEqual(2);
   expect(skipper.passed).toBeFalsy();

   bus.send(1);
   bus.send(2);
   bus.send(3);
   bus.send(4);

   expect(skipper.count).toEqual(4);
   expect(skipper.passed).toBeTruthy();
   expect(newValues).toEqual([3, 4]);
});

it(`Skips values until function returns false`, async () => {
   const bus = new Bus<number>();
   const skipper = skip<number, IBusChange>(value => value < 3);
   const pipe = bus.pipe(skipper);
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   console.log(skipper.passed); // false
   expect(skipper.passed).toBeFalsy();

   bus.send(1);
   bus.send(2);
   bus.send(3);
   bus.send(4);

   expect(skipper.passed).toBeTruthy();
   expect(newValues).toEqual([3, 4]);
});

it(`Passes value if 'initial' in 'change' is falsy`, async () => {
   const reactive = new ReactiveNumber(1);
   const pipe = reactive.pipe(
      skipInitial()
   );
   const newValues: number[] = [];

   pipe.watch(value => newValues.push(value));

   reactive.send(2);
   reactive.send(3);

   expect(newValues).toEqual([2, 3]);
});
