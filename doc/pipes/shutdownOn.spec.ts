import { Bus, debounce, EDetachMessage, IBusChange, map, Reactive, shutdownOn } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

describe(`Check - Shutdowns pipeline if condition returns "true"`, () => {
   it(`include`, async () => {
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number, IBusChange>(value => value === 2);
      const pipe = bus.pipe(shutdowner);

      let newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);
      bus.send(2);
      bus.send(3);

      expect(newValues).toEqual([1, 2]);
      expect(shutdowner.include).toBeTruthy();
      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
      expect(pipe.shutdown).toEqual(shutdowner['_shutdown']);
   });

   it(`determines, should value which caused 'true' be processed by pipeline`, async () => {
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number, IBusChange>(value => value === 2, false);
      const pipe = bus.pipe(shutdowner);

      let newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);
      bus.send(2);
      bus.send(3);

      expect(newValues).toEqual([1]);
      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
   });

   it(`Shutdowns pipeline if condition returns string with at least one charecter`, async () => {
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number, IBusChange>(v => v === 2 ? `WASTED` : ``);
      const pipe = bus.pipe(shutdowner);

      let newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);
      bus.send(2);
      bus.send(3);

      expect(newValues).toEqual([1, 2]);
      expect(pipe.shutdown).toEqual(`WASTED`);
   });
});

describe(`Limit - Shutdowns pipeline when specified number of values passed through shutdowner`, () => {
   it(`basic`, async () => {
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number>(2);
      const pipe = bus.pipe(shutdowner);
      const newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      expect(shutdowner.count).toEqual(0);
      expect(shutdowner.limit).toEqual(2);

      bus.send(1);

      expect(shutdowner.count).toEqual(1);

      bus.send(2);
      bus.send(3);

      expect(newValues).toEqual([1, 2]);
      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
      expect(pipe.shutdown).toEqual(shutdowner['_shutdown']);
   });

   it(`message`, async () => {
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number>(1, `WASTED`);
      const pipe = bus.pipe(shutdowner);
      const newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);

      expect(newValues).toEqual([1]);
      expect(pipe.shutdown).toEqual(`WASTED`);
      expect(pipe.shutdown).toEqual(shutdowner['_shutdown']);
   });
});


describe(`Source - Shutdowns pipeline when provided source sends any value`, () => {
   it(`SHOULD shutdown on first watcher`, async () => {
      const mark = new Reactive(true);
      const bus = new Bus<number>();
      const pipe = bus.pipe(shutdownOn(mark));


      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
   });

   it(`send`, async () => {
      const mark = new Bus<boolean>();
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number>(mark);
      const pipe = bus.pipe(shutdowner);
      let newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);
      bus.send(2);
      mark.send(true);
      bus.send(3);

      expect(newValues).toEqual([1, 2]);
      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
      expect(pipe.shutdown).toEqual(shutdowner.shutdown);
   });

   it(`message`, async () => {
      const mark = new Bus<string>();
      const bus = new Bus<number>();
      const shutdowner = shutdownOn<number>(mark);
      const pipe = bus.pipe(shutdowner);
      let newValues: number[] = [];

      pipe.watch(value => newValues.push(value));

      bus.send(1);
      mark.send(`WASTED`);
      bus.send(2);

      console.log(newValues); // [1]
      console.log(pipe.shutdown); // WASTED
      expect(newValues).toEqual([1]);
      expect(pipe.shutdown).toEqual(`WASTED`);
      expect(pipe.shutdown).toEqual(shutdowner.shutdown);
   });
});


/*  If shutdownOn was triggered while pipeline still processes values asynchronously,
no new values will be accepted from pipes before shutdownOn.
Will wait until asynchronous pipes after shutdownOn will finish processing their values.
 Pipeline shutdowns completely. */
it(`Async`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      shutdownOn(value => value === 2),
      debounce(1),
      map(value => `${value}`)
   );

   let newValue: string | undefined

   pipe.watch(value => { newValue = value; });

   bus.send(1);
   bus.send(2);
   bus.send(3);

   console.log(newValue); // undefined
   expect(newValue).toBeUndefined();

   await FN_DELAY(1);

   console.log(newValue); // 2
   console.log(pipe.shutdown === EDetachMessage.Auto); // true
   expect(newValue).toEqual(`2`);
   expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
});
