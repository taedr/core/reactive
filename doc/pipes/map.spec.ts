import { Bus, map, ReactiveNumberArray } from '@taedr/reactive';

/* Converts value from one form to another */
it(`Function`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      map(value => `!${value}`)
   );
   let Ivalue: string | undefined;

   pipe.watch(value => Ivalue = value);

   bus.send(1);

   expect(Ivalue).toEqual(`!1`);
});
