import { Bus, debounce, DebounceDuration, DebounceLiberator, EDetachMessage, IPipeline, Timer } from '@taedr/reactive';
import { FN_DELAY, WASTED } from '@taedr/utils';
import { checkAsyncShutdown } from './_utils/async';

describe(`After receiving value will wait for specified number of milliseconds before emitting it.
If during that time new value will appear, it will replace the old one and restart the timer`, async () => {
   it(`Basic`, async () => {
      const bus = new Bus<number>();
      const pipe = bus.pipe(
         debounce(10)
      ) as IPipeline<number, number, Bus<number>, [
         DebounceDuration<number>
      ]>;
      let Ivalue: number | undefined;

      pipe.watch(value => Ivalue = value);

      const debouncer = pipe.pipes[0];

      expect(debouncer.candidate.value).toBeNull();
      expect(debouncer.active.value).toBeFalsy();
      expect(debouncer.durationMs).toEqual(10);

      bus.send(1);

      const candidate_1 = debouncer.candidate.value;

      expect(Ivalue).toBeUndefined();
      expect(debouncer.active.value).toBeTruthy();
      expect(candidate_1?.value).toEqual(1);
      expect(candidate_1?.state.value).toEqual(`PENDING`);

      await FN_DELAY(10);

      expect(Ivalue).toEqual(1);
      expect(debouncer.candidate.value).toBeNull();
      expect(candidate_1?.value).toEqual(1);
      expect(candidate_1?.state['shutdown']).toEqual(EDetachMessage.Emittion);
      expect(candidate_1?.state.value).toEqual(`COMPLETED`);

      bus.send(2);

      const candidate_2 = debouncer.candidate.value;

      expect(candidate_2?.state.value).toEqual(`PENDING`);

      bus.send(3);

      expect(candidate_2?.state.value).toEqual(`ABORTED`);
      expect(candidate_2?.state['shutdown']).toEqual(EDetachMessage.Emittion);

      await FN_DELAY(10);

      expect(Ivalue).toEqual(3);
      expect(debouncer.candidate.value).toBeNull();
   });

   it(`Shutdown`, () => {
      const bus = new Bus<number>();
      const pipe = bus.pipe(
         debounce(10)
      ) as IPipeline<number, number, Bus<number>, [
         DebounceDuration<number>
      ]>;

      pipe.watch(() => { });

      const debouncer = pipe.pipes[0];

      bus.send(1);

      const candidate = debouncer.candidate.value;

      expect(candidate?.state.value).toEqual(`PENDING`);

      pipe.shutdown = WASTED;

      expect(candidate?.state.value).toEqual(`ABORTED`);
      expect(candidate?.state['shutdown']).toEqual(EDetachMessage.Emittion);
      expect(debouncer.candidate['shutdown']).toEqual(WASTED);
      expect(debouncer.active['shutdown']).toEqual(WASTED);
   });
});


describe(`After receiving value will wait until mapped source (liberator) emits before transferring value further.
If a new value will appear while a liberator not yet emitted, it will replace the old value and be mapped to the new liberator.`, async () => {
   it(`Basic`, async () => {
      const bus = new Bus<number>();
      const pipe = bus.pipe(
         debounce(value => new Timer(value * 10, 1))
      ) as IPipeline<number, number, Bus<number>, [
         DebounceLiberator<number, Timer<1>>
      ]>;
      let Ivalue: number | undefined;

      pipe.watch(value => Ivalue = value);

      const [debouncer] = pipe.pipes;

      expect(debouncer.candidate.value).toBeNull();
      expect(debouncer.active.value).toBeFalsy();

      bus.send(2);

      const candidate_1 = debouncer.candidate.value;

      expect(Ivalue).toBeUndefined();
      expect(debouncer.active.value).toBeTruthy();
      expect(candidate_1?.value).toEqual(2);
      expect(candidate_1?.liberator).toBeInstanceOf(Timer);
      expect(candidate_1?.state.value).toEqual(`PENDING`);

      await FN_DELAY(10);

      expect(Ivalue).toBeUndefined();

      await FN_DELAY(10);

      expect(Ivalue).toEqual(2);
      expect(candidate_1?.state.value).toEqual(`COMPLETED`);


      bus.send(0);

      const candidate_2 = debouncer.candidate.value;

      expect(candidate_2?.liberator.watchers.size).toEqual(1);

      bus.send(1);

      expect(candidate_2?.liberator.watchers.size).toEqual(0);

      await FN_DELAY(10);

      expect(Ivalue).toEqual(1);
   })

   it(`Shutdown`, () => {
      const bus = new Bus<number>();
      const pipe = bus.pipe(
         debounce(value => new Timer(value * 20, 1))
      ) as IPipeline<number, number, Bus<number>, [
         DebounceLiberator<number, Timer<1>>
      ]>;

      pipe.watch(() => { });

      const debouncer = pipe.pipes[0];

      bus.send(1);

      const candidate = debouncer.candidate.value;

      expect(candidate?.state.value).toEqual(`PENDING`);

      pipe.shutdown = WASTED;

      expect(candidate?.state.value).toEqual(`ABORTED`);
      expect(candidate?.state['shutdown']).toEqual(EDetachMessage.Emittion);
      expect(debouncer.candidate['shutdown']).toEqual(WASTED);
      expect(debouncer.active['shutdown']).toEqual(WASTED);
   });
});

describe(`WHEN shutdowned automatically SHOULD shutdown pipeline only afrer value will be released`, () => {
   it(`Time`, async () => {
      await checkAsyncShutdown(
         () => debounce(20),
         () => FN_DELAY(20).then(() => 1)
      );
   });

   it(`Emit`, async () => {
      await checkAsyncShutdown(
         () => debounce(value => new Timer(value * 20, 1)),
         () => FN_DELAY(20).then(() => 1)
      );
   });
});
