import { Bus, IPipeline, Pair, pair } from '@taedr/reactive';

/* Pairs previous and incoming values  */
it(`Pair`, async () => {
   const bus = new Bus<number>();
   const pipe = bus.pipe(
      pair()
   ) as IPipeline<number, [number | undefined, number], Bus<number>, [
      Pair<number>
   ]>;

   let Ivalue: [number | undefined, number] | undefined;

   pipe.watch(value => Ivalue = value);

   expect(Ivalue).toEqual(undefined);
   expect(pipe.pipes[0].prev.value).toEqual(undefined);

   bus.send(1);

   expect(Ivalue).toEqual([undefined, 1]);
   expect(pipe.pipes[0].prev.value).toEqual(1);

   bus.send(2);

   expect(Ivalue).toEqual([1, 2]);
});
