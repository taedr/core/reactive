import { Bus, Reactive, loop, map, skipInitial } from '@taedr/reactive';

it(`Side`, async () => {
   const bus = new Bus<number>();
   const side_1 = new Bus<number>().pipe(
      map(v => `${v}`)
   );
   const side_2 = new Reactive(``).pipe(
      skipInitial(),
      map(v => `)${v}(`)
   );
   const pipe = bus.pipe(
      loop(side_1),
      loop(side_2)
   );

   const Ivalue: string[] = [];

   pipe.watch(value => Ivalue.push(value));

   bus.send(1);

   expect(side_2.origin.value).toEqual(`1`);

   side_1.origin.send(2);
   side_2.origin.send(`3`);

   expect(Ivalue).toEqual([`)1(`, `)2(`, `)3(`]);
});
