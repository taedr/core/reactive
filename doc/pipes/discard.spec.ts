import { Bus, discard, Timer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

/* After receiving value will emit it instantly and start timer for specified number of milliseconds.
Until timer ends all new incoming values will be discarded.  */
it(`Duration`, async () => {
   const bus = new Bus<number>();
   const discarder = discard<number>(10);
   const pipe = bus.pipe(discarder);
   let newValue: number | undefined;

   pipe.watch(value => newValue = value);


   console.log(discarder.durationMs); // 10
   console.log(discarder.activatedAt); // undefined
   expect(discarder.durationMs).toEqual(10);
   expect(discarder.activatedAt).toBeUndefined();

   bus.send(1);

   console.log(newValue); // 1
   console.log(discarder.activatedAt !== undefined); // true
   expect(discarder.activatedAt).not.toBeUndefined();
   expect(newValue).toEqual(1);

   await FN_DELAY(10);

   console.log(newValue); // 1
   console.log(discarder.activatedAt); // undefined
   expect(newValue).toEqual(1);
   expect(discarder.activatedAt).toBeUndefined();

   bus.send(2);
   bus.send(3);

   await FN_DELAY(10);

   console.log(newValue); // 2
   expect(newValue).toEqual(2);
});
/* After receiving value will emit it instantly and map it to the source (liberator).
 * Until the liberator emits, all new incoming values will be discarded  */
it(`Window`, async () => {
   const bus = new Bus<number>();
   const discarder = discard<number>(value => new Timer(value * 10, 1));
   const pipe = bus.pipe(discarder);
   let newValue: number | undefined;

   pipe.watch(value => newValue = value);

   bus.send(3);

   await FN_DELAY(15);

   console.log(newValue); // 3
   expect(newValue).toEqual(3);

   bus.send(2);

   await FN_DELAY(15);

   console.log(newValue); // 3
   expect(newValue).toEqual(3);

   bus.send(1);
   bus.send(0);

   await FN_DELAY(10);

   console.log(newValue); // 1
   expect(newValue).toEqual(1);
});
