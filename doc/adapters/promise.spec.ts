import { EDetachMessage } from '@taedr/reactive';
import { PromiseBus } from '@taedr/reactive/adapter/classes/promise';
import { FN_DELAY, WASTED } from '@taedr/utils';

it(`resolve`, async () => {
   const promised = new PromiseBus(Promise.resolve(1));
   let Ivalue_1: number | undefined;
   let Ivalue_2: number | undefined;

   promised.watch(value => Ivalue_1 = value);

   expect(Ivalue_1).toEqual(undefined);
   expect(promised.value).toBeUndefined();

   await FN_DELAY(1);

   expect(Ivalue_1).toEqual(1);
   expect(promised.value).toEqual(1);
   expect(promised.shutdown).toEqual(EDetachMessage.Emittion);
   expect(promised['_promise']).toEqual(null);

   promised.watch(value => Ivalue_2 = value);

   expect(Ivalue_2).toEqual(1);
});

it(`reject`, async () => {
   const message = WASTED;
   const promised = new PromiseBus(Promise.reject(message));
   let Ivalue_1: number | undefined;
   let Ivalue_2: number | undefined;
   let Imessage: string | undefined;

   promised.watch(value => Ivalue_1 = value);

   expect(Ivalue_1).toEqual(undefined);

   await FN_DELAY(1);

   expect(Ivalue_1).toEqual(undefined);
   expect(promised.value).toBeUndefined();
   expect(promised.shutdown).toEqual(message);

   promised.watch({
      value: value => Ivalue_2 = value,
      detach: message => Imessage = message,
   });

   expect(Ivalue_2).toEqual(undefined);
   expect(Imessage).toEqual(message);
});
