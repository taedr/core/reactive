import { EventBus } from '@taedr/reactive';
import { WASTED } from '@taedr/utils';

it(`EventAdapter`, () => {
   const adapter = new EventBus(document, `click`);
   const event = new MouseEvent(`click`);
   let newValue: Event | undefined;

   adapter.watch(value => newValue = value);

   document.dispatchEvent(event)

   expect(newValue).toEqual(event);

   adapter.shutdown = WASTED;

   expect(adapter['_source']).toEqual(null);
});

it(`adapter`, () => {
   const adapter = new EventBus(document, `click`);

   expect(adapter).toBeInstanceOf(EventBus);
});
