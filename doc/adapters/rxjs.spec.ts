import { EDetachMessage, ObservableBus } from '@taedr/reactive';
import { FN_MIRROR, WASTED } from '@taedr/utils';
import { BehaviorSubject } from 'rxjs';

it(`Adapter for RXJS observables`, async () => {
   const subject = new BehaviorSubject(0);

   const adapter = new ObservableBus(subject);

   let newValue: number | undefined;

   console.log(subject.observers.length); // 0
   expect(subject.observers).toHaveLength(0);

   const watcherId = adapter.watch(value => newValue = value);

   expect(subject.observers).toHaveLength(1);
   expect(newValue).toEqual(0);

   subject.next(1);

   expect(newValue).toEqual(1);

   adapter.watchers.detach(watcherId);

   expect(subject.observers).toHaveLength(0);

   adapter.watch(FN_MIRROR);

   expect(subject.observers).toHaveLength(1);

   adapter.shutdown = WASTED;

   expect(adapter['_source']).toEqual(null);
});

it(`Error`, () => {
   const subject = new BehaviorSubject(1);
   const adapter = new ObservableBus(subject).suffix();
   const mess = WASTED;

   subject.error(mess);

   expect(subject.observers).toHaveLength(0);
   expect(adapter.shutdown).toEqual(mess);
});

it(`Complete`, () => {
   const subject = new BehaviorSubject(1);
   const adapter = new ObservableBus(subject).suffix();

   subject.complete();

   expect(adapter.shutdown).toEqual(EDetachMessage.Emittion);
});
