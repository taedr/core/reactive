import { Bus, EDetachMessage, ERRORS, map, passIf, Reactive, shutdownOn, skipInitial } from '@taedr/reactive';

/*
Bus is the simplest observable, it informs subscribers about value changes, but not store value itself.
Implements basic funtionality for all observables.
*/
it(`Bus`, () => {
   const bus = new Bus<number>();
   const Ivalues: number[] = [];

   bus.watch(value => { Ivalues.push(value) });

   bus.send(1);
   bus.send(2);

   expect(Ivalues).toEqual([1, 2]);
});
/*
Reactive is the Bus which stores last sended value.
Reactive will instantly transfer stored value to new watcher
*/
it(`Reactive`, () => {
   const reactive = new Reactive(0);
   const Ivalues: number[] = [];

   expect(reactive.value).toEqual(0);

   reactive.watch(value => { Ivalues.push(value) });

   expect(Ivalues).toEqual([0]);

   reactive.send(1);

   expect(reactive.value).toEqual(1);
   expect(Ivalues).toEqual([0, 1]);
});
/* Pipleine is chain of pipes where each will process incoming value in some way.
Pipeline becomes active when first watcher appears and deactivates
when all watchers unsubscribed or on shutdown */
it(`Pipleine`, () => {
   const bus = new Bus<number>();
   const Ivalues: string[] = [];

   const pipeline = bus.pipe(
      passIf(number => number > 1),
      map(number => `Value is: ${number}`)
   );

   expect(bus.watchers.size).toEqual(0);
   expect(pipeline.origin).toEqual(bus);

   const watcherId = pipeline.watch(value => { Ivalues.push(value); });

   expect(bus.watchers.size).toEqual(1);

   bus.send(1);
   bus.send(2);
   pipeline.origin.send(3);

   expect(Ivalues).toEqual(['Value is: 2', 'Value is: 3']);

   pipeline.watchers.detach(watcherId);

   expect(bus.watchers.size).toEqual(0);

   bus.send(4);

   expect(Ivalues).toEqual(['Value is: 2', 'Value is: 3']);
});
/* Suffix is middle point Reactive which allows to store value without explicate subscription.
It subscribes to source instantly and should be detached by shutdown if not needed anymore. */
it(`Suffix`, () => {
   const reactive = new Reactive(1);
   const shutdowner = new Bus<null>();

   const suffix = reactive.pipe(
      shutdownOn(shutdowner),
      map(number => `!${number}`)
   ).suffix();

   expect(reactive.watchers.size).toEqual(1);
   expect(suffix.origin).toEqual(reactive);
   expect(suffix.value).toEqual(`!1`);

   reactive.send(2);

   expect(suffix.value).toEqual(`!2`);

   suffix.origin.send(3);

   expect(suffix.value).toEqual(`!3`);

   // suffix.shutdown = EDetachMessage.Auto - will have same effect
   shutdowner.send(null);

   expect(suffix.shutdown).toEqual(EDetachMessage.Auto);
   expect(reactive.watchers.size).toEqual(0);

   /* Ref for advenced suffix */
});
/* Indicates that provided value are currently stored by Reactive.   */
it(`Initial`, () => {
   const reactive = new Reactive(0);
   const Ivalues: number[] = [];

   reactive.watch({
      value: (value, { initial }) => {
         if (initial) return;
         Ivalues.push(value)
      },
   });

   reactive.pipe(
      skipInitial()
   ).watch(value => Ivalues.push(value))

   expect(Ivalues).toEqual([]);

   reactive.send(1);

   expect(reactive.value).toEqual(1);
   expect(Ivalues).toEqual([1, 1]);
});
/* Indicates that provided value are currently stored by Reactive.   */
it(`Intial`, () => {
   const reactive = new Reactive(0);
   const Ivalues: number[] = [];

   reactive.watch({
      value: (value, { initial }) => {
         if (initial) return;
         Ivalues.push(value)
      },
   });

   reactive.pipe(
      skipInitial()
   ).watch(value => Ivalues.push(value))

   expect(Ivalues).toEqual([]);

   reactive.send(1);

   expect(reactive.value).toEqual(1);
   expect(Ivalues).toEqual([1, 1]);
});
/*
String value which reflects possibility of future value changes.
Empty string (default) - observable can be changed.
Meaninfull string - reason why observable can not be changed.
After shutdown:
- Stored value will remain
- All watchers will be detached
- Attempt to transfer new value will cause an error
- Shutdown message can not be changed

! Accepts string with at least one character
! In order to detach any mapped reactive from source you will have to shutdown it
*/
it(`Shutdown`, () => {
   const reactive = new Reactive(0);

   expect(reactive.shutdown).toEqual(``);

   /* Shutdown message can not be an empty string */
   expect(() => reactive.shutdown = ``).toThrowError(ERRORS.shutdown.message);

   reactive.watch(() => { });

   expect(reactive.watchers.size).toEqual(1);

   reactive.shutdown = `No more values!`;

   expect(reactive.shutdown).toEqual(`No more values!`);
   expect(reactive.value).toEqual(0);
   expect(reactive.watchers.size).toEqual(0);

   /* Shutdown message can not be changed */
   reactive.shutdown = `Another reason`;

   expect(reactive.shutdown).toEqual(`No more values!`);

   /* Can not change value after shutdown */
   expect(() => reactive.send(1)).toThrowError(ERRORS.shutdown.change);
});
/*
   There are few ways to stop detach watcher from bus
*/
describe(`Detach`, () => {
   it(`By Id`, () => {
      const bus = new Bus<number>();
      const Ivalues: number[] = [];
      let detachMessage = ``;

      const watcherId = bus.watch({
         value: value => { Ivalues.push(value) },
         detach: message => { detachMessage = message },
      });

      expect(bus.watchers.size).toEqual(1);
      expect(bus.watchers.has(watcherId)).toBeTruthy();

      bus.send(1);
      bus.send(2);

      expect(Ivalues).toEqual([1, 2]);

      const detached = bus.watchers.detach(watcherId, `Reason`); // Reason is optional

      expect(detached).toBeTruthy();
      expect(detachMessage).toEqual(`Reason`);
      expect(bus.watchers.size).toEqual(0);
      expect(bus.watchers.has(watcherId)).toBeFalsy();

      bus.send(4);

      expect(Ivalues).toEqual([1, 2]);
   });

   it(`By Custom Id`, () => {
      const bus = new Bus<number>();
      const id = `Test`;
      let detachMessage = ``;

      const watcherId = bus.watch({
         id,
         value: () => { },
         detach: message => detachMessage = message
      });

      expect(watcherId).toEqual(id);

      // When message is not provided currently used watcher id will be used
      bus.watchers.detach(id);

      expect(bus.watchers.size).toEqual(0);
      expect(detachMessage).toEqual(id);
   });

   it(`shutdownOn`, () => {
      const bus = new Bus<number>();
      const Ivalues: number[] = [];
      const pipe = bus.pipe(
         shutdownOn(1)
      );

      pipe.watch(value => Ivalues.push(value));

      bus.send(1);

      expect(Ivalues).toEqual([1]);
      expect(pipe.shutdown).toEqual(EDetachMessage.Auto);
      expect(bus.watchers.size).toEqual(0);

      bus.send(2);

      expect(Ivalues).toEqual([1]);
   });
});

/*
Advantages:
1. Separate entities for array, record, object.
Those entities can inform about changes that were made specifically: add, delete, update, move (array).
Which makes easier to avoid unneeded actions.
3. Ability to monitor state of the pipeline, access to pipes and their current state
4. Suffix - possibility to redirect pipeline results into fitting reactive
*/
