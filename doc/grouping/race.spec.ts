import { Race, Bus, EDetachMessage } from "@taedr/reactive";
import { FN_MIRROR } from '@taedr/utils';

it(`Transfers values only from winner (source which emitted first) `, async () => {
   const bus_1 = new Bus<number>();
   const bus_2 = new Bus<number>();
   const racer = new Race([bus_1, bus_2]);
   let newValue: number | undefined;

   const watcherId = racer.watch(value => newValue = value);

   expect(newValue).toBeUndefined();
   expect(bus_1.watchers.size).toBe(1);
   expect(bus_2.watchers.size).toBe(1);

   bus_2.send(1);

   expect(racer['_sources']).toEqual([bus_2]);
   expect(newValue).toBe(1);
   expect(bus_1.watchers.size).toBe(0);
   expect(bus_2.watchers.size).toBe(1);

   bus_2.send(2);

   expect(newValue).toBe(2);

   racer.watchers.detach(watcherId);

   expect(bus_2.watchers.size).toEqual(0);

   racer.watch(FN_MIRROR);

   expect(bus_2.watchers.size).toEqual(1);

   bus_2.shutdown = `WASTED`;

   expect(racer.shutdown).toEqual(`WASTED`);
});

it(`Shutdowns if detached from all sources if there are no winner yet`, async () => {
   const bus_1 = new Bus<number>();
   const bus_2 = new Bus<number>();
   const racer = new Race([bus_1, bus_2]);
   let newValue: number | undefined;

   racer.watch(value => newValue = value);

   bus_1.shutdown = `WASTED`;
   bus_2.shutdown = `WASTED`;

   expect(racer.shutdown).toEqual(EDetachMessage.Sources);
   expect(newValue).toEqual(undefined);
});

it(`Shutdowns if detached from the winner`, async () => {
   const bus_1 = new Bus<number>();
   const bus_2 = new Bus<number>();
   const racer = new Race([bus_1, bus_2]);
   let newValue: number | undefined;

   racer.watch(value => newValue = value);

   bus_1.shutdown = `WASTED`;

   expect(racer.shutdown).toEqual(``);

   bus_2.send(1);

   expect(newValue).toEqual(1);
});
