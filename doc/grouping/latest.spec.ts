import { EDetachMessage, Reactive, latest } from '@taedr/reactive';
import { FN_MIRROR } from '@taedr/utils';

it(`Transfers value as it come from any source `, () => {
   const reactive_1 = new Reactive(1);
   const reactive_2 = new Reactive(2);
   const grouped = latest([reactive_1, reactive_2]);
   const emitted: number[] = [];

   const watcherId = grouped.watch(value => emitted.push(value));

   expect(emitted).toEqual([2]);

   reactive_1.send(3);
   reactive_2.send(4);

   expect(emitted).toEqual([2, 3, 4]);

   grouped.watchers.detach(watcherId);

   expect(reactive_1.watchers.size).toEqual(0);
   expect(reactive_2.watchers.size).toEqual(0);

   grouped.watch(FN_MIRROR);

   expect(reactive_1.watchers.size).toEqual(1);
   expect(reactive_2.watchers.size).toEqual(1);

   grouped.shutdown = `WASTED`;

   expect(reactive_1.watchers.size).toEqual(0);
   expect(reactive_2.watchers.size).toEqual(0);
});

it(`Shutdowns when detached from all source`, async () => {
   const reactive_1 = new Reactive(1);
   const reactive_2 = new Reactive(2);
   const grouped = latest([reactive_1, reactive_2]);
   const emitted: number[] = [];

   grouped.watch(value => emitted.push(value));

   reactive_1.shutdown = `WASTED`;

   expect(grouped.shutdown).toEqual(``);

   reactive_2.shutdown = `WASTED`;

   expect(grouped.shutdown).toEqual(EDetachMessage.Sources);
});
