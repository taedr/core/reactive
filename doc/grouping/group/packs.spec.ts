import { Bus, EDetachMessage, group, IGroupChange, IGroupUpdate, mergeChange, passIf, Reactive, ReactiveNumber, ReactiveNumberArray, ReactiveString, shutdownOn } from "@taedr/reactive";
import { FN_MIRROR } from '@taedr/utils';

it(`Emits values from sources packed in array when all sources emitted the new value`, async () => {
   const bus_1 = new Reactive<number>(0);
   const bus_2 = new Bus<string>();
   const grouped = group([bus_1, bus_2]);
   const pipe = grouped.pipe(
      passIf((_, change) => {
         if (change.all) Ichange = change
         return change.all;
      })
   );

   let Ichange: IGroupChange | undefined;
   let Ivalue: readonly [number, string] | undefined;

   const watcherId = pipe.watch((value, change) => {
      Ivalue = value;
   });

   const Eupdated: IGroupUpdate = {
      sources: [
         {
            detached: false,
            updated: true,
            updates: 1,
         },
         {
            detached: false,
            updated: false,
            updates: 0,
         }
      ],
      all: false,
      some: true,
      rounds: 0,
   };

   expect(Ichange).toEqual(undefined);
   expect(grouped.updated).toEqual(Eupdated);

   bus_2.send(`a`);

   const Evalue_1: typeof Ivalue = [0, 'a'];
   const Echange_1: typeof Ichange = {
      sources: [
         {
            detached: false,
            updated: false,
            updates: 1,
         },
         {
            detached: false,
            updated: false,
            updates: 1,
         }
      ],
      all: true,
      some: false,
      rounds: 1,
      changed: true,
   };

   expect(Ivalue).toEqual(Evalue_1);
   expect(Ichange).toEqual(Echange_1);

   bus_1.send(1);

   const Evalue_2: typeof Ivalue = [0, 'a'];
   const Echange_2: typeof Ichange = {
      sources: [
         {
            detached: false,
            updated: false,
            updates: 1,
         },
         {
            detached: false,
            updated: false,
            updates: 1,
         }
      ],
      all: true,
      some: false,
      rounds: 1,
      changed: true,
   };

   expect(Ivalue).toEqual(Evalue_2);
   expect(Ichange).toEqual(Echange_2);

   pipe.watchers.detach(watcherId);

   expect(bus_1.watchers.size).toEqual(0);
   expect(bus_2.watchers.size).toEqual(0);

   pipe.watch(FN_MIRROR);

   expect(bus_1.watchers.size).toEqual(1);
   expect(bus_2.watchers.size).toEqual(1);

   pipe.shutdown = `WASTED`;

   expect(bus_1.watchers.size).toEqual(0);
   expect(bus_2.watchers.size).toEqual(0);
   expect(grouped.sources.length).toEqual(2);
});

it(`Shutdowns when detached from any source`, async () => {
   const reactive_1 = new Reactive(1);
   const reactive_2 = new Reactive(2);
   const grouped = group([reactive_1, reactive_2]).pipe(
      shutdownOn((_, { sources }) => sources.some(s => s.detached))
   );

   grouped.watch(() => { });

   reactive_1.shutdown = `WASTED`;
   expect(grouped.shutdown).toEqual(EDetachMessage.Auto);
});

it(`Initial notify on instant value & shutdown`, () => {
   const grouped = group([
      new ReactiveNumber(1, { shutdown: true }),
      new ReactiveString(`a`, { shutdown: true }),
   ])
   const pipe = grouped.pipe(mergeChange()).suffix();

   let Ichange: IGroupChange & { value: readonly [number, string] } | undefined;
   let Imessage: string | undefined;


   pipe.watch({
      value: (change) => Ichange = change,
      detach: message => Imessage = message
   });

   const Echange: typeof Ichange = {
      value: [1, 'a'],
      initial: true,
      sources: [
         {
            detached: true,
            updated: false,
            updates: 1,
         },
         {
            detached: true,
            updated: false,
            updates: 1,
         }
      ],
      all: true,
      some: false,
      rounds: 1,
      changed: true,
   };

   expect(Ichange).toEqual(Echange);
   expect(Imessage).toEqual(EDetachMessage.Sources);
});

it(`Initial notify on instant value & shutdown`, () => {
   const grouped = group([
      new ReactiveNumberArray([1]),
      group([]),
   ]);

   let Ichange: IGroupChange | undefined;
   let Ivalue: readonly [readonly number[], readonly unknown[]] | undefined;

   grouped.watch((value, change) => {
      Ivalue = value;
      Ichange = change;
   },);

   const Evalue = [[1], []];
   const Echange: typeof Ichange = {
      initial: true,
      sources: [
         {
            detached: false,
            updated: false,
            updates: 1,
         },
         {
            detached: true,
            updated: false,
            updates: 1,
         }
      ],
      all: true,
      some: false,
      rounds: 1,
      changed: true,
   };
   expect(Ivalue).toEqual(Evalue);
   expect(Ichange).toEqual(Echange);
});
