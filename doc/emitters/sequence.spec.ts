import { adapt, EDetachMessage, Sequence } from '@taedr/reactive';

/* Observable which emit supplied values one by one to each new watcher. */
it(`Sequence`, async () => {
   const sequence = new Sequence([1, 2, 3]);
   const Ivalues: number[] = [];
   let shutdown = ``;

   sequence.watch({
      value: value => { Ivalues.push(value); },
      detach: mess => shutdown = mess,
   });

   expect(Ivalues).toEqual([1, 2, 3]);
   expect(sequence.values).toEqual(Ivalues);
   expect(shutdown).toEqual(EDetachMessage.Emittion);
});

it(`Adapter`, () => {
   const sequence = adapt<number>([1]);

   expect(sequence).toBeInstanceOf(Sequence);
   expect(sequence.values).toEqual([1]);
});
