import { EDetachMessage, Timer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

/* Observable which will emit provided value after specified timeout.
Every watcher will have its own timeout.*/
it(`Timer`, async () => {
   const delayMs = 20;
   const value = 1;
   const timer = new Timer(delayMs, value);
   let newValue_1: number | undefined;
   let shutdown_1: string | undefined;
   let newValue_2: number | undefined;
   let shutdown_2: string | undefined;

   expect(timer.value).toEqual(value);
   expect(timer.delayMs).toEqual(delayMs);

   timer.watch({
      value: value => { newValue_1 = value },
      detach: message => shutdown_1 = message,
   });

   timer.watch({
      value: value => { newValue_2 = value },
      detach: message => shutdown_2 = message,
   });

   await FN_DELAY(delayMs);

   expect(shutdown_1).toEqual(EDetachMessage.Emittion);
   expect(newValue_1).toEqual(value);
   expect(shutdown_2).toEqual(EDetachMessage.Emittion);
   expect(newValue_2).toEqual(value);
});
