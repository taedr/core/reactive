import { adapt, Producer } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

/*Observable which emit values supplied by callback.
Callback will be executed on instance creation.
All values are stored and will be provided to new watcher instantly.
*/
it(`Producer`, async () => {
   const producer = new Producer<number>(next => {
      let num = 0;
      const intervalId = setInterval(() => next(++num), 20);
      return () => clearInterval(intervalId);
   });

   const Ivalues: number[] = [];

   expect(producer.values).toEqual([]);

   producer.watch(value => Ivalues.push(value));

   await FN_DELAY(50);

   expect(Ivalues).toEqual([1, 2]);
   expect(producer.values).toEqual([1, 2]);

   producer.shutdown = `WASTED`;

   expect(producer.shutdown).toEqual(`WASTED`);

   await FN_DELAY(30);

   expect(Ivalues).toEqual([1, 2]);

   const shutValues: number[] = [];

   producer.watch(value => shutValues.push(value));

   expect(shutValues).toEqual(Ivalues);
});

it(`Adapter`, () => {
   const producer = adapt<number>((next, shutdown) => {
      next(1);
      shutdown(`WASTED`);
   });

   expect(producer).toBeInstanceOf(Producer);
   expect(producer.values).toEqual([1]);
   expect(producer.shutdown).toEqual(`WASTED`);
});
