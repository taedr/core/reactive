import { Ticker } from '@taedr/reactive';
import { FN_DELAY } from '@taedr/utils';

/* Observable which will start emitting in specified interval once first watcher will appear */
it(`Ticker`, async () => {
   const delayMs = 10;
   const ticker = new Ticker(delayMs);



   let newValue: number | undefined;

   expect(ticker.ticks).toEqual(0);
   expect(ticker.delayMs).toEqual(10);

   const watcherId = ticker.watch(tick => { newValue = tick;});

   await FN_DELAY(delayMs);

   expect(newValue).toEqual(1);
   expect(ticker.ticks).toEqual(newValue);

   await FN_DELAY(delayMs);

   expect(newValue).toEqual(2);
   expect(ticker.ticks).toEqual(newValue);

   ticker.watchers.detach(watcherId);
}, 1000);
