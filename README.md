> Note that API is on the prototype stage and can be changed dramatically without backward compatibility support!

## Overview
Reactive is a package that implements "Observer" pattern for JavaScript. Purpose is to give set of tools for comfortable monitoring of value changes, tranformations, subscriptions handling. 

## Capabilities
Value observation can be achieved with 3 main classes:
- Bus - transporter used for sending data to its subscribers, doesn't store any value.
- Reactive - shell that stores last transfered value value which can be updated over time. Informs subscribers about each update.
- Collection - shell that stores array of values. Provides api for array modification and observing 4 event types: change, add, delete, update. 

Value transformation uses "Pipeline" pattern which means that you can use multiple handlers to process data step by step.

## Example
```typescript
import { Bus, passIf, map } from '@taedr/reactive';

const bus = new Bus<number>();
/* Pipeline with 2 pipes: 
   - passIf - will pass number further if it bigger than 2, if not will discard it
   - map - will convert number into string and pass it further
 */
const pipeline = bus.pipe(
   passIf(number => number > 2),
   map(number => `Value is: ${number}`)
]);
/* Storage for emitted values */
const array: string[] = [];
/* Subscribing for "pipeline" emittions  */
pipeline.watch(string => array.push(string));

for (let i = 0; i < 5; i++) {
   bus.send(i);
}

console.log(array); // [ 'Value is: 3', 'Value is: 4' ]
```
